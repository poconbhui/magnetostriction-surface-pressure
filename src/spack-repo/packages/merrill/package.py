import copy
import shutil
import re
import llnl.util.tty as tty
import spack
from spack.fetch_strategy import VCSFetchStrategy, _needs_stage

from spack import *

class DirFetchStrategy(VCSFetchStrategy):

    """
    Fetch strategy that fetches from a directory
    Use like this in a package:

       version('name', dir='/home/user/prog/...')
    """
    enabled = True
    required_attributes = ('dir', )

    def __init__(self, **kwargs):
        # Discards the keywords in kwargs that may conflict with the next
        # call to __init__
        forwarded_args = copy.copy(kwargs)
        forwarded_args.pop('name', None)

        super(DirFetchStrategy, self).__init__('dir', **forwarded_args)

        self._dir = re.sub(r'^\$spack', spack.prefix, self.url)

    @_needs_stage
    def fetch(self):
        self.stage.chdir()

        tty.msg("Trying to copy directory:", self.url)

        print "FETCH DIR: ", self._dir
        import os
        import os.path
        src_dir = os.path.join(os.getcwd(), 'src')
        try:
            shutil.rmtree(src_dir)
        except OSError:
            pass
        shutil.copytree(self._dir, src_dir)

    def archive(self, destination):
        super(DirFetchStrategy, self).archive(destination, exclude='.git')

    @_needs_stage
    def reset(self):
        self.stage.chdir_to_source()
        shutil.copytree(self._dir, '.')

    def __str__(self):
        return "[dir] %s" % self._dir


class Merrill(Package):
    """MERRILL with magnetostriction for pressure_surface"""

    homepage = "none"

    version('current', dir=spack.prefix + '/../merrill')

    depends_on('cmake@3.1:')
    depends_on('fenics@1.6.0')

    def install(self, spec, prefix):
        with working_dir('spack-build', create=True):
            cmake(  
                '..',
                '-DMERRILL_ENABLE_MAGNETOSTRICTION:BOOL=ON',
                '-DBUILD_SHARED_LIBS:BOOL=ON',
                '-DMERRILL_ENABLE_TESTING:BOOL=ON',
                *std_cmake_args
            )

            make()
            #make("test", "ARGS=-V")
            make('install')

MODULE Tetrahedral_Mesh_Data
  USE Material_Parameters
  USE Utils
  IMPLICIT NONE
  SAVE

  ! NNODE : Number of nodes in the mesh
  ! NTRI  : Number of tetrahedra in the mesh
  ! BFCE  : Number of boundary faces in the mesh
  ! BNODE : Number of boundary nodes in the mesh
  ! NFIX  : Number of nodes with fixed magnetization in the mesh

  INTEGER  MaxMeshNumber

  INTEGER  NNODE, NTRI, BFCE, BNODE ,NFIX
  INTEGER, ALLOCATABLE  ::  SaveNNODE(:), SaveNTRI(:), SaveBFCE(:), SaveBNODE(:)
  CHARACTER (LEN=100), ALLOCATABLE  ::  MeshFileName(:)

  ! TIL(:,5) : 1-4 : List of indices of tetrahedra vertices 5: index of body
  ! BDFACE(:,2) : 1-4 : List of (index of tetrahedron, face index) for boundary
  !     faces
  ! BlockNumber(:) : List of block number to which each node is assigned.
  !     Nodes which are free during minimization have number 1

  INTEGER, ALLOCATABLE :: TIL(:,:), BDFACE(:,:), FACELST(:,:), NEIGH(:,:)
  ! Used to characterize fixed or free nodes in the mesh. Free=1
  INTEGER, ALLOCATABLE :: BlockNumber(:)
  TYPE(SaveIntArray), ALLOCATABLE  :: SaveTIL(:),SaveBDFACE(:)


  ! VCL(:,4) : 1-3 : List of node coordinates
  !     4: Index of body (1,2.. for first, second,... separate polyhedra ...)
  ! vol(:) :  List of tetrahedra volumes
  ! vbox(:) :  List of  volumes associated to nodes
  ! solid(:) :  List of  solid angles associated to nodes (4 pi for interior)
  ! b,c,d(:4) :  Lists of  shape coefficients associated to tetrahedra


  REAL(KIND=DP), ALLOCATABLE  :: &
    & solid(:) , vbox(:), VCL(:,:), vol(:), b(:,:), c(:,:), d(:,:)
  TYPE(SaveDPRow), ALLOCATABLE  ::   Savevbox(:)  ,Savevol(:)
  TYPE(SaveDPArray), ALLOCATABLE  :: SaveVCL(:),Saveb(:),Savec(:),Saved(:)

  ! m(:,3) : Magnetization vectors associated to nodes
  ! mold(:,3) : Previous magnetization vectors associated to nodes
  ! mdotn(:,3) : normal components ???

  REAL(KIND=DP), ALLOCATABLE  :: mremesh(:,:), gradc(:,:), &
    & mdotn(:,:), m(:,:), totfx(:),  fx(:), fx2(:)
  TYPE(SaveDPRow), ALLOCATABLE  :: Savefx(:),Savefx2(:)

  REAL(KIND=DP)   total_volume, MeanMag(3)
  REAL(KIND=DP)   zone, zoneinc, zoneflag  ! Zone variables for WriteTecplot


  ! Variables for MeshInterpolate: Values depend on m

   LOGICAL :: InterpolatableQ=.FALSE.

   INTEGER  IntNQ,IntNW,IntNR
   INTEGER, ALLOCATABLE :: IntLCELL(:,:,:),IntLNEXT(:)
   REAL(KIND=DP) IntXYZMin(3),IntXYZDel(3), IntRMax(3)
   REAL(KIND=DP), ALLOCATABLE :: IntRSQ(:,:), IntA(:,:,:)


  EXTERNAL qshep3
  DOUBLE PRECISION, EXTERNAL :: qs3val


  CONTAINS

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Subroutines for Mesh related operations
  !    ->  MeshAllocate and FaceAllocate  : allocate right amount of memory
  !    ->  readmeshpat                    : read mesh from a patran file
  !    |----> facelstsort                 : Quicksort for FACELST
  !                                            (Numerical Recipes)
  !    '----> smaller                     : Comparison for facelstsort
  !    ->  GETBOUNDARY                    : analyses the mesh for
  !                                            boundaries
  !    ->  SOLIDANGLE                     : calculate solid angles at
  !                                            boundary nodes
  !    ->  GETANGLE                       : calculate solid angle for
  !                                            tetrahedron
  !    ->  SHAPECOEFFICIENTS              : calculate volumes and shape
  !                                            coefficients for tetrahedra
  !    ->  WRITEmag                       : writes out the magnetization
  !                                            (.dat) and restart
  !                                            (.restart) files
  !    ->  WRITEhyst                      : writes out m.h_est against
  !                                            |h_ext|
  !    ->  WRITETecplot                   : writes out the  TecPlot format
  !                                            for visualization
  !    ->  MeshInterpolate                : Interpolate mesh functions at
  !                                            arbitrary points
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  ! Set some reasonable default values
  SUBROUTINE InitializeTetrahedralMeshData()

    IMPLICIT NONE

    NFIX=0  ! No fixed mesh nodes as default

    ! Redefine if more than 5 meshes needed
    ! not memory critical because only pointers are allocated
    ! Large memory blocks are allocated only when meshes are loaded
    MaxMeshNumber=5

    ! zone/zoneinc not used
    zone=0.0
    zoneinc=1.0
    zoneflag=0.0

  END SUBROUTINE InitializeTetrahedralMeshData


  !---------------------------------------------------------------
  !          MeshAllocate and FaceAllocate
  !---------------------------------------------------------------

  SUBROUTINE MeshAllocate(nodes, tetrahedra)

    IMPLICIT NONE
    INTEGER nodes, tetrahedra


    NNODE=nodes
    NTRI= tetrahedra
    If(ALLOCATED(TIL)) DEALLOCATE(TIL,FACELST,b,c,d,vol,NEIGH,VCL,solid,vbox)

    Allocate(TIL(NTRI,5),FACELST(4*NTRI,5))
    Allocate(b(NTRI,4),c(NTRI,4),d(NTRI,4),vol(NTRI), NEIGH(NTRI,4))
    Allocate(VCL(NNODE,5), solid(NNODE)  ,vbox(NNODE))

    ! Don't deallocate/allocate mremesh
    If(ALLOCATED(m)) DEALLOCATE(m,gradc,fx,fx2,mdotn,totfx)

    Allocate(m(NNODE,3))
    Allocate(gradc(NNODE,3))
    Allocate(fx(NNODE))
    Allocate(fx2(NNODE))
    Allocate(mdotn(NNODE,3))
    Allocate(totfx(NNODE))

    ! Initialize values so Valgrind stops complaining
    m = 0
    gradc = 0
    fx = 0
    fx2 = 0
    mdotn = 0
    totfx = 0

    If(Allocated(BlockNumber)) DEALLOCATE(Blocknumber)
    Allocate(BlockNumber(NNODE))
    BlockNumber(:)=1  ! Default : All nodes are free

  END SUBROUTINE MeshAllocate


  SUBROUTINE FaceAllocate()
    IMPLICIT NONE
    If(ALLOCATED(BDFACE)) DEALLOCATE(BDFACE)
    Allocate(BDFACE(BFCE,2))
  END SUBROUTINE FaceAllocate



!---------------------------------------------------------------
! BEM  : READMESH  .pat FORMAT
!---------------------------------------------------------------

  SUBROUTINE readmeshpat( )

    USE Material_Parameters
    IMPLICIT NONE

    INTEGER i, j, k, NBlock,NBlockDat
    INTEGER, ALLOCATABLE :: idx(:)
    REAL(KIND=DP) mnorm,dum1,dum2,dum3
    LOGICAL :: exist1

    !---------------------------------------------------------------
    !            Start reading MESH file (.pat format)
    !---------------------------------------------------------------


    OPEN(unit=102,file=meshfile,status='unknown')
    READ(102,*)
    READ(102,*)
    READ(102,*) dum1,dum1,dum1,dum1,NNODE,NTRI,dum1,NBlock,dum1
    READ(102,*)
    READ(102,*)

    !---------------------------------------------------------------
    !            Allocate all arrays that depend on NNODE and NTRI
    !---------------------------------------------------------------

    CALL MeshAllocate(NNODE, NTRI)


    DO i=1,NNODE
      READ(102,*) VCL(i,1),VCL(i,2),VCL(i,3)
      VCL(i,4)=0.0
      READ(102,*)
      READ(102,*)
    END DO

    DO i=1,NTRI
      READ(102,*)
      READ(102,*) TIL(i,1),TIL(i,2),TIL(i,3),TIL(i,4)
      READ(102,*)
    END DO

    NFIX=0
    If(NBlock<1) NBlock=1

    IF (NBlock>1) THEN
      WRITE(*,*) 'Patran file contains ',NBlock, ' blocks!'
      WRITE(*,*) 'Note that all blocks apart from block 1 &
        &are assumed to contain fixed nodes ! '
      DO i=1,20
        READ(102,*)   ! ignore 96 material parameters
      END DO

      DO i=1,NBlock
        READ(102,*) dum1 !Ignore two lines for each block
        READ(102,*) !
      END DO

      DO i=1,NBlock
        ! NBlock is 2* number of nodes in block i
        READ(102,*)  dum1,dum1, NBlockDat,dum1,dum1,dum1,dum1,dum1,dum1
        READ(102,*) ! Ignore block name

        WRITE(*,*) 'Block ',i,'  -> ',NBlockDat/2

        IF(Allocated(idx)) DEALLOCATE(idx)
        ALLOCATE(idx(NBlockDat))

        READ(102,*) (idx(k),k=1,NBlockDat)

        DO j=1,NBlockDat/2
          ! sets the block number for each node of the mesh
          BlockNumber(idx(2*j))=i
        END DO

        If(i>1) NFIX=NFIX+NBlockDat/2
        DEALLOCATE(idx)
      END DO

    ENDIF

    CLOSE(102)

    ! NEIGHfromFACELST
    ! sets neighborship relations for tetrahedra
    ! and determines boundary faces

    CALL NEIGHfromFACELST()

    WRITE(*,*) ' Mesh Data'
    WRITE(*,*) '-----------'
    WRITE(*,*) NNODE,'nodes'
    WRITE(*,*) NTRI,'elements'
    WRITE(*,*) BFCE,'boundary faces'
    WRITE(*,*) BNODE,'boundary nodes'

    IF (NONZERO(new(4))) THEN
      WRITE(*,*) 'setting initial mag to',new(1),new(2),new(3)
      DO i=1,NNODE
        m(i,1)=new(1)
        m(i,2)=new(2)
        m(i,3)=new(3)

        mnorm=sqrt(m(i,1)**2 + m(i,2)**2 + m(i,3)**2)
        ! print*,'norm=',mnorm
        m(i,1)=m(i,1)/mnorm
        m(i,2)=m(i,2)/mnorm
        m(i,3)=m(i,3)/mnorm
      ENDDO
    ENDIF


    !  UNCOMMENT NEXT BLOCK TO RESTART FROM PREVIOUS SOLUTION
    WRITE(*,*) 'REST =   ', rest

    IF(REST/=0) THEN
      ! WRITE(*,*) 'Initial guess from input file'
      ! WRITE(*,*) 'Filename to READ from?'
      ! READ(*,*) infile
      INQUIRE(FILE=infile, EXIST=exist1)
      DO WHILE(exist1.EQV..FALSE.)
        IF(exist1) THEN
          WRITE(*,*) 'OPENed file ',infile
        ELSE
          WRITE(*,*) 'Cannot OPEN file ', infile
          WRITE(*,*) 'Input new INPUT file name'

          READ(*,*) infile

          INQUIRE(FILE=infile, EXIST=exist1)
          IF(exist1) WRITE(*,*) 'File OPENed:', infile
        ENDIF
      ENDDO


      OPEN(unit=111,file=infile ,status='unknown')

      DO i=1,NNODE
        IF(REST==1) READ (111,*)  m(i,1),m(i,2),m(i,3),fx(i),fx2(i)
        IF(REST==2) THEN
          !          print*,'READing in  hdat file'
          READ (111,*) dum1,dum2,dum3, m(i,1),m(i,2),m(i,3)
        ENDIF
      END DO

      CLOSE(111)

    ENDIF ! for the IF (REST=1)

    CALL FaceAllocate()
    ! Mesh preparation for calculations  MODULE Tetrahedral_Mesh_Data
    CALL getboundary()
    CALL shapecoeffs()
    CALL solidangle()

    RETURN

  END SUBROUTINE readmeshpat

  !---------------------------------------------------------------
  !        NEIGHfromFACELST
  !        sets neighborship relations for tetrahedra
  !        and determines boundary faces
  !---------------------------------------------------------------

  SUBROUTINE NEIGHfromFACELST( )

    IMPLICIT NONE

    INTEGER i, swap
    LOGICAL :: eq3


    ! FACELST contains all faces
    ! FACELST(i,:): i-th face
    !   1-3 : node indices, 4
    !   4 : position of missing vertex from TIL(j,:)  (= tetrahedron face index)
    !   5 : Index j of tetrahedron in  TIL

    DO i=1,NTRI
      FACELST(4*i-3,1)=TIL(i,2)
      FACELST(4*i-3,2)=TIL(i,4)
      FACELST(4*i-3,3)=TIL(i,3)

      FACELST(4*i-2,1)=TIL(i,1)
      FACELST(4*i-2,2)=TIL(i,3)
      FACELST(4*i-2,3)=TIL(i,4)

      FACELST(4*i-1,1)=TIL(i,1)
      FACELST(4*i-1,2)=TIL(i,4)
      FACELST(4*i-1,3)=TIL(i,2)

      FACELST(4*i  ,1)=TIL(i,1)
      FACELST(4*i  ,2)=TIL(i,2)
      FACELST(4*i  ,3)=TIL(i,3)

      FACELST(4*i-3,4)=1
      FACELST(4*i-3,5)=i
      FACELST(4*i-2,4)=2
      FACELST(4*i-2,5)=i

      FACELST(4*i-1,4)=3
      FACELST(4*i-1,5)=i
      FACELST(4*i  ,4)=4
      FACELST(4*i  ,5)=i
    END DO

    ! SORT vertices of each face in FACELST by index number,
    !   keeping record of orientation
    ! This makes sure that the same face has the same order in all
    DO i=1,4*NTRI
      IF(FACELST(i,1)>FACELST(i,2)) THEN
        swap=FACELST(i,1); FACELST(i,1)=FACELST(i,2); FACELST(i,2)=swap
        FACELST(i,4)= -FACELST(i,4)
      END IF
      IF(FACELST(i,2)>FACELST(i,3)) THEN
        swap=FACELST(i,2); FACELST(i,2)=FACELST(i,3); FACELST(i,3)=swap
        FACELST(i,4)= -FACELST(i,4)
      END IF
      IF(FACELST(i,1)>FACELST(i,2)) THEN
        swap=FACELST(i,1); FACELST(i,1)=FACELST(i,2);FACELST(i,2)=swap
        FACELST(i,4)= -FACELST(i,4)
      END IF
    END DO

    !Here happens the
    CALL facelstsort( )

    ! Get the number of boundary faces
    Neigh(:,:)=0
    BFCE=4*NTRI
    DO i=1,4*NTRI-1
      eq3=(FACELST(i,1)==FACELST(i+1,1).AND. &
           FACELST(i,2)==FACELST(i+1,2).AND. &
           FACELST(i,3)==FACELST(i+1,3))
      IF (eq3) THEN
        ! FACELST(i,5) and FACELST(i+1,5) are neighbors
        NEIGH(FACELST(i,5),Abs(FACELST(i,4)))=FACELST(i+1,5)
        NEIGH(FACELST(i+1,5),Abs(FACELST(i+1,4)))=FACELST(i,5)
        BFCE=BFCE-2
      END IF
    END DO

    ! Get the number of boundary vertices
    !
    ! Using Euler's characteristic V - E + F = 2 for a convex polyhedron and
    ! for a triangle mesh 2E = 3F, V = 2 + F/2
    BNODE = 2 + BFCE/2

    RETURN

    CONTAINS

      SUBROUTINE facelstsort( )

        IMPLICIT NONE

        INTEGER M,NSTACK

        PARAMETER (M=7,NSTACK=50)
        INTEGER i,ir,j,jstack,k,l,istack(NSTACK)
        INTEGER a(5),temp(5)
        LOGICAL tt

        jstack=0
        l=1
        ir=4*NTRI
        1 if(ir-l.lt.M) then

          do 12 j=l+1,ir
            a(:)=FACELST(j,:)
            do 11 i=j-1,1,-1
              call  smaller(FACELST(i,:),a(:),tt)
              if(tt)goto 2
              FACELST(i+1,:)=FACELST(i,:)
            11 continue
              i=0
              2 FACELST(i+1,:)=a(:)
          12 continue

          if(jstack.eq.0) return

          ir=istack(jstack)
          l=istack(jstack-1)
          jstack=jstack-2

        else
          k=(l+ir)/2
          temp(:)=FACELST(k,:)
          FACELST(k,:)=FACELST(l+1,:)
          FACELST(l+1,:)=temp(:)

          call  smaller(FACELST(ir,:),FACELST(l+1,:),tt)
          if(tt)then
            temp(:)=FACELST(l+1,:)
            FACELST(l+1,:)=FACELST(ir,:)
            FACELST(ir,:)=temp(:)
          endif

          call  smaller(FACELST(ir,:),FACELST(l,:),tt)
          if(tt)then
            temp(:)=FACELST(l,:)
            FACELST(l,:)=FACELST(ir,:)
            FACELST(ir,:)=temp(:)
          endif

          call  smaller(FACELST(l,:),FACELST(l+1,:),tt)
          if(tt)then
            temp(:)=FACELST(l+1,:)
            FACELST(l+1,:)=FACELST(l,:)
            FACELST(l,:)=temp(:)
          endif

          i=l+1
          j=ir
          a(:)=FACELST(l,:)

          3 continue

          i=i+1
          call smaller(FACELST(i,:),a(:),tt)
          if(tt)goto 3

          4 continue

          j=j-1
          call smaller(a(:),FACELST(j,:),tt)
          if(tt) goto 4
          if(j.lt.i) goto 5

          temp(:)=FACELST(i,:)
          FACELST(i,:)=FACELST(j,:)
          FACELST(j,:)=temp(:)
          goto 3

          5 FACELST(l,:)=FACELST(j,:)
          FACELST(j,:)=a(:)
          jstack=jstack+2
          if(jstack.gt.NSTACK) WRITE(*,*) 'NSTACK too small in facelstsort'
          if(ir-i+1.ge.j-l)then
            istack(jstack)=ir
            istack(jstack-1)=i
            ir=j-1
          else
            istack(jstack)=j-1
            istack(jstack-1)=l
            l=i
          endif
        endif

        goto 1

        return
      END SUBROUTINE facelstsort

      SUBROUTINE smaller(x,y,t)
        IMPLICIT NONE

        LOGICAL :: t
        INTEGER x(5),y(5)

        IF(x(1)==y(1)) THEN
          IF(x(2)==y(2)) THEN
             t = (x(3) <y(3))
          ELSE
             t = (x(2)<y(2))
          END IF
        ELSE
          t = (x(1)<y(1))
        END IF

        RETURN
      END SUBROUTINE smaller

  END SUBROUTINE NEIGHfromFACELST


  !---------------------------------------------------------------
  ! BEM  : GETBOUNDARY
  !---------------------------------------------------------------

  SUBROUTINE getboundary( )
    IMPLICIT NONE

    INTEGER i,j, BF
    INTEGER e2,n1,n2,n3,nn1,nn2,chk,cubdy,l


    ! create element neighbour list in NEIGH

    ! create list of boundary elements in BDFACE
    BF=0
    DO i=1,NTRI
      DO j=1,4
        IF (NEIGH(i,j)==0) THEN
          BF=BF+1

          BDFACE(BF,1)=i
          BDFACE(BF,2)=j

          n1=MOD(j+1,4)+1
          n2=MOD(j+2,4)+1
          n3=MOD(j,4)+1

          VCL(TIL(i,n1),4)=1.0
          VCL(TIL(i,n2),4)=1.0
          VCL(TIL(i,n3),4)=1.0
        ENDIF
      END DO
    END DO

    ! assign body number to TIL(:,5)
    TIL(:,5)=0
    ! set starting element e2 for body 1
    e2=1
    chk=0
    cubdy=0
    DO WHILE (chk==0)
      cubdy=cubdy+1
      TIL(e2,5)=cubdy

      IF (NEIGH(e2,1)/=0) TIL(NEIGH(e2,1),5)=cubdy
      IF (NEIGH(e2,2)/=0) TIL(NEIGH(e2,2),5)=cubdy
      IF (NEIGH(e2,3)/=0) TIL(NEIGH(e2,3),5)=cubdy
      IF (NEIGH(e2,4)/=0) TIL(NEIGH(e2,4),5)=cubdy

      chk=0
      DO WHILE (chk==0)
        chk=1
        DO i=1,NTRI
          DO j=1,4
            IF (NEIGH(i,j)/=0) THEN
              IF ( TIL(NEIGH(i,j),5)==cubdy ) THEN
                IF (TIL(i,5)==0) THEN
                  TIL(i,5)=cubdy
                  chk=0
                ENDIF
                DO l=1,4
                  IF (NEIGH(i,l)/=0) THEN
                    IF (TIL(NEIGH(i,l),5)==0) THEN
                      TIL(NEIGH(i,l),5)=cubdy
                      chk=0
                    ENDIF
                  ENDIF
                END DO
              ENDIF
            ENDIF
          END DO
        END DO
      END DO

      ! find starting element for next body
      chk=1
      DO i=1,NTRI
        IF (TIL(i,5)==0) THEN
          e2=i
          chk=0
        ENDIF
      END DO
    END DO

    ! assign body number to VCL(:,5)
    DO i=1,NTRI
      DO j=1,4
        VCL(TIL(i,j),5)=TIL(i,5)
      END DO
    END DO

    ! count number of nodes and elements in each body
    WRITE(*,*) cubdy,'separate bodies'
    DO i=1,cubdy
      nn1=0
      nn2=0
      DO j=1,NNODE
        IF (ABS( VCL(j,5) - i ) < MachEps) nn1=nn1+1
      END DO
      WRITE(*,*) nn1,'nodes in body ',i
      DO j=1,NTRI
        IF (ABS( TIL(j,5) - i ) < MachEps) nn2=nn2+1
      END DO
      WRITE(*,*) nn2,'elements in body',i
    END DO

    RETURN
  END SUBROUTINE getboundary


  !---------------------------------------------------------------
  ! BEM  : SOLIDANGLE
  !---------------------------------------------------------------

  SUBROUTINE solidangle()

    ! --- All we have to do in order to find the solid angle that
    ! --- each node subtends at the surface is to add up all the solid angles
    ! --- that each surface node makes with the opposite surafce of each
    ! --- tetahedra to which it belongs.

    IMPLICIT NONE

    INTEGER i,cn,p1,p2,p3
    ! REAL(KIND=DP), INTRINSIC :: GET_ANGLE

    solid(:)=0.

    DO i=1, NTRI
      cn = TIL(i,1)
      p1 = TIL(i,3)
      p2 = TIL(i,2)
      p3 = TIL(i,4)
      ! using the GET_ANGLE function
      solid(cn) = solid(cn) + GET_ANGLE(cn,p1,p2,p3)

      cn = TIL(i,2)
      p1 = TIL(i,1)
      p2 = TIL(i,3)
      p3 = TIL(i,4)
      ! using the GET_ANGLE function
      solid(cn) = solid(cn) + GET_ANGLE(cn,p1,p2,p3 )

      cn = TIL(i,3)
      p1 = TIL(i,4)
      p2 = TIL(i,2)
      p3 = TIL(i,1)
      ! using the GET_ANGLE function
      solid(cn) = solid(cn) + GET_ANGLE(cn,p1,p2,p3 )

      cn = TIL(i,4)
      p1 = TIL(i,1)
      p2 = TIL(i,2)
      p3 = TIL(i,3)
      ! using the GET_ANGLE function
      solid(cn) = solid(cn) + GET_ANGLE(cn,p1,p2,p3)
    END DO ! the i loop

    DO i=1, NNODE
      ! from the above solid=4pi if an interior point do we need to set to 0?
      ! in fact doesn't matter as we are using ((solid/4pi)-1) later on only
      ! for boundary nodes
      IF ( .NOT.(NONZERO(VCL(i,4))) ) solid(i)=0.0
      ! IF (VCL(i,4)/=0.0) print*,' ANGLE ',i,' = ',solid(i),i
    ENDDO

    RETURN
  END SUBROUTINE solidangle


  !---------------------------------------------------------------
  ! BEM  FUNCTION: GETANGLE
  !---------------------------------------------------------------


  REAL(KIND=DP) FUNCTION GET_ANGLE(cn,p1,p2,p3 )

    IMPLICIT NONE
    INTEGER  p1,p2,p3,cn
    REAL(KIND=DP) pi,pp1(3),pp2(3),pp3(3),top,bottom

    pi=4.0d0*atan(1.0d0)

    ! -- p1= r0-r1
    pp1(1:3)=VCL(cn,1:3)-VCL(p1,1:3)

    ! -- p2= r0-r2
    pp2(1:3)=VCL(cn,1:3)-VCL(p2,1:3)

    ! -- p3= r0-r3
    pp3(1:3)=VCL(cn,1:3)-VCL(p3,1:3)

    top=SQRT(DOT_PRODUCT(pp1,pp1)*DOT_PRODUCT(pp2,pp2)*DOT_PRODUCT(pp3,pp3)) &
      & +SQRT(DOT_PRODUCT(pp1,pp1))*DOT_PRODUCT(pp2,pp3) &
      & +SQRT(DOT_PRODUCT(pp2,pp2))*DOT_PRODUCT(pp3,pp1) &
      & +SQRT(DOT_PRODUCT(pp3,pp3))*DOT_PRODUCT(pp1,pp2)

    bottom=SQRT( &
      &     2*( &
      &         SQRT(DOT_PRODUCT(pp2,pp2)*DOT_PRODUCT(pp3,pp3)) &
      &         + DOT_PRODUCT(pp2,pp3) &
      &     ) &
      &     *( &
      &         SQRT(DOT_PRODUCT(pp3,pp3)*DOT_PRODUCT(pp1,pp1)) &
      &         + DOT_PRODUCT(pp3,pp1) &
      &     ) &
      &     *( &
      &         SQRT(DOT_PRODUCT(pp1,pp1)*DOT_PRODUCT(pp2,pp2)) &
      &         + DOT_PRODUCT(pp1,pp2) &
      &     ) &
      & )

    ! -- the result

    IF ( (.NOT.(NONZERO(bottom))).OR.(abs(top)>abs(bottom))) THEN
      GET_ANGLE= 0
    ELSE
      GET_ANGLE = (2.0d0*(dacos(top/bottom)))
    ENDIF

  END FUNCTION GET_ANGLE


  !---------------------------------------------------------------
  ! BEM  : SHAPECOEFFICIENTS
  !---------------------------------------------------------------

  SUBROUTINE shapecoeffs()
    IMPLICIT NONE

    INTEGER i,j
    REAL(KIND=DP) volume
    INTEGER n1,n2,n3,n4



    DO i=1,NTRI

      DO j=0,3

        n1=TIL(i,mod((j+1),4)+1)
        n2=TIL(i,mod((j+2),4)+1)
        n3=TIL(i,mod((j+3),4)+1)
        n4=TIL(i,j+1)

        volume=  &
          &  VCL(n1,1)*VCL(n2,2)*VCL(n3,3)-VCL(n1,1)*VCL(n2,3)*VCL(n3,2) &
          & -VCL(n2,1)*VCL(n1,2)*VCL(n3,3)+VCL(n2,1)*VCL(n1,3)*VCL(n3,2) &
          & +VCL(n3,1)*VCL(n1,2)*VCL(n2,3)-VCL(n3,1)*VCL(n1,3)*VCL(n2,2) &
          & -VCL(n4,1)*VCL(n2,2)*VCL(n3,3)+VCL(n4,1)*VCL(n2,3)*VCL(n3,2) &
          & +VCL(n2,1)*VCL(n4,2)*VCL(n3,3)-VCL(n2,1)*VCL(n4,3)*VCL(n3,2) &
          & -VCL(n3,1)*VCL(n4,2)*VCL(n2,3)+VCL(n3,1)*VCL(n4,3)*VCL(n2,2) &
          & +VCL(n4,1)*VCL(n1,2)*VCL(n3,3)-VCL(n4,1)*VCL(n1,3)*VCL(n3,2) &
          & -VCL(n1,1)*VCL(n4,2)*VCL(n3,3)+VCL(n1,1)*VCL(n4,3)*VCL(n3,2) &
          & +VCL(n3,1)*VCL(n4,2)*VCL(n1,3)-VCL(n3,1)*VCL(n4,3)*VCL(n1,2) &
          & -VCL(n4,1)*VCL(n1,2)*VCL(n2,3)+VCL(n4,1)*VCL(n1,3)*VCL(n2,2) &
          & +VCL(n1,1)*VCL(n4,2)*VCL(n2,3)-VCL(n1,1)*VCL(n4,3)*VCL(n2,2) &
          & -VCL(n2,1)*VCL(n4,2)*VCL(n1,3)+VCL(n2,1)*VCL(n4,3)*VCL(n1,2)

        vol(i)=abs(volume)/6.d0

        b(i,j+1) = ( &
          &     VCL(n2,2)*VCL(n1,3)-VCL(n3,2)*VCL(n1,3) &
          &     -VCL(n1,2)*VCL(n2,3)+VCL(n3,2)*VCL(n2,3) &
          &     +VCL(n1,2)*VCL(n3,3)-VCL(n2,2)*VCL(n3,3) &
          & )/sign(1.0d0,volume)

        c(i,j+1) = ( &
          &     -VCL(n2,1)*VCL(n1,3)+VCL(n3,1)*VCL(n1,3) &
          &     +VCL(n1,1)*VCL(n2,3)-VCL(n3,1)*VCL(n2,3) &
          &     -VCL(n1,1)*VCL(n3,3)+VCL(n2,1)*VCL(n3,3) &
          & )/sign(1.0d0,volume)

        d(i,j+1) = ( &
          &     VCL(n2,1)*VCL(n1,2)-VCL(n3,1)*VCL(n1,2) &
          &     -VCL(n1,1)*VCL(n2,2)+VCL(n3,1)*VCL(n2,2) &
          &     +VCL(n1,1)*VCL(n3,2)-VCL(n2,1)*VCL(n3,2) &
          & )/sign(1.0d0,volume)

      END DO
    END DO


    ! calculate volume vbox associated with node i

    vbox(:)=0.
    DO i=1,NTRI
      DO j=1,4
        vbox(TIL(i,j))=vbox(TIL(i,j))+(0.25*vol(i))
      END DO
    END DO

    total_volume=0.0d0
    do i= 1, NTRI
      total_volume=total_volume+ vol(i)
    end do

    write(*,*) 'Total volume = ',total_volume
    RETURN
  END SUBROUTINE shapecoeffs


  !---------------------------------------------------------------
  ! WRITEmag
  !---------------------------------------------------------------

  SUBROUTINE WRITEmag( )
    USE Material_Parameters
    IMPLICIT NONE

    INTEGER i

    OPEN(unit=405,file=datafile,status='unknown')
    DO i=1,NNODE
      WRITE(405,3000) VCL(i,1),VCL(i,2),VCL(i,3),m(i,1),m(i,2),m(i,3)
    END DO
    CLOSE(405)

    3000 FORMAT(7(f10.6, ' '))
    RETURN
  END SUBROUTINE WRITEmag


  !---------------------------------------------------------------
  ! WRITEhyst
  !---------------------------------------------------------------

  SUBROUTINE WRITEhyst( )
    USE Material_Parameters
    IMPLICIT NONE

    INTEGER i
    REAL(KIND=DP) mhdot, mag_av(3)

    OPEN(unit=405, file=hystfile, status='unknown', position='append')
    mhdot=0.0
    mag_av(:)=0.0
    DO i=1,NNODE
      mhdot=mhdot + (m(i,1)*Hz(1) + m(i,2)*Hz(2) + m(i,3)*Hz(3))*vbox(i)
      mag_av(:)= mag_av(:) + m(i,:)*vbox(i)
    END DO
    WRITE(405,3000) mhdot/total_volume,extapp, mag_av(:)/total_volume
    CLOSE(405)

    3000 FORMAT(5(f10.6, ' '))
    RETURN
  END SUBROUTINE WRITEhyst


  !---------------------------------------------------------------
  ! WriteDisplacement
  !---------------------------------------------------------------

  SUBROUTINE WriteDisplacement(filename)
    USE Material_Parameters
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: filename
    INTEGER :: i

    OPEN(UNIT=405, FILE=filename, STATUS='REPLACE')

    DO i=1,NNODE
      WRITE(405, *) umstr(i,:)
    END DO

    CLOSE(405)

  END SUBROUTINE WriteDisplacement


  !---------------------------------------------------------------
  ! WriteDisplacementTecplot
  !---------------------------------------------------------------
  SUBROUTINE WriteDisplacementTecplot(filename)
    USE Material_Parameters
    IMPLICIT NONE

    CHARACTER(LEN=*), INTENT(IN) :: filename
    INTEGER :: i

    OPEN(UNIT=405, FILE=filename, STATUS='REPLACE')

    WRITE(405, *) 'TITLE = "', TRIM(filename), '"'
    WRITE(405, *) 'VARIABLES = "X","Y","Z","Ux","Uy","Uz"'
    WRITE(405, *) 'ZONE T="0.000"  N=', NNODE, ',  E=', NTRI, &
        ', F=FEPOINT, ET=TETRAHEDRON'

    DO i=1,NNODE
      WRITE(405, *)                        &
        VCL(i,1), VCL(i,2), VCL(i,3),      &
        umstr(i,1), umstr(i,2), umstr(i,3)
    END DO

    DO i=1,NTRI
      WRITE(405,*) TIL(i,1), TIL(i,2), TIL(i,3), TIL(i,4)
    ENDDO

    CLOSE(405)

  END SUBROUTINE WriteDisplacementTecplot


  !---------------------------------------------------------------
  ! READmag
  !---------------------------------------------------------------

  SUBROUTINE READmag(dfile)
    USE Material_Parameters
    IMPLICIT NONE

    INTEGER i
    DOUBLE Precision x,y,z
    CHARACTER (LEN=200) ::  dfile

    OPEN(unit=405,file=dfile,status='unknown')
    DO i=1,NNODE
      READ(405,*) x,y,z,m(i,1),m(i,2),m(i,3)
    END DO
    CLOSE(405)

    RETURN
  END SUBROUTINE READmag


  !---------------------------------------------------------------
  ! WRITETecplot
  !---------------------------------------------------------------

  SUBROUTINE WRITETecplot( extapp,stem,loopcount)
    USE strings
    IMPLICIT NONE

    INTEGER i, loopcount
    REAL(KIND=DP) extapp
    CHARACTER (LEN=200) ::  stem
    CHARACTER (LEN=80) ::  Nstr,Estr,Zstr
    CHARACTER (LEN=200) :: head

    if(abs(extapp - (-1.0)) < MachEps) then
      call writenum(zone, Zstr,'(F10.3)')
      zone = zone + zoneinc
    else
      call writenum(extapp, Zstr,'(F10.3)')
    endif

    call writenum(NNODE,  Nstr,'(I8)')
    call writenum(NTRI,   Estr,'(I8)')
    head='ZONE T="'//Zstr
    head=head(:LEN_TRIM(head))&
      &//'"  N='//Nstr(:LEN_TRIM(Nstr))//',  E='//Estr(:LEN_TRIM(Estr))

    IF(loopcount==1) THEN

      OPEN(unit=407,file=stem(:LEN_TRIM(stem))//"_mult.tec" ,status='unknown')
      WRITE(407,*) 'TITLE = ','"'//stem(:LEN_TRIM(stem))//'"'
      WRITE(407,*) 'VARIABLES = "X","Y","Z","Mx","My","Mz"'
      WRITE(407,*) head,  'F=FEPOINT, ET=TETRAHEDRON'
      DO i=1,NNODE
        WRITE(407,3001) VCL(i,1),VCL(i,2),VCL(i,3),m(i,1),m(i,2),m(i,3)
      ENDDO
      DO i=1,NTRI
        WRITE(407,3002) TIL(i,1),TIL(i,2),TIL(i,3),TIL(i,4)
      ENDDO

    ENDIF


    IF(loopcount>1) THEN

      OPEN(unit=407,file=stem(:LEN_TRIM(stem))//"_mult.dat" &
        &  ,status='unknown',position='APPEND')
      WRITE(407,*) head,  'F=FEPOINT, ET=TETRAHEDRON',&
        &'VARSHARELIST =([1-3]=1), CONNECTIVITYSHAREZONE = 1'

      DO i=1,NNODE
        WRITE(407,3003) m(i,1),m(i,2),m(i,3)
      ENDDO
    ENDIF

    CLOSE(unit=407)


    3001 FORMAT(6(f10.6, ' '))
    3002 FORMAT(4(i7))
    3003 FORMAT(3(f10.6, ' '))
    RETURN
  END SUBROUTINE WRITETecplot


  !---------------------------------------------------------------
  ! MeshInterpolate
  !---------------------------------------------------------------

  SUBROUTINE MeshInterpolate(vec,IntM )
    IMPLICIT NONE

    INTEGER IntERROR
    DOUBLE PRECISION vec(3), IntM(3),IntNorm
    ! 0, if no errors were encountered.
    ! 1, if n, nq, nw, or nr is out of range.
    ! 2, if duplicate nodes were encountered.
    ! 3, if all nodes are coplanar.

    IntNQ=10
    IntNW=15
    IntNR=int( real(NNODE/2)**(1.0/3.0) )

    IF(InterpolatableQ.EQV..FALSE.) THEN
      IF(ALLOCATED(IntLCELL)) DEALLOCATE(IntLCELL,IntLNEXT,IntRSQ,IntA)
      allocate(IntLCELL(IntNR,IntNR,IntNR),IntLNEXT(NNODE))
      allocate(IntRSQ(NNODE,3),IntA(9,NNODE,3))

      call qshep3 (NNODE, VCL(:,1),VCL(:,2),VCL(:,3),m(:,1),&
        & IntNQ,IntNW,IntNR,IntLCELL,IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(1), IntRSQ(:,1),IntA(:,:,1), IntERROR)
      IF(IntERROR/=0) Write(*,*) 'IntERROR mx. ERR=',IntERROR

      call qshep3 (NNODE, VCL(:,1),VCL(:,2),VCL(:,3),m(:,2),&
        & IntNQ,IntNW,IntNR,IntLCELL,IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(2), IntRSQ(:,2),IntA(:,:,2), IntERROR)
      IF(IntERROR/=0) Write(*,*) 'IntERROR my. ERR=',IntERROR

      call qshep3 (NNODE, VCL(:,1),VCL(:,2),VCL(:,3),m(:,3),&
        & IntNQ,IntNW,IntNR,IntLCELL,IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(3), IntRSQ(:,3),IntA(:,:,3), IntERROR)
      IF(IntERROR/=0) Write(*,*) 'IntERROR mz. ERR=',IntERROR

      InterpolatableQ=.TRUE.
    ENDIF

    IntM(1) = qs3val(vec(1),vec(2),vec(3), &
      &  NNODE,VCL(:,1),VCL(:,2),VCL(:,3), m(:,1), &
      & IntNR, IntLCELL, IntLNEXT, &
      & IntXYZMin, IntXYZDel, IntRMax(1), IntRSQ(:,1), IntA(:,:,1))

    IntM(2) = qs3val(vec(1),vec(2),vec(3), &
      &  NNODE,VCL(:,1),VCL(:,2),VCL(:,3), m(:,2), &
      & IntNR, IntLCELL, IntLNEXT, &
      & IntXYZMin, IntXYZDel, IntRMax(2), IntRSQ(:,2), IntA(:,:,2))

    IntM(3) = qs3val(vec(1),vec(2),vec(3), &
      &  NNODE,VCL(:,1),VCL(:,2),VCL(:,3), m(:,3), &
      & IntNR, IntLCELL, IntLNEXT, &
      & IntXYZMin, IntXYZDel, IntRMax(3), IntRSQ(:,3), IntA(:,:,3))

    IntNorm= sqrt(IntM(1)*IntM(1)+IntM(2)*IntM(2)+IntM(3)*IntM(3))
    IntM(1)=IntM(1)/IntNorm;IntM(2)=IntM(2)/IntNorm;IntM(3)=IntM(3)/IntNorm;
  END SUBROUTINE MeshInterpolate


  !---------------------------------------------------------------
  !         Working with multiple meshes
  !---------------------------------------------------------------


  !---------------------------------------------------------------
  !           SaveMesh(MeshNo) saves all important global variables
  !                            of the mesh into arrays
  !                            The index of the mesh is MeshNo
  !---------------------------------------------------------------

  SUBROUTINE SaveMesh(MeshNo)
    IMPLICIT NONE

    INTEGER MeshNo

    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF


    If(ALLOCATED(SaveNNODE).EQV..FALSE.)  THEN
      ALLOCATE(MeshFileName(MaxMeshNumber))
      ALLOCATE(SaveNNODE(MaxMeshNumber))
      ALLOCATE(SaveNTRI(MaxMeshNumber))
      ALLOCATE(SaveBFCE(MaxMeshNumber))
      ALLOCATE(SaveBNODE(MaxMeshNumber))
      ALLOCATE(Savevbox(MaxMeshNumber))
      ALLOCATE(Savevol(MaxMeshNumber))
      ALLOCATE(SaveVCL(MaxMeshNumber))
      ALLOCATE(Saveb(MaxMeshNumber))
      ALLOCATE(Savec(MaxMeshNumber))
      ALLOCATE(Saved(MaxMeshNumber))
      ALLOCATE(SaveTIL(MaxMeshNumber))
      ALLOCATE(SaveBDFACE(MaxMeshNumber))
    ENDIF

    SaveNNODE(MeshNo)=NNODE
    SaveNTRI(MeshNo)=NTRI
    SaveBFCE(MeshNo)=BFCE
    SaveBNODE(MeshNo)=BNODE

    ALLOCATE( Savevbox(MeshNo)%DPSave(NNODE))
    Savevbox(MeshNo)%DPSave(:)=vbox(:)

    ALLOCATE( Savevol(MeshNo)%DPSave(NTRI))
    Savevol(MeshNo)%DPSave(:)=vol(:)

    ALLOCATE( SaveVCL(MeshNo)%DPArrSave(NNODE,5))
    SaveVCL(MeshNo)%DPArrSave(:,:)=VCL(:,:)

    ALLOCATE( Saveb(MeshNo)%DPArrSave(NTRI,4))
    Saveb(MeshNo)%DPArrSave(:,:)=b(:,:)

    ALLOCATE( Savec(MeshNo)%DPArrSave(NTRI,4))
    Savec(MeshNo)%DPArrSave(:,:)=c(:,:)

    ALLOCATE( Saved(MeshNo)%DPArrSave(NTRI,4))
    Saved(MeshNo)%DPArrSave(:,:)=d(:,:)

    ALLOCATE( SaveTIL(MeshNo)%IntArrSave(NTRI,5))
    SaveTIL(MeshNo)%IntArrSave(:,:)=TIL(:,:)
    ALLOCATE( SaveBDFACE(MeshNo)%IntArrSave(BFCE,2))
    SaveBDFACE(MeshNo)%IntArrSave(:,:)=BDFACE(:,:)

  END SUBROUTINE SaveMesh


  !---------------------------------------------------------------
  !           LoadMesh(MeshNo) loads all important global variables
  !                            of the mesh
  !                            The index of the mesh is MeshNo
  !---------------------------------------------------------------

  SUBROUTINE LoadMesh(MeshNo)
    IMPLICIT NONE

    INTEGER MeshNo

    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF

    If(ALLOCATED(SaveNNODE).EQV..FALSE.)  THEN
      Write(*,*) ' No meshes saved ... '
      RETURN
    ENDIF

    NNODE=SaveNNODE(MeshNo)
    NMAX=NNODE
    CALL FieldAllocate( )
    NTRI=SaveNTRI(MeshNo)
    BFCE=SaveBFCE(MeshNo)
    BNODE=SaveBNODE(MeshNo)
    call MeshAllocate(NNODE, NTRI)
    call FaceAllocate()

    vbox(:)=Savevbox(MeshNo)%DPSave(:)
    vol(:)=Savevol(MeshNo)%DPSave(:)
    VCL(:,:)=SaveVCL(MeshNo)%DPArrSave(:,:)
    b(:,:)=Saveb(MeshNo)%DPArrSave(:,:)
    c(:,:)=Savec(MeshNo)%DPArrSave(:,:)
    d(:,:)=Saved(MeshNo)%DPArrSave(:,:)
    TIL(:,:)=SaveTIL(MeshNo)%IntArrSave(:,:)
    BDFACE(:,:)=SaveBDFACE(MeshNo)%IntArrSave(:,:)

  END SUBROUTINE LoadMesh


  !---------------------------------------------------------------
  ! RemeshTo(MeshNo) takes the current magnetization m
  !                  of the mesh and remeshes it to the saved
  !                  mesh with index MeshNo
  !                  The resulting magnetization is in the array mremesh
  !                  This is accessible and of right size after LoadMesh(MeshNo)
  !---------------------------------------------------------------

  SUBROUTINE RemeshTo(MeshNo)
    IMPLICIT NONE

    INTEGER MeshNo, SNNODE , i
    DOUBLE PRECISION srvec(3)

    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF

    If(ALLOCATED(SaveNNODE).EQV..FALSE.)  THEN
      Write(*,*) ' No meshes saved ... '
      RETURN
    ENDIF

    ! If(ALLOCATED(SaveVCL(MeshNo)%DPArrSave).EQV..FALSE.)  THEN
    !   Write(*,*) ' No XYZ  saved  for Meshnumber ',MeshNo
    !   RETURN
    ! ENDIF

    SNNODE = SaveNNODE(MeshNo)
    If(ALLOCATED(mremesh) ) DEALLOCATE(mremesh)
    ALLOCATE( mremesh(SNNODE,3))


    Do i=1,SNNODE
      srvec(:)=SaveVCL(MeshNo)%DPArrSave(i,:)
      call MeshInterpolate(srvec, mremesh(i,:) )
    END DO

  END SUBROUTINE RemeshTo


  !---------------------------------------------------------------
  !     subroutine  InvertMag( strx,stry,strz  )
  !---------------------------------------------------------------

  subroutine InvertMag( strx,stry,strz  )
    USE strings
    IMPLICIT NONE

    CHARACTER (LEN=20) :: strx,stry,strz
    integer ::  ios,i
    double precision :: inv(3)

    call value(strx,inv(1),ios)
    if(ios.eq.0) THEN
      call value(stry,inv(2),ios)
      if(ios.eq.0)  call value(strz,inv(3),ios)
    endif
    if (ios/=0) return


    inv(:)=inv(:)/abs(inv(:))
    Do i=1,NNODE
      m(i,:)=m(i,:)*inv(:)
    EndDo

    return
  end subroutine InvertMag


  !---------------------------------------------------------------
  !     subroutine  ModifyMag( str,angle )
  !---------------------------------------------------------------

  subroutine ModifyMag( str,angle )
    USE strings
    IMPLICIT NONE

    CHARACTER (LEN=20) :: str,lstr
    integer ::  i,j
    double precision :: angle, dm(3),pm(3),dmnorm,rangle=0.

    lstr=lowercase(str)
    rangle=tan(angle/180.*3.1415926535897)
    do j=1,3  ! calculate a random but uniform disturbation vector
      dm(j)=DRAND()-0.5
    enddo

    do i=1,NNODE
      ! in this case every spin is disturbed independently
      if (lstr.eq.'random') then
        do j=1,3
          dm(j)=DRAND()-0.5
        enddo
      endif

      pm(1)=dm(2)*m(i,3)-dm(3)*m(i,2)
      pm(2)=dm(3)*m(i,1)-dm(1)*m(i,3)
      pm(3)=dm(1)*m(i,2)-dm(2)*m(i,1)
      dmnorm=rangle/sqrt(pm(1)**2 + pm(2)**2 + pm(3)**2)

      do j=1,3
        dm(j)=m(i,j)+pm(j)*dmnorm
      enddo

      dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
      IF(dmnorm > 0.0) THEN
        dm(1)=dm(1)/dmnorm
        dm(2)=dm(2)/dmnorm
        dm(3)=dm(3)/dmnorm
      ENDIF

      m(i,:)=dm(:)
    enddo

    return
  end subroutine ModifyMag


!---------------------------------------------------------------
!     subroutine randomchange(args(2),args(3) )
!---------------------------------------------------------------

  subroutine randomchange( str,sran )
    USE strings
    IMPLICIT NONE

    CHARACTER (LEN=100) :: str,sran,lstr
    integer ::  ios,i,j
    double precision :: dm(3),pm(3),dmnorm,rangle=0.

    lstr=lowercase(str)
    if (lstr.eq.'all') then
      do i=1,NNODE
        if(BlockNumber(i)<2) then  ! only free nodes are randomized !!
          do j=1,3
            dm(j)=DRAND()-0.5
          enddo
          dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
          IF(dmnorm > 0.0) THEN
            dm(1)=dm(1)/dmnorm
            dm(2)=dm(2)/dmnorm
            dm(3)=dm(3)/dmnorm
          ENDIF
          m(i,:)=dm(:)
        endif
      enddo
    else
      call value(sran,rangle,ios)
      if(ios/=0) write (*,*)  'ERROR setting random angle: ',sran
      rangle=tan(rangle/180.*3.1415926535897)
      do i=1,NNODE
        if(BlockNumber(i)<2) then  ! only free nodes are randomized !!
          do j=1,3
            dm(j)=DRAND()-0.5
          enddo
          pm(1)=dm(2)*m(i,3)-dm(3)*m(i,2)
          pm(2)=dm(3)*m(i,1)-dm(1)*m(i,3)
          pm(3)=dm(1)*m(i,2)-dm(2)*m(i,1)
          dmnorm=rangle/sqrt(pm(1)**2 + pm(2)**2 + pm(3)**2)
          do j=1,3
            dm(j)=m(i,j)+pm(j)*dmnorm
          enddo
          dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
          IF(dmnorm > 0.0) THEN
            dm(1)=dm(1)/dmnorm
            dm(2)=dm(2)/dmnorm
            dm(3)=dm(3)/dmnorm
          ENDIF
          m(i,:)=dm(:)
        endif
      enddo
    endif

    return
  end subroutine randomchange



END MODULE Tetrahedral_Mesh_Data

MODULE Finite_Element
  USE Tetrahedral_Mesh_Data
  USE Utils
  IMPLICIT NONE
  SAVE


  ! nze :  Number of non-zero entries in sparse matrix for Phi1
  ! nze2  : Number of non-zero entries in sparse matrix for Phi2

  INTEGER nze,nze2 
  INTEGER, ALLOCATABLE  ::  Savenze(:), Savenze2(:) 

  REAL(KIND=DP) FEMTolerance

  ! kstiff,kstiff2,kanistiff : Geometric stiffness matrices for
  ! finite elements on the mesh

  REAL(KIND=DP), ALLOCATABLE  :: kstiff(:),kanistiff(:), kstiff2(:,:)  

  ! krow,kcol  : Indices on sparse stiffness matrix
  ! sparse     : Index on sparse matrix  ??

  INTEGER, ALLOCATABLE  :: krow(:),kcol(:),sparse(:)

  ! Matrices constructed from finite element formulation of problem
  ! stored in column major format
  ! namely (YA, RNR, CNR, nze) (BA, BRNR, BCRNR, BNODE)
  ! (YA4,FAX,FAY,FAZ,RNR, CNR,nze )
  ! (YA2, RNR2, CNR2,nze2 )

  ! YA(:),YA2(:),YA4(:,:) : Reformatted matrices for sparse solver
  ! BA(:),FAX(:),FAY(:),FAZ(:)    : Reformatted matrices for sparse solver


  REAL(KIND=DP), ALLOCATABLE  :: YA(:),YA2(:),YA4(:,:)
  REAL(KIND=DP), ALLOCATABLE  :: FAX(:),FAY(:),FAZ(:)
  REAL(KIND=DP), ALLOCATABLE  :: BA(:)

  TYPE(SaveDPRow), ALLOCATABLE  :: SaveYA(:),SaveYA2(:)  
  TYPE(SaveDPArray), ALLOCATABLE  ::  SaveYA4(:) 
  TYPE(SaveDPRow), ALLOCATABLE  :: SaveFAX(:),SaveFAY(:),SaveFAZ(:),SaveBA(:) 


  ! RNR(:), CNR(:), RNR2(:),CNR2(:), BRNR(:), BCNR(:) : Indices on reformatted
  !     sparse  matrices

  INTEGER, ALLOCATABLE :: RNR(:), CNR(:), RNR2(:),CNR2(:) 
  INTEGER, ALLOCATABLE :: BRNR(:), BCNR(:)

  TYPE(SaveIntRow), ALLOCATABLE  :: SaveRNR(:),SaveRNR2(:),SaveCNR(:),SaveCNR2(:)
  TYPE(SaveIntRow), ALLOCATABLE  :: SaveBRNR(:),SaveBCNR(:) 


  CONTAINS

  !---------------------------------------------------------------
  !          StiffMatrixAllocate and SparseMatrixAllocate
  !---------------------------------------------------------------


  SUBROUTINE StiffMatrixAllocate()
    USE Tetrahedral_Mesh_Data
    IMPLICIT NONE

    If(ALLOCATED(krow))       DEALLOCATE(krow)
    If(ALLOCATED(kcol))       DEALLOCATE(kcol)
    If(ALLOCATED(kstiff))     DEALLOCATE(kstiff)
    If(ALLOCATED(kstiff2))    DEALLOCATE(kstiff2)
    If(ALLOCATED(sparse))     DEALLOCATE(sparse)
    If(ALLOCATED(kanistiff))  DEALLOCATE(kanistiff)
    If(ALLOCATED(krow))       DEALLOCATE(krow)

    Allocate(kstiff(16*NTRI),krow(16*NTRI),kstiff2(16*NTRI,3) &
            & ,kcol(16*NTRI),sparse(16*NTRI),kanistiff(16*NTRI))

  END SUBROUTINE StiffMatrixAllocate


  SUBROUTINE SparseMatrixAllocate()
    USE Tetrahedral_Mesh_Data
    IMPLICIT NONE
    If(ALLOCATED(YA)) DEALLOCATE(YA,RNR,YA2,RNR2,YA4,FAX,FAY,FAZ,CNR,CNR2)
    Allocate(YA(nze),RNR(nze),YA2(nze2),RNR2(nze2),YA4(nze,3))
    Allocate(FAX(nze),FAY(nze),FAZ(nze),CNR(NNODE+1),CNR2(NNODE+1))
  END SUBROUTINE SparseMatrixAllocate








  !---------------------------------------------------------------
  ! BEM  : DEMAGSTIFF  ===  Purely geometric 
  !---------------------------------------------------------------

  SUBROUTINE demagstiff()
    USE Tetrahedral_Mesh_Data

    IMPLICIT NONE
    INTEGER knc, i,j,k, rc,rl,temp, rnum, prerow,precol
    INTEGER, ALLOCATABLE  :: colrowind(:,:)


    call StiffMatrixAllocate()
    ALLOCATE (colrowind(16*NTRI,3))

    kstiff(:)=0.d0
    kstiff2(:,:)=0.d0
    knc=0
    DO i=1,NTRI

      DO j=1,4
        DO k=1,4
          knc=knc+1
          kstiff(knc)=(b(i,j)*b(i,k)+c(i,j)*c(i,k)+d(i,j)*d(i,k)) &
          &             /(36.d0*vol(i))
          kstiff2(knc,1)=b(i,j)/(6.d0)
          kstiff2(knc,2)=c(i,j)/(6.d0)
          kstiff2(knc,3)=d(i,j)/(6.d0)

          krow(knc)=TIL(i,j)
          kcol(knc)=TIL(i,k)
          colrowind(knc,1)=TIL(i,k)
          colrowind(knc,2)=TIL(i,j)
          colrowind(knc,3)=knc
          IF (j/=k) THEN
            kanistiff(knc)=vol(i)/20.
          ELSE
            kanistiff(knc)=vol(i)/10.
          ENDIF
        END DO
      END DO

    END DO

    !   colrowsort + DO REPLACES inner DO LOOP below	  
    call colrowsort( )

    DO i=1,knc ! knc here is 16*NTRI
      sparse(i)=colrowind(i,3)
    END DO

!     sparse matrices in SLAP column FORMAT (for GMRES) 

    rc=1
    rnum=0

    DO rl=1,NNODE
      DO WHILE((rnum.LT.16*NTRI).AND.(kcol(sparse(rnum+1)).EQ.rl))
        rnum=rnum+1
      END DO

      nze=1
      DO WHILE (nze/=0)
        nze=0
        DO i=rc,rnum-1
          IF ( ( (krow(sparse(i+1))==rl).and.(krow(sparse(i))/=rl) ).or. &
                  &        ( (krow(sparse(i+1))<krow(sparse(i)) ).and. &
                      &        (krow(sparse(i))/=rl) )                 )  THEN
            temp=sparse(i)
            sparse(i)=sparse(i+1)
            sparse(i+1)=temp
            nze=1
          ENDIF
        END DO
      END DO

      rc=rnum+1
    END DO  

    !     count # of actual non zero entries, nze for phi
    !     count # of actual non zero entries, nze2 for phi2
    rc=0
    nze=1 

    prerow=1
    precol=1
    DO i=1,16*NTRI
      IF (                            &
      & (kcol(sparse(i))/=precol)     &
      & .or.(krow(sparse(i))/=prerow) &
      &) THEN
        nze=nze+1
        prerow=krow(sparse(i))
        precol=kcol(sparse(i))

        IF (                                                  &
        &   (NONZERO(VCL(prerow,4)) .OR. (NONZERO(VCL(precol,4))) ) &
        &   .AND.(prerow/=precol)                             &
        &) THEN
          rc=rc+1
        END IF

      ENDIF
    END DO
    nze2=nze-rc
    DEALLOCATE (colrowind) 

    RETURN

    CONTAINS

    SUBROUTINE colrowsort( )

      INTEGER M,NSTACK

      PARAMETER (M=7,NSTACK=50)
      INTEGER i,ir,j,jstack,k,l,istack(NSTACK)
      INTEGER a(3),temp(3)
      LOGICAL tt

      jstack=0
      l=1
      ir=16*NTRI
      1 if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a(:)=colrowind(j,:)
          do 11 i=j-1,1,-1
            call  smaller(colrowind(i,:),a(:),tt)
            if(tt)goto 2
            colrowind(i+1,:)=colrowind(i,:)
          11        continue
          i=0
          2         colrowind(i+1,:)=a(:)
        12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp(:)=colrowind(k,:)
        colrowind(k,:)=colrowind(l+1,:)
        colrowind(l+1,:)=temp(:)
        call  smaller(colrowind(ir,:),colrowind(l+1,:),tt)
        if(tt)then  
          temp(:)=colrowind(l+1,:)
          colrowind(l+1,:)=colrowind(ir,:)
          colrowind(ir,:)=temp(:)
        endif
        call  smaller(colrowind(ir,:),colrowind(l,:),tt)
        if(tt)then        
          temp(:)=colrowind(l,:)
          colrowind(l,:)=colrowind(ir,:)
          colrowind(ir,:)=temp(:)
        endif
        call  smaller(colrowind(l,:),colrowind(l+1,:),tt)
        if(tt)then  
          temp(:)=colrowind(l+1,:)
          colrowind(l+1,:)=colrowind(l,:)
          colrowind(l,:)=temp(:)
        endif
        i=l+1
        j=ir
        a(:)=colrowind(l,:)
        3 continue
        i=i+1
        call  smaller(colrowind(i,:),a(:),tt)
        if(tt)goto 3  
        4 continue
        j=j-1
        call  smaller(a(:),colrowind(j,:),tt)
        if(tt)goto 4  
        if(j.lt.i)goto 5
        temp(:)=colrowind(i,:)
        colrowind(i,:)=colrowind(j,:)
        colrowind(j,:)=temp(:)
        goto 3
        5 colrowind(l,:)=colrowind(j,:)
        colrowind(j,:)=a(:)
        jstack=jstack+2
        if(jstack.gt.NSTACK) print*, 'NSTACK too small in colrowsort'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      return
    END SUBROUTINE colrowsort 

    SUBROUTINE smaller(x,y,t)
      LOGICAL :: t
      INTEGER x(3),y(3)

      IF(x(1)==y(1)) THEN
        IF(x(2)==y(2)) THEN
          t = (x(3) <y(3))
        ELSE
          t = (x(2)<y(2))
        END IF
      ELSE
        t = (x(1)<y(1))
      END IF
      RETURN
    END SUBROUTINE smaller


  END SUBROUTINE demagstiff


  !---------------------------------------------------------------
  ! BEM  : NONZEROSTIFF   purely geometric 
  !---------------------------------------------------------------

  SUBROUTINE nonzerostiff()

    USE Tetrahedral_Mesh_Data

    IMPLICIT NONE
    INTEGER i,j,l,prerow,precol

    call SparseMatrixAllocate()


    YA(:)=0.0d0
    YA2(:)=0.0d0
    YA4(:,:)=0.0d0
    RNR(1)=krow(sparse(1))
    CNR(1)=1
    RNR2(1)=krow(sparse(1))
    CNR2(1)=1

    j=1
    l=1

    prerow=1
    precol=1

    DO i=1,16*NTRI
      IF ((krow(sparse(i))/=prerow) &
      & .or.(kcol(sparse(i))/=precol)) THEN
        IF (kcol(sparse(i))/=precol) THEN
          l=l+1
          CNR(l)=j+1
        ENDIF
        j=j+1 
        YA(j)=kstIFf(sparse(i))
        YA4(j,1)=kstiff2(sparse(i),1)
        YA4(j,2)=kstiff2(sparse(i),2)
        YA4(j,3)=kstiff2(sparse(i),3)
        RNR(j)=krow(sparse(i))
        prerow=krow(sparse(i))
        precol=kcol(sparse(i))
      ELSE
        YA(j)=YA(j)+kstIFf(sparse(i))
        YA4(j,1)=YA4(j,1)+kstiff2(sparse(i),1)
        YA4(j,2)=YA4(j,2)+kstiff2(sparse(i),2)
        YA4(j,3)=YA4(j,3)+kstiff2(sparse(i),3)
      ENDIF
    END DO

    CNR(NNODE+1)=nze+1

    !     form YA2 which is a duplicate of YA  
    !     with boundary nodes set to 1 for phi2
    !     set exterior boundary values to 0  

    j=1
    DO i=1,NNODE

      CNR2(i)=j

      DO l=CNR(i),CNR(i+1)-1
        IF (NONZERO(VCL(RNR(l),4)).and.(RNR(l)==i) ) THEN
          YA2(j)=1.0d0
          RNR2(j)=i
          j=j+1
        ENDIF
        IF ((.NOT.NONZERO(VCL(RNR(l),4))).AND.(.NOT.NONZERO(VCL(i,4)))) THEN
          YA2(j)=YA(l)
          RNR2(j)=RNR(l)
          j=j+1
        ENDIF
      END DO

    END DO

    CNR2(NNODE+1)=nze2+1

    WRITE(*,*) nze,'non-zero entries in sparse matrix for Phi1'
    WRITE(*,*) nze2,'non-zero entries in sparse matrix for Phi2'

    RETURN
  END SUBROUTINE nonzerostiff


  !---------------------------------------------------------------
  ! BEM  : FORCEMAT   geometric (boundary conditions)
  !---------------------------------------------------------------

  SUBROUTINE forcemat( )
    USE Tetrahedral_Mesh_Data
    IMPLICIT NONE

    REAL(KIND=DP) pi, BAXFULL(9*BFCE),BAYFULL(9*BFCE),BAZFULL(9*BFCE)
    INTEGER i,j,k,l,el,opp,nj,nk,nl, BCOL(9*BFCE),BROW(9*BFCE)


    DEALLOCATE(kstiff,kanistiff,krow,kcol,sparse)
    If(ALLOCATED(BA)) DEALLOCATE(BA,BRNR,BCNR)
    Allocate( BA(BNODE*BNODE),BRNR(BNODE*BNODE),BCNR(NNODE+1))



    pi=4.0d0*atan(1.0d0)
    write(*,*) 'Assembling multiplicative matrix for phi1'
    ! Form force vector matrices, FAX,FAY,FAZ
    !     calculate phi1, i.e. f   
    !     Add div m to force vector
    FAX(:)=-pi*YA4(:,1)
    FAY(:)=-pi*YA4(:,2)
    FAZ(:)=-pi*YA4(:,3)

    !      IF (pi==0.0) THEN
    !     Add m.n Neumann condition to force vector
    !     Build up element contributions

    DO i=1,BFCE
      opp=BDFACE(i,2)
      SELECT CASE (opp)
        CASE(3)
          j = 4
          k = 1
          l = 2
        CASE(1)
          j = 2
          k = 3
          l = 4
        CASE(2)
          j = 3
          k = 1
          l = 4
        CASE(4)
          j = 2
          k = 1
          l = 3
      END SELECT

      el=BDFACE(i,1)
      nj=TIL(el,j)
      nk=TIL(el,k)
      nl=TIL(el,l)

      BROW(9*(i-1)+1)=nj
      BCOL(9*(i-1)+1)=nj
      BAXFULL(9*(i-1)+1)=-pi*b(el,opp)/3.d0
      BAYFULL(9*(i-1)+1)=-pi*c(el,opp)/3.d0
      BAZFULL(9*(i-1)+1)=-pi*d(el,opp)/3.d0

      BROW(9*(i-1)+2)=nj
      BCOL(9*(i-1)+2)=nk
      BAXFULL(9*(i-1)+2)=-pi*b(el,opp)/6.d0
      BAYFULL(9*(i-1)+2)=-pi*c(el,opp)/6.d0
      BAZFULL(9*(i-1)+2)=-pi*d(el,opp)/6.d0

      BROW(9*(i-1)+3)=nj
      BCOL(9*(i-1)+3)=nl
      BAXFULL(9*(i-1)+3)=-pi*b(el,opp)/6.d0
      BAYFULL(9*(i-1)+3)=-pi*c(el,opp)/6.d0
      BAZFULL(9*(i-1)+3)=-pi*d(el,opp)/6.d0

      BROW(9*(i-1)+4)=nk
      BCOL(9*(i-1)+4)=nj
      BAXFULL(9*(i-1)+4)=-pi*b(el,opp)/6.d0
      BAYFULL(9*(i-1)+4)=-pi*c(el,opp)/6.d0
      BAZFULL(9*(i-1)+4)=-pi*d(el,opp)/6.d0

      BROW(9*(i-1)+5)=nk
      BCOL(9*(i-1)+5)=nk
      BAXFULL(9*(i-1)+5)=-pi*b(el,opp)/3.d0
      BAYFULL(9*(i-1)+5)=-pi*c(el,opp)/3.d0
      BAZFULL(9*(i-1)+5)=-pi*d(el,opp)/3.d0

      BROW(9*(i-1)+6)=nk
      BCOL(9*(i-1)+6)=nl
      BAXFULL(9*(i-1)+6)=-pi*b(el,opp)/6.d0
      BAYFULL(9*(i-1)+6)=-pi*c(el,opp)/6.d0
      BAZFULL(9*(i-1)+6)=-pi*d(el,opp)/6.d0

      BROW(9*(i-1)+7)=nl
      BCOL(9*(i-1)+7)=nj
      BAXFULL(9*(i-1)+7)=-pi*b(el,opp)/6.d0
      BAYFULL(9*(i-1)+7)=-pi*c(el,opp)/6.d0
      BAZFULL(9*(i-1)+7)=-pi*d(el,opp)/6.d0

      BROW(9*(i-1)+8)=nl
      BCOL(9*(i-1)+8)=nk
      BAXFULL(9*(i-1)+8)=-pi*b(el,opp)/6.d0
      BAYFULL(9*(i-1)+8)=-pi*c(el,opp)/6.d0
      BAZFULL(9*(i-1)+8)=-pi*d(el,opp)/6.d0

      BROW(9*(i-1)+9)=nl
      BCOL(9*(i-1)+9)=nl
      BAXFULL(9*(i-1)+9)=-pi*b(el,opp)/3.d0
      BAYFULL(9*(i-1)+9)=-pi*c(el,opp)/3.d0
      BAZFULL(9*(i-1)+9)=-pi*d(el,opp)/3.d0

    END DO

    !     Add element contributions to sparse format force vector

    DO i=1,9*BFCE
      l=BROW(i)
      k=BCOL(i)
      DO j=CNR(l),CNR(l+1)-1
        IF (RNR(j)==k) THEN
          FAX(j)=FAX(j)+BAXFULL(i)
          FAY(j)=FAY(j)+BAYFULL(i)
          FAZ(j)=FAZ(j)+BAZFULL(i)
        ENDIF
      END DO
    END DO

    RETURN 
  END SUBROUTINE forcemat


  !---------------------------------------------------------------
  ! BEM  : BOUNDMATA
  !---------------------------------------------------------------

  SUBROUTINE boundmata()
    USE Tetrahedral_Mesh_Data

    IMPLICIT NONE

    REAL(KIND=DP) pi,rx,ry,rz &
      & ,area, normb,normc,normd,normval,SAngle & 
      & ,neta1,neta2,neta3,contrib,zeta,xp,xq,xr &
      & ,yp,yq,yr,zp,zq,zr,s1,s2,s3,rho1,rho2,rho3,gam11 &
      & ,gam12,gam13,gam22,gam21,gam23,gam33,gam31,gam32,q1,q2,q3 &
      & ,tempval(3),dsparse(NNODE)
    INTEGER i,j,el,opp   &
      & ,k,l,dum2,p1,p2,p3,cn,dum3,p,q,r,n(3),rc,rl  &
      & ,rnum 


    pi=4.0d0*atan(1.0d0) 
    write(*,*) 'Assembling multiplicative matrix for phi2'
    BCNR(:)=0
    BRNR(:)=0
    BA(:)=0.0
    rl=0
    rnum=0


    DO k=1,NNODE
      contrib=0.0d0
      dsparse(:)=0.0d0
      !      node k is the observation point
      IF (NONZERO(VCL(k,4))) THEN
        rx=VCL(k,1)
        ry=VCL(k,2)
        rz=VCL(k,3)

        !     calculate contributions to fx2
        DO dum3=1,BFCE

          el=BDFACE(dum3,1)
          opp=BDFACE(dum3,2)
          SELECT CASE (opp)
            CASE (3) 
              p = 4
              q = 2
              r = 1

            CASE (1) 
              p = 3
              q = 2
              r = 4

            CASE (2) 
              p = 1
              q = 3
              r = 4

            CASE (4)
              p = 1
              q = 2
              r = 3
          END SELECT

          cn=k
          p3=TIL(el,p)
          p2=TIL(el,q)
          p1=TIL(el,r)

          xp=VCL(p1,1)
          xq=VCL(p2,1)
          xr=VCL(p3,1)
          yp=VCL(p1,2)
          yq=VCL(p2,2)
          yr=VCL(p3,2)
          zp=VCL(p1,3)
          zq=VCL(p2,3)
          zr=VCL(p3,3)

          !  get the solid anglemage by the element surface dum3 at the obervation point
          ! Lindholm's variable S_t

          SAngle=GET_ANGLE(cn,p1,p2,p3)
          ! IF the solid angle is ZERO THEN the observation poimt and the element dum3
          ! must be in the same place, and the linDOlm funtion will be zero 

          ! -- the INWARD normal to surface face of a boundary element
          !   (ie Lindholms variable Zeta)

          normval=sqrt(b(el,opp)**2+c(el,opp)**2+d(el,opp)**2)
          normb=-1.*(b(el,opp)/normval)
          normc=-1.*(c(el,opp)/normval)
          normd=-1.*(d(el,opp)/normval)

          area=0.5*normval

          s1=sqrt((xq-xp)**2+(yq-yp)**2+(zq-zp)**2)
          s2=sqrt((xr-xq)**2+(yr-yq)**2+(zr-zq)**2)
          s3=sqrt((xp-xr)**2+(yp-yr)**2+(zp-zr)**2)

          rho1=sqrt((xp-rx)**2+(yp-ry)**2+(zp-rz)**2)
          rho2=sqrt((xq-rx)**2+(yq-ry)**2+(zq-rz)**2)
          rho3=sqrt((xr-rx)**2+(yr-ry)**2+(zr-rz)**2)

          zeta=((normb)*(xp-rx)+(normc)*(yp-ry)+normd*(zp-rz))

          gam11=((xr-xq)*(xq-xp)+(yr-yq)*(yq-yp)+(zr-zq)*(zq-zp))/(s2*s1)
          gam21=((xp-xr)*(xq-xp)+(yp-yr)*(yq-yp)+(zp-zr)*(zq-zp))/(s3*s1)
          gam31=1.0d0
          gam12=1.0d0
          gam22=((xp-xr)*(xr-xq)+(yp-yr)*(yr-yq)+(zp-zr)*(zr-zq))/(s3*s2)
          gam32=gam11
          gam13=gam22
          gam23=1.0d0
          gam33=gam21

          neta1=((normc*(zq-zp)-normd*(yq-yp))*(xp-rx)+(normd*(xq-xp)-normb &
            &   *(zq-zp))*(yp-ry) + (normb*(yq-yp)-normc*(xq-xp))*(zp-rz) )/s1

          neta2=((normc*(zr-zq)-normd*(yr-yq))*(xq-rx)+(normd*(xr-xq)-normb &
            &   *(zr-zq))*(yq-ry)+(normb*(yr-yq)-normc*(xr-xq))*(zq-rz) )/s2

          neta3=((normc*(zp-zr)-normd*(yp-yr))*(xr-rx)+(normd*(xp-xr)-normb &
            &   *(zp-zr))*(yr-ry)+(normb*(yp-yr)-normc*(xp-xr))*(zr-rz) )/s3

          !       IF ((p1/=k).AND.(p2/=k).AND.(p3/=k)) THEN
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          IF ((p1==k).OR.(p2==k).OR.(p3==k)) THEN
            if (NONZERO(SAngle)) then
              print*,'Solid = ',SAngle
            ENDIF

            !      contrib=contrib

          else
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            q1=log((rho1+rho2+s1)/(rho1+rho2-s1))
            q2=log((rho2+rho3+s2)/(rho2+rho3-s2))
            q3=log((rho3+rho1+s3)/(rho3+rho1-s3))

            dum2=int(DSIGN(1.0d0,zeta))

            n(1)=TIL(el,p)
            tempval(1)=                                                     & 
              & ((s2*( dum2*neta2*SAngle-zeta*(gam11*q1+gam12*q2+gam13*q3) )     &
              & /(8*pi*area)))
            n(2)=TIL(el,q)
            tempval(2)=                                                     &
              & ((s3*(dum2*neta3*SAngle-zeta*(gam21*q1+gam22*q2+gam23*q3))       &
              & /(8*pi*area)))
            n(3)=TIL(el,r)
            tempval(3)=                                                     &
            & ((s1*(dum2*neta1*SAngle-zeta*(gam31*q1+gam32*q2+gam33*q3))       &
            & /(8*pi*area)))
            dsparse(n(:))=dsparse(n(:))+tempval(:)
          ENDIF

        ENDDO !loop over all boundary elements


        ! add on solid angle part

        dsparse(k)=dsparse(k)+((solid(k)/(4.*pi))-1.d0)

        BCNR(k)=rl+1
        rc=0
        DO i=1,NNODE
          IF (NONZERO(dsparse(i))) THEN
            rc=rc+1
            BA(rl+rc)=dsparse(i)
            BRNR(rl+rc)=i
            ! write(*,*) k,BRNR(rl+rc),BA(rl+rc)
            rnum=rnum+1
          ENDIF
        ENDDO ! i loop
        rl=rl+rc
      ENDIF

    END DO !k

    BCNR(NNODE+1)=rl+1

    ! loop over entire boundary element matrix

    rnum=0
    DO i=1,NNODE
      !         write(*,*) i,BCNR(i)
      IF ((BCNR(i)/=0)) THEN
        l=1
        DO WHILE (BCNR(i+l)==0)
          l=l+1
        END DO ! while
        DO j=BCNR(i),BCNR(i+l)-1
          !          write(*,*) i,BRNR(j),BA(j)
          rnum=rnum+1
        ENDDO ! j loop
      ENDIF
    ENDDO ! i loop

    RETURN 
  END SUBROUTINE boundmata


  !---------------------------------------------------------------		
  !           SaveFEM(MeshNo) saves all important global variables
  !                            of the FEM into arrays    
  !                            The index of the FEMState  is MeshNo   
  !---------------------------------------------------------------

  SUBROUTINE SaveFEM(MeshNo)
    IMPLICIT NONE
    INTEGER MeshNo 


    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF

    If(ALLOCATED(Savenze).EQV..FALSE.)  THEN
      ALLOCATE(Savenze(MaxMeshNumber),Savenze2(MaxMeshNumber))
      ALLOCATE(SaveYA(MaxMeshNumber),SaveYA2(MaxMeshNumber))
      ALLOCATE(SaveYA4(MaxMeshNumber))
      ALLOCATE(SaveFAX(MaxMeshNumber),SaveFAY(MaxMeshNumber))
      ALLOCATE(SaveFAZ(MaxMeshNumber))
      ALLOCATE(SaveBA(MaxMeshNumber),SaveRNR(MaxMeshNumber))
      ALLOCATE(SaveRNR2(MaxMeshNumber))
      ALLOCATE(SaveCNR(MaxMeshNumber),SaveCNR2(MaxMeshNumber))
      ALLOCATE(SaveBRNR(MaxMeshNumber),SaveBCNR(MaxMeshNumber))
    ENDIF

    Savenze(MeshNo)=nze
    Savenze2(MeshNo)=nze2

    ALLOCATE( SaveYA(MeshNo)%DPSave(nze)) 
    SaveYA(MeshNo)%DPSave(:)= YA(:)
    ALLOCATE( SaveYA2(MeshNo)%DPSave(nze2)) 
    SaveYA2(MeshNo)%DPSave(:)= YA2(:)
    ALLOCATE( SaveFAX(MeshNo)%DPSave(nze)) 
    SaveFAX(MeshNo)%DPSave(:)= FAX(:)
    ALLOCATE( SaveFAY(MeshNo)%DPSave(nze)) 
    SaveFAY(MeshNo)%DPSave(:)= FAY(:)
    ALLOCATE( SaveFAZ(MeshNo)%DPSave(nze)) 
    SaveFAZ(MeshNo)%DPSave(:)= FAZ(:)
    ALLOCATE( SaveBA(MeshNo)%DPSave(BNODE*BNODE)) 
    SaveBA(MeshNo)%DPSave(:)= BA(:)

    ALLOCATE( SaveYA4(MeshNo)%DPArrSave(nze,3)) 
    SaveYA4(MeshNo)%DPArrSave(:,:)= YA4(:,:)

    ALLOCATE( SaveRNR(MeshNo)%IntSave(nze)) 
    SaveRNR(MeshNo)%IntSave(:)= RNR(:)
    ALLOCATE( SaveRNR2(MeshNo)%IntSave(nze2)) 
    SaveRNR2(MeshNo)%IntSave(:)= RNR2(:)
    ALLOCATE( SaveCNR(MeshNo)%IntSave(NNODE+1)) 
    SaveCNR(MeshNo)%IntSave(:)= CNR(:)
    ALLOCATE( SaveCNR2(MeshNo)%IntSave(NNODE+1)) 
    SaveCNR2(MeshNo)%IntSave(:)= CNR2(:)

    ALLOCATE( SaveBRNR(MeshNo)%IntSave(BNODE*BNODE)) 
    SaveBRNR(MeshNo)%IntSave(:)= BRNR(:)
    ALLOCATE( SaveBCNR(MeshNo)%IntSave(NNODE+1)) 
    SaveBCNR(MeshNo)%IntSave(:)= BCNR(:)


  END SUBROUTINE SaveFEM


    !---------------------------------------------------------------		
    !           LoadFEM(MeshNo) loads all important global variables
    !                            of the FEM     
    !                            The index of the FEMState  is MeshNo   
    !---------------------------------------------------------------

  SUBROUTINE LoadFEM(MeshNo)
    IMPLICIT NONE
    INTEGER MeshNo 


    If(MeshNo> MaxMeshNumber) THEN
      Write(*,*) ' Meshnumber ',MeshNo,' too large'
      RETURN
    ENDIF

    If(ALLOCATED(Savenze).EQV..FALSE.)  THEN
      Write(*,*) ' No FEM data saved ... '
      RETURN
    ENDIF
    If(ALLOCATED(BA)) DEALLOCATE(BA,BRNR,BCNR)
    Allocate( BA(BNODE*BNODE),BRNR(BNODE*BNODE),BCNR(NNODE+1))

    nze    = Savenze(MeshNo)
    nze2   = Savenze2(MeshNo)

    call StiffMatrixAllocate()
    call SparseMatrixAllocate()


    YA(:)  = SaveYA(MeshNo)%DPSave(:)
    YA2(:) = SaveYA2(MeshNo)%DPSave(:) 

    FAX(:)  = SaveFAX(MeshNo)%DPSave(:)
    FAY(:)  = SaveFAY(MeshNo)%DPSave(:)
    FAZ(:)  = SaveFAZ(MeshNo)%DPSave(:)
    BA(:)   = SaveBA(MeshNo)%DPSave(:)

    YA4(:,:) =  SaveYA4(MeshNo)%DPArrSave(:,:)
    RNR(:)   =  SaveRNR(MeshNo)%IntSave(:)
    RNR2(:)  =  SaveRNR2(MeshNo)%IntSave(:)

    CNR(:)  = SaveCNR(MeshNo)%IntSave(:)
    CNR2(:) = SaveCNR2(MeshNo)%IntSave(:)

    BRNR(:) = SaveBRNR(MeshNo)%IntSave(:)
    BCNR(:) = SaveBCNR(MeshNo)%IntSave(:)


  END SUBROUTINE LoadFEM

END MODULE Finite_Element

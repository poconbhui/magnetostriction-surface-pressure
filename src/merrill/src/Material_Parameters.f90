MODULE Material_Parameters

  USE Utils

  IMPLICIT NONE
  SAVE

  ! Global type definitions for saving meshes or other  arrays  varying in size

  TYPE SaveDPRow
    REAL(KIND=DP), DIMENSION(:), POINTER :: DPSave
  END TYPE SaveDPRow

  TYPE SaveDPArray
    REAL(KIND=DP), DIMENSION(:,:), POINTER :: DPArrSave
  END TYPE SaveDPArray

  TYPE SaveIntRow
    INTEGER, DIMENSION(:), POINTER :: INTSave
  END TYPE SaveIntRow

  TYPE SaveIntArray
    INTEGER, DIMENSION(:,:), POINTER :: INTArrSave
  END TYPE SaveIntArray

  !        Input from   .consts file variables

  REAL(KIND=DP)  mcon(6), T, ag, ea(3)
  REAL(KIND=DP)  extappst,extappfin,extappstep
  REAL(KIND=DP)  hz(3)
  CHARACTER (LEN=5) ::  anisform
  CHARACTER (LEN=16) ::  solver
  INTEGER MF,JACFLG,JPRE,MAXORD
  REAL(KIND=DP)  new(4)

  INTEGER rest, MaxRestarts, MaxEnergyEval, MaxPathEval, WhichExchange

  REAL(KIND=DP)  alpharot,thetarot, phirot

  !        Material parameters

  REAL(KIND=DP) Ms, K1, Aex, Ls, mu
  REAL(KIND=DP)  Kd, LambdaEx,QHardness, EnergyUnit


  !        Rotation matrix for anisotropy

  REAL(KIND=DP) rot(3,3)

  !        Time bookkeeping

  REAL(KIND=DP) cputime_now,cputime_start , cpuold
  INTEGER  elapsed_start, elapsed_now, elapsedold


  !        Problem size (usually = NNODES or 2*NNODES )

  INTEGER NMAX

  REAL(KIND=DP) extapp


  CHARACTER (LEN=200) ::  meshfile,logfile,restartfile,iteratfile &
    & ,datafile, stem, infile, hystfile
  CHARACTER(LEN=5) :: field

  !        Effective field contributions

  REAL(KIND=DP)  magx,magy,magz,hznorm,eanorm


  REAL(KIND=DP)         &
    AnisEnerg, DemagEnerg, BEnerg,           &
    ExchangeE, ExEnerg2, ExEnerg3, ExEnerg4, &
    MstrEnerg

  REAL(KIND=DP), ALLOCATABLE ::   &
    htot(:,:),                                          &
    hanis(:,:), hdemag(:,:), hext(:,:),                 &
    hexch(:,:), hexch2(:,:), hexch3(:,:) , hexch4(:,:), &
    hmstr(:,:)

  REAL(KIND=DP), ALLOCATABLE :: &
    DExPhi(:), DExTheta(:)

  REAL(KIND=DP), ALLOCATABLE :: umstr(:,:)

  LOGICAL :: EnergyLogging,CalcAllExchQ
  INTEGER NBadTets


CONTAINS


  !---------------------------------------------------------------
  !            Evaluate program arguments
  !---------------------------------------------------------------

  ! Initialize all variables in this module
  SUBROUTINE InitializeMaterialParameters()

    ! Unused
    ! mcon(6), T, ag, extappst ,extappfin, extappstep
    ! solver
    ! MF,JACFLG,JPRE,MAXORD
    ! magx,magy,magz,
    ! cputime_now,cputime_start , cpuold
    ! elapsed_start, elapsed_now, elapsedold

    ! Uniaxial gradient direction
    ea = (/ 1.0, 0.0, 0.0 /)

    ! External field
    hz = 0.0

    ! Anisotropy form
    anisform='cubic'

    ! Initial magnetization when reading patran file
    new = 0.0


    ! Minimizer parameters
    rest = 0
    MaxRestarts=4
    MaxEnergyEval=100000
    MaxPathEval=10000
    WhichExchange = 1 ! Laplace

    ! Axis rotation matrix angles
    alpharot = 0
    thetarot = 0
    phirot   = 0

    ! Saturation Magnetization, Anisotropy, Exchange constants.
    Ms  = 0.0
    K1  = 0.0
    Aex = 0.0
    ! Default length scale in microns. Ls = length scale squared.
    Ls = 1e12
    ! Permeability of free space / (4*pi)
    mu = 1e-7

    Kd = 0.0
    LambdaEx   = 0.0
    QHardness  = 0.0
    EnergyUnit = 0.0


    ! Define Identity as default anisotropy rotation
    rot(:,:) = 0.0
    rot(1,1) = 1.0
    rot(2,2) = 1.0
    rot(3,3) = 1.0


    ! !        Problem size (usually = NNODES or 2*NNODES )

    ! INTEGER NMAX

    ! External field strength
    extapp=0.0


    ! CHARACTER (LEN=200) ::  meshfile,logfile,restartfile,iteratfile &
    !   & ,datafile, stem, infile, hystfile
    ! CHARACTER(LEN=5) :: field

    ! !        Effective field contributions

    ! REAL(KIND=DP)  hznorm,eanorm


    AnisEnerg  = 0.0
    DemagEnerg = 0.0
    BEnerg     = 0.0
    ExchangeE  = 0.0
    ExEnerg2   = 0.0
    ExEnerg3   = 0.0
    ExEnerg4   = 0.0
    MstrEnerg  = 0.0


    ! (no logfile is written)
    EnergyLogging = .FALSE.

    ! To speed up calculation : .false. suppresses calculation of
    !   unused exchange energies
    CalcAllExchQ=.FALSE.

    NBadTets = 0

  END SUBROUTINE InitializeMaterialParameters

  !---------------------------------------------------------------
  !            SetRotationMatrix
  !---------------------------------------------------------------

  SUBROUTINE SetRotationMatrix( )

    IMPLICIT NONE

    rot(1,1) = dcos(phirot)*dcos(thetarot)
    rot(2,1) = -dsin(phirot)*dcos(alpharot) + &
      &               dcos(phirot)*dsin(thetarot)*dsin(alpharot)
    rot(3,1) = -dsin(thetarot)*dcos(phirot)*dcos(alpharot) - &
      &               dsin(phirot)*dsin(alpharot)

    rot(1,2) = dsin(phirot)*dcos(thetarot)
    rot(2,2) = dcos(phirot)*dcos(alpharot) +  &
      &               dsin(phirot)*dsin(thetarot)*dsin(alpharot)
    rot(3,2) = -dsin(phirot)*dsin(thetarot)*dcos(alpharot) + &
      &               dcos(phirot)*dsin(alpharot)

    rot(1,3) = dsin(thetarot)
    rot(2,3) = -dcos(thetarot)*dsin(alpharot)
    rot(3,3) = dcos(thetarot)*dcos(alpharot)

    write(*,*) "Set Rotation Matrix:"
    write(*,*) "  ", rot(:,1)
    write(*,*) "  ", rot(:,2)
    write(*,*) "  ", rot(:,3)
  END SUBROUTINE SetRotationMatrix


  !---------------------------------------------------------------
  !          FieldAllocate
  !---------------------------------------------------------------

  SUBROUTINE FieldAllocate( )
    IMPLICIT NONE
    REAL(KIND=DP)  hznorm,eanorm

    IF(NMAX<1) THEN
      write(*,*) ' ERROR ALLOCATING FIELDS: NMAX=',NMAX
      STOP
    ENDIF

    IF(ALLOCATED(hdemag)) THEN
      DEALLOCATE(                        &
        htot,                          &
        hdemag, hanis, hext,           &
        hexch, hexch2, hexch3, hexch4, &
        hmstr )
    ENDIF
    IF(ALLOCATED(DExPhi)) DEALLOCATE(DExPhi, DExTheta)
    IF(ALLOCATED(umstr)) DEALLOCATE(umstr)

    Allocate(                                                         &
      htot(NMAX,3),                                                 &
      hdemag(NMAX,3), hanis(NMAX,3), hext(NMAX,3),                  &
      hexch(NMAX,3) ,hexch2(NMAX,3),hexch3(NMAX,3) ,hexch4(NMAX,3), &
      hmstr(NMAX,3) )
    Allocate(DExPhi(NMAX),DExTheta(NMAX))
    Allocate(umstr(NMAX, 3))

    htot   = 0.0
    hdemag = 0.0
    hanis  = 0.0
    hext   = 0.0
    hexch  = 0.0
    hexch2 = 0.0
    hexch3 = 0.0
    hexch4 = 0.0
    hmstr  = 0.0

    DExPhi   = 0.0
    DExTheta = 0.0

    umstr = 0


    !---------------------------------------------------------------
    ! Normalize external field and anisotropy direction vectors.
    !---------------------------------------------------------------

    hznorm=sqrt(hz(1)**2 + hz(2)**2 + hz(3)**2)
    IF(hznorm > 0.0) THEN
      hz(1)=hz(1)/hznorm
      hz(2)=hz(2)/hznorm
      hz(3)=hz(3)/hznorm
    ENDIF

    eanorm=sqrt(ea(1)**2 + ea(2)**2 + ea(3)**2)
    IF(eanorm > 0.0) THEN
      ea(1)=ea(1)/eanorm
      ea(2)=ea(2)/eanorm
      ea(3)=ea(3)/eanorm
    ENDIF


  END SUBROUTINE FieldAllocate


  !---------------------------------------------------------------
  ! SUBROUTINE Magnetite( TCelsius)
  !        defines temperature dependent material constants
  !        for magnetite above room temperature
  !---------------------------------------------------------------

  SUBROUTINE Magnetite( TCelsius)
    IMPLICIT NONE
    REAL(KIND=DP)  TC, T, TCelsius


    T=TCelsius
    TC=580 ! Curie temperature of magnetite in Celsius
    if(T<0) write(*,*) 'Warning :: Magnetite material constants for &
        &T<0 deg Celsius are not reliable !! T=',TCelsius
    if(T.LE.TC) then
      anisform='cubic'
      Aex= (sqrt( 21622.526+816.476*(TC-T))-147.046 )/408.238e11
      Ms=737.384*51.876*(TC-T)**0.4
      write(*,*) "We're using Ms=", Ms
      K1=-2.13074e-5*(TC-T)**3.2
      Kd= 4* (3.1415926535897932)*mu*Ms*Ms*0.5
      LambdaEx= Sqrt(Aex/Kd)
      QHardness= K1/Kd
    else
      write(*,*) 'Warning :: Magnetite material constants for &
        &T>580 deg Celsius are ZERO T=',TCelsius
      anisform='cubic'
      Aex= 0.
      Ms=0.
      K1=0.
    endif
  END SUBROUTINE Magnetite

END MODULE Material_Parameters

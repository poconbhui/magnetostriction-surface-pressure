! DSDGMR and all it's dependencies follow from here
!
      SUBROUTINE DSDGMR(N, B, X, NELT, IA, JA, A, ISYM, NSAVE, &
           ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, &
           IWORK, LENIW )
!***BEGIN PROLOGUE  DSDGMR
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSDGMR-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Generalized Minimum Residual
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE  Diagonally scaled GMRES iterative sparse Ax=b solver.
!            This routine uses the generalized minimum residual
!            (GMRES) method with diagonal scaling to solve possibly
!            non-symmetric linear systems of the form: A*x = b.
!***DESCRIPTION
! *Usage:
!      INTEGER   N, NELT, IA(NELT), JA(NELT), ISYM, NSAVE
!      INTEGER   ITOL, ITMAX, IERR, IUNIT, LENW, IWORK(LENIW), LENIW
!      DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR
!      DOUBLE PRECISION RWORK(LENW)
!      EXTERNAL  MATVEC, MSOLVE
!
!      CALL DSDGMR(N, B, X, NELT, IA, JA, A, ISYM, NSAVE,
!     $     ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT,
!     $     RWORK, LENW, IWORK, LENIW)
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :INOUT    Double Precision X(N).
!         On input X is your initial guess for solution vector.
!         On output X is the final approximate solution.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(NELT).
! A      :IN       Double Precision A(NELT).
!         These arrays should hold the matrix A in either the SLAP
!         Triad format or the SLAP Column format.  See "Description",
!         below.  If the SLAP Triad format is chosen it is changed
!         internally to the SLAP Column format.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! NSAVE  :IN       Integer.
!         Number of direction vectors to save and orthogonalize against.
!         Must be greater than 1.
! ITOL   :IN       Integer.
!         Flag to indicate the type of convergence criterion used.
!         ITOL=0  Means the  iteration stops when the test described
!                 below on  the  residual RL  is satisfied.  This is
!                 the  "Natural Stopping Criteria" for this routine.
!                 Other values  of   ITOL  cause  extra,   otherwise
!                 unnecessary, computation per iteration and     are
!                 therefore  much less  efficient.  See  ISDGMR (the
!                 stop test routine) for more information.
!         ITOL=1  Means   the  iteration stops   when the first test
!                 described below on  the residual RL  is satisfied,
!                 and there  is either right  or  no preconditioning
!                 being used.
!         ITOL=2  Implies     that   the  user    is   using    left
!                 preconditioning, and the second stopping criterion
!                 below is used.
!         ITOL=3  Means the  iteration stops   when  the  third test
!                 described below on Minv*Residual is satisfied, and
!                 there is either left  or no  preconditioning begin
!                 used.
!         ITOL=11 is    often  useful  for   checking  and comparing
!                 different routines.  For this case, the  user must
!                 supply  the  "exact" solution or  a  very accurate
!                 approximation (one with  an  error much less  than
!                 TOL) through a common block,
!                     COMMON /SOLBLK/ SOLN(1)
!                 if ITOL=11, iteration stops when the 2-norm of the
!                 difference between the iterative approximation and
!                 the user-supplied solution  divided by the  2-norm
!                 of the  user-supplied solution  is  less than TOL.
!                 Note that this requires  the  user to  set up  the
!                 "COMMON     /SOLBLK/ SOLN(LENGTH)"  in the calling
!                 routine.  The routine with this declaration should
!                 be loaded before the stop test so that the correct
!                 length is used by  the loader.  This procedure  is
!                 not standard Fortran and may not work correctly on
!                 your   system (although  it  has  worked  on every
!                 system the authors have tried).  If ITOL is not 11
!                 then this common block is indeed standard Fortran.
! TOL    :INOUT    Double Precision.
!         Convergence criterion, as described below.  If TOL is set
!         to zero on input, then a default value of 500*(the smallest
!         positive magnitude, machine epsilon) is used.
! ITMAX  :IN       Integer.
!         Maximum number of iterations.  This routine uses the default
!         of NRMAX = ITMAX/NSAVE to determine the when each restart
!         oshould ccur.  See the description of NRMAX and MAXL in
!         DGMRES for a full and frightfully interesting discussion of
!         this topic.
! ITER   :OUT      Integer.
!         Number of iterations required to reach convergence, or
!         ITMAX+1 if convergence criterion could not be achieved in
!         ITMAX iterations.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as
!         defined by ITOL.  Letting norm() denote the Euclidean
!         norm, ERR is defined as follows...
!         If ITOL=0, then ERR = norm(SB*(B-A*X(L)))/norm(SB*B),
!                               for right or no preconditioning, and
!                         ERR = norm(SB*(M-inverse)*(B-A*X(L)))/
!                                norm(SB*(M-inverse)*B),
!                               for left preconditioning.
!         If ITOL=1, then ERR = norm(SB*(B-A*X(L)))/norm(SB*B),
!                               since right or no preconditioning
!                               being used.
!         If ITOL=2, then ERR = norm(SB*(M-inverse)*(B-A*X(L)))/
!                                norm(SB*(M-inverse)*B),
!                               since left preconditioning is being
!                               used.
!         If ITOL=3, then ERR =  Max  |(Minv*(B-A*X(L)))(i)/x(i)|
!                               i=1,n
!         If ITOL=11, then ERR = norm(SB*(X(L)-SOLN))/norm(SB*SOLN).
! IERR   :OUT      Integer.
!         Return error flag.
!               IERR = 0 => All went well.
!               IERR = 1 => Insufficient storage allocated for
!                           RGWK or IGWK.
!               IERR = 2 => Routine DPIGMR failed to reduce the norm
!                           of the current residual on its last call,
!                           and so the iteration has stalled.  In
!                           this case, X equals the last computed
!                           approximation.  The user must either
!                           increase MAXL, or choose a different
!                           initial guess.
!               IERR =-1 => Insufficient length for RGWK array.
!                           IGWK(6) contains the required minimum
!                           length of the RGWK array.
!               IERR =-2 => Inconsistent ITOL and JPRE values.
!         For IERR <= 2, RGWK(1) = RHOL, which is the norm on the
!         left-hand-side of the relevant stopping test defined
!         below associated with the residual for the current
!         approximation X(L).
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration,
!         if this is desired for monitoring convergence.  If unit
!         number is 0, no writing will occur.
! RWORK  :WORK    Double Precision RWORK(LENW).
!         Double Precision array of size LENW.
! LENW   :IN       Integer.
!         Length of the double precision workspace, RWORK.
!         LENW >= 1 + N*(NSAVE+7) + NSAVE*(NSAVE+3).
!         For the recommended values of NSAVE (10), RWORK has size at
!         least 131 + 17*N.
! IWORK  :INOUT    Integer IWORK(USER DEFINED >= 30).
!         Used to hold pointers into the RWORK array.
!         Upon return the following locations of IWORK hold information
!         which may be of use to the user:
!         IWORK(9)  Amount of Integer workspace actually used.
!         IWORK(10) Amount of Double Precision workspace actually used.
! LENIW  :IN       Integer.
!         Length of the integer workspace IWORK.  LENIW >= 30.
!
! *Description:
!       DSDGMR solves a linear system A*X = B rewritten in the form:
!
!        (SB*A*(M-inverse)*(SX-inverse))*(SX*M*X) = SB*B,
!
!       with right preconditioning, or
!
!        (SB*(M-inverse)*A*(SX-inverse))*(SX*X) = SB*(M-inverse)*B,
!
!       with left preconditioning, where a is an n-by-n double
!       precision matrix,
!       X and  B  are N-vectors,  SB and  SX  are  diagonal  scaling
!       matrices, and  M   is   the  diagonal  of   A.     It   uses
!       preconditioned   Krylov  subpace   methods  based    on  the
!       generalized  minimum residual method (GMRES).   This routine
!       is  a  driver routine  which   assumes a  SLAP matrix   data
!       structure  and   sets  up the  necessary information   to do
!       diagonal preconditioning and  calls  the main GMRES  routine
!       DGMRES   for  the  solution  of the   linear system.  DGMRES
!       optionally   performs   either the   full  orthogonalization
!       version of the GMRES algorithm or an  incomplete  variant of
!       it.  Both versions use restarting of the linear iteration by
!       default, although the user can disable this feature.
!
!       The GMRES  algorithm generates a sequence  of approximations
!       X(L) to the  true solution of the above  linear system.  The
!       convergence criteria for stopping the  iteration is based on
!       the size  of the  scaled norm of  the residual  R(L)  =  B -
!       A*X(L).  The actual stopping test is either:
!
!               norm(SB*(B-A*X(L))) .le. TOL*norm(SB*B),
!
!       for right preconditioning, or
!
!               norm(SB*(M-inverse)*(B-A*X(L))) .le.
!                       TOL*norm(SB*(M-inverse)*B),
!
!       for left preconditioning, where norm() denotes the euclidean
!       norm, and TOL is  a positive scalar less  than one  input by
!       the user.  If TOL equals zero  when DSDGMR is called, then a
!       default  value  of 500*(the   smallest  positive  magnitude,
!       machine epsilon) is used.  If the  scaling arrays SB  and SX
!       are used, then  ideally they  should be chosen  so  that the
!       vectors SX*X(or SX*M*X) and  SB*B have all their  components
!       approximately equal  to  one in  magnitude.  If one wants to
!       use the same scaling in X  and B, then  SB and SX can be the
!       same array in the calling program.
!
!       The following is a list of the other routines and their
!       functions used by GMRES:
!       DGMRES  Contains the matrix structure independent driver
!               routine for GMRES.
!       DPIGMR  Contains the main iteration loop for GMRES.
!       DORTH   Orthogonalizes a new vector against older basis vects.
!       DHEQR   Computes a QR decomposition of a Hessenberg matrix.
!       DHELS   Solves a Hessenberg least-squares system, using QR
!               factors.
!       RLCALC  Computes the scaled residual RL.
!       XLCALC  Computes the solution XL.
!       ISDGMR  User-replaceable stopping routine.
!
!       The Sparse Linear Algebra Package (SLAP) utilizes two matrix
!       data structures: 1) the  SLAP Triad  format or  2)  the SLAP
!       Column format.  The user can hand this routine either of the
!       of these data structures and SLAP  will figure out  which on
!       is being used and act accordingly.
!
!       =================== S L A P Triad format ===================
!       This routine requires that the  matrix A be   stored in  the
!       SLAP  Triad format.  In  this format only the non-zeros  are
!       stored.  They may appear in  *ANY* order.  The user supplies
!       three arrays of  length NELT, where  NELT is  the number  of
!       non-zeros in the matrix: (IA(NELT), JA(NELT), A(NELT)).  For
!       each non-zero the user puts the row and column index of that
!       matrix element  in the IA and  JA arrays.  The  value of the
!       non-zero   matrix  element is  placed  in  the corresponding
!       location of the A array.   This is  an  extremely  easy data
!       structure to generate.  On  the  other hand it   is  not too
!       efficient on vector computers for  the iterative solution of
!       linear systems.  Hence,   SLAP changes   this  input    data
!       structure to the SLAP Column format  for  the iteration (but
!       does not change it back).
!
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix       SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a
!       column):
!
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
! *Precision:           Double Precision
! *Side Effects:
!       The SLAP Triad format (IA, JA, A) is modified internally to be
!       the SLAP Column format.  See above.
!***REFERENCES  1. Peter N. Brown and A. C. Hindmarsh,
!                 "Reduced Storage Matrix Methods In Stiff ODE
!                 Systems," LLNL report UCRL-95088, Rev. 1,
!                 June 1987.
!***ROUTINES CALLED  DS2Y, DCHKW, DSDS, DGMRES
!***END PROLOGUE  DSDGMR
!         The following is for optimized compilation on LLNL/LTSS Crays.
!LLL. OPTIMIZE
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER  N, NELT, IA(NELT), JA(NELT), ISYM, NSAVE, ITOL
      INTEGER  ITMAX, ITER, IERR, IUNIT, LENW, LENIW, IWORK(LENIW)
      DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
      EXTERNAL DSMV, DSDI
      PARAMETER (LOCRB=1, LOCIB=11)
!
!         Change the SLAP input matrix IA, JA, A to SLAP-Column format.
!***FIRST EXECUTABLE STATEMENT  DSDGMR
      IERR = 0
      ERR  = 0.0
      IF( NSAVE.LE.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, IA, JA, A, ISYM )
!
!         Set up the workspace.  We assume MAXL=KMP=NSAVE.
!         Compute the inverse of the diagonal of the matrix.
      LOCIGW = LOCIB
      LOCIW = LOCIGW + 20
!
      LOCDIN = LOCRB
      LOCRGW = LOCDIN + N
      LOCW = LOCRGW + 1+N*(NSAVE+6)+NSAVE*(NSAVE+3)
!
      IWORK(4) = LOCDIN
      IWORK(9) = LOCIW
      IWORK(10) = LOCW
!
!         Check the workspace allocations.
      CALL DCHKW( 'DSDGMR', LOCIW, LENIW, LOCW, LENW, IERR, ITER, ERR )
      IF( IERR.NE.0 ) RETURN
!
      CALL DSDS(N, NELT, IA, JA, A, ISYM, RWORK(LOCDIN))
!
!         Perform the Diagonaly Scaled Generalized Minimum
!         Residual iteration algorithm.  The following DGMRES
!         defaults are used MAXL = KMP = NSAVE, JSCAL = 0,
!         JPRE = -1, NRMAX = ITMAX/NSAVE
      IWORK(LOCIGW  ) = NSAVE
      IWORK(LOCIGW+1) = NSAVE
      IWORK(LOCIGW+2) = 0
      IWORK(LOCIGW+3) = -1
      IWORK(LOCIGW+4) = ITMAX/NSAVE
      MYITOL = 0
!
      CALL DGMRES( N, B, X, NELT, IA, JA, A, ISYM, DSMV, DSDI, &
           MYITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK, RWORK, &
           RWORK(LOCRGW), LENW-LOCRGW, IWORK(LOCIGW), 20, &
           RWORK, IWORK )
!
      IF( ITER.GT.ITMAX ) IERR = 2
      RETURN
!------------- LAST LINE OF DSDGMR FOLLOWS ----------------------------
      END
      SUBROUTINE DGMRES(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MSOLVE, &
           ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, SB, SX, &
           RGWK, LRGW, IGWK, LIGW, RWORK, IWORK )
!***BEGIN PROLOGUE  DGMRES
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DGMRES-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Generalized Minimum Residual
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE  Preconditioned GMRES iterative sparse Ax=b solver.
!            This routine uses the generalized minimum residual
!            (GMRES) method with preconditioning to solve
!            non-symmetric linear systems of the form: A*x = b.
!***DESCRIPTION
! *Usage:
!      INTEGER   N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
!      INTEGER   IERR, IUNIT, LRGW, LIGW, IGWK(LIGW)
!      INTEGER   IWORK(USER DEFINED)
!      DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, SB(N), SX(N)
!      DOUBLE PRECISION RGWK(LRGW), RWORK(USER DEFINED)
!      EXTERNAL  MATVEC, MSOLVE
!
!      CALL DGMRES(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MSOLVE,
!     $     ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, SB, SX,
!     $     RGWK, LRGW, IGWK, LIGW, RWORK, IWORK)
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :INOUT    Double Precision X(N).
!         On input X is your initial guess for the solution vector.
!         On output X is the final approximate solution.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(NELT).
! A      :IN       Double Precision A(NELT).
!         These arrays contain the matrix data structure for A.
!         It could take any form.  See "Description", below
!         for more late breaking details...
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! MATVEC :EXT      External.
!         Name of a routine which performs the matrix vector multiply
!         Y = A*X given A and X.  The name of the MATVEC routine must
!         be declared external in the calling program.  The calling
!         sequence to MATVEC is:
!             CALL MATVEC( N, X, Y, NELT, IA, JA, A, ISYM )
!         where N is the number of unknowns, Y is the product A*X
!         upon return, X is an input vector, and NELT is the number of
!         non-zeros in the SLAP IA, JA, A storage for the matrix A.
!         ISYM is a flag which, if non-zero, denotes that A is
!         symmetric and only the lower or upper triangle is stored.
! MSOLVE :EXT      External.
!         Name of the routine which solves a linear system Mz = r for
!         z given r with the preconditioning matrix M (M is supplied via
!         RWORK and IWORK arrays.  The name of the MSOLVE routine must
!         be declared external in the calling program.  The calling
!         sequence to MSLOVE is:
!             CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         Where N is the number of unknowns, R is the right-hand side
!         vector, and z is the solution upon return.  RWORK is a
!         double precision
!         array that can be used to pass necessary preconditioning
!         information and/or workspace to MSOLVE.  IWORK is an integer
!         work array for the same purpose as RWORK.
! ITOL   :IN       Integer.
!         Flag to indicate the type of convergence criterion used.
!         ITOL=0  Means the  iteration stops when the test described
!                 below on  the  residual RL  is satisfied.  This is
!                 the  "Natural Stopping Criteria" for this routine.
!                 Other values  of   ITOL  cause  extra,   otherwise
!                 unnecessary, computation per iteration and     are
!                 therefore  much less  efficient.  See  ISDGMR (the
!                 stop test routine) for more information.
!         ITOL=1  Means   the  iteration stops   when the first test
!                 described below on  the residual RL  is satisfied,
!                 and there  is either right  or  no preconditioning
!                 being used.
!         ITOL=2  Implies     that   the  user    is   using    left
!                 preconditioning, and the second stopping criterion
!                 below is used.
!         ITOL=3  Means the  iteration stops   when  the  third test
!                 described below on Minv*Residual is satisfied, and
!                 there is either left  or no  preconditioning begin
!                 used.
!         ITOL=11 is    often  useful  for   checking  and comparing
!                 different routines.  For this case, the  user must
!                 supply  the  "exact" solution or  a  very accurate
!                 approximation (one with  an  error much less  than
!                 TOL) through a common block,
!                     COMMON /SOLBLK/ SOLN(1)
!                 if ITOL=11, iteration stops when the 2-norm of the
!                 difference between the iterative approximation and
!                 the user-supplied solution  divided by the  2-norm
!                 of the  user-supplied solution  is  less than TOL.
!                 Note that this requires  the  user to  set up  the
!                 "COMMON     /SOLBLK/ SOLN(LENGTH)"  in the calling
!                 routine.  The routine with this declaration should
!                 be loaded before the stop test so that the correct
!                 length is used by  the loader.  This procedure  is
!                 not standard Fortran and may not work correctly on
!                 your   system (although  it  has  worked  on every
!                 system the authors have tried).  If ITOL is not 11
!                 then this common block is indeed standard Fortran.
! TOL    :INOUT    Double Precision.
!         Convergence criterion, as described below.  If TOL is set
!         to zero on input, then a default value of 500*(the smallest
!         positive magnitude, machine epsilon) is used.
! ITMAX  :DUMMY    Integer.
!         Maximum number of iterations in most SLAP routines.  In
!         this routine this does not make sense.  The maximum number
!         of iterations here is given by ITMAX = MAXL*(NRMAX+1).
!         See IGWK for definitions of MAXL and NRMAX.
! ITER   :OUT      Integer.
!         Number of iterations required to reach convergence, or
!         ITMAX if convergence criterion could not be achieved in
!         ITMAX iterations.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as
!         defined by ITOL.  Letting norm() denote the Euclidean
!         norm, ERR is defined as follows..
!
!         If ITOL=0, then ERR = norm(SB*(B-A*X(L)))/norm(SB*B),
!                               for right or no preconditioning, and
!                         ERR = norm(SB*(M-inverse)*(B-A*X(L)))/
!                                norm(SB*(M-inverse)*B),
!                               for left preconditioning.
!         If ITOL=1, then ERR = norm(SB*(B-A*X(L)))/norm(SB*B),
!                               since right or no preconditioning
!                               being used.
!         If ITOL=2, then ERR = norm(SB*(M-inverse)*(B-A*X(L)))/
!                                norm(SB*(M-inverse)*B),
!                               since left preconditioning is being
!                               used.
!         If ITOL=3, then ERR =  Max  |(Minv*(B-A*X(L)))(i)/x(i)|
!                               i=1,n
!         If ITOL=11, then ERR = norm(SB*(X(L)-SOLN))/norm(SB*SOLN).
! IERR   :OUT      Integer.
!         Return error flag.
!               IERR = 0 => All went well.
!               IERR = 1 => Insufficient storage allocated for
!                           RGWK or IGWK.
!               IERR = 2 => Routine Dgmres failed to reduce the norm
!                           of the current residual on its last call,
!                           and so the iteration has stalled.  In
!                           this case, X equals the last computed
!                           approximation.  The user must either
!                           increase MAXL, or choose a different
!                           initial guess.
!               IERR =-1 => Insufficient length for RGWK array.
!                           IGWK(6) contains the required minimum
!                           length of the RGWK array.
!               IERR =-2 => Inconsistent ITOL and JPRE values.
!         For IERR <= 2, RGWK(1) = RHOL, which is the norm on the
!         left-hand-side of the relevant stopping test defined
!         below associated with the residual for the current
!         approximation X(L).
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration,
!         if this is desired for monitoring convergence.  If unit
!         number is 0, no writing will occur.
! SB     :IN       Double Precision SB(N).
!         Array of length N containing scale factors for the right
!         hand side vector B.  If JSCAL.eq.0 (see below), SB need
!         not be supplied.
! SX     :IN       Double Precision SX(N).
!         Array of length N containing scale factors for the solution
!         vector X.  If JSCAL.eq.0 (see below), SX need not be
!         supplied.  SB and SX can be the same array in the calling
!         program if desired.
! RGWK   :INOUT    Double Precision RGWK(LRGW).
!         Double Precision array of size at least
!         1 + N*(MAXL+6) + MAXL*(MAXL+3)
!         used for work space by DGMRES.  See below for definition of
!         MAXL.
!         On return, RGWK(1) = RHOL.  See IERR for definition of RHOL.
! LRGW   :IN       Integer.
!         Length of the double precision workspace, RGWK.
!         LRGW > 1 + N*(MAXL+6) + MAXL*(MAXL+3).
!         For the default values, RGWK has size at least 131 + 16*N.
! IGWK   :INOUT    Integer IGWK(LIGW).
!         The following IGWK parameters should be set by the user
!         before calling this routine.
!         IGWK(1) = MAXL.  Maximum dimension of Krylov subspace in
!            which X - X0 is to be found (where, X0 is the initial
!            guess).  The default value of MAXL is 10.
!         IGWK(2) = KMP.  Maximum number of previous Krylov basis
!            vectors to which each new basis vector is made orthogonal.
!            The default value of KMP is MAXL.
!         IGWK(3) = JSCAL.  Flag indicating whether the scaling
!            arrays SB and SX are to be used.
!            JSCAL = 0 => SB and SX are not used and the algorithm
!               will perform as if all SB(I) = 1 and SX(I) = 1.
!            JSCAL = 1 =>  Only SX is used, and the algorithm
!               performs as if all SB(I) = 1.
!            JSCAL = 2 =>  Only SB is used, and the algorithm
!               performs as if all SX(I) = 1.
!            JSCAL = 3 =>  Both SB and SX are used.
!         IGWK(4) = JPRE.  Flag indicating whether preconditioning
!            is being used.
!            JPRE = 0  =>  There is no preconditioning.
!            JPRE > 0  =>  There is preconditioning on the right
!               only, and the solver will call routine MSOLVE.
!            JPRE < 0  =>  There is preconditioning on the left
!               only, and the solver will call routine MSOLVE.
!         IGWK(5) = NRMAX.  Maximum number of restarts of the
!            Krylov iteration.  The default value of NRMAX = 10.
!            if IWORK(5) = -1,  then no restarts are performed (in
!            this case, NRMAX is set to zero internally).
!         The following IWORK parameters are diagnostic information
!         made available to the user after this routine completes.
!         IGWK(6) = MLWK.  Required minimum length of RGWK array.
!         IGWK(7) = NMS.  The total number of calls to MSOLVE.
! LIGW   :IN       Integer.
!         Length of the integer workspace, IGWK.  LIGW >= 20.
!
! *Description:
!       DGMRES solves a linear system A*X = B rewritten in the form:
!
!        (SB*A*(M-inverse)*(SX-inverse))*(SX*M*X) = SB*B,
!
!       with right preconditioning, or
!
!        (SB*(M-inverse)*A*(SX-inverse))*(SX*X) = SB*(M-inverse)*B,
!
!       with left preconditioning, where A is an N-by-N double
!       precision matrix,
!       X  and  B are N-vectors,   SB and SX   are  diagonal scaling
!       matrices,   and M is  a preconditioning    matrix.   It uses
!       preconditioned  Krylov   subpace  methods  based     on  the
!       generalized minimum residual  method (GMRES).   This routine
!       optionally performs  either  the  full     orthogonalization
!       version of the  GMRES  algorithm or an incomplete variant of
!       it.  Both versions use restarting of the linear iteration by
!       default, although the user can disable this feature.
!
!       The GMRES  algorithm generates a sequence  of approximations
!       X(L) to the  true solution of the above  linear system.  The
!       convergence criteria for stopping the  iteration is based on
!       the size  of the  scaled norm of  the residual  R(L)  =  B -
!       A*X(L).  The actual stopping test is either:
!
!               norm(SB*(B-A*X(L))) .le. TOL*norm(SB*B),
!
!       for right preconditioning, or
!
!               norm(SB*(M-inverse)*(B-A*X(L))) .le.
!                       TOL*norm(SB*(M-inverse)*B),
!
!       for left preconditioning, where norm() denotes the euclidean
!       norm, and TOL is  a positive scalar less  than one  input by
!       the user.  If TOL equals zero  when DGMRES is called, then a
!       default  value  of 500*(the   smallest  positive  magnitude,
!       machine epsilon) is used.  If the  scaling arrays SB  and SX
!       are used, then  ideally they  should be chosen  so  that the
!       vectors SX*X(or SX*M*X) and  SB*B have all their  components
!       approximately equal  to  one in  magnitude.  If one wants to
!       use the same scaling in X  and B, then  SB and SX can be the
!       same array in the calling program.
!
!       The following is a list of the other routines and their
!       functions used by DGMRES:
!       DPIGMR  Contains the main iteration loop for GMRES.
!       DORTH   Orthogonalizes a new vector against older basis vects.
!       DHEQR   Computes a QR decomposition of a Hessenberg matrix.
!       DHELS   Solves a Hessenberg least-squares system, using QR
!               factors.
!       DRLCAL  Computes the scaled residual RL.
!       DXLCAL  Computes the solution XL.
!       ISDGMR  User-replaceable stopping routine.
!
!       This routine does  not care  what matrix data   structure is
!       used for  A and M.  It simply   calls  the MATVEC and MSOLVE
!       routines, with  the arguments as  described above.  The user
!       could write any type of structure and the appropriate MATVEC
!       and MSOLVE routines.  It is assumed  that A is stored in the
!       IA, JA, A  arrays in some fashion and  that M (or INV(M)) is
!       stored  in  IWORK  and  RWORK   in  some fashion.   The SLAP
!       routines DSDCG and DSICCG are examples of this procedure.
!
!       Two  examples  of  matrix  data structures  are the: 1) SLAP
!       Triad  format and 2) SLAP Column format.
!
!       =================== S L A P Triad format ===================
!       This routine requires that the  matrix A be   stored in  the
!       SLAP  Triad format.  In  this format only the non-zeros  are
!       stored.  They may appear in  *ANY* order.  The user supplies
!       three arrays of  length NELT, where  NELT is  the number  of
!       non-zeros in the matrix: (IA(NELT), JA(NELT), A(NELT)).  For
!       each non-zero the user puts the row and column index of that
!       matrix element  in the IA and  JA arrays.  The  value of the
!       non-zero   matrix  element is  placed  in  the corresponding
!       location of the A array.   This is  an  extremely  easy data
!       structure to generate.  On  the  other hand it   is  not too
!       efficient on vector computers for  the iterative solution of
!       linear systems.  Hence,   SLAP changes   this  input    data
!       structure to the SLAP Column format  for  the iteration (but
!       does not change it back).
!
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix       SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a
!       column):
!
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
! *Precision:           Double Precision
!***REFERENCES  1. Peter N. Brown and A. C. Hindmarsh,
!                 "Reduced Storage Matrix Methods In Stiff ODE
!                 Systems," LLNL report UCRL-95088, Rev. 1,
!                 June 1987.
!***ROUTINES CALLED  DPIGMR, DORTH, DHEQR, DHELS, DRCAL, DXLCAL,
!                    ISDGMR, DNRM2, DDOT, DAXPY, DSCAL, IDAMAX, D1MACH.
!***END PROLOGUE  DGMRES
!         The following is for optimized compilation on LLNL/LTSS Crays.
!LLL. OPTIMIZE
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER  N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER  IERR, IUNIT, LRGW, LIGW, IGWK(LIGW)
      INTEGER  IWORK(*)
      DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, SB(N), SX(N)
      DOUBLE PRECISION RGWK(LRGW), RWORK(*)
      EXTERNAL MATVEC, MSOLVE, D1MACH
      INTEGER JPRE, KMP, MAXL, NMS, MAXLP1, NMSL, NRSTS, NRMAX
      INTEGER I, IFLAG, LR, LDL, LHES, LGMR, LQ, LV, LW
      DOUBLE PRECISION BNRM, RHOL, SUM
!
!***FIRST EXECUTABLE STATEMENT  DGMRES
      IERR = 0
!   ------------------------------------------------------------------
!         Load method parameters with user values or defaults.
!   ------------------------------------------------------------------
      MAXL = IGWK(1)
      IF (MAXL .EQ. 0) MAXL = 10
      IF (MAXL .GT. N) MAXL = N
      KMP = IGWK(2)
      IF (KMP .EQ. 0) KMP = MAXL
      IF (KMP .GT. MAXL) KMP = MAXL
      JSCAL = IGWK(3)
      JPRE = IGWK(4)
!         Check for consistent values of ITOL and JPRE.
      IF( ITOL.EQ.1 .AND. JPRE.LT.0 ) GOTO 650
      IF( ITOL.EQ.2 .AND. JPRE.GE.0 ) GOTO 650
      NRMAX = IGWK(5)
      IF( NRMAX.EQ.0 ) NRMAX = 10
!         If NRMAX .eq. -1, then set NRMAX = 0 to turn off restarting.
      IF( NRMAX.EQ.-1 ) NRMAX = 0
!         If input value of TOL is zero, set it to its default value.
      IF( TOL.EQ.0.0D0 ) TOL = 500.0*D1MACH(3)
!
!         Initialize counters.
      ITER = 0
      NMS = 0
      NRSTS = 0
!   ------------------------------------------------------------------
!         Form work array segment pointers.
!   ------------------------------------------------------------------
      MAXLP1 = MAXL + 1
      LV = 1
      LR = LV + N*MAXLP1
      LHES = LR + N + 1
      LQ = LHES + MAXL*MAXLP1
      LDL = LQ + 2*MAXL
      LW = LDL + N
      LXL = LW + N
      LZ = LXL + N
!
!         Load igwk(6) with required minimum length of the rgwk array.
      IGWK(6) = LZ + N - 1
      IF( LZ+N-1.GT.LRGW ) GOTO 640
!   ------------------------------------------------------------------
!         Calculate scaled-preconditioned norm of RHS vector b.
!   ------------------------------------------------------------------
      IF (JPRE .LT. 0) THEN
         CALL MSOLVE(N, B, RGWK(LR), NELT, IA, JA, A, ISYM, &
              RWORK, IWORK)
         NMS = NMS + 1
      ELSE
         CALL DCOPY(N, B, 1, RGWK(LR), 1)
      ENDIF
      IF( JSCAL.EQ.2 .OR. JSCAL.EQ.3 ) THEN
         SUM = 0.D0
         DO I = 1,N
            SUM = SUM + (RGWK(LR-1+I)*SB(I))**2
         end do
         BNRM = DSQRT(SUM)
      ELSE
         BNRM = DNRM2(N,RGWK(LR),1)
      ENDIF
!   ------------------------------------------------------------------
!         Calculate initial residual.
!   ------------------------------------------------------------------
      CALL MATVEC(N, X, RGWK(LR), NELT, IA, JA, A, ISYM)
      DO 50 I = 1,N
         RGWK(LR-1+I) = B(I) - RGWK(LR-1+I)
 50   CONTINUE
!   ------------------------------------------------------------------
!         If performing restarting, then load the residual into the
!         correct location in the Rgwk array.
!   ------------------------------------------------------------------
 100  CONTINUE
      IF( NRSTS.GT.NRMAX ) GOTO 610
      IF( NRSTS.GT.0 ) THEN
!         Copy the curr residual to different loc in the Rgwk array.
         CALL DCOPY(N, RGWK(LDL), 1, RGWK(LR), 1)
      ENDIF
!   ------------------------------------------------------------------
!         Use the DPIGMR algorithm to solve the linear system A*Z = R.
!   ------------------------------------------------------------------
      CALL DPIGMR(N, RGWK(LR), SB, SX, JSCAL, MAXL, MAXLP1, KMP, &
             NRSTS, JPRE, MATVEC, MSOLVE, NMSL, RGWK(LZ), RGWK(LV), &
             RGWK(LHES), RGWK(LQ), LGMR, RWORK, IWORK, RGWK(LW), &
             RGWK(LDL), RHOL, NRMAX, B, BNRM, X, RGWK(LXL), ITOL, &
             TOL, NELT, IA, JA, A, ISYM, IUNIT, IFLAG, ERR)
      ITER = ITER + LGMR
      NMS = NMS + NMSL
!
!         Increment X by the current approximate solution Z of A*Z = R.
!
      LZM1 = LZ - 1
      DO 110 I = 1,N
         X(I) = X(I) + RGWK(LZM1+I)
 110  CONTINUE
      IF( IFLAG.EQ.0 ) GOTO 600
      IF( IFLAG.EQ.1 ) THEN
         NRSTS = NRSTS + 1
         GOTO 100
      ENDIF
      IF( IFLAG.EQ.2 ) GOTO 620
!   ------------------------------------------------------------------
!         All returns are made through this section.
!   ------------------------------------------------------------------
!         The iteration has converged.
!
 600  CONTINUE
      IGWK(7) = NMS
      RGWK(1) = RHOL
      IERR = 0
      RETURN
!
!         Max number((NRMAX+1)*MAXL) of linear iterations performed.
 610  CONTINUE
      IGWK(7) = NMS
      RGWK(1) = RHOL
      IERR = 1
      RETURN
!
!         GMRES failed to reduce last residual in MAXL iterations.
!         The iteration has stalled.
 620  CONTINUE
      IGWK(7) = NMS
      RGWK(1) = RHOL
      IERR = 2
      RETURN
!         Error return.  Insufficient length for Rgwk array.
 640  CONTINUE
      ERR = TOL
      IERR = -1
      RETURN
!         Error return.  Inconsistent ITOL and JPRE values.
 650  CONTINUE
      ERR = TOL
      IERR = -2
      RETURN
!------------- LAST LINE OF DGMRES FOLLOWS ----------------------------
      END
      SUBROUTINE DAXPY(N,DA,DX,INCX,DY,INCY)
!
!     OVERWRITE DOUBLE PRECISION DY WITH DOUBLE PRECISION DA*DX + DY.
!     FOR I = 0 TO N-1, REPLACE  DY(LY+I*INCY) WITH DA*DX(LX+I*INCX) +
!       DY(LY+I*INCY), WHERE LX = 1 IF INCX .GE. 0, ELSE LX = (-INCX)*N,
!       AND LY IS DEFINED IN A SIMILAR WAY USING INCY.
!
      DOUBLE PRECISION DX(1),DY(1),DA
      IF(N.LE.0.OR.DA.EQ.0.D0) RETURN
      IF(INCX.EQ.INCY) IF(INCX-1) 5,20,60
    5 CONTINUE
!
!        CODE FOR NONEQUAL OR NONPOSITIVE INCREMENTS.
!
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO I = 1,N
        DY(IY) = DY(IY) + DA*DX(IX)
        IX = IX + INCX
        IY = IY + INCY
      end do

      RETURN
!
!        CODE FOR BOTH INCREMENTS EQUAL TO 1
!
!
!        CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 4.
!
   20 M = MOD(N,4)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DY(I) = DY(I) + DA*DX(I)
   30 CONTINUE
      IF( N .LT. 4 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,4
        DY(I) = DY(I) + DA*DX(I)
        DY(I + 1) = DY(I + 1) + DA*DX(I + 1)
        DY(I + 2) = DY(I + 2) + DA*DX(I + 2)
        DY(I + 3) = DY(I + 3) + DA*DX(I + 3)
   50 CONTINUE
      RETURN
!
!        CODE FOR EQUAL, POSITIVE, NONUNIT INCREMENTS.
!
   60 CONTINUE
      NS = N*INCX
          DO 70 I=1,NS,INCX
          DY(I) = DA*DX(I) + DY(I)
   70     CONTINUE
      RETURN
      END
      SUBROUTINE DCOPY(N,DX,INCX,DY,INCY)
!
!     COPY DOUBLE PRECISION DX TO DOUBLE PRECISION DY.
!     FOR I = 0 TO N-1, COPY DX(LX+I*INCX) TO DY(LY+I*INCY),
!     WHERE LX = 1 IF INCX .GE. 0, ELSE LX = (-INCX)*N, AND LY IS
!     DEFINED IN A SIMILAR WAY USING INCY.
!
      DOUBLE PRECISION DX(1),DY(1)
      IF(N.LE.0)RETURN
      IF(INCX.EQ.INCY) IF(INCX-1) 5,20,60
    5 CONTINUE
!
!        CODE FOR UNEQUAL OR NONPOSITIVE INCREMENTS.
!
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO I = 1,N
        DY(IY) = DX(IX)
        IX = IX + INCX
        IY = IY + INCY
      end do

      RETURN
!
!        CODE FOR BOTH INCREMENTS EQUAL TO 1
!
!
!        CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 7.
!
   20 M = MOD(N,7)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DY(I) = DX(I)
   30 CONTINUE
      IF( N .LT. 7 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,7
        DY(I) = DX(I)
        DY(I + 1) = DX(I + 1)
        DY(I + 2) = DX(I + 2)
        DY(I + 3) = DX(I + 3)
        DY(I + 4) = DX(I + 4)
        DY(I + 5) = DX(I + 5)
        DY(I + 6) = DX(I + 6)
   50 CONTINUE
      RETURN
!
!        CODE FOR EQUAL, POSITIVE, NONUNIT INCREMENTS.
!
   60 CONTINUE
      NS=N*INCX
          DO 70 I=1,NS,INCX
          DY(I) = DX(I)
   70     CONTINUE
      RETURN
      END
      DOUBLE PRECISION FUNCTION DDOT(N,DX,INCX,DY,INCY)
!
!     RETURNS THE DOT PRODUCT OF DOUBLE PRECISION DX AND DY.
!     DDOT = SUM FOR I = 0 TO N-1 OF  DX(LX+I*INCX) * DY(LY+I*INCY)
!     WHERE LX = 1 IF INCX .GE. 0, ELSE LX = (-INCX)*N, AND LY IS
!     DEFINED IN A SIMILAR WAY USING INCY.
!
      DOUBLE PRECISION DX(1),DY(1)
      DDOT = 0.D0
      IF(N.LE.0)RETURN
      IF(INCX.EQ.INCY) IF(INCX-1) 5,20,60
    5 CONTINUE
!
!         CODE FOR UNEQUAL OR NONPOSITIVE INCREMENTS.
!
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO I = 1,N
         DDOT = DDOT + DX(IX)*DY(IY)
        IX = IX + INCX
        IY = IY + INCY
      end do

      RETURN
!
!        CODE FOR BOTH INCREMENTS EQUAL TO 1.
!
!
!        CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 5.
!
   20 M = MOD(N,5)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
         DDOT = DDOT + DX(I)*DY(I)
   30 CONTINUE
      IF( N .LT. 5 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,5
         DDOT = DDOT + DX(I)*DY(I) + DX(I+1)*DY(I+1) + &
         DX(I + 2)*DY(I + 2) + DX(I + 3)*DY(I + 3) + DX(I + 4)*DY(I + 4)
   50 CONTINUE
      RETURN
!
!         CODE FOR POSITIVE EQUAL INCREMENTS .NE.1.
!
   60 CONTINUE
      NS = N*INCX
          DO 70 I=1,NS,INCX
          DDOT = DDOT + DX(I)*DY(I)
   70     CONTINUE
      RETURN
      END
      DOUBLE PRECISION FUNCTION DNRM2 ( N, DX, INCX)
      INTEGER          NEXT
      DOUBLE PRECISION   DX(1), CUTLO, CUTHI, HITEST, SUM, XMAX,ZERO,ONE
      DATA   ZERO, ONE /0.0D0, 1.0D0/
!
!     EUCLIDEAN NORM OF THE N-VECTOR STORED IN DX() WITH STORAGE
!     INCREMENT INCX .
!     IF    N .LE. 0 RETURN WITH RESULT = 0.
!     IF N .GE. 1 THEN INCX MUST BE .GE. 1
!
!           C.L.LAWSON, 1978 JAN 08
!
!     FOUR PHASE METHOD     USING TWO BUILT-IN CONSTANTS THAT ARE
!     HOPEFULLY APPLICABLE TO ALL MACHINES.
!         CUTLO = MAXIMUM OF  DSQRT(U/EPS)  OVER ALL KNOWN MACHINES.
!         CUTHI = MINIMUM OF  DSQRT(V)      OVER ALL KNOWN MACHINES.
!     WHERE
!         EPS = SMALLEST NO. SUCH THAT EPS + 1. .GT. 1.
!         U   = SMALLEST POSITIVE NO.   (UNDERFLOW LIMIT)
!         V   = LARGEST  NO.            (OVERFLOW  LIMIT)
!
!     BRIEF OUTLINE OF ALGORITHM..
!
!     PHASE 1    SCANS ZERO COMPONENTS.
!     MOVE TO PHASE 2 WHEN A COMPONENT IS NONZERO AND .LE. CUTLO
!     MOVE TO PHASE 3 WHEN A COMPONENT IS .GT. CUTLO
!     MOVE TO PHASE 4 WHEN A COMPONENT IS .GE. CUTHI/M
!     WHERE M = N FOR X() REAL AND M = 2*N FOR COMPLEX.
!
!     VALUES FOR CUTLO AND CUTHI..
!     FROM THE ENVIRONMENTAL PARAMETERS LISTED IN THE IMSL CONVERTER
!     DOCUMENT THE LIMITING VALUES ARE AS FOLLOWS..
!     CUTLO, S.P.   U/EPS = 2**(-102) FOR  HONEYWELL.  CLOSE SECONDS ARE
!                   UNIVAC AND DEC AT 2**(-103)
!                   THUS CUTLO = 2**(-51) = 4.44089E-16
!     CUTHI, S.P.   V = 2**127 FOR UNIVAC, HONEYWELL, AND DEC.
!                   THUS CUTHI = 2**(63.5) = 1.30438E19
!     CUTLO, D.P.   U/EPS = 2**(-67) FOR HONEYWELL AND DEC.
!                   THUS CUTLO = 2**(-33.5) = 8.23181D-11
!     CUTHI, D.P.   SAME AS S.P.  CUTHI = 1.30438D19
!     DATA CUTLO, CUTHI / 8.232D-11,  1.304D19 /
!     DATA CUTLO, CUTHI / 4.441E-16,  1.304E19 /
      DATA CUTLO, CUTHI / 8.232D-11,  1.304D19 /
!
      IF(N .GT. 0) GO TO 10
         DNRM2  = ZERO
         GO TO 300
!
   10 ASSIGN 30 TO NEXT
      SUM = ZERO
      NN = N * INCX
!                                                 BEGIN MAIN LOOP
      I = 1
   20    GO TO NEXT,(30, 50, 70, 110)
   30 IF( DABS(DX(I)) .GT. CUTLO) GO TO 85
      ASSIGN 50 TO NEXT
      XMAX = ZERO
!
!                        PHASE 1.  SUM IS ZERO
!
   50 IF( DX(I) .EQ. ZERO) GO TO 200
      IF( DABS(DX(I)) .GT. CUTLO) GO TO 85
!
!                                PREPARE FOR PHASE 2.
      ASSIGN 70 TO NEXT
      GO TO 105
!
!                                PREPARE FOR PHASE 4.
!
  100 I = J
      ASSIGN 110 TO NEXT
      SUM = (SUM / DX(I)) / DX(I)
  105 XMAX = DABS(DX(I))
      GO TO 115
!
!                   PHASE 2.  SUM IS SMALL.
!                             SCALE TO AVOID DESTRUCTIVE UNDERFLOW.
!
   70 IF( DABS(DX(I)) .GT. CUTLO ) GO TO 75
!
!                     COMMON CODE FOR PHASES 2 AND 4.
!                     IN PHASE 4 SUM IS LARGE.  SCALE TO AVOID OVERFLOW.
!
  110 IF( DABS(DX(I)) .LE. XMAX ) GO TO 115
         SUM = ONE + SUM * (XMAX / DX(I))**2
         XMAX = DABS(DX(I))
         GO TO 200
!
  115 SUM = SUM + (DX(I)/XMAX)**2
      GO TO 200
!
!
!                  PREPARE FOR PHASE 3.
!
   75 SUM = (SUM * XMAX) * XMAX
!
!
!     FOR REAL OR D.P. SET HITEST = CUTHI/N
!     FOR COMPLEX      SET HITEST = CUTHI/(2*N)
!
   85 HITEST = CUTHI/FLOAT( N )
!
!                   PHASE 3.  SUM IS MID-RANGE.  NO SCALING.
!
      DO 95 J =I,NN,INCX
      IF(DABS(DX(J)) .GE. HITEST) GO TO 100
   95    SUM = SUM + DX(J)**2
      DNRM2 = DSQRT( SUM )
      GO TO 300
!
  200 CONTINUE
      I = I + INCX
      IF ( I .LE. NN ) GO TO 20
!
!              END OF MAIN LOOP.
!
!              COMPUTE SQUARE ROOT AND ADJUST FOR SCALING.
!
      DNRM2 = XMAX * DSQRT(SUM)
  300 CONTINUE
      RETURN
      END
!DECK DCHKW
      SUBROUTINE DCHKW( NAME, LOCIW, LENIW, LOCW, LENW, &
           IERR, ITER, ERR )
!***BEGIN PROLOGUE  DCHKW
!***DATE WRITTEN   880225   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  R2
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DCHKW-D),
!             SLAP, Error Checking, Workspace Checking
!***AUTHOR  Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP WORK/IWORK Array Bounds Checker.
!            This routine checks the work array lengths  and  inter-
!            faces to the SLATEC  error  handler  if  a  problem  is
!            found.
!***DESCRIPTION
! *Usage:
!     CHARACTER*(*) NAME
!     INTEGER LOCIW, LENIW, LOCW, LENW, IERR, ITER
!     DOUBLE PRECISION ERR
!
!     CALL DCHKW( NAME, LOCIW, LENIW, LOCW, LENW, IERR, ITER, ERR )
!
! *Arguments:
! NAME   :IN       Character*(*).
!         Name of the calling routine.  This is used in the output
!         message, if an error is detected.
! LOCIW  :IN       Integer.
!         Location of the first free element in the integer workspace
!         array.
! LENIW  :IN       Integer.
!         Length of the integer workspace array.
! LOCW   :IN       Integer.
!         Location of the first free element in the double precision
!         workspace array.
! LENRW  :IN       Integer.
!         Length of the double precision workspace array.
! IERR   :OUT      Integer.
!         Return error flag.
!               IERR = 0 => All went well.
!               IERR = 1 => Insufficient storage allocated for
!                           WORK or IWORK.
! ITER   :OUT      Integer.
!         Set to 0 if an error is detected.
! ERR    :OUT      Double Precision.
!         Set to a very large number if an error is detected.
!
! *Precision:           Double Precision
!
!***REFERENCES  (NONE)
!***ROUTINES CALLED  D1MACH, XERRWV
!***END PROLOGUE  DCHKW
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      CHARACTER*(*) NAME
      CHARACTER*72 MESG
      INTEGER LOCIW, LENIW, LOCW, LENW, IERR, ITER
      DOUBLE PRECISION ERR, D1MACH
      EXTERNAL D1MACH, XERRWV
!
!         Check the Integer workspace situation.
!***FIRST EXECUTABLE STATEMENT  DCHKW
      IERR = 0
      IF( LOCIW.GT.LENIW ) THEN
         IERR = 1
         ITER = 0
         ERR = D1MACH(2)
         MESG = NAME // ': INTEGER work array too short. '// &
              ' IWORK needs i1: have allocated i2.'
         CALL XERRWV( MESG, LEN(MESG), 1, 1, 2, LOCIW, LENIW, &
              0, 0.0, 0.0 )
      ENDIF
!
!         Check the Double Precision workspace situation.
      IF( LOCW.GT.LENW ) THEN
         IERR = 1
         ITER = 0
         ERR = D1MACH(2)
         MESG = NAME // ': DOUBLE PRECISION work array too short. '// &
              ' RWORK needs i1: have allocated i2.'
         CALL XERRWV( MESG, LEN(MESG), 1, 1, 2, LOCW, LENW, &
              0, 0.0, 0.0 )
      ENDIF
      RETURN
!------------- LAST LINE OF DCHKW FOLLOWS ----------------------------
      END
!DECK QS2I1D
      SUBROUTINE QS2I1D( IA, JA, A, N, KFLAG )
!***BEGIN PROLOGUE  QS2I1D
!***DATE WRITTEN   761118   (YYMMDD)
!***REVISION DATE  890125   (YYMMDD)
!***CATEGORY NO.  N6A2A
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=INTEGER(QS2I1D-I),
!             QUICKSORT,DOUBLETON QUICKSORT,SORT,SORTING
!***AUTHOR  Jones, R. E., (SNLA)
!           Kahaner, D. K., (NBS)
!           Seager, M. K., (LLNL) seager@lll-crg.llnl.gov
!           Wisniewski, J. A., (SNLA)
!***PURPOSE  Sort an integer array also moving an integer and DP array
!            This routine sorts the integer  array  IA and makes the
!            same interchanges   in the integer   array  JA  and the
!            double precision array A.  The  array IA may be  sorted
!            in increasing order or decreas- ing  order.  A slightly
!            modified QUICKSORT algorithm is used.
!
!***DESCRIPTION
!     Written by Rondall E Jones
!     Modified by John A. Wisniewski to use the Singleton QUICKSORT
!     algorithm. date 18 November 1976.
!
!     Further modified by David K. Kahaner
!     National Bureau of Standards
!     August, 1981
!
!     Even further modification made to bring the code up to the
!     Fortran 77 level and make it more readable and to carry
!     along one integer array and one double precision array during
!     the sort by
!     Mark K. Seager
!     Lawrence Livermore National Laboratory
!     November, 1987
!     This routine was adapted from the ISORT routine.
!
!     ABSTRACT
!         This routine sorts an integer array IA and makes the same
!         interchanges in the integer array JA and the double precision
!          array A.
!         The array a may be sorted in increasing order or decreasing
!         order.  A slightly modified quicksort algorithm is used.
!
!     DESCRIPTION OF PARAMETERS
!        IA - Integer array of values to be sorted.
!        JA - Integer array to be carried along.
!         A - Double Precision array to be carried along.
!         N - Number of values in integer array IA to be sorted.
!     KFLAG - Control parameter
!           = 1 means sort IA in INCREASING order.
!           =-1 means sort IA in DECREASING order.
!
!***REFERENCES
!     Singleton, R. C., Algorithm 347, "An Efficient Algorithm for
!     Sorting with Minimal Storage", cacm, Vol. 12, No. 3, 1969,
!     Pp. 185-187.
!***ROUTINES CALLED  XERROR
!***END PROLOGUE  QS2I1D
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
!VD$R NOVECTOR
!VD$R NOCONCUR
      DIMENSION IL(21),IU(21)
      INTEGER   IA(N),JA(N),IT,IIT,JT,JJT
      DOUBLE PRECISION A(N), TA, TTA
!
!***FIRST EXECUTABLE STATEMENT  QS2I1D
      NN = N
      IF (NN.LT.1) THEN
         CALL XERROR ( &
         'QS2I1D- the number of values to be sorted was not positive.',59,1,1)
         RETURN
      ENDIF
      IF( N.EQ.1 ) RETURN
      KK = IABS(KFLAG)
      IF ( KK.NE.1 ) THEN
         CALL XERROR ( &
           'QS2I1D- the sort control parameter, k, was not 1 or -1.',55,2,1)
         RETURN
      ENDIF
!
!     Alter array IA to get decreasing order if needed.
!
      IF( KFLAG.LT.1 ) THEN
         DO 20 I=1,NN
            IA(I) = -IA(I)
 20      CONTINUE
      ENDIF
!
!     Sort IA and carry JA and A along.
!     And now...Just a little black magic...
      M = 1
      I = 1
      J = NN
      R = .375
 210  IF( R.LE.0.5898437 ) THEN
         R = R + 3.90625E-2
      ELSE
         R = R-.21875
      ENDIF
 225  K = I
!
!     Select a central element of the array and save it in location
!     it, jt, at.
!
      IJ = I + IDINT( DBLE(J-I)*R )
      IT = IA(IJ)
      JT = JA(IJ)
      TA = A(IJ)
!
!     If first element of array is greater than it, interchange with it.
!
      IF( IA(I).GT.IT ) THEN
         IA(IJ) = IA(I)
         IA(I)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(I)
         JA(I)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(I)
         A(I)   = TA
         TA     = A(IJ)
      ENDIF
      L=J
!
!     If last element of array is less than it, swap with it.
!
      IF( IA(J).LT.IT ) THEN
         IA(IJ) = IA(J)
         IA(J)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(J)
         JA(J)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(J)
         A(J)   = TA
         TA     = A(IJ)
!
!     If first element of array is greater than it, swap with it.
!
         IF ( IA(I).GT.IT ) THEN
            IA(IJ) = IA(I)
            IA(I)  = IT
            IT     = IA(IJ)
            JA(IJ) = JA(I)
            JA(I)  = JT
            JT     = JA(IJ)
            A(IJ)  = A(I)
            A(I)   = TA
            TA     = A(IJ)
         ENDIF
      ENDIF
!
!     Find an element in the second half of the array which is
!     smaller than it.
!
  240 L=L-1
      IF( IA(L).GT.IT ) GO TO 240
!
!     Find an element in the first half of the array which is
!     greater than it.
!
  245 K=K+1
      IF( IA(K).LT.IT ) GO TO 245
!
!     Interchange these elements.
!
      IF( K.LE.L ) THEN
         IIT   = IA(L)
         IA(L) = IA(K)
         IA(K) = IIT
         JJT   = JA(L)
         JA(L) = JA(K)
         JA(K) = JJT
         TTA   = A(L)
         A(L)  = A(K)
         A(K)  = TTA
         GOTO 240
      ENDIF
!
!     Save upper and lower subscripts of the array yet to be sorted.
!
      IF( L-I.GT.J-K ) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 260
!
!     Begin again on another portion of the unsorted array.
!
  255 M = M-1
      IF( M.EQ.0 ) GO TO 300
      I = IL(M)
      J = IU(M)
  260 IF( J-I.GE.1 ) GO TO 225
      IF( I.EQ.J ) GO TO 255
      IF( I.EQ.1 ) GO TO 210
      I = I-1
  265 I = I+1
      IF( I.EQ.J ) GO TO 255
      IT = IA(I+1)
      JT = JA(I+1)
      TA =  A(I+1)
      IF( IA(I).LE.IT ) GO TO 265
      K=I
  270 IA(K+1) = IA(K)
      JA(K+1) = JA(K)
      A(K+1)  =  A(K)
      K = K-1
      IF( IT.LT.IA(K) ) GO TO 270
      IA(K+1) = IT
      JA(K+1) = JT
      A(K+1)  = TA
      GO TO 265
!
!     Clean up, if necessary.
!
  300 IF( KFLAG.LT.1 ) THEN
         DO 310 I=1,NN
            IA(I) = -IA(I)
 310     CONTINUE
      ENDIF
      RETURN
!------------- LAST LINE OF QS2I1D FOLLOWS ----------------------------
      END
!DECK DS2Y
      SUBROUTINE DS2Y(N, NELT, IA, JA, A, ISYM )
!***BEGIN PROLOGUE  DS2Y
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DS2Y-D),
!             Linear system, SLAP Sparse
!***AUTHOR  Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP Triad to SLAP Column Format Converter.
!            Routine to convert from the SLAP Triad to SLAP Column
!            format.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
!     DOUBLE PRECISION A(NELT)
!
!     CALL DS2Y( N, NELT, IA, JA, A, ISYM )
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! NELT   :IN       Integer.
!         Number of non-zeros stored in A.
! IA     :INOUT    Integer IA(NELT).
! JA     :INOUT    Integer JA(NELT).
! A      :INOUT    Double Precision A(NELT).
!         These arrays should hold the matrix A in either the SLAP
!         Triad format or the SLAP Column format.  See "LONG
!         DESCRIPTION", below.  If the SLAP Triad format is used
!         this format is translated to the SLAP Column format by
!         this routine.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the lower
!         triangle of the matrix is stored.
!
! *Precision:           Double Precision
!
!***LONG DESCRIPTION
!       The Sparse Linear Algebra Package (SLAP) utilizes two matrix
!       data structures: 1) the  SLAP Triad  format or  2)  the SLAP
!       Column format.  The user can hand this routine either of the
!       of these data structures.  If the SLAP Triad format is give
!       as input then this routine transforms it into SLAP Column
!       format.  The way this routine tells which format is given as
!       input is to look at JA(N+1).  If JA(N+1) = NELT+1 then we
!       have the SLAP Column format.  If that equality does not hold
!       then it is assumed that the IA, JA, A arrays contain the
!       SLAP Triad format.
!
!       =================== S L A P Triad format ===================
!       This routine requires that the  matrix A be   stored in  the
!       SLAP  Triad format.  In  this format only the non-zeros  are
!       stored.  They may appear in  *ANY* order.  The user supplies
!       three arrays of  length NELT, where  NELT is  the number  of
!       non-zeros in the matrix: (IA(NELT), JA(NELT), A(NELT)).  For
!       each non-zero the user puts the row and column index of that
!       matrix element  in the IA and  JA arrays.  The  value of the
!       non-zero   matrix  element is  placed  in  the corresponding
!       location of the A array.   This is  an  extremely  easy data
!       structure to generate.  On  the  other hand it   is  not too
!       efficient on vector computers for  the iterative solution of
!       linear systems.  Hence,   SLAP changes   this  input    data
!       structure to the SLAP Column format  for  the iteration (but
!       does not change it back).
!
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix       SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a
!       column):
!
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!***REFERENCES  (NONE)
!***ROUTINES CALLED  QS2I1D
!***END PROLOGUE  DS2Y
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      DOUBLE PRECISION A(NELT)
!
!         Check to see if the (IA,JA,A) arrays are in SLAP Column
!         format.  If it's not then transform from SLAP Triad.
!***FIRST EXECUTABLE STATEMENT  DS2LT
      IF( JA(N+1).EQ.NELT+1 ) RETURN
!
!         Sort into ascending order by COLUMN (on the ja array).
!         This will line up the columns.
!
      CALL QS2I1D( JA, IA, A, NELT, 1 )
!
!         Loop over each column to see where the column indicies change
!         in the column index array ja.  This marks the beginning of the
!         next column.
!
!VD$R NOVECTOR
      JA(1) = 1
      DO 20 ICOL = 1, N-1
         DO J = JA(ICOL)+1, NELT
            IF( JA(J).NE.ICOL ) THEN
               JA(ICOL+1) = J
               GOTO 20
            ENDIF
         end do
 20   CONTINUE
      JA(N+1) = NELT+1
!
!         Mark the n+2 element so that future calls to a SLAP routine
!         utilizing the YSMP-Column storage format will be able to tell.
!
      JA(N+2) = 0
!
!         Now loop thru the ia(i) array making sure that the Diagonal
!         matrix element appears first in the column.  Then sort the
!         rest of the column in ascending order.
!
      DO 70 ICOL = 1, N
         IBGN = JA(ICOL)
         IEND = JA(ICOL+1)-1
         DO 30 I = IBGN, IEND
            IF( IA(I).EQ.ICOL ) THEN
!         Swap the diag element with the first element in the column.
               ITEMP = IA(I)
               IA(I) = IA(IBGN)
               IA(IBGN) = ITEMP
               TEMP = A(I)
               A(I) = A(IBGN)
               A(IBGN) = TEMP
               GOTO 40
            ENDIF
 30      CONTINUE
 40      IBGN = IBGN + 1
         IF( IBGN.LT.IEND ) THEN
            DO 60 I = IBGN, IEND
               DO 50 J = I+1, IEND
                  IF( IA(I).GT.IA(J) ) THEN
                     ITEMP = IA(I)
                     IA(I) = IA(J)
                     IA(J) = ITEMP
                     TEMP = A(I)
                     A(I) = A(J)
                     A(J) = TEMP
                  ENDIF
 50            CONTINUE
 60         CONTINUE
         ENDIF
 70   CONTINUE
      RETURN
!------------- LAST LINE OF DS2Y FOLLOWS ----------------------------
      END
      SUBROUTINE DSCAL(N,DA,DX,INCX)
!
!     REPLACE DOUBLE PRECISION DX BY DOUBLE PRECISION DA*DX.
!     FOR I = 0 TO N-1, REPLACE DX(1+I*INCX) WITH  DA * DX(1+I*INCX)
!
      DOUBLE PRECISION DA,DX(1)
      IF(N.LE.0)RETURN
      IF(INCX.EQ.1)GOTO 20
!
!        CODE FOR INCREMENTS NOT EQUAL TO 1.
!
      NS = N*INCX
      DO I = 1,NS,INCX
          DX(I) = DA*DX(I)
      end do

      RETURN
!
!        CODE FOR INCREMENTS EQUAL TO 1.
!
!
!        CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 5.
!
   20 M = MOD(N,5)
      IF( M .EQ. 0 ) GO TO 40
      DO 30 I = 1,M
        DX(I) = DA*DX(I)
   30 CONTINUE
      IF( N .LT. 5 ) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,N,5
        DX(I) = DA*DX(I)
        DX(I + 1) = DA*DX(I + 1)
        DX(I + 2) = DA*DX(I + 2)
        DX(I + 3) = DA*DX(I + 3)
        DX(I + 4) = DA*DX(I + 4)
   50 CONTINUE
      RETURN
      END
      SUBROUTINE DPIGMR(N, R0, SR, SZ, JSCAL, MAXL, MAXLP1, KMP, &
           NRSTS, JPRE, MATVEC, MSOLVE, NMSL, Z, V, HES, Q, LGMR, &
           RPAR, IPAR, WK, DL, RHOL, NRMAX, B, BNRM, X, XL, &
           ITOL, TOL, NELT, IA, JA, A, ISYM, IUNIT, IFLAG, ERR)
!***BEGIN PROLOGUE  DPIGMR
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DPIGMR-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Generalized Minimum Residual
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE  Internal routine for DGMRES.
!***DESCRIPTION
!         This routine solves the linear system A * Z = R0 using a
!         scaled preconditioned version of the generalized minimum
!         residual method.  An initial guess of Z = 0 is assumed.
!
! *Usage:
!      EXTERNAL MATVEC, MSOLVE
!      INTEGER N,MAXL,MAXLP1,KMP,JPRE,NMSL,LGMR,IPAR,IFLAG,JSCAL,NRSTS
!      INTEGER NRMAX,ITOL,NELT,ISYM
!      DOUBLE PRECISION R0,SR,SZ,Z,V,HES,Q,RPAR,WK,DL,RHOL,BNRM,TOL,
!     $     A,B,X, R0(1), SR(1), SZ(1), Z(1), V(N,1),
!     $     HES(MAXLP1,1), Q(1), RPAR(1), IPAR(1), WK(1), DL(1),
!     $     IA(NELT), JA(NELT), A(NELT), B(1), X(1), XL(1)
!
!      CALL DPIGMR(N, R0, SR, SZ, JSCAL, MAXL, MAXLP1, KMP,
!     $     NRSTS, JPRE, MATVEC, MSOLVE, NMSL, Z, V, HES, Q, LGMR,
!     $     RPAR, IPAR, WK, DL, RHOL, NRMAX, B, BNRM, X, XL,
!     $     ITOL, TOL, NELT, IA, JA, A, ISYM, IUNIT, IFLAG, ERR)
!
! *Arguments:
! R0     :IN       Double Precision R0(N)
!         R0 = the right hand side of the system A*Z = R0.
!         R0 is also used as work space when computing
!         the final approximation.
!         (R0 is the same as V(*,MAXL+1) in the call to DPIGMR.)
! SR     :IN       Double Precision SR(N)
!         SR is a vector of length N containing the nonzero
!         elements of the diagonal scaling matrix for R0.
! SZ     :IN       Double Precision SZ(N)
!         SZ is a vector of length N containing the nonzero
!         elements of the diagonal scaling matrix for Z.
! JSCAL  :IN       Integer
!         A flag indicating whether arrays SR and SZ are used.
!         JSCAL=0 means SR and SZ are not used and the
!                 algorithm will perform as if all
!                 SR(i) = 1 and SZ(i) = 1.
!         JSCAL=1 means only SZ is used, and the algorithm
!                 performs as if all SR(i) = 1.
!         JSCAL=2 means only SR is used, and the algorithm
!                 performs as if all SZ(i) = 1.
!         JSCAL=3 means both SR and SZ are used.
! N      :IN       Integer
!         The order of the matrix A, and the lengths
!         of the vectors SR, SZ, R0 and Z.
! MAXL   :IN       Integer
!         The maximum allowable order of the matrix H.
! MAXLP1 :IN       Integer
!         MAXPL1 = MAXL + 1, used for dynamic dimensioning of HES.
! KMP    :IN       Integer
!         The number of previous vectors the new vector VNEW
!         must be made orthogonal to.  (KMP .le. MAXL)
! NRSTS  :IN       Integer
!         Counter for the number of restarts on the current
!         call to DGMRES.  If NRSTS .gt. 0, then the residual
!         R0 is already scaled, and so scaling of it is
!         not necessary.
! JPRE   :IN       Integer
!         Preconditioner type flag.
! WK     :IN       Double Precision WK(N)
!         A double precision work array of length N used by routine
!         MATVEC
!         and MSOLVE.
! DL     :INOUT    Double Precision DL(N)
!         On input, a double precision work array of length N used for
!         calculation of the residual norm RHO when the method is
!         incomplete (KMP.lt.MAXL), and/or when using restarting.
!         On output, the scaled residual vector RL.  It is only loaded
!         when performing restarts of the Krylov iteration.
! NRMAX  :IN       Integer
!         The maximum number of restarts of the Krylov iteration.
!         NRMAX .gt. 0 means restarting is active, while
!         NRMAX = 0 means restarting is not being used.
! B      :IN       Double Precision B(N)
!         The right hand side of the linear system A*X = B.
! BNRM   :IN       Double Precision
!         The scaled norm of b.
! X      :IN       Double Precision X(N)
!         The current approximate solution as of the last
!         restart.
! XL     :IN       Double Precision XL(N)
!         An array of length N used to hold the approximate
!         solution X(L) when ITOL=11.
! ITOL   :IN       Integer
!         A flag to indicate the type of convergence criterion
!         used.  see the driver for its description.
! TOL    :IN       Double Precision
!         The tolerance on residuals R0-A*Z in scaled norm.
! NELT   :IN       Integer
!         The length of arrays IA, JA and A.
! IA     :IN       Integer IA(NELT)
!         An integer array of length NELT containing matrix data.
!         It is passed directly to the MATVEC and MSOLVE routines.
! JA     :IN       Integer JA(NELT)
!         An integer array of length NELT containing matrix data.
!         It is passed directly to the MATVEC and MSOLVE routines.
! A      :IN       Double Precision A(NELT)
!         A double precision array of length NELT containing matrix
!         data. It is passed directly to the MATVEC and MSOLVE routines.
! ISYM   :IN       Integer
!         A flag to indicate symmetric matrix storage.
!         If ISYM=0, all nonzero entries of the matrix are
!         stored.  If ISYM=1, the matrix is symmetric and
!         only the upper or lower triangular part is stored.
! IUNIT  :IN       Integer
!         The i/o unit number for writing intermediate residual
!         norm values.
! Z      :OUT      Double Precision Z(N)
!         The final computed approximation to the solution
!         of the system A*Z = R0.
! LGMR   :OUT      Integer
!         The number of iterations performed and
!         the current order of the upper hessenberg
!         matrix HES.
! RPAR   :IN       Double Precision RPAR(*)
!         Double Precision work space passed directly to the MSOLVE
!         routine.
! IPAR   :IN       Integer IPAR(*)
!         Integer work space passed directly to the MSOLVE
!         routine.
! NMSL   :OUT      Integer
!         The number of calls to MSOLVE.
! V      :OUT      Double Precision V(N,MAXLP1)
!         The N by (LGMR+1) array containing the LGMR
!         orthogonal vectors V(*,1) to V(*,LGMR).
! HES    :OUT      Double Precision HES(MAXLP1,MAXL)
!         The upper triangular factor of the QR decomposition
!         of the (LGMR+1) by LGMR upper Hessenberg matrix whose
!         entries are the scaled inner-products of A*V(*,I)
!         and V(*,K).
! Q      :OUT      Double Precision Q(2*MAXL)
!         A double precision array of length 2*MAXL containing the
!         components of the Givens rotations used in the QR
!         decomposition of HES.  It is loaded in DHEQR and used in
!         DHELS.
! RHOL   :OUT      Double Precision
!         A double precision scalar containing the norm of the final
!         residual.
! IFLAG  :OUT      Integer
!         An integer error flag..
!         0 means convergence in LGMR iterations, LGMR.le.MAXL.
!         1 means the convergence test did not pass in MAXL
!           iterations, but the residual norm is .lt. norm(R0),
!           and so Z is computed.
!         2 means the convergence test did not pass in MAXL
!           iterations, residual .ge. norm(R0), and Z = 0.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as
!         defined by ITOL.
!
! *See Also:
!         DGMRES
!
!***ROUTINES CALLED  ISDGMR, MATVEC, MSOLVE, DORTH, DRLCAL, DHELS,
!                    DHEQR, DXLCAL, DAXPY, DCOPY, DSCAL,
!***END PROLOGUE  DPIGMR
!         The following is for optimized compilation on LLNL/LTSS Crays.
!LLL. OPTIMIZE
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL MATVEC, MSOLVE
      INTEGER N,MAXL,MAXLP1,KMP,JPRE,NMSL,LGMR,IFLAG,JSCAL,NRSTS
      INTEGER NRMAX,ITOL,NELT,ISYM
      DOUBLE PRECISION RHOL, BNRM, TOL
      DOUBLE PRECISION R0(*), SR(*), SZ(*), Z(*), V(N,*)
      DOUBLE PRECISION HES(MAXLP1,*), Q(*), RPAR(*), WK(*), DL(*)
      DOUBLE PRECISION A(NELT), B(*), X(*), XL(*)
      INTEGER IPAR(*), IA(NELT), JA(NELT)
!
!         Local variables.
!
      INTEGER I, INFO, IP1, I2, J, K, LL, LLP1
      DOUBLE PRECISION R0NRM,C,DLNRM,PROD,RHO,S,SNORMW,TEM
!
!         Zero out the z array.
!***FIRST EXECUTABLE STATEMENT  DPIGMR
      DO 5 I = 1,N
         Z(I) = 0.0D0
 5    CONTINUE
!
      IFLAG = 0
      LGMR = 0
      NMSL = 0
!         Load ITMAX, the maximum number of iterations.
      ITMAX =(NRMAX+1)*MAXL
!   -------------------------------------------------------------------
!         The initial residual is the vector R0.
!         Apply left precon. if JPRE < 0 and this is not a restart.
!         Apply scaling to R0 if JSCAL = 2 or 3.
!   -------------------------------------------------------------------
      IF ((JPRE .LT. 0) .AND.(NRSTS .EQ. 0)) THEN
         CALL DCOPY(N, R0, 1, WK, 1)
         CALL MSOLVE(N, WK, R0, NELT, IA, JA, A, ISYM, RPAR, IPAR)
         NMSL = NMSL + 1
      ENDIF
      IF (((JSCAL.EQ.2) .OR.(JSCAL.EQ.3)) .AND.(NRSTS.EQ.0)) THEN

         V(1:n,1) = R0(1:n) * SR(1:n)

      ELSE

         V(1:n,1) = R0(1:n)

      ENDIF

      R0NRM = DNRM2(N, V, 1)
      ITER = NRSTS*MAXL
!
!         Call stopping routine ISDGMR.
!
      IF (ISDGMR(N, B, X, XL, NELT, IA, JA, A, ISYM, MSOLVE, &
          NMSL, ITOL, TOL, ITMAX, ITER, ERR, IUNIT, V(1,1), Z, WK, &
          RPAR, IPAR, R0NRM, BNRM, SR, SZ, JSCAL, &
          KMP, LGMR, MAXL, MAXLP1, V, Q, SNORMW, PROD, R0NRM, &
          HES, JPRE) .NE. 0) RETURN
      TEM = 1.0D0/R0NRM
      CALL DSCAL(N, TEM, V(1,1), 1)
!
!         Zero out the HES array.
!
      DO 50 J = 1,MAXL
         DO 40 I = 1,MAXLP1
            HES(I,J) = 0.0D0
 40      CONTINUE
 50   CONTINUE
!   -------------------------------------------------------------------
!         main loop to compute the vectors V(*,2) to V(*,MAXL).
!         The running product PROD is needed for the convergence test.
!   -------------------------------------------------------------------
      PROD = 1.0D0
      DO 90 LL = 1,MAXL
         LGMR = LL
!   -------------------------------------------------------------------
!        Unscale  the  current V(LL)  and store  in WK.  Call routine
!        msolve    to   compute(M-inverse)*WK,   where    M   is  the
!        preconditioner matrix.  Save the answer in Z.   Call routine
!        MATVEC to compute  VNEW  = A*Z,  where  A is  the the system
!        matrix.  save the answer in  V(LL+1).  Scale V(LL+1).   Call
!        routine DORTH  to  orthogonalize the    new vector VNEW   =
!        V(*,LL+1).  Call routine DHEQR to update the factors of HES.
!   -------------------------------------------------------------------
        IF ((JSCAL .EQ. 1) .OR.(JSCAL .EQ. 3)) THEN
           DO 60 I = 1,N
              WK(I) = V(I,LL)/SZ(I)
 60        CONTINUE
        ELSE
           CALL DCOPY(N, V(1,LL), 1, WK, 1)
        ENDIF
        IF (JPRE .GT. 0) THEN
           CALL MSOLVE(N, WK, Z, NELT, IA, JA, A, ISYM, RPAR, IPAR)
           NMSL = NMSL + 1
           CALL MATVEC(N, Z, V(1,LL+1), NELT, IA, JA, A, ISYM)
        ELSE
           CALL MATVEC(N, WK, V(1,LL+1), NELT, IA, JA, A, ISYM)
        ENDIF
        IF (JPRE .LT. 0) THEN
           CALL DCOPY(N, V(1,LL+1), 1, WK, 1)
           CALL MSOLVE(N,WK,V(1,LL+1),NELT,IA,JA,A,ISYM,RPAR,IPAR)
           NMSL = NMSL + 1
        ENDIF
        IF ((JSCAL .EQ. 2) .OR.(JSCAL .EQ. 3)) THEN
           DO 65 I = 1,N
              V(I,LL+1) = V(I,LL+1)*SR(I)
 65        CONTINUE
        ENDIF
        CALL DORTH(V(1,LL+1), V, HES, N, LL, MAXLP1, KMP, SNORMW)
        HES(LL+1,LL) = SNORMW
        CALL DHEQR(HES, MAXLP1, LL, Q, INFO, LL)
        IF (INFO .EQ. LL) GO TO 120
!   -------------------------------------------------------------------
!         Update RHO, the estimate of the norm of the residual R0-A*ZL.
!         If KMP <  MAXL, then the vectors V(*,1),...,V(*,LL+1) are not
!         necessarily orthogonal for LL > KMP.  The vector DL must then
!         be computed, and its norm used in the calculation of RHO.
!   -------------------------------------------------------------------
        PROD = PROD*Q(2*LL)
        RHO = ABS(PROD*R0NRM)
        IF ((LL.GT.KMP) .AND.(KMP.LT.MAXL)) THEN
           IF (LL .EQ. KMP+1) THEN
              CALL DCOPY(N, V(1,1), 1, DL, 1)
              DO 75 I = 1,KMP
                 IP1 = I + 1
                 I2 = I*2
                 S = Q(I2)
                 C = Q(I2-1)
                 DO 70 K = 1,N
                    DL(K) = S*DL(K) + C*V(K,IP1)
 70              CONTINUE
 75           CONTINUE
           ENDIF
           S = Q(2*LL)
           C = Q(2*LL-1)/SNORMW
           LLP1 = LL + 1
           DO 80 K = 1,N
              DL(K) = S*DL(K) + C*V(K,LLP1)
 80        CONTINUE
           DLNRM = DNRM2(N, DL, 1)
           RHO = RHO*DLNRM
        ENDIF
        RHOL = RHO
!   -------------------------------------------------------------------
!         Test for convergence.  If passed, compute approximation ZL.
!         If failed and LL < MAXL, then continue iterating.
!   -------------------------------------------------------------------
        ITER = NRSTS*MAXL + LGMR
        IF (ISDGMR(N, B, X, XL, NELT, IA, JA, A, ISYM, MSOLVE, &
            NMSL, ITOL, TOL, ITMAX, ITER, ERR, IUNIT, DL, Z, WK, &
            RPAR, IPAR, RHOL, BNRM, SR, SZ, JSCAL, &
            KMP, LGMR, MAXL, MAXLP1, V, Q, SNORMW, PROD, R0NRM, &
            HES, JPRE) .NE. 0) GO TO 200
        IF (LL .EQ. MAXL) GO TO 100
!   -------------------------------------------------------------------
!         Rescale so that the norm of V(1,LL+1) is one.
!   -------------------------------------------------------------------
        TEM = 1.0D0/SNORMW
        CALL DSCAL(N, TEM, V(1,LL+1), 1)
 90   CONTINUE
 100  CONTINUE
      IF (RHO .LT. R0NRM) GO TO 150
 120  CONTINUE
      IFLAG = 2
!
!         Load approximate solution with zero.
!
      DO 130 I = 1,N
         Z(I) = 0.D0
 130  CONTINUE
      RETURN
 150  IFLAG = 1
!
!         Tolerance not met, but residual norm reduced.
!
      IF (NRMAX .GT. 0) THEN
!
!        If performing restarting (NRMAX > 0)  calculate the residual
!        vector RL and  store it in the DL  array.  If the incomplete
!        version is being used (KMP < MAXL) then DL has  already been
!        calculated up to a scaling factor.   Use DRLCAL to calculate
!        the scaled residual vector.
!
         CALL DRLCAL(N, KMP, MAXL, MAXL, V, Q, DL, SNORMW, PROD, &
              R0NRM)
      ENDIF
!   -------------------------------------------------------------------
!         Compute the approximation ZL to the solution.  Since the
!         vector Z was used as work space, and the initial guess
!         of the linear iteration is zero, Z must be reset to zero.
!   -------------------------------------------------------------------
 200  CONTINUE
      LL = LGMR
      LLP1 = LL + 1
      DO 210 K = 1,LLP1
         R0(K) = 0.0D0
 210  CONTINUE
      R0(1) = R0NRM
      CALL DHELS(HES, MAXLP1, LL, Q, R0)
      DO 220 K = 1,N
         Z(K) = 0.0D0
 220  CONTINUE
      DO 230 I = 1,LL
         CALL DAXPY(N, R0(I), V(1,I), 1, Z, 1)
 230  CONTINUE
      IF ((JSCAL .EQ. 1) .OR.(JSCAL .EQ. 3)) THEN
         DO 240 I = 1,N
            Z(I) = Z(I)/SZ(I)
 240     CONTINUE
      ENDIF
      IF (JPRE .GT. 0) THEN
         CALL DCOPY(N, Z, 1, WK, 1)
         CALL MSOLVE(N, WK, Z, NELT, IA, JA, A, ISYM, RPAR, IPAR)
         NMSL = NMSL + 1
      ENDIF
      RETURN
!------------- LAST LINE OF DPIGMR FOLLOWS ----------------------------
      END
      SUBROUTINE DRLCAL(N, KMP, LL, MAXL, V, Q, RL, SNORMW, PROD, &
           R0NRM)
!***BEGIN PROLOGUE  DRLCAL
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DRLCAL-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Generalized Minimum Residual
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE  Internal routine for DGMRES.
!***DESCRIPTION
!         This routine calculates the scaled residual RL from the
!         V(I)'s.
! *Usage:
!      INTEGER N, KMP, LL, MAXL
!      DOUBLE PRECISION SNORMW
!      DOUBLE PRECISION V(N,1), Q(1), RL(N)
!
!      CALL DRLCAL(N, KMP, LL, MAXL, V, Q, RL, SNORMW, PROD,
!     $     R0NRM)
!
! *Arguments:
! N      :IN       Integer
!         The order of the matrix A, and the lengths
!         of the vectors SR, SZ, R0 and Z.
! KMP    :IN       Integer
!         The number of previous V vectors the new vector VNEW
!         must be made orthogonal to. (KMP .le. MAXL)
! LL     :IN       Integer
!         The current dimension of the Krylov subspace.
! MAXL   :IN       Integer
!         The maximum dimension of the Krylov subspace.
! Q      :IN       Double Precision Q(2*MAXL)
!         A double precision array of length 2*MAXL containing the
!         components of the Givens rotations used in the QR
!         decomposition of HES.  It is loaded in DHEQR and used in
!         DHELS.
! PROD   :IN       Double Precision
!        The product s1*s2*...*sl = the product of the sines of the
!        givens rotations used in the QR factorization of
!        the hessenberg matrix HES.
! R0NRM  :IN       Double Precision
!         The scaled norm of initial residual R0.
! RL     :OUT      Double Precision RL(N)
!         The residual vector RL.  This is either SB*(B-A*XL) if
!         not preconditioning or preconditioning on the right,
!         or SB*(M-inverse)*(B-A*XL) if preconditioning on the
!         left.
!
! *See Also:
!         DGMRES
!
!***ROUTINES CALLED  DCOPY, DSCAL
!***END PROLOGUE  DRLCAL
!         The following is for optimized compilation on LLNL/LTSS Crays.
!LLL. OPTIMIZE
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, KMP, LL, MAXL
      DOUBLE PRECISION SNORMW
      DOUBLE PRECISION V(N,*), Q(*), RL(N)
!
!         Internal Variables.
!
      INTEGER I, IP1, I2, K
!
!***FIRST EXECUTABLE STATEMENT  DRLCAL
      IF (KMP .EQ. MAXL) THEN
!
!         calculate RL.  Start by copying V(*,1) into RL.
!
         CALL DCOPY(N, V(1,1), 1, RL, 1)
         LLM1 = LL - 1
         DO 20 I = 1,LLM1
            IP1 = I + 1
            I2 = I*2
            S = Q(I2)
            C = Q(I2-1)

            RL(1:n) = S*RL(1:n) + C*V(1:n,IP1)

 20      CONTINUE
         S = Q(2*LL)
         C = Q(2*LL-1)/SNORMW
         LLP1 = LL + 1
         DO 30 K = 1,N
            RL(K) = S*RL(K) + C*V(K,LLP1)
 30      CONTINUE
      ENDIF
!
!         When KMP < MAXL, RL vector already partially calculated.
!         Scale RL by R0NRM*PROD to obtain the residual RL.
!
      TEM = R0NRM*PROD
      CALL DSCAL(N, TEM, RL, 1)
      RETURN
!------------- LAST LINE OF DRLCAL FOLLOWS ----------------------------
      END
      SUBROUTINE DXLCAL(N, LGMR, X, XL, ZL, HES, MAXLP1, Q, V, R0NRM, &
           WK, SZ, JSCAL, JPRE, MSOLVE, NMSL, RPAR, IPAR, &
           NELT, IA, JA, A, ISYM)
!***BEGIN PROLOGUE  DXLCAL
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DXLCAL-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Generalized Minimum Residual
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE  Internal routine for DGMRES.
!***DESCRIPTION
!        This  routine computes the solution  XL,  the current DGMRES
!        iterate, given the  V(I)'s and  the  QR factorization of the
!        Hessenberg  matrix HES.   This routine  is  only called when
!        ITOL=11.
!
! *Usage:
!      EXTERNAL MSOLVE
!      DOUBLE PRECISION R0NRM
!      DOUBLE PRECISION X(N), XL(N), ZL(N), HES(MAXLP1,1), Q(1)
!      DOUBLE PRECISION V(N,1), WK(N), SZ(1), RPAR(1)
!      DOUBLE PRECISION A(NELT)
!      INTEGER N, LGMR, MAXLP1, JSCAL, JPRE, IPAR, NMSL, NELT, ISYM
!      INTEGER IPAR(1), IA(NELT), JA(NELT)
!
!      CALL DXLCAL(N, LGMR, X, XL, ZL, HES, MAXLP1, Q, V, R0NRM,
!     $     WK, SZ, JSCAL, JPRE, MSOLVE, NMSL, RPAR, IPAR,
!     $     NELT, IA, JA, A, ISYM)
!
! *Arguments:
! N      :IN       Integer
!         The order of the matrix A, and the lengths
!         of the vectors SR, SZ, R0 and Z.
! LGMR   :IN       Integer
!         The number of iterations performed and
!         the current order of the upper Hessenberg
!         matrix HES.
! X      :IN       Double Precision X(N)
!         The current approximate solution as of the last restart.
! ZL     :IN       Double Precision ZL(N)
!         An array of length N used to hold the approximate
!         solution Z(L).
! SZ     :IN       Double Precision SZ(N)
!         A vector of length N containing the nonzero
!         elements of the diagonal scaling matrix for Z.
! JSCAL  :IN       Integer
!         A flag indicating whether arrays SR and SZ are used.
!         JSCAL=0 means SR and SZ are not used and the
!                 algorithm will perform as if all
!                 SR(i) = 1 and SZ(i) = 1.
!         JSCAL=1 means only SZ is used, and the algorithm
!                 performs as if all SR(i) = 1.
!         JSCAL=2 means only SR is used, and the algorithm
!                 performs as if all SZ(i) = 1.
!         JSCAL=3 means both SR and SZ are used.
! MAXLP1 :IN       Integer
!         MAXLP1 = MAXL + 1, used for dynamic dimensioning of HES.
!         MAXL is the maximum allowable order of the matrix HES.
! JPRE   :IN       Integer
!         The preconditioner type flag.
! WK     :IN       Double Precision WK(N)
!         A double precision work array of length N.
! NMSL   :IN       Integer
!         The number of calls to MSOLVE.
! V      :IN       Double Precision V(N,MAXLP1)
!         The N by(LGMR+1) array containing the LGMR
!         orthogonal vectors V(*,1) to V(*,LGMR).
! HES    :IN       Double Precision HES(MAXLP1,MAXL)
!         The upper triangular factor of the QR decomposition
!         of the (LGMR+1) by LGMR upper Hessenberg matrix whose
!         entries are the scaled inner-products of A*V(*,i) and V(*,k).
! Q      :IN       Double Precision Q(2*MAXL)
!         A double precision array of length 2*MAXL containing the
!         components of the givens rotations used in the QR
!         decomposition of HES.  It is loaded in DHEQR.
! R0NRM  :IN       Double Precision
!         The scaled norm of the initial residual for the
!         current call to DPIGMR.
! RPAR   :IN       Double Precision RPAR(*)
!         Double Precision work space passed directly to the MSOLVE
!         routine.
! IPAR   :IN       Integer IPAR(*)
!         Integer work space passed directly to the MSOLVE
!         routine.
! NELT   :IN       Integer
!         The length of arrays IA, JA and A.
! IA     :IN       Integer IA(NELT)
!         An integer array of length NELT containing matrix data.
!         It is passed directly to the MATVEC and MSOLVE routines.
! JA     :IN       Integer JA(NELT)
!         An integer array of length NELT containing matrix data.
!         It is passed directly to the MATVEC and MSOLVE routines.
! A      :IN       Double Precision A(NELT)
!         A double precision array of length NELT containing matrix
!         data.
!         It is passed directly to the MATVEC and MSOLVE routines.
! ISYM   :IN       Integer
!         A flag to indicate symmetric matrix storage.
!         If ISYM=0, all nonzero entries of the matrix are
!         stored.  If ISYM=1, the matrix is symmetric and
!         only the upper or lower triangular part is stored.
! XL     :OUT      Double Precision XL(N)
!         An array of length N used to hold the approximate
!         solution X(L).
!         Warning: XL and ZL are the same array in the calling routine.
!
! *See Also:
!         DGMRES
!
!***ROUTINES CALLED  MSOLVE, DHELS, DAXPY, DCOPY, DSCAL
!***END PROLOGUE  DXLCAL
!         The following is for optimized compilation on LLNL/LTSS Crays.
!LLL. OPTIMIZE
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      EXTERNAL MSOLVE
      INTEGER N, LGMR, MAXLP1, JSCAL, JPRE, IPAR(*), NMSL, NELT
      INTEGER IA(NELT), JA(NELT), ISYM
      DOUBLE PRECISION R0NRM, X(N), XL(N), ZL(N), HES(MAXLP1,*)
      DOUBLE PRECISION Q(*), V(N,*), WK(N), SZ(*), RPAR(*), A(NELT)
!
!         Internal variables.
!
      INTEGER I, K, LL, LLP1
!
!***FIRST EXECUTABLE STATEMENT  DXLCAL
      LL = LGMR
      LLP1 = LL + 1

      WK(1:llp1) = 0.0D0

      WK(1) = R0NRM
      CALL DHELS(HES, MAXLP1, LL, Q, WK)
      DO 20 K = 1,N
         ZL(K) = 0.0D0
 20   CONTINUE
      DO 30 I = 1,LL
         CALL DAXPY(N, WK(I), V(1,I), 1, ZL, 1)
 30   CONTINUE
      IF ((JSCAL .EQ. 1) .OR.(JSCAL .EQ. 3)) THEN
         DO 40 K = 1,N
            ZL(K) = ZL(K)/SZ(K)
 40      CONTINUE
      ENDIF
      IF (JPRE .GT. 0) THEN
         CALL DCOPY(N, ZL, 1, WK, 1)
         CALL MSOLVE(N, WK, ZL, NELT, IA, JA, A, ISYM, RPAR, IPAR)
         NMSL = NMSL + 1
      ENDIF
!         calculate XL from X and ZL.
      DO 50 K = 1,N
         XL(K) = X(K) + ZL(K)
 50   CONTINUE
      RETURN
!------------- LAST LINE OF DXLCAL FOLLOWS ----------------------------
      END
      SUBROUTINE DHELS(A, LDA, N, Q, B)
!***BEGIN PROLOGUE  DHEQR
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DHEQR-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Generalized Minimum Residual
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE  Internal routine for DGMRES.
!***DESCRIPTION
!        This routine is extraced from the LINPACK routine SGESL with
!        changes  due to  the fact  that  A is an  upper   Hessenberg
!        matrix.
!
!        DHELS solves the least squares problem:
!
!                   MIN(B-A*X,B-A*X)
!
!        using the factors computed by DHEQR.
!
! *Usage:
!      INTEGER LDA, N
!      DOUBLE PRECISION A(LDA,1), B(1), Q(1)
!
!      CALL DHELS(A, LDA, N, Q, B)
!
! *Arguments:
! A       :IN       Double Precision A(LDA,N)
!          The output from DHEQR which contains the upper
!          triangular factor R in the QR decomposition of A.
! LDA     :IN       Integer
!          The leading dimension of the array A.
! N       :IN       Integer
!          A is originally an (N+1) by N matrix.
! Q       :IN       Double Precision Q(2*N)
!          The coefficients of the N givens rotations
!          used in the QR factorization of A.
! B       :INOUT    Double Precision B(N+1)
!          On input, B is the right hand side vector.
!          On output, B is the solution vector X.
! *See Also:
!         DGMRES
!
!***ROUTINES CALLED  DAXPY
!***END PROLOGUE  DHEQR
!         The following is for optimized compilation on LLNL/LTSS Crays.
!LLL. OPTIMIZE
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER LDA, N
      DOUBLE PRECISION A(LDA,1), B(1), Q(1)
!
!         Local Variables.
!
      INTEGER IQ, K, KB, KP1
      DOUBLE PRECISION C, S, T, T1, T2
!
!         minimize(B-A*X,B-A*X).  First form Q*B.
!
      DO 20 K = 1, N
         KP1 = K + 1
         IQ = 2*(K-1) + 1
         C = Q(IQ)
         S = Q(IQ+1)
         T1 = B(K)
         T2 = B(KP1)
         B(K) = C*T1 - S*T2
         B(KP1) = S*T1 + C*T2
 20   CONTINUE
!
!         Now solve  R*X = Q*B.
!
      DO 40 KB = 1, N
         K = N + 1 - KB
         B(K) = B(K)/A(K,K)
         T = -B(K)
         CALL DAXPY(K-1, T, A(1,K), 1, B(1), 1)
 40   CONTINUE
      RETURN
!------------- LAST LINE OF DHELS FOLLOWS ----------------------------
      END
      SUBROUTINE DHEQR(A, LDA, N, Q, INFO, IJOB)
!***BEGIN PROLOGUE  DHEQR
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DHEQR-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Generalized Minimum Residual
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE  Internal routine for DGMRES.
!***DESCRIPTION
!        This   routine  performs  a QR   decomposition  of an  upper
!        Hessenberg matrix A using Givens  rotations.  There  are two
!        options  available: 1)  Performing  a fresh decomposition 2)
!        updating the QR factors by adding a row and  a column to the
!        matrix A.
!
! *Usage:
!      INTEGER LDA, N, INFO, IJOB
!      DOUBLE PRECISION A(LDA,1), Q(1)
!
!      CALL DHEQR(A, LDA, N, Q, INFO, IJOB)
!
! *Arguments:
! A      :INOUT    Double Precision A(LDA,N)
!         On input, the matrix to be decomposed.
!         On output, the upper triangular matrix R.
!         The factorization can be written Q*A = R, where
!         Q is a product of Givens rotations and R is upper
!         triangular.
! LDA    :IN       Integer
!         The leading dimension of the array A.
! N      :IN       Integer
!         A is an (N+1) by N Hessenberg matrix.
! IJOB   :IN       Integer
!         = 1     means that a fresh decomposition of the
!                 matrix A is desired.
!         .ge. 2  means that the current decomposition of A
!                 will be updated by the addition of a row
!                 and a column.
! Q      :OUT      Double Precision Q(2*N)
!         The factors c and s of each Givens rotation used
!         in decomposing A.
! INFO   :OUT      Integer
!         = 0  normal value.
!         = K  if  A(K,K) .eq. 0.0 .  This is not an error
!           condition for this subroutine, but it does
!           indicate that DHELS will divide by zero
!           if called.
!
! *See Also:
!         DGMRES
!
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DHEQR
!         The following is for optimized compilation on LLNL/LTSS Crays.
!LLL. OPTIMIZE
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER LDA, N, INFO, IJOB
      DOUBLE PRECISION A(LDA,*), Q(*)
!
!         Local Variables.
!
      INTEGER I, IQ, J, K, KM1, KP1, NM1
      DOUBLE PRECISION C, S, T, T1, T2
!
!***FIRST EXECUTABLE STATEMENT  DHEQR
      IF (IJOB .GT. 1) GO TO 70
!   -------------------------------------------------------------------
!         A new facorization is desired.
!   -------------------------------------------------------------------
!         QR decomposition without pivoting.
!
      INFO = 0
      DO 60 K = 1, N
         KM1 = K - 1
         KP1 = K + 1
!
!           Compute K-th column of R.
!           First, multiply the K-th column of a by the previous
!           K-1 Givens rotations.
!
         IF (KM1 .LT. 1) GO TO 20
         DO J = 1, KM1
            I = 2*(J-1) + 1
            T1 = A(J,K)
            T2 = A(J+1,K)
            C = Q(I)
            S = Q(I+1)
            A(J,K) = C*T1 - S*T2
            A(J+1,K) = S*T1 + C*T2
         end do
!
!         Compute Givens components C and S.
!
 20      CONTINUE
         IQ = 2*KM1 + 1
         T1 = A(K,K)
         T2 = A(KP1,K)
         IF( T2.EQ.0.0D0 ) THEN
            C = 1.0D0
            S = 0.0D0
         ELSEIF( ABS(T2).GE.ABS(T1) ) THEN
            T = T1/T2
            S = -1.0D0/DSQRT(1.0D0+T*T)
            C = -S*T
         ELSE
            T = T2/T1
            C = 1.0D0/DSQRT(1.0D0+T*T)
            S = -C*T
         ENDIF
         Q(IQ) = C
         Q(IQ+1) = S
         A(K,K) = C*T1 - S*T2
         IF( A(K,K).EQ.0.0D0 ) INFO = K
 60   CONTINUE
      RETURN
!   -------------------------------------------------------------------
!         The old factorization of a will be updated.  A row and a
!         column has been added to the matrix A.  N by N-1 is now
!         the old size of the matrix.
!   -------------------------------------------------------------------
 70   CONTINUE
      NM1 = N - 1
!   -------------------------------------------------------------------
!         Multiply the new column by the N previous Givens rotations.
!   -------------------------------------------------------------------
      DO K = 1,NM1
         I = 2*(K-1) + 1
         T1 = A(K,N)
         T2 = A(K+1,N)
         C = Q(I)
         S = Q(I+1)
         A(K,N) = C*T1 - S*T2
         A(K+1,N) = S*T1 + C*T2
      end do
!   -------------------------------------------------------------------
!         Complete update of decomposition by forming last Givens
!         rotation, and multiplying it times the column
!         vector(A(N,N),A(NP1,N)).
!   -------------------------------------------------------------------
      INFO = 0
      T1 = A(N,N)
      T2 = A(N+1,N)
      IF ( T2.EQ.0.0D0 ) THEN
         C = 1.0D0
         S = 0.0D0
      ELSEIF( ABS(T2).GE.ABS(T1) ) THEN
         T = T1/T2
         S = -1.0D0/DSQRT(1.0D0+T*T)
         C = -S*T
      ELSE
         T = T2/T1
         C = 1.0D0/DSQRT(1.0D0+T*T)
         S = -C*T
      ENDIF
      IQ = 2*N - 1
      Q(IQ) = C
      Q(IQ+1) = S
      A(N,N) = C*T1 - S*T2
      IF (A(N,N) .EQ. 0.0D0) INFO = N
      RETURN
!------------- LAST LINE OF DHEQR FOLLOWS ----------------------------
      END
      SUBROUTINE DORTH(VNEW, V, HES, N, LL, LDHES, KMP, SNORMW)
!***BEGIN PROLOGUE  DORTH
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DORTH-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Generalized Minimum Residual
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE  Internal routine for DGMRES.
!***DESCRIPTION
!        This routine  orthogonalizes  the  vector  VNEW  against the
!        previous KMP  vectors in the   V array.  It uses  a modified
!        gram-schmidt   orthogonalization procedure with  conditional
!        reorthogonalization.
!
! *Usage:
!      INTEGER N, LL, LDHES, KMP
!      DOUBLE PRECISION VNEW, V, HES, SNORMW
!      DIMENSION VNEW(1), V(N,1), HES(LDHES,1)
!
!      CALL DORTH(VNEW, V, HES, N, LL, LDHES, KMP, SNORMW)
!
! *Arguments:
! VNEW   :INOUT    Double Precision VNEW(N)
!         On input, the vector of length n containing a scaled
!         product of the jacobian and the vector v(*,ll).
!         On output, the new vector orthogonal to v(*,i0) to v(*,ll),
!         where i0 = max(1, ll-kmp+1).
! V      :IN       Double Precision V(N,1)
!         The n x ll array containing the previous ll
!         orthogonal vectors v(*,1) to v(*,ll).
! HES    :INOUT    Double Precision HES(LDHES,1)
!         On input, an LL x LL upper hessenberg matrix containing,
!         in HES(I,K), K.lt.LL, the scaled inner products of
!         A*V(*,K) and V(*,i).
!         On return, column LL of HES is filled in with
!         the scaled inner products of A*V(*,LL) and V(*,i).
! LDHES  :IN       Integer
!         The leading dimension of the HES array.
! N      :IN       Integer
!         The order of the matrix A, and the length of VNEW.
! LL     :IN       Integer
!         The current order of the matrix HES.
! KMP    :IN       Integer
!         The number of previous vectors the new vector VNEW
!         must be made orthogonal to (KMP .le. MAXL).
! SNORMW :OUT      DOUBLE PRECISION
!         Scalar containing the l-2 norm of VNEW.
!
! *See Also:
!         DGMRES
!
!***ROUTINES CALLED  DAXPY
!***END PROLOGUE  DORTH
!         The following is for optimized compilation on LLNL/LTSS Crays.
!LLL. OPTIMIZE
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, LL, LDHES, KMP
      DOUBLE PRECISION VNEW, V, HES, SNORMW
      DIMENSION VNEW(1), V(N,1), HES(LDHES,1)
!
!         Internal variables.
!
      INTEGER I, I0
      DOUBLE PRECISION ARG, SUMDSQ, TEM, VNRM
!
!         Get norm of unaltered VNEW for later use.
!***FIRST EXECUTABLE STATEMENT  DORTH
      VNRM = DNRM2(N, VNEW, 1)
!   -------------------------------------------------------------------
!         Perform the modified gram-schmidt procedure on VNEW =A*V(LL).
!         Scaled inner products give new column of HES.
!         Projections of earlier vectors are subtracted from VNEW.
!   -------------------------------------------------------------------
      I0 = MAX0(1,LL-KMP+1)
      DO I = I0,LL
         HES(I,LL) = DDOT(N, V(1,I), 1, VNEW, 1)
         TEM = -HES(I,LL)
         CALL DAXPY(N, TEM, V(1,I), 1, VNEW, 1)
      end do
!   -------------------------------------------------------------------
!         Compute SNORMW = norm of VNEW.  If VNEW is small compared
!         to its input value (in norm), then reorthogonalize VNEW to
!         V(*,1) through V(*,LL).  Correct if relative correction
!         exceeds 1000*(unit roundoff).  Finally, correct SNORMW using
!         the dot products involved.
!   -------------------------------------------------------------------
      SNORMW = DNRM2(N, VNEW, 1)
      IF (VNRM + 0.001D0*SNORMW .NE. VNRM) RETURN
      SUMDSQ = 0.0D0
      DO 30 I = I0,LL
         TEM = -DDOT(N, V(1,I), 1, VNEW, 1)
         IF (HES(I,LL) + 0.001D0*TEM .EQ. HES(I,LL)) GO TO 30
         HES(I,LL) = HES(I,LL) - TEM
         CALL DAXPY(N, TEM, V(1,I), 1, VNEW, 1)
         SUMDSQ = SUMDSQ + TEM**2
 30   CONTINUE
      IF (SUMDSQ .EQ. 0.0D0) RETURN
      ARG = MAX(0.0D0,SNORMW**2 - SUMDSQ)
      SNORMW = DSQRT(ARG)
!
      RETURN
!------------- LAST LINE OF DORTH FOLLOWS ----------------------------
      END
      FUNCTION ISDGMR(N, B, X, XL, NELT, IA, JA, A, ISYM, MSOLVE, &
           NMSL, ITOL, TOL, ITMAX, ITER, ERR, IUNIT, R, Z, DZ, &
           RWORK, IWORK, RNRM, BNRM, SB, SX, JSCAL, &
           KMP, LGMR, MAXL, MAXLP1, V, Q, SNORMW, PROD, R0NRM, &
           HES, JPRE)
!***BEGIN PROLOGUE ISDGMR
!***DATE WRITTEN   890404  (YYMMDD)
!***REVISION DATE  890404  (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=INTEGER(ISDGMR-I)
!             Linear system, Sparse, Stop Test, GMRES
!***AUTHOR  Brown, Peter,    (LLNL), brown@lll-crg.llnl.gov
!           Hindmarsh, Alan, (LLNL), alanh@lll-crg.llnl.gov
!           Seager, Mark K., (LLNL), seager@lll-crg.llnl.gov
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!***PURPOSE Generalized Minimum Residual Stop Test.
!         This routine calculates the stop test for the Generalized
!         Minimum RESidual (GMRES) iteration scheme.   It returns a
!         nonzero if  the  error  estimate (the  type  of  which is
!         determined  by   ITOL)  is  less  than the user specified
!         tolerence TOL.
!***DESCRIPTION
! *Usage:
!      INTEGER KMP, LGMR, MAXL, MAXLP1, JPRE, NMSL
!      DOUBLE PRECISION DXNRM, RNRM, R0NRM, SNORMW, SOLNRM, PROD
!      DOUBLE PRECISION B(1), X(1), IA(1), JA(1), A(1), R(1), Z(1)
!      DOUBLE PRECISION DZ(1), RWORK(1), IWORK(1), SB(1), SX(1)
!      DOUBLE PRECISION Q(1), V(N,1), HES(MAXLP1,MAXL), XL(1)
!      EXTERNAL MSOLVE
!
!      IF (ISDGMR(N, B, X, XL, NELT, IA, JA, A, ISYM, MSOLVE,
!     $     NMSL, ITOL, TOL, ITMAX, ITER, ERR, IUNIT, R, Z, DZ,
!     $     RWORK, IWORK, RNRM, BNRM, SB, SX, JSCAL,
!     $     KMP, LGMR, MAXL, MAXLP1, V, Q, SNORMW, PROD, R0NRM,
!     $     HES, JPRE) .NE. 0) THEN ITERATION DONE
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand-side vector.
! X      :IN       Double Precision X(N).
!         Approximate solution vector as of the last restart.
! XL     :OUT      Double Precision XL(N)
!         An array of length N used to hold the approximate
!         solution as of the current iteration.  Only computed by
!         this routine when ITOL=11.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(NELT).
! A      :IN       Double Precision A(NELT).
!         These arrays contain the matrix data structure for A.
!         It could take any form.  See "Description", in the DGMRES,
!         DSLUGM and DSDGMR routines for more late breaking details...
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! MSOLVE :EXT      External.
!         Name of a routine which solves a linear system Mz = r for  z
!         given r with the preconditioning matrix M (M is supplied via
!         RWORK and IWORK arrays.  The name of the MSOLVE routine must
!         be declared external in the calling program.  The calling
!         sequence to MSLOVE is:
!             CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         Where N is the number of unknowns, R is the right-hand side
!         vector, and z is the solution upon return.  RWORK is a
!         double precision
!         array that can be used to pass necessary preconditioning
!         information and/or workspace to MSOLVE.  IWORK is an integer
!         work array for the same purpose as RWORK.
! NMSL   :INOUT    Integer.
!         A counter for the number of calls to MSOLVE.
! ITOL   :IN       Integer.
!         Flag to indicate the type of convergence criterion used.
!         ITOL=0  Means the  iteration stops when the test described
!                 below on  the  residual RL  is satisfied.  This is
!                 the  "Natural Stopping Criteria" for this routine.
!                 Other values  of   ITOL  cause  extra,   otherwise
!                 unnecessary, computation per iteration and     are
!                 therefore  much less  efficient.  See  ISDGMR (the
!                 stop test routine) for more information.
!         ITOL=1  Means   the  iteration stops   when the first test
!                 described below on  the residual RL  is satisfied,
!                 and there  is either right  or  no preconditioning
!                 being used.
!         ITOL=2  Implies     that   the  user    is   using    left
!                 preconditioning, and the second stopping criterion
!                 below is used.
!         ITOL=3  Means the  iteration stops   when  the  third test
!                 described below on Minv*Residual is satisfied, and
!                 there is either left  or no  preconditioning begin
!                 used.
!         ITOL=11 is    often  useful  for   checking  and comparing
!                 different routines.  For this case, the  user must
!                 supply  the  "exact" solution or  a  very accurate
!                 approximation (one with  an  error much less  than
!                 TOL) through a common block,
!                     COMMON /SOLBLK/ SOLN(1)
!                 if ITOL=11, iteration stops when the 2-norm of the
!                 difference between the iterative approximation and
!                 the user-supplied solution  divided by the  2-norm
!                 of the  user-supplied solution  is  less than TOL.
!                 Note that this requires  the  user to  set up  the
!                 "COMMON     /SOLBLK/ SOLN(LENGTH)"  in the calling
!                 routine.  The routine with this declaration should
!                 be loaded before the stop test so that the correct
!                 length is used by  the loader.  This procedure  is
!                 not standard Fortran and may not work correctly on
!                 your   system (although  it  has  worked  on every
!                 system the authors have tried).  If ITOL is not 11
!                 then this common block is indeed standard Fortran.
! TOL    :IN       Double Precision.
!         Convergence criterion, as described above.
! ITMAX  :IN       Integer.
!         Maximum number of iterations.
! ITER   :IN       Integer.
!         The iteration for which to check for convergence.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as
!         defined by ITOL.  Letting norm() denote the Euclidean
!         norm, ERR is defined as follows..
!
!         If ITOL=0, then ERR = norm(SB*(B-A*X(L)))/norm(SB*B),
!                               for right or no preconditioning, and
!                         ERR = norm(SB*(M-inverse)*(B-A*X(L)))/
!                                norm(SB*(M-inverse)*B),
!                               for left preconditioning.
!         If ITOL=1, then ERR = norm(SB*(B-A*X(L)))/norm(SB*B),
!                               since right or no preconditioning
!                               being used.
!         If ITOL=2, then ERR = norm(SB*(M-inverse)*(B-A*X(L)))/
!                                norm(SB*(M-inverse)*B),
!                               since left preconditioning is being
!                               used.
!         If ITOL=3, then ERR =  Max  |(Minv*(B-A*X(L)))(i)/x(i)|
!                               i=1,n
!         If ITOL=11, then ERR = norm(SB*(X(L)-SOLN))/norm(SB*SOLN).
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration,
!         if this is desired for monitoring convergence.  If unit
!         number is 0, no writing will occur.
! R      :INOUT    Double Precision R(N).
!         Work array used in calling routine.  It contains
!         information necessary to compute the residual RL = B-A*XL.
! Z      :WORK     Double Precision Z(N).
!         Workspace used to hold the pseudo-residule M z = r.
! DZ     :WORK     Double Precision DZ(N).
!         Workspace used to hold temporary vector(s).
! RWORK  :WORK     Double Precision RWORK(USER DEFINABLE).
!         Double Precision array that can be used by MSOLVE.
! IWORK  :WORK     Integer IWORK(USER DEFINABLE).
!         Integer array that can be used by MSOLVE.
! RNRM   :IN       Double Precision.
!         Norm of the current residual.  Type of norm depends on ITOL.
! BNRM   :IN       Double Precision.
!         Norm of the right hand side.  Type of norm depends on ITOL.
! SB     :IN       Double Precision SB(N).
!         Scaling vector for B.
! SX     :IN       Double Precision SX(N).
!         Scaling vector for X.
! JSCAL  :IN       Integer.
!         Flag indicating if scaling arrays SB and SX are being
!         used in the calling routine DPIGMR.
!         JSCAL=0 means SB and SX are not used and the
!                 algorithm will perform as if all
!                 SB(i) = 1 and SX(i) = 1.
!         JSCAL=1 means only SX is used, and the algorithm
!                 performs as if all SB(i) = 1.
!         JSCAL=2 means only SB is used, and the algorithm
!                 performs as if all SX(i) = 1.
!         JSCAL=3 means both SB and SX are used.
! KMP    :IN       Integer
!         The number of previous vectors the new vector VNEW
!         must be made orthogonal to.  (KMP .le. MAXL)
! LGMR   :IN       Integer
!         The number of GMRES iterations performed on the current call
!         to DPIGMR (i.e., # iterations since the last restart) and
!         the current order of the upper hessenberg
!         matrix HES.
! MAXL   :IN       Integer
!         The maximum allowable order of the matrix H.
! MAXLP1 :IN       Integer
!         MAXPL1 = MAXL + 1, used for dynamic dimensioning of HES.
! V      :IN       Double Precision V(N,MAXLP1)
!         The N by (LGMR+1) array containing the LGMR
!         orthogonal vectors V(*,1) to V(*,LGMR).
! Q      :IN       Double Precision Q(2*MAXL)
!         A double precision array of length 2*MAXL containing the
!         components of the Givens rotations used in the QR
!         decomposition
!         of HES.
! SNORMW :IN       Double Precision
!         A scalar containing the scaled norm of VNEW before it
!         is renormalized in DPIGMR.
! PROD   :IN       Double Precision
!        The product s1*s2*...*sl = the product of the sines of the
!        givens rotations used in the QR factorization of
!        the hessenberg matrix HES.
! R0NRM  :IN       Double Precision
!         The scaled norm of initial residual R0.
! HES    :IN       Double Precision HES(MAXLP1,MAXL)
!         The upper triangular factor of the QR decomposition
!         of the (LGMR+1) by LGMR upper Hessenberg matrix whose
!         entries are the scaled inner-products of A*V(*,I)
!         and V(*,K).
! JPRE   :IN       Integer
!         Preconditioner type flag.
!
! *Description
!       When using the GMRES solver,  the preferred value  for ITOL
!       is 0.  This is due to the fact that when ITOL=0 the norm of
!       the residual required in the stopping test is  obtained for
!       free, since this value is already  calculated  in the GMRES
!       algorithm.   The  variable  RNRM contains the   appropriate
!       norm, which is equal to norm(SB*(RL - A*XL))  when right or
!       no   preconditioning is  being  performed,   and equal   to
!       norm(SB*Minv*(RL - A*XL))  when using left preconditioning.
!       Here, norm() is the Euclidean norm.  Nonzero values of ITOL
!       require  additional work  to  calculate the  actual  scaled
!       residual  or its scaled/preconditioned  form,  and/or   the
!       approximate solution XL.  Hence, these values of  ITOL will
!       not be as efficient as ITOL=0.
!
!***ROUTINES CALLED     MSOLVE, DNRM2, DCOPY,
!***END PROLOG ISDGMR
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER KMP, LGMR, MAXL, MAXLP1, JPRE, NMSL
      DOUBLE PRECISION DXNRM, RNRM, R0NRM, SNORMW, SOLNRM, PROD
      DOUBLE PRECISION B(*), X(*), IA(*), JA(*), A(*), R(*), Z(*), DZ(*)
      DOUBLE PRECISION RWORK(*), IWORK(*), SB(*), SX(*), Q(*), V(N,*)
      DOUBLE PRECISION HES(MAXLP1,MAXL), XL(*)
      EXTERNAL MSOLVE
      COMMON /SOLBLK/ SOLN(1)
      SAVE SOLNRM
!
!***FIRST EXECUTABLE STATEMENT ISDGMR
      ISDGMR = 0
      IF ( ITOL.EQ.0 ) THEN
!
!       Use input from DPIGMR to determine if stop conditions are met.
!
         ERR = RNRM/BNRM
      ENDIF
      IF ( (ITOL.GT.0) .AND. (ITOL.LE.3) ) THEN
!
!       Use DRLCAL to calculate the scaled residual vector.
!       Store answer in R.
!
         IF ( LGMR.NE.0 ) CALL DRLCAL(N, KMP, LGMR, MAXL, V, Q, R, &
                                      SNORMW, PROD, R0NRM)
         IF ( ITOL.LE.2 ) THEN
!         err = ||Residual||/||RightHandSide||(2-Norms).
            ERR = DNRM2(N, R, 1)/BNRM
!
!         Unscale R by R0NRM*PROD when KMP < MAXL.
!
            IF ( (KMP.LT.MAXL) .AND. (LGMR.NE.0) ) THEN
               TEM = 1.0D0/(R0NRM*PROD)
               CALL DSCAL(N, TEM, R, 1)
            ENDIF
         ELSEIF ( ITOL.EQ.3 ) THEN
!         err = Max |(Minv*Residual)(i)/x(i)|
!         When jpre .lt. 0, r already contains Minv*Residual.
            IF ( JPRE.GT.0 ) THEN
               CALL MSOLVE(N, R, DZ, NELT, IA, JA, A, ISYM, RWORK, &
                    IWORK)
               NMSL = NMSL + 1
            ENDIF
!
!         Unscale R by R0NRM*PROD when KMP < MAXL.
!
            IF ( (KMP.LT.MAXL) .AND. (LGMR.NE.0) ) THEN
               TEM = 1.0D0/(R0NRM*PROD)
               CALL DSCAL(N, TEM, R, 1)
            ENDIF
!
            FUZZ = D1MACH(1)
            IELMAX = 1
            RATMAX = ABS(DZ(1))/MAX(ABS(X(1)),FUZZ)
            DO 25 I = 2, N
               RAT = ABS(DZ(I))/MAX(ABS(X(I)),FUZZ)
               IF( RAT.GT.RATMAX ) THEN
                  IELMAX = I
                  RATMAX = RAT
               ENDIF
 25         CONTINUE
            ERR = RATMAX
            IF( RATMAX.LE.TOL ) ISDGMR = 1
            IF( IUNIT.GT.0 ) WRITE(IUNIT,1020) ITER, IELMAX, RATMAX
            RETURN
         ENDIF
      ENDIF
      IF ( ITOL.EQ.11 ) THEN
!
!       Use DXLCAL to calculate the approximate solution XL.
!
         IF ( (LGMR.NE.0) .AND. (ITER.GT.0) ) THEN
            CALL DXLCAL(N, LGMR, X, XL, XL, HES, MAXLP1, Q, V, R0NRM, &
                 DZ, SX, JSCAL, JPRE, MSOLVE, NMSL, RWORK, IWORK, &
                 NELT, IA, JA, A, ISYM)
         ELSEIF ( ITER.EQ.0 ) THEN
!         Copy X to XL to check if initial guess is good enough.
            CALL DCOPY(N, X, 1, XL, 1)
         ELSE
!         Return since this is the first call to DPIGMR on a restart.
            RETURN
         ENDIF
!
         IF ((JSCAL .EQ. 0) .OR.(JSCAL .EQ. 2)) THEN
!         err = ||x-TrueSolution||/||TrueSolution||(2-Norms).
            IF ( ITER.EQ.0 ) SOLNRM = DNRM2(N, SOLN, 1)
            DO 30 I = 1, N
               DZ(I) = XL(I) - SOLN(I)
 30         CONTINUE
            ERR = DNRM2(N, DZ, 1)/SOLNRM
         ELSE
            IF (ITER .EQ. 0) THEN
               SOLNRM = 0.D0
               DO 40 I = 1,N
                  SOLNRM = SOLNRM + (SX(I)*SOLN(I))**2
 40            CONTINUE
               SOLNRM = DSQRT(SOLNRM)
            ENDIF
            DXNRM = 0.D0
            DO 50 I = 1,N
               DXNRM = DXNRM + (SX(I)*(XL(I)-SOLN(I)))**2
 50         CONTINUE
            DXNRM = DSQRT(DXNRM)
!         err = ||SX*(x-TrueSolution)||/||SX*TrueSolution|| (2-Norms).
            ERR = DXNRM/SOLNRM
         ENDIF
      ENDIF
!
      IF( IUNIT.NE.0 ) THEN
         IF( ITER.EQ.0 ) THEN
            WRITE(IUNIT,1000) N, ITOL, MAXL, KMP
         ENDIF
         WRITE(IUNIT,1010) ITER, RNRM/BNRM, ERR
      ENDIF
      IF ( ERR.LE.TOL ) ISDGMR = 1
!
      RETURN
 1000 FORMAT(' Generalized Minimum Residual(',I3,I3,') for ', &
           'N, ITOL = ',I5, I5, &
           /' ITER','   Natral Err Est','   Error Estimate')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7)
 1020 FORMAT(1X,' ITER = ',I5, ' IELMAX = ',I5, &
           ' |R(IELMAX)/X(IELMAX)| = ',E12.5)
!------------- LAST LINE OF ISDGMR FOLLOWS ----------------------------
      END
!DECK DSMV
      SUBROUTINE DSMV( N, X, Y, NELT, IA, JA, A, ISYM )
!***BEGIN PROLOGUE  DSMV
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSMV-S),
!             Matrix Vector Multiply, Sparse
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP Column Format Sparse Matrix Vector Product.
!            Routine to calculate the sparse matrix vector product:
!            Y = A*X.
!***DESCRIPTION
! *Usage:
!     INTEGER  N, NELT, IA(NELT), JA(N+1), ISYM
!     DOUBLE PRECISION X(N), Y(N), A(NELT)
!
!     CALL DSMV(N, X, Y, NELT, IA, JA, A, ISYM )
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! X      :IN       Double Precision X(N).
!         The vector that should be multiplied by the matrix.
! Y      :OUT      Double Precision Y(N).
!         The product of the matrix and the vector.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(N+1).
! A      :IN       Integer A(NELT).
!         These arrays should hold the matrix A in the SLAP Column
!         format.  See "Description", below.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
!
! *Description
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a
!       column):
!
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       With  the SLAP  format  the "inner  loops" of  this  routine
!       should vectorize   on machines with   hardware  support  for
!       vector gather/scatter operations.  Your compiler may require
!       a  compiler directive  to  convince   it that there  are  no
!       implicit vector  dependencies.  Compiler directives  for the
!       Alliant FX/Fortran and CRI CFT/CFT77 compilers  are supplied
!       with the standard SLAP distribution.
!
! *Precision:           Double Precision
! *Cautions:
!     This   routine   assumes  that  the matrix A is stored in SLAP
!     Column format.  It does not check  for  this (for  speed)  and
!     evil, ugly, ornery and nasty things  will happen if the matrix
!     data  structure  is,  in fact, not SLAP Column.  Beware of the
!     wrong data structure!!!
!
! *See Also:
!       DSMTV
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSMV
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      DOUBLE PRECISION A(NELT), X(N), Y(N)
!
!         Zero out the result vector.
!***FIRST EXECUTABLE STATEMENT  DSMV

      Y(1:n) = 0.0D0

!
!         Multiply by A.
!
!VD$R NOCONCUR
      DO 30 ICOL = 1, N
         IBGN = JA(ICOL)
         IEND = JA(ICOL+1)-1
!LLL. OPTION ASSERT (NOHAZARD)
!DIR$ IVDEP
!VD$ NODEPCHK
         DO 20 I = IBGN, IEND
            Y(IA(I)) = Y(IA(I)) + A(I)*X(ICOL)
 20      CONTINUE
 30   CONTINUE
!
      IF( ISYM.EQ.1 ) THEN
!
!         The matrix is non-symmetric.  Need to get the other half in...
!         This loops assumes that the diagonal is the first entry in
!         each column.
!
         DO 50 IROW = 1, N
            JBGN = JA(IROW)+1
            JEND = JA(IROW+1)-1
            IF( JBGN.GT.JEND ) GOTO 50
            DO 40 J = JBGN, JEND
               Y(IROW) = Y(IROW) + A(J)*X(IA(J))
 40         CONTINUE
 50      CONTINUE
      ENDIF
      RETURN
!------------- LAST LINE OF DSMV FOLLOWS ----------------------------
      END
!DECK DSDS
      SUBROUTINE DSDS(N, NELT, IA, JA, A, ISYM, DINV)
!***BEGIN PROLOGUE  DSDS
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSDS-D),
!             SLAP Sparse, Diagonal
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Diagonal Scaling Preconditioner SLAP Set Up.
!            Routine to compute the inverse of the diagonal of a matrix
!            stored in the SLAP Column format.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
!     DOUBLE PRECISION A(NELT), DINV(N)
!
!     CALL DSDS( N, NELT, IA, JA, A, ISYM, DINV )
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! NELT   :IN       Integer.
!         Number of elements in arrays IA, JA, and A.
! IA     :INOUT    Integer IA(NELT).
! JA     :INOUT    Integer JA(NELT).
! A      :INOUT    Double Precision A(NELT).
!         These arrays should hold the matrix A in the SLAP Column
!         format.  See "Description", below.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! DINV   :OUT      Double Precision DINV(N).
!         Upon return this array holds 1./DIAG(A).
!
! *Description
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a
!       column):
!
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       With the SLAP  format  all  of  the   "inner  loops" of this
!       routine should vectorize  on  machines with hardware support
!       for vector   gather/scatter  operations.  Your compiler  may
!       require a compiler directive to  convince it that  there are
!       no  implicit  vector  dependencies.  Compiler directives for
!       the Alliant    FX/Fortran and CRI   CFT/CFT77 compilers  are
!       supplied with the standard SLAP distribution.
!
! *Precision:           Double Precision
!
! *Cautions:
!       This routine assumes that the diagonal of A is all  non-zero
!       and that the operation DINV = 1.0/DIAG(A) will not underflow
!       or overflow.    This  is done so that the  loop  vectorizes.
!       Matricies with zero or near zero or very  large entries will
!       have numerical difficulties  and  must  be fixed before this
!       routine is called.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSDS
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      DOUBLE PRECISION A(NELT), DINV(N)
!
!         Assume the Diagonal elements are the first in each column.
!         This loop should *VECTORIZE*.  If it does not you may have
!         to add a compiler directive.  We do not check for a zero
!         (or near zero) diagonal element since this would interfere
!         with vectorization.  If this makes you nervous put a check
!         in!  It will run much slower.
!***FIRST EXECUTABLE STATEMENT  DSDS
 1    CONTINUE
 
      DINV(1:n) = 1.0D0/A(JA(1:n))

!
      RETURN
!------------- LAST LINE OF DSDS FOLLOWS ----------------------------
      END
!DECK DSDI
      SUBROUTINE DSDI(N, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!***BEGIN PROLOGUE  DSDI
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213  (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSDI-S),
!             Linear system solve, Sparse, Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Diagonal Matrix Vector Multiply.
!            Routine to calculate the product  X = DIAG*B,
!            where DIAG is a diagonal matrix.
!***DESCRIPTION
! *Usage:
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Vector to multiply the diagonal by.
! X      :OUT      Double Precision X(N).
!         Result of DIAG*B.
! NELT   :DUMMY    Integer.
!         Retained for compatibility with SLAP MSOLVE calling sequence.
! IA     :DUMMY    Integer IA(NELT).
!         Retained for compatibility with SLAP MSOLVE calling sequence.
! JA     :DUMMY    Integer JA(N+1).
!         Retained for compatibility with SLAP MSOLVE calling sequence.
!  A     :DUMMY    Double Precision A(NELT).
!         Retained for compatibility with SLAP MSOLVE calling sequence.
! ISYM   :DUMMY    Integer.
!         Retained for compatibility with SLAP MSOLVE calling sequence.
! RWORK  :IN       Double Precision RWORK(USER DEFINABLE).
!         Work array holding the diagonal of some matrix to scale
!         B by.  This array must be set by the user or by a call
!         to the slap routine DSDS or DSD2S.  The length of RWORK
!         must be > IWORK(4)+N.
! IWORK  :IN       Integer IWORK(10).
!         IWORK(4) holds the offset into RWORK for the diagonal matrix
!         to scale B by.  This is usually set up by the SLAP pre-
!         conditioner setup routines DSDS or DSD2S.
!
! *Description:
!         This routine is supplied with the SLAP package to perform
!         the  MSOLVE  operation for iterative drivers that require
!         diagonal  Scaling  (e.g., DSDCG, DSDBCG).   It  conforms
!         to the SLAP MSOLVE CALLING CONVENTION  and hence does not
!         require an interface routine as do some of the other pre-
!         conditioners supplied with SLAP.
!
! *Precision:           Double Precision
! *See Also:
!       DSDS, DSD2S
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSDI
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, IWORK(10)
      DOUBLE PRECISION B(N), X(N), A(NELT), RWORK(1)
!
!         Determine where the inverse of the diagonal
!         is in the work array and then scale by it.
!***FIRST EXECUTABLE STATEMENT  DSDI
      LOCD = IWORK(4) - 1
      DO I = 1, N
         X(I) = RWORK(LOCD+I)*B(I)
      end do
      RETURN
!------------- LAST LINE OF DSDI FOLLOWS ----------------------------
      END
!deck xerrwv
      subroutine xerrwv(messg,nmessg,nerr,level,ni,i1,i2,nr,r1,r2)
!***begin prologue  xerrwv
!***date written   800319   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  process an error message allowing 2 integer and 2 real
!            values to be included in the message.
!***description
!
!     abstract
!        xerrwv processes a diagnostic message, in a manner
!        determined by the value of level and the current value
!        of the library error control flag, kontrl.
!        (see subroutine xsetf for details.)
!        in addition, up to two integer values and two real
!        values may be printed along with the message.
!
!     description of parameters
!      --input--
!        messg - the hollerith message to be processed.
!        nmessg- the actual number of characters in messg.
!        nerr  - the error number associated with this message.
!                nerr must not be zero.
!        level - error category.
!                =2 means this is an unconditionally fatal error.
!                =1 means this is a recoverable error.  (i.e., it is
!                   non-fatal if xsetf has been appropriately called.)
!                =0 means this is a warning message only.
!                =-1 means this is a warning message which is to be
!                   printed at most once, regardless of how many
!                   times this call is executed.
!        ni    - number of integer values to be printed. (0 to 2)
!        i1    - first integer value.
!        i2    - second integer value.
!        nr    - number of real values to be printed. (0 to 2)
!        r1    - first real value.
!        r2    - second real value.
!
!     examples
!        call xerrwv('smooth -- num (=i1) was zero.',29,1,2,
!    1   1,num,0,0,0.,0.)
!        call xerrwv('quadxy -- requested error (r1) less than minimum (
!    1r2).,54,77,1,0,0,0,2,errreq,errmin)
!
!     latest revision ---  1 august 1985
!     written by ron jones, with slatec common math library subcommittee
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  fdump,i1mach,j4save,xerabt,xerctl,xerprt,xersav,
!                    xgetua
!***end prologue  xerrwv
      character*(*) messg
      character*20 lfirst
      character*37 form
      dimension lun(5)
!     get flags
!***first executable statement  xerrwv
      lkntrl = j4save(2,0,.false.)
      maxmes = j4save(4,0,.false.)
!     check for valid input
      if ((nmessg.gt.0).and.(nerr.ne.0).and. &
          (level.ge.(-1)).and.(level.le.2)) go to 10
         if (lkntrl.gt.0) call xerprt('fatal error in...',17)
         call xerprt('xerror -- invalid input',23)
!        if (lkntrl.gt.0) call fdump
         if (lkntrl.gt.0) call xerprt('job abort due to fatal error.', &
        29)
         if (lkntrl.gt.0) call xersav(' ',0,0,0,kdummy)
         call xerabt('xerror -- invalid input',23)
         return
   10 continue
!     record message
      junk = j4save(1,nerr,.true.)
      call xersav(messg,nmessg,nerr,level,kount)
!     let user override
      lfirst = messg
      lmessg = nmessg
      lerr = nerr
      llevel = level
      call xerctl(lfirst,lmessg,lerr,llevel,lkntrl)
!     reset to original values
      lmessg = nmessg
      lerr = nerr
      llevel = level
      lkntrl = max0(-2,min0(2,lkntrl))
      mkntrl = iabs(lkntrl)
!     decide whether to print message
      if ((llevel.lt.2).and.(lkntrl.eq.0)) go to 100
      if (((llevel.eq.(-1)).and.(kount.gt.min0(1,maxmes))) &
      .or.((llevel.eq.0)   .and.(kount.gt.maxmes)) &
      .or.((llevel.eq.1)   .and.(kount.gt.maxmes).and.(mkntrl.eq.1)) &
      .or.((llevel.eq.2)   .and.(kount.gt.max0(1,maxmes)))) go to 100
         if (lkntrl.le.0) go to 20
            call xerprt(' ',1)
!           introduction
            if (llevel.eq.(-1)) call xerprt &
      ('warning message...this message will only be printed once.',57)
            if (llevel.eq.0) call xerprt('warning in...',13)
            if (llevel.eq.1) call xerprt &
            ('recoverable error in...',23)
            if (llevel.eq.2) call xerprt('fatal error in...',17)
   20    continue
!        message
         call xerprt(messg,lmessg)
         call xgetua(lun,nunit)
         isizei = log10(float(i1mach(9))) + 1.0
         isizef = log10(float(i1mach(10))**i1mach(11)) + 1.0
         do 50 kunit=1,nunit
            iunit = lun(kunit)
            if (iunit.eq.0) iunit = i1mach(4)
            do 22 i=1,min(ni,2)
               write (form,21) i,isizei
   21          format ('(11x,21hin above message, i',i1,'=,i',i2,')   ')
               if (i.eq.1) write (iunit,form) i1
               if (i.eq.2) write (iunit,form) i2
   22       continue
            do 24 i=1,min(nr,2)
               write (form,23) i,isizef+10,isizef
   23          format ('(11x,21hin above message, r',i1,'=,e', &
               i2,'.',i2,')')
               if (i.eq.1) write (iunit,form) r1
               if (i.eq.2) write (iunit,form) r2
   24       continue
            if (lkntrl.le.0) go to 40
!              error number
               write (iunit,30) lerr
   30          format (15h error number =,i10)
   40       continue
   50    continue
!        trace-back
!        if (lkntrl.gt.0) call fdump
  100 continue
      ifatal = 0
      if ((llevel.eq.2).or.((llevel.eq.1).and.(mkntrl.eq.2))) &
      ifatal = 1
!     quit here if message is not fatal
      if (ifatal.le.0) return
      if ((lkntrl.le.0).or.(kount.gt.max0(1,maxmes))) go to 120
!        print reason for abort
         if (llevel.eq.1) call xerprt &
         ('job abort due to unrecovered error.',35)
         if (llevel.eq.2) call xerprt &
         ('job abort due to fatal error.',29)
!        print error summary
         call xersav(' ',-1,0,0,kdummy)
  120 continue
!     abort
      if ((llevel.eq.2).and.(kount.gt.max0(1,maxmes))) lmessg = 0
      call xerabt(messg,lmessg)
      return
      end
!deck xersav
      subroutine xersav(messg,nmessg,nerr,level,icount)
!***begin prologue  xersav
!***date written   800319   (yymmdd)
!***revision date  851213   (yymmdd)
!***category no.  r3
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  record that an error has occurred.
!***description
!
!     abstract
!        record that this error occurred.
!
!     description of parameters
!     --input--
!       messg, nmessg, nerr, level are as in xerror,
!       except that when nmessg=0 the tables will be
!       dumped and cleared, and when nmessg is less than zero the
!       tables will be dumped and not cleared.
!     --output--
!       icount will be the number of times this message has
!       been seen, or zero if the table has overflowed and
!       does not contain this message specifically.
!       when nmessg=0, icount will not be altered.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  1 august 1985
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  i1mach,xgetua
!***end prologue  xersav
      integer lun(5)
      character*(*) messg
      character*20 mestab(10),mes
      dimension nertab(10),levtab(10),kount(10)
      save mestab,nertab,levtab,kount,kountx
!     next two data statements are necessary to provide a blank
!     error table initially
      data kount(1),kount(2),kount(3),kount(4),kount(5), &
           kount(6),kount(7),kount(8),kount(9),kount(10) &
           /0,0,0,0,0,0,0,0,0,0/
      data kountx/0/
!***first executable statement  xersav
      if (nmessg.gt.0) go to 80
!     dump the table
         if (kount(1).eq.0) return
!        print to each unit
         call xgetua(lun,nunit)
         do 60 kunit=1,nunit
            iunit = lun(kunit)
            if (iunit.eq.0) iunit = i1mach(4)
!           print table header
            write (iunit,10)
   10       format (32h0          error message summary/ &
            51h message start             nerr     level     count)
!           print body of table
            do 20 i=1,10
               if (kount(i).eq.0) go to 30
               write (iunit,15) mestab(i),nertab(i),levtab(i),kount(i)
   15          format (1x,a20,3i10)
   20       continue
   30       continue
!           print number of other errors
            if (kountx.ne.0) write (iunit,40) kountx
   40       format (41h0other errors not individually tabulated=,i10)
            write (iunit,50)
   50       format (1x)
   60    continue
         if (nmessg.lt.0) return
!        clear the error tables
         do 70 i=1,10
   70       kount(i) = 0
         kountx = 0
         return
   80 continue
!     process a message...
!     search for this messg, or else an empty slot for this messg,
!     or else determine that the error table is full.
      mes = messg
      do 90 i=1,10
         ii = i
         if (kount(i).eq.0) go to 110
         if (mes.ne.mestab(i)) go to 90
         if (nerr.ne.nertab(i)) go to 90
         if (level.ne.levtab(i)) go to 90
         go to 100
   90 continue
!     three possible cases...
!     table is full
         kountx = kountx+1
         icount = 1
         return
!     message found in table
  100    kount(ii) = kount(ii) + 1
         icount = kount(ii)
         return
!     empty slot found for new message
  110    mestab(ii) = mes
         nertab(ii) = nerr
         levtab(ii) = level
         kount(ii)  = 1
         icount = 1
         return
      end
      subroutine xgetf(kontrl)
!***begin prologue  xgetf
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  return the current value of the error control flag.
!***description
!
!   abstract
!        xgetf returns the current value of the error control flag
!        in kontrl.  see subroutine xsetf for flag value meanings.
!        (kontrl is an output parameter only.)
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  7 june 1978
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xgetf
!***first executable statement  xgetf
      kontrl = j4save(2,0,.false.)
      return
      end
!deck xgetua
      subroutine xgetua(iunita,n)
!***begin prologue  xgetua
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  return unit number(s) to which error messages are being
!            sent.
!***description
!
!     abstract
!        xgetua may be called to determine the unit number or numbers
!        to which error messages are being sent.
!        these unit numbers may have been set by a call to xsetun,
!        or a call to xsetua, or may be a default value.
!
!     description of parameters
!      --output--
!        iunit - an array of one to five unit numbers, depending
!                on the value of n.  a value of zero refers to the
!                default unit, as defined by the i1mach machine
!                constant routine.  only iunit(1),...,iunit(n) are
!                defined by xgetua.  the values of iunit(n+1),...,
!                iunit(5) are not defined (for n .lt. 5) or altered
!                in any way by xgetua.
!        n     - the number of units to which copies of the
!                error messages are being sent.  n will be in the
!                range from 1 to 5.
!
!     latest revision ---  19 mar 1980
!     written by ron jones, with slatec common math library subcommittee
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xgetua
      dimension iunita(5)
!***first executable statement  xgetua
      n = j4save(5,0,.false.)
      do 30 i=1,n
         index = i+4
         if (i.eq.1) index = 3
         iunita(i) = j4save(index,0,.false.)
   30 continue
      return
      end
!deck j4save
      function j4save(iwhich,ivalue,iset)
!***begin prologue  j4save
!***refer to  xerror
!***routines called  (none)
!***description
!
!     abstract
!        j4save saves and recalls several global variables needed
!        by the library error handling routines.
!
!     description of parameters
!      --input--
!        iwhich - index of item desired.
!                = 1 refers to current error number.
!                = 2 refers to current error control flag.
!                 = 3 refers to current unit number to which error
!                    messages are to be sent.  (0 means use standard.)
!                 = 4 refers to the maximum number of times any
!                     message is to be printed (as set by xermax).
!                 = 5 refers to the total number of units to which
!                     each error message is to be written.
!                 = 6 refers to the 2nd unit for error messages
!                 = 7 refers to the 3rd unit for error messages
!                 = 8 refers to the 4th unit for error messages
!                 = 9 refers to the 5th unit for error messages
!        ivalue - the value to be set for the iwhich-th parameter,
!                 if iset is .true. .
!        iset   - if iset=.true., the iwhich-th parameter will be
!                 given the value, ivalue.  if iset=.false., the
!                 iwhich-th parameter will be unchanged, and ivalue
!                 is a dummy parameter.
!      --output--
!        the (old) value of the iwhich-th parameter will be returned
!        in the function value, j4save.
!
!     written by ron jones, with slatec common math library subcommittee
!    adapted from bell laboratories port library error handler
!     latest revision ---  1 august 1985
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***end prologue  j4save
      logical iset
      integer iparam(9)
      save iparam
      data iparam(1),iparam(2),iparam(3),iparam(4)/0,2,0,10/
      data iparam(5)/1/
      data iparam(6),iparam(7),iparam(8),iparam(9)/0,0,0,0/
!***first executable statement  j4save
      j4save = iparam(iwhich)
      if (iset) iparam(iwhich) = ivalue
      return
      end
!deck xerclr
      subroutine xerclr
!***begin prologue  xerclr
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  reset current error number to zero.
!***description
!
!     abstract
!        this routine simply resets the current error number to zero.
!        this may be necessary to do in order to determine that
!        a certain error has occurred again since the last time
!        numxer was referenced.
!
!     written by ron jones, with slatec common math library subcommittee
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xerclr
!***first executable statement  xerclr
      junk = j4save(1,0,.true.)
      return
      end
      subroutine xerdmp
!***begin prologue  xerdmp
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  print the error tables and then clear them.
!***description
!
!     abstract
!        xerdmp prints the error tables, then clears them.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  7 june 1978
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  xersav
!***end prologue  xerdmp
!***first executable statement  xerdmp
      call xersav(' ',0,0,0,kount)
      return
      end
      subroutine xermax(max)
!***begin prologue  xermax
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  set maximum number of times any error message is to be
!            printed.
!***description
!
!     abstract
!        xermax sets the maximum number of times any message
!        is to be printed.  that is, non-fatal messages are
!        not to be printed after they have occured max times.
!        such non-fatal messages may be printed less than
!        max times even if they occur max times, if error
!        suppression mode (kontrl=0) is ever in effect.
!
!     description of parameter
!      --input--
!        max - the maximum number of times any one message
!              is to be printed.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  7 june 1978
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xermax
!***first executable statement  xermax
      junk = j4save(4,max,.true.)
      return
      end
      subroutine xgetun(iunit)
!***begin prologue  xgetun
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  return the (first) output file to which error messages
!            are being sent.
!***description
!
!     abstract
!        xgetun gets the (first) output file to which error messages
!        are being sent.  to find out if more than one file is being
!        used, one must use the xgetua routine.
!
!     description of parameter
!      --output--
!        iunit - the logical unit number of the  (first) unit to
!                which error messages are being sent.
!                a value of zero means that the default file, as
!                defined by the i1mach routine, is being used.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision --- 23 may 1979
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xgetun
!***first executable statement  xgetun
      iunit = j4save(3,0,.false.)
      return
      end
      subroutine xsetf(kontrl)
!***begin prologue  xsetf
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3a
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  set the error control flag.
!***description
!
!     abstract
!        xsetf sets the error control flag value to kontrl.
!        (kontrl is an input parameter only.)
!        the following table shows how each message is treated,
!        depending on the values of kontrl and level.  (see xerror
!        for description of level.)
!
!        if kontrl is zero or negative, no information other than the
!        message itself (including numeric values, if any) will be
!        printed.  if kontrl is positive, introductory messages,
!        trace-backs, etc., will be printed in addition to the message.
!
!              iabs(kontrl)
!        level        0              1              2
!        value
!          2        fatal          fatal          fatal
!
!          1     not printed      printed         fatal
!
!          0     not printed      printed        printed
!
!         -1     not printed      printed        printed
!                                  only           only
!                                  once           once
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  19 mar 1980
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save,xerrwv
!***end prologue  xsetf
!***first executable statement  xsetf
      if ((kontrl.ge.(-2)).and.(kontrl.le.2)) go to 10
         call xerrwv('xsetf  -- invalid value of kontrl (i1).',33,1,2, &
        1,kontrl,0,0,0.,0.)
         return
   10 junk = j4save(2,kontrl,.true.)
      return
      end
      subroutine xsetua(iunita,n)
!***begin prologue  xsetua
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3b
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  set logical unit numbers (up to 5) to which error
!            messages are to be sent.
!***description
!
!     abstract
!        xsetua may be called to declare a list of up to five
!        logical units, each of which is to receive a copy of
!        each error message processed by this package.
!        the purpose of xsetua is to allow simultaneous printing
!        of each error message on, say, a main output file,
!        an interactive terminal, and other files such as graphics
!        communication files.
!
!     description of parameters
!      --input--
!        iunit - an array of up to five unit numbers.
!                normally these numbers should all be different
!                (but duplicates are not prohibited.)
!        n     - the number of unit numbers provided in iunit
!                must have 1 .le. n .le. 5.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  19 mar 1980
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save,xerrwv
!***end prologue  xsetua
      dimension iunita(5)
!***first executable statement  xsetua
      if ((n.ge.1).and.(n.le.5)) go to 10
         call xerrwv('xsetua -- invalid value of n (i1).',34,1,2, &
        1,n,0,0,0.,0.)
         return
   10 continue
      do 20 i=1,n
         index = i+4
         if (i.eq.1) index = 3
         junk = j4save(index,iunita(i),.true.)
   20 continue
      junk = j4save(5,n,.true.)
      return
      end
      subroutine xsetun(iunit)
!***begin prologue  xsetun
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3b
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  set output file to which error messages are to be sent.
!***description
!
!     abstract
!        xsetun sets the output file to which error messages are to
!        be sent.  only one file will be used.  see xsetua for
!        how to declare more than one file.
!
!     description of parameter
!      --input--
!        iunit - an input parameter giving the logical unit number
!                to which error messages are to be sent.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  7 june 1978
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xsetun
!***first executable statement  xsetun
      junk = j4save(3,iunit,.true.)
      junk = j4save(5,1,.true.)
      return
      end
      FUNCTION RAND(R)
!***BEGIN PROLOGUE  RAND
!***DATE WRITTEN   770401   (YYMMDD)
!***REVISION DATE  861211   (YYMMDD)
!***CATEGORY NO.  L6A21
!***KEYWORDS  LIBRARY=SLATEC(FNLIB),TYPE=SINGLE PRECISION(RAND-S),
!             RANDOM NUMBER,SPECIAL FUNCTIONS,UNIFORM
!***AUTHOR  FULLERTON, W., (LANL)
!***PURPOSE  Generates a uniformly distributed random number.
!***DESCRIPTION
!
!      This pseudo-random number generator is portable among a wide
! variety of computers.  RAND(R) undoubtedly is not as good as many
! readily available installation dependent versions, and so this
! routine is not recommended for widespread usage.  Its redeeming
! feature is that the exact same random numbers (to within final round-
! off error) can be generated from machine to machine.  Thus, programs
! that make use of random numbers can be easily transported to and
! checked in a new environment.
!      The random numbers are generated by the linear congruential
! method described, e.g., by Knuth in Seminumerical Methods (p.9),
! Addison-Wesley, 1969.  Given the I-th number of a pseudo-random
! sequence, the I+1 -st number is generated from
!             X(I+1) = (A*X(I) + C) MOD M,
! where here M = 2**22 = 4194304, C = 1731 and several suitable values
! of the multiplier A are discussed below.  Both the multiplier A and
! random number X are represented in double precision as two 11-bit
! words.  The constants are chosen so that the period is the maximum
! possible, 4194304.
!      In order that the same numbers be generated from machine to
! machine, it is necessary that 23-bit integers be reducible modulo
! 2**11 exactly, that 23-bit integers be added exactly, and that 11-bit
! integers be multiplied exactly.  Furthermore, if the restart option
! is used (where R is between 0 and 1), then the product R*2**22 =
! R*4194304 must be correct to the nearest integer.
!      The first four random numbers should be .0004127026,
! .6750836372, .1614754200, and .9086198807.  The tenth random number
! is .5527787209, and the hundredth is .3600893021 .  The thousandth
! number should be .2176990509 .
!      In order to generate several effectively independent sequences
! with the same generator, it is necessary to know the random number
! for several widely spaced calls.  The I-th random number times 2**22,
! where I=K*P/8 and P is the period of the sequence (P = 2**22), is
! still of the form L*P/8.  In particular we find the I-th random
! number multiplied by 2**22 is given by
! I   =  0  1*P/8  2*P/8  3*P/8  4*P/8  5*P/8  6*P/8  7*P/8  8*P/8
! RAND=  0  5*P/8  2*P/8  7*P/8  4*P/8  1*P/8  6*P/8  3*P/8  0
! Thus the 4*P/8 = 2097152 random number is 2097152/2**22.
!      Several multipliers have been subjected to the spectral test
! (see Knuth, p. 82).  Four suitable multipliers roughly in order of
! goodness according to the spectral test are
!    3146757 = 1536*2048 + 1029 = 2**21 + 2**20 + 2**10 + 5
!    2098181 = 1024*2048 + 1029 = 2**21 + 2**10 + 5
!    3146245 = 1536*2048 +  517 = 2**21 + 2**20 + 2**9 + 5
!    2776669 = 1355*2048 + 1629 = 5**9 + 7**7 + 1
!
!      In the table below LOG10(NU(I)) gives roughly the number of
! random decimal digits in the random numbers considered I at a time.
! C is the primary measure of goodness.  In both cases bigger is better.
!
!                   LOG10 NU(I)              C(I)
!       A       I=2  I=3  I=4  I=5    I=2  I=3  I=4  I=5
!
!    3146757    3.3  2.0  1.6  1.3    3.1  1.3  4.6  2.6
!    2098181    3.3  2.0  1.6  1.2    3.2  1.3  4.6  1.7
!    3146245    3.3  2.2  1.5  1.1    3.2  4.2  1.1  0.4
!    2776669    3.3  2.1  1.6  1.3    2.5  2.0  1.9  2.6
!   Best
!    Possible   3.3  2.3  1.7  1.4    3.6  5.9  9.7  14.9
!
!             Input Argument --
! R      If R=0., the next random number of the sequence is generated.
!        If R .LT. 0., the last generated number will be returned for
!          possible use in a restart procedure.
!        If R .GT. 0., the sequence of random numbers will start with
!          the seed R mod 1.  This seed is also returned as the value of
!          RAND provided the arithmetic is done exactly.
!
!             Output Value --
! RAND   a pseudo-random number between 0. and 1.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  RAND
      SAVE IA1, IA0, IA1MA0, IC, IX1, IX0
      DATA IA1, IA0, IA1MA0 /1536, 1029, 507/
      DATA IC /1731/
      DATA IX1, IX0 /0, 0/
!***FIRST EXECUTABLE STATEMENT  RAND
      IF (R.LT.0.) GO TO 10
      IF (R.GT.0.) GO TO 20
!
!           A*X = 2**22*IA1*IX1 + 2**11*(IA1*IX1 + (IA1-IA0)*(IX0-IX1)
!                   + IA0*IX0) + IA0*IX0
!
      IY0 = IA0*IX0
      IY1 = IA1*IX1 + IA1MA0*(IX0-IX1) + IY0
      IY0 = IY0 + IC
      IX0 = MOD (IY0, 2048)
      IY1 = IY1 + (IY0-IX0)/2048
      IX1 = MOD (IY1, 2048)
!
 10   RAND = IX1*2048 + IX0
      RAND = RAND / 4194304.
      RETURN
!
 20   IX1 = AMOD(R,1.)*4194304. + 0.5
      IX0 = MOD (IX1, 2048)
      IX1 = (IX1-IX0)/2048
      GO TO 10
!
      END
subroutine timestamp ( )

!*******************************************************************************
!
!! TIMESTAMP prints the current YMDHMS date as a time stamp.
!
!  Example:
!
!    May 31 2001   9:45:54.872 AM
!
!  Modified:
!
!    15 March 2003
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    None
!
  implicit none
!
  character ( len = 40 ) string
!
  call timestring ( string )

  write ( *, '(a)' ) trim ( string )

  return
end
subroutine timestring ( string )

!*******************************************************************************
!
!! TIMESTRING writes the current YMDHMS date into a string.
!
!  Example:
!
!    STRING = 'May 31 2001   9:45:54.872 AM'
!
!  Modified:
!
!    15 March 2003
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Output, character ( len = * ) STRING, contains the date information.
!    A character length of 40 should always be sufficient.
!
  implicit none
!
  character ( len = 8 ) ampm
  integer d
  character ( len = 8 ) date
  integer h
  integer m
  integer mm
  character ( len = 9 ), parameter, dimension(12) :: month = (/ &
    'January  ', 'February ', 'March    ', 'April    ', &
    'May      ', 'June     ', 'July     ', 'August   ', &
    'September', 'October  ', 'November ', 'December ' /)
  integer n
  integer s
  character ( len = * ) string
  character ( len = 10 ) time
  integer values(8)
  integer y
  character ( len = 5 ) zone
!
  call date_and_time ( date, time, zone, values )

  y = values(1)
  m = values(2)
  d = values(3)
  h = values(5)
  n = values(6)
  s = values(7)
  mm = values(8)

  if ( h < 12 ) then
    ampm = 'AM'
  else if ( h == 12 ) then
    if ( n == 0 .and. s == 0 ) then
      ampm = 'Noon'
    else
      ampm = 'PM'
    end if
  else
    h = h - 12
    if ( h < 12 ) then
      ampm = 'PM'
    else if ( h == 12 ) then
      if ( n == 0 .and. s == 0 ) then
        ampm = 'Midnight'
      else
        ampm = 'AM'
      end if
    end if
  end if

  write ( string, '(a,1x,i2,1x,i4,2x,i2,a1,i2.2,a1,i2.2,a1,i3.3,1x,a)' ) &
    trim ( month(m) ), d, y, h, ':', n, ':', s, '.', mm, trim ( ampm )

  return
end
function d1mach ( i )

!*****************************************************************************80
!
!! D1MACH returns double precision real machine constants.
!
!  Discussion:
!
!    Assuming that the internal representation of a double precision real
!    number is in base B, with T the number of base-B digits in the mantissa,
!    and EMIN the smallest possible exponent and EMAX the largest possible
!    exponent, then
!
!      D1MACH(1) = B**(EMIN-1), the smallest positive magnitude.
!      D1MACH(2) = B**EMAX*(1-B**(-T)), the largest magnitude.
!      D1MACH(3) = B**(-T), the smallest relative spacing.
!      D1MACH(4) = B**(1-T), the largest relative spacing.
!      D1MACH(5) = log10(B).
!
!  Modified:
!
!    24 April 2007
!
!  Author:
!
!    Phyllis Fox, Andrew Hall, Norman Schryer
!
!  Reference:
!
!    Phyllis Fox, Andrew Hall, Norman Schryer,
!    Algorithm 528:
!    Framework for a Portable Library,
!    ACM Transactions on Mathematical Software,
!    Volume 4, Number 2, June 1978, page 176-188.
!
!  Parameters:
!
!    Input, integer I, chooses the parameter to be returned.
!    1 <= I <= 5.
!
!    Output, real ( kind = 8 ) D1MACH, the value of the chosen parameter.
!
  implicit none

  real ( kind = 8 ) d1mach
  integer i

  if ( i < 1 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'D1MACH - Fatal error!'
    write ( *, '(a)' ) '  The input argument I is out of bounds.'
    write ( *, '(a)' ) '  Legal values satisfy 1 <= I <= 5.'
    write ( *, '(a,i12)' ) '  I = ', i
    d1mach = 0.0D+00
    stop
  else if ( i == 1 ) then
    d1mach = 4.450147717014403D-308
  else if ( i == 2 ) then
    d1mach = 8.988465674311579D+307
  else if ( i == 3 ) then
    d1mach = 1.110223024625157D-016
  else if ( i == 4 ) then
    d1mach = 2.220446049250313D-016
  else if ( i == 5 ) then
    d1mach = 0.301029995663981D+000
  else if ( 5 < i ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'D1MACH - Fatal error!'
    write ( *, '(a)' ) '  The input argument I is out of bounds.'
    write ( *, '(a)' ) '  Legal values satisfy 1 <= I <= 5.'
    write ( *, '(a,i12)' ) '  I = ', i
    d1mach = 0.0D+00
    stop
  end if

  return
end
function i1mach ( i )

!***********************************************************************
!
!! I1MACH returns integer machine constants.
!
!  Discussion:
!
!    Input/output unit numbers.
!
!      I1MACH(1) = the standard input unit.
!      I1MACH(2) = the standard output unit.
!      I1MACH(3) = the standard punch unit.
!      I1MACH(4) = the standard error message unit.
!
!    Words.
!
!      I1MACH(5) = the number of bits per integer storage unit.
!      I1MACH(6) = the number of characters per integer storage unit.
!
!    Integers.
!
!    Assume integers are represented in the S digit base A form:
!
!      Sign * (X(S-1)*A**(S-1) + ... + X(1)*A + X(0))
!
!    where 0 <= X(1:S-1) < A.
!
!      I1MACH(7) = A, the base.
!      I1MACH(8) = S, the number of base A digits.
!      I1MACH(9) = A**S-1, the largest integer.
!
!    Floating point numbers
!
!    Assume floating point numbers are represented in the T digit 
!    base B form:
!
!      Sign * (B**E) * ((X(1)/B) + ... + (X(T)/B**T) )
!
!    where 0 <= X(I) < B for I=1 to T, 0 < X(1) and EMIN <= E <= EMAX.
!
!      I1MACH(10) = B, the base.
!
!    Single precision
!
!      I1MACH(11) = T, the number of base B digits.
!      I1MACH(12) = EMIN, the smallest exponent E.
!      I1MACH(13) = EMAX, the largest exponent E.
!
!    Double precision
!
!      I1MACH(14) = T, the number of base B digits.
!      I1MACH(15) = EMIN, the smallest exponent E.
!      I1MACH(16) = EMAX, the largest exponent E.
!
!    To alter this function for a particular environment, the desired set of 
!    statements should be activated. 
!
!    On rare machines, a STATIC statement may need to be added, but 
!    probably more systems prohibit than require it.
!
!    Also, the values of I1MACH(1) through I1MACH(4) should be
!    checked for consistency with the local operating system.   
!
!    For IEEE-arithmetic machines (binary standard), the first set of
!    constants below should be appropriate, except perhaps for 
!    IMACH(1) - IMACH(4).
!
!  Modified:
!
!    18 April 2004
!
!  Reference:
!
!    P A Fox, A D Hall, N L Schryer,
!    Algorithm 528,
!    Framework for a Portable Library,
!    ACM Transactions on Mathematical Software,
!    Volume 4, number 2, page 176-188.
!
!  Parameters:
!
!    Input, integer I, chooses the parameter to be returned.
!    1 <= I <= 16.
!
!    Output, integer I1MACH, the value of the chosen parameter.
!
  implicit none
!
  integer i
  integer i1mach
!
!  All IEEE machines:
!
!    AT&T 3B series
!    AT&T PC 6300
!    AT&T PC 7300
!    DEC/COMPAQ/HP ALPHA
!    DEC PMAX
!    HP RISC-chip machines
!    IBM PC
!    IBM RISC-chip machines
!    Intel 80x87 machines
!    Motorola 68000 machines
!    NEXT
!    SGI Iris
!    SPARC RISC-chip machines
!    SUN 3
!
  integer, parameter, dimension ( 16 ) :: imach = (/ &
    5, 6, 7, 6, 32, 4, 2, 31, 2147483647, 2, &
    24, -125, 128, 53, -1021, 1024 /)
!
!  ALLIANT FX/8 UNIX FORTRAN compiler:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 6, 0, 32, 4, 2, 32, 2147483647, 2, &
!    24, -126, 128, 53, -1022, 1024 /)
!
!  AMDAHL machines:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 32, 4, 2, 31, 2147483647, 16, &
!    6, -64, 63, 14, -64, 63 /)
!
!  BURROUGHS 1700 system:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    7, 2, 2, 2, 36, 4, 2, 33, Z1FFFFFFFF, 2, &
!    24, -256, 255, 60, -256, 255 /)
!
!  BURROUGHS 5700 system:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 48, 6, 2, 39, O0007777777777777, 8, &
!    13, -50, 76, 26, -50, 76 /)
!
!  BURROUGHS 6700/7700 systems:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 48, 6, 2, 39, O0007777777777777, 8, &
!    13, -50, 76, 26, -32754, 32780 /)
!
!  CDC CYBER 170/180 series using NOS:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 60, 10, 2, 48, O"00007777777777777777", 2, &
!    48, -974, 1070, 96, -927, 1070 /)
!
!  CDC CYBER 170/180 series using NOS/VE:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 64, 8, 2, 63, 9223372036854775807, 2, &
!    47, -4095, 4094, 94, -4095, 4094 /)
!
!  CDC CYBER 200 series:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 64, 8, 2, 47, X'00007FFFFFFFFFFF', 2, &
!    47, -28625, 28718, 94, -28625, 28718 /)
!
!  CDC 6000/7000 series using FTN4:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 60, 10, 2, 48, 00007777777777777777B, 2, &
!    47, -929, 1070, 94, -929, 1069 /)
!
!  CDC 6000/7000 series using FTN5:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 60, 10, 2, 48, O"00007777777777777777", 2, &
!    47, -929, 1070, 94, -929, 1069 /)
!
!  CONVEX C-1:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 32, 4, 2, 31, 2147483647, 2, &
!    24, -128, 127, 53, -1024, 1023 /)
!
!  CONVEX C-120 (native mode) without -R8 option:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 0, 6, 32, 4, 2, 31, 2147483647, 2, &
!    24, -127, 127, 53, -1023, 1023 /)
!
!  CONVEX C-120 (native mode) with -R8 option:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 0, 6, 32, 4, 2, 31, 2147483647, 2, &
!    53, -1023, 1023, 53, -1023, 1023 /)
!
!  CONVEX C-120 (IEEE mode) without -R8 option:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 0, 6, 32, 4, 2, 31, 2147483647, 2,
!    24, -125, 128, 53, -1021, 1024 /)
!
!  CONVEX C-120 (IEEE mode) with -R8 option:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 0, 6, 32, 4, 2, 31, 2147483647, 2,
!    53, -1021, 1024, 53, -1021, 1024 /)
!
!  CRAY 1, 2, 3, XMP, YMP, and C90:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 102, 6, 64, 8, 2, 63, 777777777777777777777B, 2, &
!    47, -8189, 8190, 94, -8099, 8190 /)
!
!  DATA GENERAL ECLIPSE S/200:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    11, 12, 8, 10, 16, 2, 2, 15, 32767, 16, &
!    6, -64, 63, 14, -64, 63 /)
!
!  ELXSI 6400:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 6, 6, 32, 4, 2, 32, 2147483647, 2, &
!    24, -126, 127, 53, -1022, 1023 /)
!
!  HARRIS 220:
!
!      data imach( 1) /       5 /
!      data imach( 2) /       6 /
!      data imach( 3) /       0 /
!      data imach( 4) /       6 /
!      data imach( 5) /      24 /
!      data imach( 6) /       3 /
!      data imach( 7) /       2 /
!      data imach( 8) /      23 /
!      data imach( 9) / 8388607 /
!      data imach(10) /       2 /
!      data imach(11) /      23 /
!      data imach(12) /    -127 /
!      data imach(13) /     127 /
!      data imach(14) /      38 /
!      data imach(15) /    -127 /
!      data imach(16) /     127 /
!
!  HARRIS SLASH 6 and SLASH 7:
!
!      data imach( 1) /       5 /
!      data imach( 2) /       6 /
!      data imach( 3) /       0 /
!      data imach( 4) /       6 /
!      data imach( 5) /      24 /
!      data imach( 6) /       3 /
!      data imach( 7) /       2 /
!      data imach( 8) /      23 /
!      data imach( 9) / 8388607 /
!      data imach(10) /       2 /
!      data imach(11) /      23 /
!      data imach(12) /    -127 /
!      data imach(13) /     127 /
!      data imach(14) /      38 /
!      data imach(15) /    -127 /
!      data imach(16) /     127 /
!
!  HONEYWELL DPS 8/70 and 600/6000 series:
!
!      data imach( 1) /    5 /
!      data imach( 2) /    6 /
!      data imach( 3) /   43 /
!      data imach( 4) /    6 /
!      data imach( 5) /   36 /
!      data imach( 6) /    4 /
!      data imach( 7) /    2 /
!      data imach( 8) /   35 /
!      data imach( 9) / O377777777777 /
!      data imach(10) /    2 /
!      data imach(11) /   27 /
!      data imach(12) / -127 /
!      data imach(13) /  127 /
!      data imach(14) /   63 /
!      data imach(15) / -127 /
!      data imach(16) /  127 /
!
!  HP 2100, 3 word double precision option with FTN4:
!
!      data imach( 1) /    5 /
!      data imach( 2) /    6 /
!      data imach( 3) /    4 /
!      data imach( 4) /    1 /
!      data imach( 5) /   16 /
!      data imach( 6) /    2 /
!      data imach( 7) /    2 /
!      data imach( 8) /   15 /
!      data imach( 9) / 32767 /
!      data imach(10) /    2 /
!      data imach(11) /   23 /
!      data imach(12) / -128 /
!      data imach(13) /  127 /
!      data imach(14) /   39 /
!      data imach(15) / -128 /
!      data imach(16) /  127 /
!
!  HP 2100, 4 word double precision option with FTN4:
!
!      data imach( 1) /    5 /
!      data imach( 2) /    6 /
!      data imach( 3) /    4 /
!      data imach( 4) /    1 /
!      data imach( 5) /   16 /
!      data imach( 6) /    2 /
!      data imach( 7) /    2 /
!      data imach( 8) /   15 /
!      data imach( 9) / 32767 /
!      data imach(10) /    2 /
!      data imach(11) /   23 /
!      data imach(12) / -128 /
!      data imach(13) /  127 /
!      data imach(14) /   55 /
!      data imach(15) / -128 /
!      data imach(16) /  127 /
!
!  HP 9000:
!
!      data imach( 1) /     5 /
!      data imach( 2) /     6 /
!      data imach( 3) /     6 /
!      data imach( 4) /     7 /
!      data imach( 5) /    32 /
!      data imach( 6) /     4 /
!      data imach( 7) /     2 /
!      data imach( 8) /    32 /
!      data imach( 9) / 2147483647 /
!      data imach(10) /     2 /
!      data imach(11) /    24 /
!      data imach(12) /  -126 /
!      data imach(13) /   127 /
!      data imach(14) /    53 /
!      data imach(15) / -1015 /
!      data imach(16) /  1017 /
!
!  IBM 360/370 series, 
!  XEROX SIGMA 5/7/9, 
!  SEL systems 85/86, 
!  PERKIN ELMER 3230,
!  PERKIN ELMER (INTERDATA) 3230:
!
!      data imach( 1) /   5 /
!      data imach( 2) /   6 /
!      data imach( 3) /   7 /
!      data imach( 4) /   6 /
!      data imach( 5) /  32 /
!      data imach( 6) /   4 /
!      data imach( 7) /   2 /
!      data imach( 8) /  31 /
!      data imach( 9) / Z7FFFFFFF /
!      data imach(10) /  16 /
!      data imach(11) /   6 /
!      data imach(12) / -64 /
!      data imach(13) /  63 /
!      data imach(14) /  14 /
!      data imach(15) / -64 /
!      data imach(16) /  63 /
!
!  IBM PC - Microsoft FORTRAN:
!
!      data imach( 1) /     5 /
!      data imach( 2) /     6 /
!      data imach( 3) /     6 /
!      data imach( 4) /     0 /
!      data imach( 5) /    32 /
!      data imach( 6) /     4 /
!      data imach( 7) /     2 /
!      data imach( 8) /    31 /
!      data imach( 9) / 2147483647 /
!      data imach(10) /     2 /
!      data imach(11) /    24 /
!      data imach(12) /  -126 /
!      data imach(13) /   127 /
!      data imach(14) /    53 /
!      data imach(15) / -1022 /
!      data imach(16) /  1023 /
!
!  IBM PC - Professional FORTRAN and Lahey FORTRAN:
!
!      data imach( 1) /     4 /
!      data imach( 2) /     7 /
!      data imach( 3) /     7 /
!      data imach( 4) /     0 /
!      data imach( 5) /    32 /
!      data imach( 6) /     4 /
!      data imach( 7) /     2 /
!      data imach( 8) /    31 /
!      data imach( 9) / 2147483647 /
!      data imach(10) /     2 /
!      data imach(11) /    24 /
!      data imach(12) /  -126 /
!      data imach(13) /   127 /
!      data imach(14) /    53 /
!      data imach(15) / -1022 /
!      data imach(16) /  1023 /
!
!  INTERDATA 8/32 with the UNIX system FORTRAN 77 compiler:
!
!  For the INTERDATA FORTRAN VII compiler, replace the Z's 
!  specifying hex constants with Y's.
!
!      data imach( 1) /   5 /
!      data imach( 2) /   6 /
!      data imach( 3) /   6 /
!      data imach( 4) /   6 /
!      data imach( 5) /  32 /
!      data imach( 6) /   4 /
!      data imach( 7) /   2 /
!      data imach( 8) /  31 /
!      data imach( 9) / Z'7FFFFFFF' /
!      data imach(10) /  16 /
!      data imach(11) /   6 /
!      data imach(12) / -64 /
!      data imach(13) /  62 /
!      data imach(14) /  14 /
!      data imach(15) / -64 /
!      data imach(16) /  62 /
!
!  PDP-10 (KA processor):
!
!      data imach( 1) /    5 /
!      data imach( 2) /    6 /
!      data imach( 3) /    7 /
!      data imach( 4) /    6 /
!      data imach( 5) /   36 /
!      data imach( 6) /    5 /
!      data imach( 7) /    2 /
!      data imach( 8) /   35 /
!      data imach( 9) / "377777777777 /
!      data imach(10) /    2 /
!      data imach(11) /   27 /
!      data imach(12) / -128 /
!      data imach(13) /  127 /
!      data imach(14) /   54 /
!      data imach(15) / -101 /
!      data imach(16) /  127 /
!
!  PDP-10 (KI processor):
!
!      data imach( 1) /    5 /
!      data imach( 2) /    6 /
!      data imach( 3) /    7 /
!      data imach( 4) /    6 /
!      data imach( 5) /   36 /
!      data imach( 6) /    5 /
!      data imach( 7) /    2 /
!      data imach( 8) /   35 /
!      data imach( 9) / "377777777777 /
!      data imach(10) /    2 /
!      data imach(11) /   27 /
!      data imach(12) / -128 /
!      data imach(13) /  127 /
!      data imach(14) /   62 /
!      data imach(15) / -128 /
!      data imach(16) /  127 /
!
!  PDP-11 FORTRAN supporting 32-bit integer arithmetic:
!
!      data imach( 1) /    5 /
!      data imach( 2) /    6 /
!      data imach( 3) /    7 /
!      data imach( 4) /    6 /
!      data imach( 5) /   32 /
!      data imach( 6) /    4 /
!      data imach( 7) /    2 /
!      data imach( 8) /   31 /
!      data imach( 9) / 2147483647 /
!      data imach(10) /    2 /
!      data imach(11) /   24 /
!      data imach(12) / -127 /
!      data imach(13) /  127 /
!      data imach(14) /   56 /
!      data imach(15) / -127 /
!      data imach(16) /  127 /
!
!  PDP-11 FORTRAN supporting 16-bit integer arithmetic:
!
!      data imach( 1) /    5 /
!      data imach( 2) /    6 /
!      data imach( 3) /    7 /
!      data imach( 4) /    6 /
!      data imach( 5) /   16 /
!      data imach( 6) /    2 /
!      data imach( 7) /    2 /
!      data imach( 8) /   15 /
!      data imach( 9) / 32767 /
!      data imach(10) /    2 /
!      data imach(11) /   24 /
!      data imach(12) / -127 /
!      data imach(13) /  127 /
!      data imach(14) /   56 /
!      data imach(15) / -127 /
!      data imach(16) /  127 /
!
!  PRIME 50 series systems with 32-bit integers and 64V MODE instructions:
!
!      data imach( 1) /            1 /
!      data imach( 2) /            1 /
!      data imach( 3) /            2 /
!      data imach( 4) /            1 /
!      data imach( 5) /           32 /
!      data imach( 6) /            4 /
!      data imach( 7) /            2 /
!      data imach( 8) /           31 /
!      data imach( 9) / :17777777777 /
!      data imach(10) /            2 /
!      data imach(11) /           23 /
!      data imach(12) /         -127 /
!      data imach(13) /         +127 /
!      data imach(14) /           47 /
!      data imach(15) /       -32895 /
!      data imach(16) /       +32637 /
!
!  SEQUENT BALANCE 8000:
!
!      data imach( 1) /     0 /
!      data imach( 2) /     0 /
!      data imach( 3) /     7 /
!      data imach( 4) /     0 /
!      data imach( 5) /    32 /
!      data imach( 6) /     1 /
!      data imach( 7) /     2 /
!      data imach( 8) /    31 /
!      data imach( 9) /  2147483647 /
!      data imach(10) /     2 /
!      data imach(11) /    24 /
!      data imach(12) /  -125 /
!      data imach(13) /   128 /
!      data imach(14) /    53 /
!      data imach(15) / -1021 /
!      data imach(16) /  1024 /
!
!  SUN Microsystems UNIX F77 compiler:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 6, 0, 32, 4, 2, 32, 2147483647, 2, &
!    24, -126, 128, 53, -1022, 1024 /)
!
!  SUN 3 (68881 or FPA):
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 6, 0, 32, 4, 2, 31, 2147483647, 2, &
!    24, -125, 128, 53, -1021, 1024 /)
!
!  UNIVAC 1100 series:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 36, 6, 2, 35, O377777777777, 2, &
!    27, -128, 127, 60, -1024, 1023 /)
!
!  VAX:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    5, 6, 7, 6, 32, 4, 2, 31, 2147483647, 2, &
!    24, -127, 127, 56, -127, 127 /)
!
!  Z80 microprocessor:
!
!  integer, parameter, dimension ( 16 ) :: imach = (/ &
!    1, 1, 0, 1, 16, 2, 2, 15, 32767, 2, &
!    24, -127, 127, 56, -127, 127 /)
!
  if ( i < 1 .or. 16 < i ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'I1MACH - Fatal error!'
    write ( *, '(a)' ) '  The input argument I is out of bounds.'
    write ( *, '(a)' ) '  Legal values satisfy 1 <= I <= 16.'
    write ( *, '(a,i12)' ) '  I = ', i
    i1mach = 0
    stop
  else
    i1mach = imach(i)
  end if

  return
end
!VD$G NOVECTOR
!VD$G NOCONCUR
!deck xerabt
      subroutine xerabt(messg,nmessg)
!***begin prologue  xerabt
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  abort program execution and print error message.
!***description
!
!     abstract
!        ***note*** machine dependent routine
!        xerabt aborts the execution of the program.
!        the error message causing the abort is given in the calling
!        sequence, in case one needs it for printing on a dayfile,
!        for example.
!
!     description of parameters
!        messg and nmessg are as in xerror, except that nmessg may
!        be zero, in which case no message is being supplied.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  1 august 1982
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  (none)
!***end prologue  xerabt
      dimension messg(nmessg)
!***first executable statement  xerabt
      call exit(1)
      end
!deck xerctl
      subroutine xerctl(messg1,nmessg,nerr,level,kontrl)
!***begin prologue  xerctl
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  allow user control over handling of errors.
!***description
!
!     abstract
!        allows user control over handling of individual errors.
!        just after each message is recorded, but before it is
!        processed any further (i.e., before it is printed or
!        a decision to abort is made), a call is made to xerctl.
!        if the user has provided his own version of xerctl, he
!        can then override the value of kontrol used in processing
!        this message by redefining its value.
!        kontrl may be set to any value from -2 to 2.
!        the meanings for kontrl are the same as in xsetf, except
!        that the value of kontrl changes only for this message.
!        if kontrl is set to a value outside the range from -2 to 2,
!        it will be moved back into that range.
!
!     description of parameters
!
!      --input--
!        messg1 - the first word (only) of the error message.
!        nmessg - same as in the call to xerror or xerrwv.
!        nerr   - same as in the call to xerror or xerrwv.
!        level  - same as in the call to xerror or xerrwv.
!        kontrl - the current value of the control flag as set
!                 by a call to xsetf.
!
!      --output--
!        kontrl - the new value of kontrl.  if kontrl is not
!                 defined, it will remain at its original value.
!                 this changed value of control affects only
!                 the current occurrence of the current message.
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  (none)
!***end prologue  xerctl
      character*20 messg1
!***first executable statement  xerctl
      return
      end
!deck xerprt
      subroutine xerprt(messg,nmessg)
!***begin prologue  xerprt
!***date written   790801   (yymmdd)
!***revision date  851213   (yymmdd)
!***category no.  r3
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  print error messages.
!***description
!
!     abstract
!        print the hollerith message in messg, of length nmessg,
!        on each file indicated by xgetua.
!     latest revision ---  1 august 1985
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  i1mach,xgetua
!***end prologue  xerprt
      integer lun(5)
      character*(*) messg
!     obtain unit numbers and write line to each unit
!***first executable statement  xerprt
      call xgetua(lun,nunit)
      lenmes = len(messg)
      do 20 kunit=1,nunit
         iunit = lun(kunit)
         if (iunit.eq.0) iunit = i1mach(4)
         do ichar=1,lenmes,72
            last = min0(ichar+71 , lenmes)
            write (iunit,'(1x,a)') messg(ichar:last)
         end do
   20 continue
      return
      end
!deck xerror
      subroutine xerror(messg,nmessg,nerr,level)
!***begin prologue  xerror
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  process an error (diagnostic) message.
!***description
!
!     abstract
!        xerror processes a diagnostic message, in a manner
!        determined by the value of level and the current value
!        of the library error control flag, kontrl.
!        (see subroutine xsetf for details.)
!
!     description of parameters
!      --input--
!        messg - the hollerith message to be processed, containing
!                no more than 72 characters.
!        nmessg- the actual number of characters in messg.
!        nerr  - the error number associated with this message.
!                nerr must not be zero.
!        level - error category.
!                =2 means this is an unconditionally fatal error.
!                =1 means this is a recoverable error.  (i.e., it is
!                   non-fatal if xsetf has been appropriately called.)
!                =0 means this is a warning message only.
!                =-1 means this is a warning message which is to be
!                   printed at most once, regardless of how many
!                   times this call is executed.
!
!     examples
!        call xerror('smooth -- num was zero.',23,1,2)
!        call xerror('integ  -- less than full accuracy achieved.',
!    1                43,2,1)
!        call xerror('rooter -- actual zero of f found before interval f
!    1ully collapsed.',65,3,0)
!        call xerror('exp    -- underflows being set to zero.',39,1,-1)
!
!     written by ron jones, with slatec common math library subcommittee
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  xerrwv
!***end prologue  xerror
      character*(*) messg
!***first executable statement  xerror
      call xerrwv(messg,nmessg,nerr,level,0,0,0,0,0.,0.)
      return
      end


    ! **********************************************************************
    character(len=32) function md5(string)
    ! ---------------------------------------------------------------------*
    !     Programmierer    : VEZ2/Pieper                                   *
    !     Version          : 1.0                                           *
    !     letzte nderung  : 07.05.2010                                    *
    !     Aufgabe          : Erzeugt aus einem String einen MD5 Hashwert   *
    ! **********************************************************************
     
    implicit none
     
    character(len=*) ::  string
    character(len=((int(len(string)/64)+1)*64)) :: newString
    character(len=8) :: wtmp
     
    integer(kind=4) j,n1,n2,n3,n4,umdrehen,pos
    integer(kind=4) r(64),k(64),h0,h1,h2,h3,a,b,c,d,f,g,temp,w(16),leftrotate,i,intLen
    integer(kind=8) hoch32
    real(kind=8) sinus,absolut,real8i

    ! Initial values of h0,h1,h2,h3.
    ! It's done this horrible way because f95 requires BOZ literals only in DATA
    ! statements. The original negative values of h1 and h2 are changed to the
    ! equivalent positive values, and should be multiplied by -1, because
    ! negative BOZ literals raise overflow errors on gfortran at least.
    ! Original values:
    !   h0=z'67452301', h1=z'EFCDAB89', h2=z'98BADCFE', h3=z'10325476'
    integer(kind=4) &
        z_67452301_i4, z_10325477_i4, z_67452302_i4, z_10325476_i4
    data &
        z_67452301_i4/z'67452301'/, z_10325477_i4/z'10325477'/, &
        z_67452302_i4/z'67452302'/, z_10325476_i4/z'10325476'/

     
    r = (/ &
        & 7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22, 5,  &
        & 9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20, 4, 11,  &
        & 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23, 6, 10, 15, &
        & 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21 /)
     
    do i=1,64
        real8i = real(int(i, 8))
        sinus = dsin(real8i)
        absolut = dabs(sinus)
        hoch32 = int(2.**32., kind(hoch32))
        k(i) = int(int(absolut * real(hoch32), 8), kind(k(i)))
    end do
     
    h0 = + z_67452301_i4
    h1 = - z_10325477_i4
    h2 = - z_67452302_i4
    h3 = + z_10325476_i4
     
    j = len(string)+1
    newString(:j) = string // char(128)
    i = mod(j, 64)
    do while(i /= 56)
        j = j + 1
        newString(j:j) = char(0)
        i = mod(j, 64)
    end do
     
    intLen = len(string)*8
    do i = 0,3
        temp = iand(intLen, int(z'FF', 4))
        j = j + 1
        newString(j:j) = char(temp)
        intLen = shiftr(intLen, 8)
    end do
     
    do i = 1,4
        j = j + 1
        newString(j:j) = char(0)
    end do
     
    do i = 1,int(len(newString)/64)
     
        do j = 1,16
            pos = (j-1)*4+(i-1)*64
            n1 = ichar(newString(4+pos:4+pos))
            n2 = ichar(newString(3+pos:3+pos))
            n3 = ichar(newString(2+pos:2+pos))
            n4 = ichar(newString(1+pos:1+pos))
             
            write(wtmp,'(4(z2.2))') n1,n2,n3,n4
            read(wtmp,'(z8)') w(j)
        end do
     
        a = h0
        b = h1
        c = h2
        d = h3
     
        do j = 1,64
            if (j >= 1 .and. j <= 16) then
                f = ior(iand(b, c), iand(not(b), d))
                g = j
            else if (j >= 17 .and. j <= 32) then
                f = ior(iand(d, b), iand(not(d), c))
                g = mod(5*(j-1) + 1, 16) + 1
            else if (j >= 33 .and. j <= 48) then
                f = ieor(b, ieor(c, d))
                g = mod(3*(j-1) + 5, 16) + 1
            else if (j >= 49 .and. j <= 64) then
                f = ieor(c, ior(b, not(d)))
                g = mod(7*(j-1), 16) + 1
            end if
             
            temp = d
            d = c
            c = b
            b = b + leftrotate((a + f + k(j) + w(g)) , r(j))
            a = temp
        end do
     
        h0 = h0 + a
        h1 = h1 + b
        h2 = h2 + c
        h3 = h3 + d
    end do
    h0 = umdrehen(h0)
    h1 = umdrehen(h1)
    h2 = umdrehen(h2)
    h3 = umdrehen(h3)
     
    write(md5,'(4(z8))') h0,h1,h2,h3
    return
     
    end function md5
    !
    !
    ! **********************************************************************
    integer(kind=4) function leftrotate (x, c)
    ! ---------------------------------------------------------------------*
    !     Programmierer    : VEZ2/Pieper                                   *
    !     Version          : 1.0                                           *
    !     letzte nderung  : 07.05.2010                                    *
    !     Aufgabe          : Fhrt ein Leftrotate der Bits durch           *
    ! **********************************************************************
     
    implicit none
     
    integer(kind=4) x,c,result1,result2
     
    result1 = shiftl(x,c)
    result2 = shiftr(x, (32-c))
     
    leftrotate = ior(result1, result2)
     
    return
    end function leftrotate
    !
    !
    ! **********************************************************************
    integer(kind=4) function umdrehen(zahl)
    ! ---------------------------------------------------------------------*
    !     Programmierer    : VEZ2/Pieper                                   *
    !     Version          : 1.0                                           *
    !     letzte nderung  : 07.05.2010                                    *
    !     Aufgabe          : Macht aus Big Endian -> Little Endian Bits    *
    ! **********************************************************************
     
    implicit none
     
    integer(kind=4) i,tmp,zahl
     
    umdrehen = 0
    do i = 1,4
        umdrehen = shiftl(umdrehen, 8)
        tmp = iand(zahl, int(z'FF', 4))
        umdrehen = umdrehen + tmp;
        zahl = shiftr(zahl, 8)
    end do
    
    return
    end function umdrehen

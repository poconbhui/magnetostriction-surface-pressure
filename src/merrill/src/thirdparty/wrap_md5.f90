module wrapmd5
use iso_c_binding
interface
subroutine compute_md5(str,digest) bind(c)
use iso_c_binding
 implicit none
 character(kind=c_char), dimension(*), intent(in) :: str
 character(kind=c_char),dimension(16), intent(out) :: digest
end subroutine
end interface
end module

      SUBROUTINE DSICCG(N, B, X, NELT, IA, JA, A, ISYM, ITOL, TOL, &
           ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW )
!***BEGIN PROLOGUE  DSICCG
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSICCG-D),
!             Symmetric Linear system, Sparse,
!             Iterative Precondition, Incomplete Cholesky
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Incomplete Cholesky Conjugate Gradient Sparse Ax=b Solver.
!            Routine to  solve a symmetric  positive definite linear
!            system   Ax    =  b  using  the    incomplete  Cholesky
!            Preconditioned Conjugate Gradient method.
!
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
!     INTEGER ITER, IERR, IUNIT, LENW, IWORK(NEL+2*n+1), LENIW
!     DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, RWORK(NEL+5*N)
!
!     CALL DSICCG(N, B, X, NELT, IA, JA, A, ISYM, ITOL, TOL,
!    $     ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW )
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :INOUT    Double Precision X(N).
!         On input X is your initial guess for solution vector.
!         On output X is the final approximate solution.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :INOUT    Integer IA(NELT).
! JA     :INOUT    Integer JA(NELT).
! A      :INOUT    Integer A(NELT).
!         These arrays should hold the matrix A in either the SLAP
!         Triad format or the SLAP Column format.  See ``Description'',
!         below.  If the SLAP Triad format is chosen it is changed
!         internally to the SLAP Column format.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! ITOL   :IN       Integer.
!         Flag to indicate type of convergence criterion.
!         If ITOL=1, iteration stops when the 2-norm of the residual
!         divided by the 2-norm of the right-hand side is less than TOL.
!         If ITOL=2, iteration stops when the 2-norm of M-inv times the
!         residual divided by the 2-norm of M-inv times the right hand
!         side is less than TOL, where M-inv is the inverse of the
!         diagonal of A.
!         ITOL=11 is often useful for checking and comparing different
!         routines.  For this case, the user must supply the "exact"
!         solution or a very accurate approximation (one with an error
!         much less than TOL) through a common block,
!                     COMMON /SOLBLK/ SOLN(1)
!         if ITOL=11, iteration stops when the 2-norm of the difference
!         between the iterative approximation and the user-supplied
!         solution divided by the 2-norm of the user-supplied solution
!         is less than TOL.  Note that this requires the user to set up
!         the "COMMON /SOLBLK/ SOLN(LENGTH)" in the calling routine.
!         The routine with this declaration should be loaded before the
!         stop test so that the correct length is used by the loader.
!         This procedure is not standard Fortran and may not work
!         correctly on your system (although it has worked on every
!         system the authors have tried).  If ITOL is not 11 then this
!         common block is indeed standard Fortran.
! TOL    :IN       Double Precision.
!         Convergence criterion, as described above.
! ITMAX  :IN       Integer.
!         Maximum number of iterations.
! ITER   :OUT      Integer.
!         Number of iterations required to reach convergence, or
!         ITMAX+1 if convergence criterion could not be achieved in
!         ITMAX iterations.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as
!         defined by ITOL.
! IERR   :OUT      Integer.
!         Return error flag.
!           IERR = 0 => All went well.
!           IERR = 1 => Insufficient storage allocated
!                       for WORK or IWORK.
!           IERR = 2 => Method failed to converge in
!                       ITMAX steps.
!           IERR = 3 => Error in user input.  Check input
!                       value of N, ITOL.
!           IERR = 4 => User error tolerance set too tight.
!                       Reset to 500.0*D1MACH(3).  Iteration proceeded.
!           IERR = 5 => Preconditioning matrix, M,  is not
!                       Positive Definite.  $(r,z) < 0.0$.
!           IERR = 6 => Matrix A is not Positive Definite.
!                       $(p,Ap) < 0.0$.
!           IERR = 7 => Incomplete factorization broke down
!                       and was fudged.  Resulting preconditioning may
!                       be less than the best.
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration,
!         if this is desired for monitoring convergence.  If unit
!         number is 0, no writing will occur.
! RWORK  :WORK     Double Precision RWORK(LENW).
!         Double Precision array used for workspace.  NEL is the
!         number of non-
!         zeros in the lower triangle of the matrix (including the
!         diagonal)
! LENW   :IN       Integer.
!         Length of the double precision workspace, RWORK.
!         LENW >= NEL+5*N.
! IWORK  :WORK     Integer IWORK(LENIW).
!         Integer array used for workspace.  NEL is the number of non-
!         zeros in the lower triangle of the matrix (including the
!         diagonal).
!         Upon return the following locations of IWORK hold information
!         which may be of use to the user:
!         IWORK(9)  Amount of Integer workspace actually used.
!         IWORK(10) Amount of Double Precision workspace actually used.
! LENIW  :IN       Integer.
!         Length of the integer workspace, IWORK.  LENIW >= NEL+N+11.
!
! *Description:
!       This routine  performs  preconditioned  conjugate   gradient
!       method on the   symmetric positive  definite  linear  system
!       Ax=b.   The preconditioner  is  the incomplete Cholesky (IC)
!       factorization of the matrix A.  See  DSICS for details about
!       the incomplete   factorization algorithm.  One   should note
!       here however, that the  IC factorization is a  slow  process
!       and  that  one should   save  factorizations  for  reuse, if
!       possible.  The   MSOLVE operation (handled  in  DSLLTI) does
!       vectorize on machines  with  hardware  gather/scatter and is
!       quite fast.
!
!       The Sparse Linear Algebra Package (SLAP) utilizes two matrix
!       data structures: 1) the  SLAP Triad  format or  2)  the SLAP
!       Column format.  The user can hand this routine either of the
!       of these data structures and SLAP  will figure out  which on
!       is being used and act accordingly.
!
!       =================== S L A P Triad format ===================
!
!       This routine requires that the  matrix A be   stored in  the
!       SLAP  Triad format.  In  this format only the non-zeros  are
!       stored.  They may appear in  *ANY* order.  The user supplies
!       three arrays of  length NELT, where  NELT is  the number  of
!       non-zeros in the matrix: (IA(NELT), JA(NELT), A(NELT)).  For
!       each non-zero the user puts the row and column index of that
!       matrix element  in the IA and  JA arrays.  The  value of the
!       non-zero   matrix  element is  placed  in  the corresponding
!       location of the A array.   This is  an  extremely  easy data
!       structure to generate.  On  the  other hand it   is  not too
!       efficient on vector computers for  the iterative solution of
!       linear systems.  Hence,   SLAP changes   this  input    data
!       structure to the SLAP Column format  for  the iteration (but
!       does not change it back).
!
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix       SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a
!       column):
!
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
! *Precision:           Double Precision
! *Side Effects:
!       The SLAP Triad format (IA, JA, A) is modified internally to be
!       the SLAP Column format.  See above.
!
! *See Also:
!       DCG, DSLLTI
!***REFERENCES  1. Louis Hageman \& David Young, ``Applied Iterative
!                 Methods'', Academic Press, New York (1981) ISBN
!                 0-12-313340-8.
!               2. Concus, Golub \& O'Leary, ``A Generalized Conjugate
!                 Gradient Method for the Numerical Solution of
!                 Elliptic Partial Differential Equations,'' in Sparse
!                 Matrix Computations (Bunch \& Rose, Eds.), Academic
!                 Press, New York (1979).
!***ROUTINES CALLED  DS2Y, DCHKW, DSICS, XERRWV, DCG
!***END PROLOGUE  DSICCG
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL
      INTEGER ITMAX, ITER, IUNIT, LENW, IWORK(LENIW), LENIW
      DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, RWORK(LENW)
      EXTERNAL DSMV, DSLLTI
      PARAMETER (LOCRB=1, LOCIB=11)
!
!         Change the SLAP input matrix IA, JA, A to SLAP-Column format.
!***FIRST EXECUTABLE STATEMENT  DSICCG
      IERR = 0
      IF( N.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, IA, JA, A, ISYM )
!
!         Count number of elements in lower triangle of the matrix.
!         Then set up the work arrays.
      IF( ISYM.EQ.0 ) THEN
         NEL = (NELT + N)/2
      ELSE
         NEL = NELT
      ENDIF
!
      LOCJEL = LOCIB
      LOCIEL = LOCJEL + NEL
      LOCIW = LOCIEL + N + 1
!
      LOCEL = LOCRB
      LOCDIN = LOCEL + NEL
      LOCR = LOCDIN + N
      LOCZ = LOCR + N
      LOCP = LOCZ + N
      LOCDZ = LOCP + N
      LOCW = LOCDZ + N
!
!         Check the workspace allocations.
      CALL DCHKW( 'DSICCG', LOCIW, LENIW, LOCW, LENW, IERR, ITER, ERR )
      IF( IERR.NE.0 ) RETURN
!
      IWORK(1) = NEL
      IWORK(2) = LOCJEL
      IWORK(3) = LOCIEL
      IWORK(4) = LOCEL
      IWORK(5) = LOCDIN
      IWORK(9) = LOCIW
      IWORK(10) = LOCW
!
!         Compute the Incomplete Cholesky decomposition.
!
      CALL DSICS(N, NELT, IA, JA, A, ISYM, NEL, IWORK(LOCIEL), &
           IWORK(LOCJEL), RWORK(LOCEL), RWORK(LOCDIN), &
           RWORK(LOCR), IERR )
      IF( IERR.NE.0 ) THEN
         CALL XERRWV('DSICCG: Warning...IC factorization broke down '// &
              'on step i1.  Diagonal was set to unity and '// &
              'factorization proceeded.', 113, 1, 1, 1, IERR, 0, &
              0, 0.0, 0.0 )
         IERR = 7
      ENDIF
!
!         Do the Preconditioned Conjugate Gradient.
      CALL DCG(N, B, X, NELT, IA, JA, A, ISYM, DSMV, DSLLTI, &
           ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, RWORK(LOCR), &
           RWORK(LOCZ), RWORK(LOCP), RWORK(LOCDZ), RWORK(1), &
           IWORK(1))
      RETURN
!------------- LAST LINE OF DSICCG FOLLOWS ----------------------------
      END

      SUBROUTINE DSICS(N, NELT, IA, JA, A, ISYM, NEL, IEL, JEL, &
           EL, D, R, IWARN )
!***BEGIN PROLOGUE  DSICS
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSICS-D),
!             Linear system, SLAP Sparse, Iterative Precondition
!             Incomplete Cholesky Factorization.
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Incompl Cholesky Decomposition Preconditioner SLAP Set Up.
!            Routine to generate the Incomplete Cholesky decomposition,
!            L*D*L-trans, of  a symmetric positive definite  matrix, A,
!            which  is stored  in  SLAP Column format.  The  unit lower
!            triangular matrix L is  stored by rows, and the inverse of
!            the diagonal matrix D is stored.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
!     INTEGER NEL, IEL(NEL), JEL(N+1), IWARN
!     DOUBLE PRECISION A(NELT), EL(NEL), D(N), R(N)
!
!     CALL DSICS( N, NELT, IA, JA, A, ISYM, NEL, IEL, JEL, EL, D, R,
!    $    IWARN )
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! NELT   :IN       Integer.
!         Number of elements in arrays IA, JA, and A.
! IA     :INOUT    Integer IA(NELT).
! JA     :INOUT    Integer JA(NELT).
! A      :INOUT    Double Precision A(NELT).
!         These arrays should hold the matrix A in the SLAP Column
!         format.  See "Description", below.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the lower
!         triangle of the matrix is stored.
! NEL    :OUT      Integer.
!         Number of non-zeros in the lower triangle of A.   Also
!         coresponds to the length of the JEL, EL arrays.
! IEL    :OUT      Integer IEL(N+1).
! JEL    :OUT      Integer JEL(NEL).
! EL     :OUT      Double Precision EL(NEL).
!         IEL, JEL, EL contain the unit lower triangular factor  of the
!         incomplete decomposition   of the A  matrix  stored  in  SLAP
!         Row format.   The Diagonal of   ones   *IS*   stored.     See
!         "Description", below for more details about the SLAP Row fmt.
! D      :OUT      Double Precision D(N)
!         Upon return this array holds D(I) = 1./DIAG(A).
! R      :WORK     Double Precision R(N).
!         Temporary double precision workspace needed for the
!         factorization.
! IWARN  :OUT      Integer.
!         This is a warning variable and is zero if the IC factoriza-
!         tion goes well.  It is set to the row index corresponding to
!         the last zero pivot found.  See "Description", below.
!
! *Description
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a
!       column):
!
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       ==================== S L A P Row format ====================
!       This routine requires  that the matrix A  be  stored  in the
!       SLAP  Row format.   In this format  the non-zeros are stored
!       counting across  rows (except for the diagonal  entry, which
!       must appear first in each "row") and  are stored in the
!       double precision
!       array A.  In other words, for each row in the matrix put the
!       diagonal entry in  A.   Then   put  in the   other  non-zero
!       elements   going  across the  row (except   the diagonal) in
!       order.   The  JA array  holds   the column   index for  each
!       non-zero.   The IA  array holds the  offsets into  the JA, A
!       arrays  for   the   beginning  of   each  row.   That    is,
!       JA(IA(IROW)),  A(IA(IROW)) points  to  the beginning  of the
!       IROW-th row in JA and A.   JA(IA(IROW+1)-1), A(IA(IROW+1)-1)
!       points to the  end of the  IROW-th row.  Note that we always
!       have IA(N+1) =  NELT+1, where  N  is  the number of rows  in
!       the matrix  and NELT  is the  number   of  non-zeros in  the
!       matrix.
!
!       Here is an example of the SLAP Row storage format for a  5x5
!       Matrix (in the A and JA arrays '|' denotes the end of a row):
!
!           5x5 Matrix         SLAP Row format for 5x5 matrix on left.
!                              1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 12 15 | 22 21 | 33 35 | 44 | 55 51 53
!       |21 22  0  0  0|  JA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  IA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       With the SLAP  format some  of  the   "inner  loops" of this
!       routine should vectorize  on  machines with hardware support
!       for vector   gather/scatter  operations.  Your compiler  may
!       require a compiler directive to  convince it that  there are
!       no  implicit  vector  dependencies.  Compiler directives for
!       the Alliant    FX/Fortran and CRI   CFT/CFT77 compilers  are
!       supplied with the standard SLAP distribution.
!
!       The IC  factorization is not  alway exist for SPD matricies.
!       In the event that a zero pivot is found it is set  to be 1.0
!       and the factorization procedes.   The integer variable IWARN
!       is set to the last row where the Diagonal was fudged.  This
!       eventuality hardly ever occurs in practice
!
! *Precision:           Double Precision
!
! *See Also:
!       SCG, DSICCG
!***REFERENCES  1. Gene Golub & Charles Van Loan, "Matrix Computations",
!                 John Hopkins University Press; 3 (1983) IBSN
!                 0-8018-3010-9.
!***ROUTINES CALLED  XERRWV
!***END PROLOGUE  DSICS
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
      INTEGER NEL, IEL(NEL), JEL(NEL)
      DOUBLE PRECISION A(NELT), EL(NEL), D(N), R(N)
!
!         Set the lower triangle in IEL, JEL, EL
!***FIRST EXECUTABLE STATEMENT  DSICS
      IWARN = 0
!
!         All matrix elements stored in IA, JA, A.  Pick out the lower
!         triangle (making sure that the Diagonal of EL is one) and
!         store by rows.
!
      NEL = 1
      IEL(1) = 1
      JEL(1) = 1
      EL(1) = 1.0D0
      D(1) = A(1)
!VD$R NOCONCUR
      DO 30 IROW = 2, N
!         Put in the Diagonal.
         NEL = NEL + 1
         IEL(IROW) = NEL
         JEL(NEL) = IROW
         EL(NEL) = 1.0D0
         D(IROW) = A(JA(IROW))
!
!         Look in all the lower triangle columns for a matching row.
!         Since the matrix is symmetric, we can look across the
!         irow-th row by looking down the irow-th column (if it is
!         stored ISYM=0)...
         IF( ISYM.EQ.0 ) THEN
            ICBGN = JA(IROW)
            ICEND = JA(IROW+1)-1
         ELSE
            ICBGN = 1
            ICEND = IROW-1
         ENDIF
         DO 20 IC = ICBGN, ICEND
            IF( ISYM.EQ.0 ) THEN
               ICOL = IA(IC)
               IF( ICOL.GE.IROW ) GOTO 20
            ELSE
               ICOL = IC
            ENDIF
            JBGN = JA(ICOL)+1
            JEND = JA(ICOL+1)-1
            IF( JBGN.LE.JEND .AND. IA(JEND).GE.IROW ) THEN
!VD$ NOVECTOR
               DO J = JBGN, JEND
                  IF( IA(J).EQ.IROW ) THEN
                     NEL = NEL + 1
                     JEL(NEL) = ICOL
                     EL(NEL)  = A(J)
                     GOTO 20
                  ENDIF
               end do
            ENDIF
 20      CONTINUE
 30   CONTINUE
      IEL(N+1) = NEL+1
!
!         Sort ROWS of lower triangle into descending order (count out
!         along rows out from Diagonal).
!
      DO 60 IROW = 2, N
         IBGN = IEL(IROW)+1
         IEND = IEL(IROW+1)-1
         IF( IBGN.LT.IEND ) THEN
            DO 50 I = IBGN, IEND-1
!VD$ NOVECTOR
               DO 40 J = I+1, IEND
                  IF( JEL(I).GT.JEL(J) ) THEN
                     JELTMP = JEL(J)
                     JEL(J) = JEL(I)
                     JEL(I) = JELTMP
                     ELTMP = EL(J)
                     EL(J) = EL(I)
                     EL(I) = ELTMP
                  ENDIF
 40            CONTINUE
 50         CONTINUE
         ENDIF
 60   CONTINUE
!
!         Perform the Incomplete Cholesky decomposition by looping
!         over the rows.
!         Scale the first column.  Use the structure of A to pick out
!         the rows with something in column 1.
!
      IRBGN = JA(1)+1
      IREND = JA(2)-1
      DO 65 IRR = IRBGN, IREND
         IR = IA(IRR)
!         Find the index into EL for EL(1,IR).
!         Hint: it's the second entry.
         I = IEL(IR)+1
         EL(I) = EL(I)/D(1)
 65   CONTINUE
!
      DO 110 IROW = 2, N
!
!         Update the IROW-th diagonal.
!
         DO 66 I = 1, IROW-1
            R(I) = 0.0D0
 66      CONTINUE
         IBGN = IEL(IROW)+1
         IEND = IEL(IROW+1)-1
         IF( IBGN.LE.IEND ) THEN
!LLL. OPTION ASSERT (NOHAZARD)
!DIR$ IVDEP
!VD$ NODEPCHK
            DO 70 I = IBGN, IEND
               R(JEL(I)) = EL(I)*D(JEL(I))
               D(IROW) = D(IROW) - EL(I)*R(JEL(I))
 70         CONTINUE
!
!         Check to see if we gota problem with the diagonal.
!
            IF( D(IROW).LE.0.0D0 ) THEN
               IF( IWARN.EQ.0 ) IWARN = IROW
               D(IROW) = 1.0D0
            ENDIF
         ENDIF
!
!         Update each EL(IROW+1:N,IROW), if there are any.
!         Use the structure of A to determine the Non-zero elements
!         of the IROW-th column of EL.
!
         IRBGN = JA(IROW)
         IREND = JA(IROW+1)-1

         DO 100 IRR = IRBGN, IREND

            IR = IA(IRR)

            IF( IR.LE.IROW ) then
              GO TO 100
            end if

!         Find the index into EL for EL(IR,IROW)
            IBGN = IEL(IR)+1
            IEND = IEL(IR+1)-1
            IF( JEL(IBGN).GT.IROW ) GOTO 100
            DO 90 I = IBGN, IEND
               IF( JEL(I).EQ.IROW ) THEN
                  ICEND = IEND
 91               IF( JEL(ICEND).GE.IROW ) THEN
                     ICEND = ICEND - 1
                     GOTO 91
                  ENDIF
!         Sum up the EL(IR,1:IROW-1)*R(1:IROW-1) contributions.
!LLL. OPTION ASSERT (NOHAZARD)
!DIR$ IVDEP
!VD$ NODEPCHK
                  DO 80 IC = IBGN, ICEND
                     EL(I) = EL(I) - EL(IC)*R(JEL(IC))
 80               CONTINUE
                  EL(I) = EL(I)/D(IROW)
                  GOTO 100
               ENDIF
 90         CONTINUE
!
!         If we get here, we have real problems...
            CALL XERRWV('DSICS -- A and EL data structure mismatch'// &
                 ' in row (i1)',53,1,2,1,IROW,0,0,0.0,0.0)
 100     CONTINUE
 110  CONTINUE
!
!         Replace diagonals by their inverses.
!
!VD$ CONCUR
      DO 120 I =1, N
         D(I) = 1.0D0/D(I)
 120  CONTINUE
      RETURN
!------------- LAST LINE OF DSICS FOLLOWS ----------------------------
      END
!DECK DSLLTI
      SUBROUTINE DSLLTI(N, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!***BEGIN PROLOGUE  DSLLTI
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSLLTI-S),
!             Linear system solve, Sparse, Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP MSOLVE for LDL' (IC) Factorization.
!            This routine acts as an interface between the SLAP generic
!            MSOLVE calling convention and the routine that actually
!                           -1
!            computes (LDL')  B = X.
!***DESCRIPTION
!       See the DESCRIPTION of SLLTI2 for the gory details.
!***ROUTINES CALLED SLLTI2
!
!***END PROLOGUE  DSLLTI
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, IWORK(1)
      DOUBLE PRECISION B(1), X(1), A(NELT), RWORK(1)
!
!***FIRST EXECUTABLE STATEMENT  DSLLTI
      NEL = IWORK(1)
      LOCIEL = IWORK(3)
      LOCJEL = IWORK(2)
      LOCEL  = IWORK(4)
      LOCDIN = IWORK(5)
      CALL SLLTI2(N, B, X, NEL, IWORK(LOCIEL), IWORK(LOCJEL), &
           RWORK(LOCEL), RWORK(LOCDIN))
!
      RETURN
!------------- LAST LINE OF DSLLTI FOLLOWS ----------------------------
      END
      SUBROUTINE DCG(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MSOLVE, &
           ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, R, Z, P, DZ, &
           RWORK, IWORK )
!***BEGIN PROLOGUE  DCG
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DCG-D),
!             Symmetric Linear system, Sparse, Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Preconditioned Conjugate Gradient iterative Ax=b solver.
!            Routine to  solve a  symmetric positive definite linear
!            system    Ax = b    using the Preconditioned  Conjugate
!            Gradient method.
!***DESCRIPTION
! *Usage:
!     INTEGER  N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
!     INTEGER  ITER, IERR, IUNIT, IWORK(USER DEFINABLE)
!     DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, R(N), Z(N)
!     DOUBLE PRECISION P(N), DZ(N), RWORK(USER DEFINABLE)
!     EXTERNAL MATVEC, MSOLVE
!
!     CALL DCG(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MSLOVE,
!    $     ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, R, Z, P, DZ,
!    $     RWORK, IWORK )
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :INOUT    Double Precision X(N).
!         On input X is your initial guess for solution vector.
!         On output X is the final approximate solution.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(NELT).
! A      :IN       Integer A(NELT).
!         These arrays contain the matrix data structure for A.
!         It could take any form.  See ``Description'', below
!         for more late breaking details...
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! MATVEC :EXT      External.
!         Name of a routine which performs the matrix vector multiply
!         Y = A*X given A and X.  The name of the MATVEC routine must
!         be declared external in the calling program.  The calling
!         sequence to MATVEC is:
!
!             CALL MATVEC( N, X, Y, NELT, IA, JA, A, ISYM )
!
!         Where N is the number of unknowns, Y is the product A*X
!         upon return X is an input vector, NELT is the number of
!         non-zeros in the SLAP IA, JA, A storage for the matrix A.
!         ISYM is a flag which, if non-zero, denotest that A is
!         symmetric and only the lower or upper triangle is stored.
! MSOLVE :EXT      External.
!         Name of a routine which solves a linear system MZ = R for
!         Z given R with the preconditioning matrix M (M is supplied via
!         RWORK and IWORK arrays).  The name of the MSOLVE routine must
!         be declared external in the calling program.  The calling
!         sequence to MSLOVE is:
!
!             CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!
!         Where N is the number of unknowns, R is the right-hand side
!         vector, and Z is the solution upon return.  RWORK is a double
!         precision
!         array that can be used to pass necessary preconditioning
!         information and/or workspace to MSOLVE.  IWORK is an integer
!         work array for the same purpose as RWORK.
! ITOL   :IN       Integer.
!         Flag to indicate type of convergence criterion.
!         If ITOL=1, iteration stops when the 2-norm of the residual
!         divided by the 2-norm of the right-hand side is less than TOL.
!         If ITOL=2, iteration stops when the 2-norm of M-inv times the
!         residual divided by the 2-norm of M-inv times the right hand
!         side is less than TOL, where M-inv is the inverse of the
!         diagonal of A.
!         ITOL=11 is often useful for checking and comparing different
!         routines.  For this case, the user must supply the "exact"
!         solution or a very accurate approximation (one with an error
!         much less than TOL) through a common block,
!                     COMMON /SOLBLK/ SOLN(1)
!         if ITOL=11, iteration stops when the 2-norm of the difference
!         between the iterative approximation and the user-supplied
!         solution divided by the 2-norm of the user-supplied solution
!         is less than TOL.  Note that this requires the user to set up
!         the "COMMON /SOLBLK/ SOLN(LENGTH)" in the calling routine.
!         The routine with this declaration should be loaded before the
!         stop test so that the correct length is used by the loader.
!         This procedure is not standard Fortran and may not work
!         correctly on your system (although it has worked on every
!         system the authors have tried).  If ITOL is not 11 then this
!         common block is indeed standard Fortran.
! TOL    :IN       Double Precision.
!         Convergence criterion, as described above.
! ITMAX  :IN       Integer.
!         Maximum number of iterations.
! ITER   :OUT      Integer.
!         Number of iterations required to reach convergence, or
!         ITMAX+1 if convergence criterion could not be achieved in
!         ITMAX iterations.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as
!         defined by ITOL.
! IERR   :OUT      Integer.
!         Return error flag.
!           IERR = 0 => All went well.
!           IERR = 1 => Insufficient storage allocated
!                       for WORK or IWORK.
!           IERR = 2 => Method failed to converge in
!                       ITMAX steps.
!           IERR = 3 => Error in user input.  Check input
!                       value of N, ITOL.
!           IERR = 4 => User error tolerance set too tight.
!                       Reset to 500.0*R1MACH(3).  Iteration proceeded.
!           IERR = 5 => Preconditioning matrix, M,  is not
!                       Positive Definite.  $(r,z) < 0.0$.
!           IERR = 6 => Matrix A is not Positive Definite.
!                       $(p,Ap) < 0.0$.
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration,
!         if this is desired for monitoring convergence.  If unit
!         number is 0, no writing will occur.
! R      :WORK     Double Precision R(N).
! Z      :WORK     Double Precision Z(N).
! P      :WORK     Double Precision P(N).
! DZ     :WORK     Double Precision DZ(N).
! RWORK  :WORK     Double Precision RWORK(USER DEFINABLE).
!         Double Precision array that can be used by  MSOLVE.
! IWORK  :WORK     Integer IWORK(USER DEFINABLE).
!         Integer array that can be used by  MSOLVE.
!
! *Description
!       This routine does  not care  what matrix data   structure is
!       used for  A and M.  It simply   calls  the MATVEC and MSOLVE
!       routines, with  the arguments as  described above.  The user
!       could write any type of structure and the appropriate MATVEC
!       and MSOLVE routines.  It is assumed  that A is stored in the
!       IA, JA, A  arrays in some fashion and  that M (or INV(M)) is
!       stored  in  IWORK  and  RWORK   in  some fashion.   The SLAP
!       routines DSDCG and DSICCG are examples of this procedure.
!
!       Two  examples  of  matrix  data structures  are the: 1) SLAP
!       Triad  format and 2) SLAP Column format.
!
!       =================== S L A P Triad format ===================
!
!       In  this   format only the  non-zeros are  stored.  They may
!       appear  in *ANY* order.   The user  supplies three arrays of
!       length NELT, where  NELT  is the number  of non-zeros in the
!       matrix:  (IA(NELT), JA(NELT),  A(NELT)).  For each  non-zero
!       the  user puts   the row  and  column index   of that matrix
!       element in the IA and JA arrays.  The  value of the non-zero
!       matrix  element is  placed in  the corresponding location of
!       the A  array.  This is  an extremely easy data  structure to
!       generate.  On  the other hand it  is  not too  efficient  on
!       vector  computers   for the  iterative  solution  of  linear
!       systems.  Hence, SLAP  changes this input  data structure to
!       the SLAP   Column  format for the  iteration (but   does not
!       change it back).
!
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix      SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a
!       column):
!
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
! *Precision:           Double Precision
! *See Also:
!       DSDCG, DSICCG
!***REFERENCES  1. Louis Hageman \& David Young, ``Applied Iterative
!                 Methods'', Academic Press, New York (1981) ISBN
!                 0-12-313340-8.
!
!               2. Concus, Golub \& O'Leary, ``A Generalized Conjugate
!                 Gradient Method for the Numerical Solution of
!                 Elliptic Partial Differential Equations,'' in Sparse
!                 Matrix Computations (Bunch \& Rose, Eds.), Academic
!                 Press, New York (1979).
!***ROUTINES CALLED  MATVEC, MSOLVE, ISDCG, DCOPY, DDOT, DAXPY, D1MACH
!***END PROLOGUE  DCG
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
      INTEGER IUNIT, IERR, IWORK(*)
      DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, R(N), Z(N), P(N)
      DOUBLE PRECISION DZ(N), RWORK(*)
      EXTERNAL MATVEC, MSOLVE
!
!         Check some of the input data.
!***FIRST EXECUTABLE STATEMENT  DCG
      ITER = 0
      IERR = 0
      IF( N.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      TOLMIN = 500.0*D1MACH(3)
      IF( TOL.LT.TOLMIN ) THEN
         TOL = TOLMIN
         IERR = 4
      ENDIF
!
!         Calculate initial residual and pseudo-residual, and check
!         stopping criterion.
      CALL MATVEC(N, X, R, NELT, IA, JA, A, ISYM)

      R(1:n) = B(1:n) - R(1:n)

      CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!
      IF( ISDCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, TOL, &
           ITMAX, ITER, ERR, IERR, IUNIT, R, Z, P, DZ, &
           RWORK, IWORK, AK, BK, BNRM, SOLNRM) .NE. 0 ) GO TO 200
      IF( IERR.NE.0 ) RETURN
!
!         ***** Iteration loop *****
!
      DO 100 K=1,ITMAX
         ITER = K
!
!         Calculate coefficient bk and direction vector p.
         BKNUM = DDOT(N, Z, 1, R, 1)
         IF( BKNUM.LE.0.0D0 ) THEN
            IERR = 5
            RETURN
         ENDIF
         IF(ITER .EQ. 1) THEN
            CALL DCOPY(N, Z, 1, P, 1)
         ELSE
            BK = BKNUM/BKDEN
            DO 20 I = 1, N
               P(I) = Z(I) + BK*P(I)
 20         CONTINUE
         ENDIF
         BKDEN = BKNUM
!
!         Calculate coefficient ak, new iterate x, new residual r,
!         and new pseudo-residual z.
         CALL MATVEC(N, P, Z, NELT, IA, JA, A, ISYM)
         AKDEN = DDOT(N, P, 1, Z, 1)
         IF( AKDEN.LE.0.0D0 ) THEN
            IERR = 6
            RETURN
         ENDIF
         AK = BKNUM/AKDEN
         CALL DAXPY(N, AK, P, 1, X, 1)
         CALL DAXPY(N, -AK, Z, 1, R, 1)
         CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!
!         check stopping criterion.
         IF( ISDCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, TOL, &
              ITMAX, ITER, ERR, IERR, IUNIT, R, Z, P, DZ, RWORK, &
              IWORK, AK, BK, BNRM, SOLNRM) .NE. 0 ) GO TO 200
!
 100  CONTINUE
!
!         *****   end of loop  *****
!
!         stopping criterion not satisfied.
      ITER = ITMAX + 1
      IERR = 2
!
 200  RETURN
      END

      FUNCTION ISDCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, &
           TOL, ITMAX, ITER, ERR, IERR, IUNIT, R, Z, P, DZ, &
           RWORK, IWORK, AK, BK, BNRM, SOLNRM)
!***BEGIN PROLOGUE  ISDCG
!***REFER TO  DCG, DSDCG, DSICCG
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(ISDCG-D),
!             Linear system, Sparse, Stop Test
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Preconditioned Conjugate Gradient Stop Test.
!            This routine calculates the stop test for the Conjugate
!            Gradient iteration scheme.  It returns a nonzero if the
!            error estimate (the type of which is determined by ITOL)
!            is less than the user specified tolerance TOL.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX, ITER
!     INTEGER IERR, IUNIT, IWORK(USER DEFINED)
!     DOUBLE PRECISION B(N), X(N), A(N), TOL, ERR, R(N), Z(N)
!     DOUBLE PRECISION P(N), DZ(N), RWORK(USER DEFINED), AK, BK
!     DOUBLE PRECISION BNRM, SOLNRM
!     EXTERNAL MSOLVE
!
!     IF( ISDCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, TOL,
!    $     ITMAX, ITER, ERR, IERR, IUNIT, R, Z, P, DZ, RWORK, IWORK,
!    $     AK, BK, BNRM, SOLNRM) .NE. 0 ) THEN ITERATION DONE
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :IN       Double Precision X(N).
!         The current approximate solution vector.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(NELT).
! A      :IN       Double Precision A(NELT).
!         These arrays should hold the matrix A in either the SLAP
!         Triad format or the SLAP Column format.  See ``Description''
!         in the DCG, DSDCG or DSICCG routines.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! MSOLVE :EXT      External.
!         Name of a routine which solves a linear system MZ = R for
!         Z given R with the preconditioning matrix M (M is supplied via
!         RWORK and IWORK arrays).  The name of the MSOLVE routine must
!         be declared external in the calling program.  The calling
!         sequence to MSLOVE is:
!             CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         Where N is the number of unknowns, R is the right-hand side
!         vector, and Z is the solution upon return.  RWORK is a double
!         precision
!         array that can be used to pass necessary preconditioning
!         information and/or workspace to MSOLVE.  IWORK is an integer
!         work array for the same purpose as RWORK.
! ITOL   :IN       Integer.
!         Flag to indicate type of convergence criterion.
!         If ITOL=1, iteration stops when the 2-norm of the residual
!         divided by the 2-norm of the right-hand side is less than TOL.
!         If ITOL=2, iteration stops when the 2-norm of M-inv times the
!         residual divided by the 2-norm of M-inv times the right hand
!         side is less than tol, where M-inv is the inverse of the
!         diagonal of A.
!         ITOL=11 is often useful for checking and comparing different
!         routines.  For this case, the user must supply the ``exact''
!         solution or a very accurate approximation (one with an error
!         much less than tol) through a common block,
!         COMMON /SOLBLK/ SOLN( )
!         if ITOL=11, iteration stops when the 2-norm of the difference
!         between the iterative approximation and the user-supplied
!         solution divided by the 2-norm of the user-supplied solution
!         is less than tol.
! TOL    :IN       Double Precision.
!         Convergence criterion, as described above.
! ITMAX  :IN       Integer.
!         Maximum number of iterations.
! ITER   :IN       Integer.
!         The iteration for which to check for convergence.
! ERR    :OUT      Double Precision.
!         Error estimate of error in the X(N) approximate solution, as
!         defined by ITOL.
! IERR   :OUT      Integer.
!         Error flag.  IERR is set to 3 if ITOL is not on of the
!         acceptable values, see above.
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration,
!         if this is desired for monitoring convergence.  If unit
!         number is 0, no writing will occur.
! R      :IN       Double Precision R(N).
!         The residual R = B-AX.
! Z      :WORK     Double Precision Z(N).
!         Workspace used to hold the pseudo-residual M Z = R.
! P      :IN       Double Precision P(N).
!         The conjugate direction vector.
! DZ     :WORK     Double Precision DZ(N).
!         Workspace used to hold temporary vector(s).
! RWORK  :WORK     Double Precision RWORK(USER DEFINABLE).
!         Double Precision array that can be used by MSOLVE.
! IWORK  :WORK     Integer IWORK(USER DEFINABLE).
!         Integer array that can be used by MSOLVE.
! BNRM   :INOUT    Double Precision.
!         Norm of the right hand side.  Type of norm depends on ITOL.
!         Calculated only on the first call.
! SOLNRM :INOUT    Double Precision.
!         2-Norm of the true solution, SOLN.  Only computed and used
!         if ITOL = 11.
!
! *Function Return Values:
!       0 : Error estimate (determined by ITOL) is *NOT* less than the
!           specified tolerance, TOL.  The iteration must continue.
!       1 : Error estimate (determined by ITOL) is less than the
!           specified tolerance, TOL.  The iteration can be considered
!           complete.
!
! *Precision:           Double Precision
! *See Also:
!       DCG, DSDCG, DSICCG
!
! *Cautions:
!     This routine will attempt to write to the fortran logical output
!     unit IUNIT, if IUNIT .ne. 0.  Thus, the user must make sure that
!     this  logical  unit  must  be  attached  to  a  file or terminal
!     before calling this routine with a non-zero  value  for   IUNIT.
!     This routine does not check for the validity of a non-zero IUNIT
!     unit number.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  MSOLVE, DNRM2
!***COMMON BLOCKS    SOLBLK
!***END PROLOGUE  ISDCG
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
      INTEGER ITER, IERR, IUNIT, IWORK(*)
      DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, R(N)
      DOUBLE PRECISION Z(N), P(N), DZ(N), RWORK(*)
      EXTERNAL MSOLVE
      COMMON /SOLBLK/ SOLN(1)
!
!***FIRST EXECUTABLE STATEMENT  ISDCG
      ISDCG = 0
!
      IF( ITOL.EQ.1 ) THEN
!         err = ||Residual||/||RightHandSide|| (2-Norms).
         IF(ITER .EQ. 0) BNRM = DNRM2(N, B, 1)
         ERR = DNRM2(N, R, 1)/BNRM
      ELSE IF( ITOL.EQ.2 ) THEN
!                  -1              -1
!         err = ||M  Residual||/||M  RightHandSide|| (2-Norms).
         IF(ITER .EQ. 0) THEN
            CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
            BNRM = DNRM2(N, DZ, 1)
         ENDIF
         ERR = DNRM2(N, Z, 1)/BNRM
      ELSE IF( ITOL.EQ.11 ) THEN
!         err = ||x-TrueSolution||/||TrueSolution|| (2-Norms).
         IF(ITER .EQ. 0) SOLNRM = DNRM2(N, SOLN, 1)

         DZ(1:n) = X(1:n) - SOLN(1:n)

         ERR = DNRM2(N, DZ, 1)/SOLNRM
      ELSE
!
!         If we get here ITOL is not one of the acceptable values.
         ERR = 1.0E10
         IERR = 3
      ENDIF
!
      IF(IUNIT .NE. 0) THEN
         IF( ITER.EQ.0 ) THEN
            WRITE(IUNIT,1000) N, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
      IF(ERR .LE. TOL) ISDCG = 1
      RETURN
 1000 FORMAT(' Preconditioned Conjugate Gradient for ', &
           'N, ITOL = ',I5, I5, &
           /' ITER','   Error Estimate','            Alpha', &
           '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!------------- LAST LINE OF ISDCG FOLLOWS ------------------------------
      END
!DECK SLLTI2
      SUBROUTINE SLLTI2(N, B, X, NEL, IEL, JEL, EL, DINV)
!***BEGIN PROLOGUE  SLLTI2
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(SLLTI2-S),
!             Symmetric Linear system solve, Sparse,
!             Iterative Precondition, Incomplete Factorization
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP back solve routine for LDL' Factorization.
!            Routine to solve a system of the  form  L*D*L' X  =  B,
!            where L is a unit lower triangular  matrix  and  D is a
!            diagonal matrix and ' means transpose.
!***DESCRIPTION
! *Usage:
!     INTEGER N,  NEL, IEL(N+1), JEL(NEL)
!     DOUBLE PRECISION B(N), X(N), EL(NEL), DINV(N)
!
!     CALL SLLTI2( N, B, X, NEL, IEL, JEL, EL, DINV )
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right hand side vector.
! X      :OUT      Double Precision X(N).
!         Solution to L*D*L' x = b.
! NEL    :IN       Integer.
!         Number of non-zeros in the EL array.
! IEL    :IN       Integer IEL(N+1).
! JEL    :IN       Integer JEL(NEL).
! EL     :IN       Double Precision     EL(NEL).
!         IEL, JEL, EL contain the unit lower triangular factor   of
!         the incomplete decomposition   of the A  matrix  stored in
!         SLAP Row format.   The diagonal of ones *IS* stored.  This
!         structure can be set  up  by  the DS2LT routine. See
!         "Description", below for more details about the   SLAP Row
!         format.
! DINV   :IN       Double Precision DINV(N).
!         Inverse of the diagonal matrix D.
!
! *Description:
!       This routine is supplied with  the SLAP package as a routine
!       to perform the MSOLVE operation in the SCG iteration routine
!       for  the driver  routine DSICCG.   It must be called via the
!       SLAP  MSOLVE calling sequence  convention  interface routine
!       DSLLI.
!         **** THIS ROUTINE ITSELF DOES NOT CONFORM TO THE ****
!               **** SLAP MSOLVE CALLING CONVENTION ****
!
!       IEL, JEL, EL should contain the unit lower triangular factor
!       of  the incomplete decomposition of  the A matrix  stored in
!       SLAP Row format.   This IC factorization  can be computed by
!       the  DSICS routine.  The  diagonal  (which is all one's) is
!       stored.
!
!       ==================== S L A P Row format ====================
!       This routine requires  that the matrix A  be  stored  in the
!       SLAP  Row format.   In this format  the non-zeros are stored
!       counting across  rows (except for the diagonal  entry, which
!       must appear first in each "row") and  are stored in the
!       double precision
!       array A.  In other words, for each row in the matrix put the
!       diagonal entry in  A.   Then   put  in the   other  non-zero
!       elements   going  across the  row (except   the diagonal) in
!       order.   The  JA array  holds   the column   index for  each
!       non-zero.   The IA  array holds the  offsets into  the JA, A
!       arrays  for   the   beginning  of   each  row.   That    is,
!       JA(IA(IROW)),  A(IA(IROW)) points  to  the beginning  of the
!       IROW-th row in JA and A.   JA(IA(IROW+1)-1), A(IA(IROW+1)-1)
!       points to the  end of the  IROW-th row.  Note that we always
!       have IA(N+1) =  NELT+1, where  N  is  the number of rows  in
!       the matrix  and NELT  is the  number   of  non-zeros in  the
!       matrix.
!
!       Here is an example of the SLAP Row storage format for a  5x5
!       Matrix (in the A and JA arrays '|' denotes the end of a row):
!
!           5x5 Matrix         SLAP Row format for 5x5 matrix on left.
!                              1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 12 15 | 22 21 | 33 35 | 44 | 55 51 53
!       |21 22  0  0  0|  JA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  IA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       With  the SLAP  Row format  the "inner loop" of this routine
!       should vectorize   on machines with   hardware  support  for
!       vector gather/scatter operations.  Your compiler may require
!       a  compiler directive  to  convince   it that there  are  no
!       implicit vector  dependencies.  Compiler directives  for the
!       Alliant FX/Fortran and CRI CFT/CFT77 compilers  are supplied
!       with the standard SLAP distribution.
!
! *Precision:           Double Precision
! *See Also:
!       DSICCG, DSICS
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  SLLTI2
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      INTEGER N, NEL, IEL(NEL), JEL(1)
      DOUBLE PRECISION B(N), X(N), EL(NEL), DINV(N)
!
!         solve  l*y = b,  storing result in x.
!***FIRST EXECUTABLE STATEMENT  SLLTI2
 
      X(1:n) = B(1:n)

      DO 30 IROW = 1, N
         IBGN = IEL(IROW) + 1
         IEND = IEL(IROW+1) - 1
         IF( IBGN.LE.IEND ) THEN
!LLL. OPTION ASSERT (NOHAZARD)
!DIR$ IVDEP
!VD$ NOCONCUR
!VD$ NODEPCHK
            DO 20 I = IBGN, IEND
               X(IROW) = X(IROW) - EL(I)*X(JEL(I))
 20         CONTINUE
         ENDIF
 30   CONTINUE
!
!         Solve  D*Z = Y,  storing result in X.
!
      DO 40 I=1,N
         X(I) = X(I)*DINV(I)
 40   CONTINUE
!
!         Solve  L-trans*X = Z.
!
      DO 60 IROW = N, 2, -1
         IBGN = IEL(IROW) + 1
         IEND = IEL(IROW+1) - 1
         IF( IBGN.LE.IEND ) THEN
!LLL. OPTION ASSERT (NOHAZARD)
!DIR$ IVDEP
!VD$ NOCONCUR
!VD$ NODEPCHK
            DO 50 I = IBGN, IEND
               X(JEL(I)) = X(JEL(I)) - EL(I)*X(IROW)
 50         CONTINUE
         ENDIF
 60   CONTINUE
!
      RETURN
!------------- LAST LINE OF SLTI2 FOLLOWS ----------------------------
      END


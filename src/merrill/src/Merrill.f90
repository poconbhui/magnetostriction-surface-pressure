MODULE Merrill
  ! Convenience module which loads and initializes all the modules used in
  ! a typical MERRILL program.

  USE Material_Parameters
  USE Tetrahedral_Mesh_Data
  USE Finite_Element
  USE Magnetization_Path
  USE Hubert_Minimizer
  USE Energy_Calculator
  USE Magnetostriction
  USE Utils

  IMPLICIT NONE

  CONTAINS

  SUBROUTINE InitializeMerrill()
      CALL InitializeUtils()
      CALL InitializeMaterialParameters()
      call InitializeHubertMinimizer()
      write(*,*) 'Machine epsilon =', MachEps
      CALL InitializeTetrahedralMeshData()
      CALL InitializeMagnetizationPath()
      CALL InitializeMagnetostriction()
  END SUBROUTINE InitializeMerrill

END MODULE Merrill

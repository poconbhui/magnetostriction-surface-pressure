!     ***********************************************************************
!     *                   MERRILL 1.0.2                                     *
!     * Micromagnetic Earth Related Robust Interpreter Language Laboratory  *
!     *                   Finite element solver                             *
!     *                   by                                                *
!     *                   Wyn Williams 2005                                 *
!     *                   with contributions                                *
!     *                   Hubert-Minimization / Paths /Scripting            *
!     *                   by Karl Fabian  2014                              *
!     *                                                                     *
!     *                  Open source routines  by                           *
!     *                  Press et al. :   sorting                           *
!     *                  G. Benthien  :   string module                     *
!     *                  NAG library  :   sparse matrix solver              *
!     ***********************************************************************

PROGRAM BEM_Script

  USE Loop_Parser
  USE strings
  USE Merrill

  IMPLICIT NONE

  EXTERNAL DSDGMR
  CHARACTER(len=32), EXTERNAL  ::  md5

  INTEGER i,ios, nargs, linecnt, cnt, sys
  CHARACTER (LEN=200) ::  scriptfile ,line, args(20), command, tempstr
  LOGICAL :: exist2
  DOUBLE PRECISION dbltmp
  CHARACTER(LEN=1024) :: sysline


  !---------------------------------------------------------------
  !            Initialize all modules used.
  !---------------------------------------------------------------

  CALL InitializeMerrill()


  !cccccccccccccccccc  Argument is the name of the script file  ccccccccccccccc
  call GET_COMMAND_ARGUMENT(1, scriptfile)
  call RemoveSp(scriptfile)
  WRITE(*,*)  'MERRILL 1.0.2 '
  WRITE(*,*)  'Script file is : <'//scriptfile(:LEN_TRIM(scriptfile))//'>'


  !  CHECK IF INPUT FILES EXIST
  INQUIRE(FILE=scriptfile, EXIST=exist2)
  DO WHILE(exist2.EQV..FALSE.)
    IF(exist2) THEN
      CONTINUE
    ELSE
      WRITE(*,*) 'Cannot OPEN file :', scriptfile
      WRITE(*,*) 'Input new script file name:'
      READ(*,*) scriptfile
      scriptfile=scriptfile(:LEN_TRIM(scriptfile))
      INQUIRE(FILE=scriptfile, EXIST=exist2)
      IF(exist2) WRITE(*,*) 'Found scriptfile :',scriptfile
    ENDIF
  ENDDO

  !---------------------------------------------------------------
  !            SCRIPT LANGUAGE INTERPRETER
  !---------------------------------------------------------------
  !            Start reading and executing scriptfile.
  !            Relies heavily on the parsing functions of Module strings
  !            by  G. Benthien (http://gbenthien.net/strings/)
  !---------------------------------------------------------------
  !
  call ReadLoopFile(scriptfile)  ! Performs loop parser

  DO linecnt=1,AllLineCnt

    nargs=NArgsOut(linecnt)
       args(1:nargs)=LineOutStack(linecnt,1:nargs)
       args(nargs+1:) = ""

    command=lowercase(args(1))
    line=''
    Do i=1,nargs
      line=line(:LEN_TRIM(line))//' '//args(i)(:LEN_TRIM(args(i)))
    End Do
    Write(*,*) " Parsing : "//line(:LEN_TRIM(line))
    select case(command)

      case('print')
        line=''
        Do i=2,nargs
        line=line(:LEN_TRIM(line))//' '//args(i)(:LEN_TRIM(args(i)))
        End Do
        Write(*,*) line(:LEN_TRIM(line))

      case('systemcommand')
        if (nargs>2) then
          sysline=''
          Do i=2,nargs
          sysline=sysline(:LEN_TRIM(sysline))//' '//args(i)(:LEN_TRIM(args(i)))
          End Do
        endif
        CALL EXECUTE_COMMAND_LINE(sysline, exitstat=sys)
        if(sys.eq.0) then
          write(*,*)          '          `-> OK'
        else
          write(*,'(A20,I3)') '          `-> Error:',sys
        endif

      case('set')
        call setval(args(2),args(3))

      case('cubic','uniaxial','uni')
        call setanis(args(1))

      ! define phi, theta, alpha of cubic axes rotation matrix
      case('cubicrotation')
        if (nargs>3) then
          call value(args(2),dbltmp,ios)
          if(ios.eq.0) THEN
            phirot=dbltmp
            call value(args(3),dbltmp,ios)
            if(ios.eq.0) then
              thetarot= dbltmp
              call value(args(4),dbltmp,ios)
              if(ios.eq.0) then
                alpharot= dbltmp
                call SetRotationMatrix()
              endif
            endif
          endif
        endif

      ! eg "magnetostriction on", "magnetostriction off"
      case('magnetostriction')
        IF ( nargs .EQ. 1 ) THEN
          calculating_magnetostriction = .TRUE.
        ELSE IF ( nargs .EQ. 2 ) THEN
          IF( uppercase(TRIM(args(2))) .EQ. "OFF" ) THEN
            calculating_magnetostriction = .FALSE.
          ELSE
            calculating_magnetostriction = .TRUE.
          END IF
        ELSE
          WRITE(*,*) "Error setting magnetostriction: too many arguments!"
        END IF

        IF ( calculating_magnetostriction .EQV. .TRUE. ) THEN
          WRITE(*,*) "Calculating magnetostriction."
        ELSE
          WRITE(*,*) "Not calculating magnetostriction."
        END IF

      case('conjugategradient')
        ConGradQ=.true.

      case('steepestdescent')
        ConGradQ=.false.

      case('easy')
        call seteasyaxis(args(3),args(4),args(5))

      case('mdfive')
        write (*,*) 'MD5:'//md5(args(2))

      !  "Magnetite  400 C"  sets material constants for magnetite at 400 deg C
      case('magnetite')
        if (nargs>2) then
          if(args(3).eq.'C') then
            call value(args(2),dbltmp,ios)
            if(ios.eq.0) THEN
              call magnetite(dbltmp)
            endif
          endif
        endif

      ! 'external field direction 1 0 0 '  or 'magnetic field strength 1e-3'
      case('external','magnetic')
        call setfield(args(3),args(4),args(5),args(6))

      !  e.g.:  ' ReadMesh 4 sphere-7.pat'
      case('readmesh')
        call value(args(2),cnt,ios)
        if( (ios.EQ.0).and. (cnt.LE.maxmeshnumber)) then
          meshfile=args(3)
          call readmeshpat() !-> MODULE Tetrahedral_Mesh_Data
          NMAX=NNODE
          CALL demagstiff( )      !-> MODULE Finite_Element
          CALL nonzerostiff( )    !-> MODULE Finite_Element
          CALL forcemat()         !-> MODULE Finite_Element
          CALL boundmata()        !-> MODULE Finite_Element
          write(*,*) 'Number of boundary nodes',bnode
          !  Allocates field variables -> MODULE Material_Parameters
          CALL FieldAllocate( )
          call SaveMesh(cnt)
          call SaveFEM(cnt)
        endif

      !  e.g.:  ' LoadMesh 2'
      case('loadmesh')
        call value(args(2),cnt,ios)
        if( (ios.EQ.0).and. (cnt.LE.maxmeshnumber)) then
          call LoadMesh(cnt)
          call LoadFEM(cnt)
        endif

      !  e.g.:  ' ReadMag trapez.dat'  Make sure it fits to the current mesh!!
      case('readmag','readmagnetization')
        call readmag(args(2))

      !  e.g.:  ' Minimize'
      ! Calls the minimization routine for the current mesh and magnetization
      case('minimize')
        call EnergyMin( )

      !  e.g.:  ' PathMinimize'
      ! Calls the path minimization routine for the current path
      case('pathminimize')
        call PathMinimize( )

      !  e.g.:  'ReMesh 3'
      ! Remeshes current magnetization to the saved mesh with index 3
      ! and loads this
      case('remesh')
        call value(args(2),cnt,ios)
        if( (ios.EQ.0).and. (cnt.LE.maxmeshnumber)) then
          call RemeshTo(cnt)
          call loadmesh(cnt)
          call loadFEM(cnt)
          m(:,:)=mremesh(:,:)
        endif

      !  e.g.:  ' Resize 500 100'
      ! resizes mesh lengthes from 500 nm to 100 nm, len=len/500*100
      case('resize')
        call value(args(2),dbltmp,ios)
        if(ios.eq.0) THEN
          Ls=Sqrt(Ls)*dbltmp ! Note that lengthes are  Len = 1/Sqrt(Ls)
          call value(args(3),dbltmp,ios)
          if(ios.eq.0) Ls=(Ls/dbltmp)
          ls=ls*ls
        endif

      !  e.g.:  ' WriteMag trapez'  uses stem 'trapez'
      case('writemag','writemagnetization')
        if (nargs>1) then
          stem=args(2)
          call compact(stem)
          datafile=stem(:LEN_TRIM(stem))//'.dat'
        endif
        !  Writes mag ->  MODULE Tetrahedral_Mesh_Data
        call WRITEmag()
        ! zoneflag = -1.0 implies use of zone/zoneinc
        call WRITETecplot(zoneflag,stem,1)

      ! e.g.: ' WriteHyst trapez' uses stem 'trapez' to output file 'trapez.hyst'
      case('writehyst')
        if (nargs>1) then
          stem=args(2)
          call compact(stem)
          hystfile=stem(:LEN_TRIM(stem))//'.hyst'
        endif
        INQUIRE(FILE=hystfile, EXIST = exist2)
        if(exist2.EQV..FALSE.) then
          OPEN(unit=405, file=hystfile, status='unknown')
          WRITE(405, '(A7,A14,A9,A11,A11)') "M.H", "H (Telsa)","Mx","My","Mz"
          CLOSE(405)
        endif
        call WRITEhyst()

      case('writedisplacement')
        if(nargs .NE. 2) THEN
          WRITE(*,*) "WriteDisplacement Error: Expected 2 arguments"
        endif
        call WriteDisplacement(TRIM(args(2)) // ".dat")
        call WriteDisplacementTecplot(TRIM(args(2)) // ".tec")

      !  stop script
      case('stop','end')
        exit

      ! tests the energy gradient
      case('gradienttest')
        call GradientTest()

      ! reports the micromagnetic energies, e.g. for test purposes
      case('reportenergy')
        call ReportEnergy()

      !  Wait for [enter]
      case('keypause')
        Write(*,*) ' Pause: Press any key + ENTER to continue'
        Read (*,*) tempstr

      !  "uniform magnetization 0 1 1"
      ! Create uniform   magnetization  with defined direction
      case('uniform')
        if (nargs>5) call setuniform(args(3),args(4),args(5),args(6))
        if (nargs==5) then
          args(6)='0'
          call setuniform(args(3),args(4),args(5),args(6))
        endif

      ! "randomize magnetization 20"
      ! Changes current magnetization by random angles <20 deg
      case('randomize')
        if (nargs>2) then
          call randomchange(args(2), args(3)) ! ->Module_Mesh
        endif

      ! "invert magnetization 1 -1 1"
      ! Changes current magnetization mx,my,mz -> mx, -my, mz
      case('invert')
        if (nargs>4) then
          call InvertMag(args(3), args(4), args(5)) ! ->Module_Mesh
        endif

      ! Start logging the energy
      case('energylog','logfile')
        if (nargs>1) then
          write(*,*) 'logfile: ', logfile
          stem=args(2)
          call compact(stem)
          logfile=stem(:LEN_TRIM(stem))//'.log'
          if(EnergyLogging) Close(607)
          EnergyLogging=.TRUE.
          OPEN(unit=607,file=logfile,status='unknown')
          write(607, '(A)') "Micromagnetic energy calculation : BEMScript 2014"
          write(607, '(A)') "Mesh data:"
          call tstamp(607)
          write(607, '(5A14)') "Nodes","Tetrahedra", "Bndry nodes", &
            "Bndry faces", "Volume"
          write(607, '(4I14,E14.6)') NNODE,NTRI,BNODE,BFCE,total_volume
          write(607, '(A)') "Material data:"
          write(607, '(7A14)') "Ms","K1", "A_ex", "Vol^(1/3) (m)", &
            "QHardness", "Lam_Ex (nm)", "Kd V (J)"
          write(607, '(7E14.6)') &
            Ms, K1, Aex, (total_volume**(1./3.))/sqrt(Ls), &
            QHardness, LambdaEx*1.d9, &
            Kd*total_volume/(sqrt(Ls)**3)
          write(607, '(A)') "External field direction (x,y,z) &
            &and strength B (T):"
          write(607, '(4A14)') "hx","hy", "hz", "B (T)"
          write(607, '(4E14.6)')  hz(1),hz(2),hz(3),extapp
          write(607, '(A)') "Energies in units of Kd V:"
          write(607, '(2A12,12A15)') &
            "Global-N-Eval", "N-Eval","E-Anis", "E-Exch1","E-Exch2",&
            "E-Exch3","E-Exch4", "E-ext", "E-Demag",&
            "E-Tot","Mx","My","Mz"
        endif

      ! Start logging the energy
      case('pathlogfile')
        if (nargs>1) then
          stem=args(2)
          call compact(stem)
          PLogEnFile=stem(:LEN_TRIM(stem))//'.enlog'
          PLogGradFile=stem(:LEN_TRIM(stem))//'.grlog'
          PLogDistFile=stem(:LEN_TRIM(stem))//'.dlog'
          PLogFile=stem(:LEN_TRIM(stem))//'-last.dat'
          if(PathLoggingQ) Then
            Close(607)
            Close(608)
            Close(609)
          endif
          PathLoggingQ=.TRUE.
          OPEN(unit=607,file=PLogEnFile,status='unknown')
          OPEN(unit=608,file=PLogGradFile,status='unknown')
          OPEN(unit=609,file=PLogDistFile,status='unknown')
          write(607, '(A)') "Micromagnetic path energies: MERRILL 2015"
          write(607, '(A)') "Mesh data:"
          call tstamp(607)
          write(607, '(5A14)') "Nodes","Tetrahedra", "Bndry nodes", &
            "Bndry faces", "Volume"
          write(607, '(4I14,E14.6)') NNODE,NTRI,BNODE,BFCE,total_volume
          write(607, '(A)') "Material data:"
          write(607, '(7A14)') "Ms","K1", "A_ex", "V^(1/3) (m)", "QHardness", &
            "Lam_Ex (nm)", "Kd V (J)"
          write(607, '(7E14.6)') &
            Ms, K1, Aex, &
            (total_volume**(1./3.))/sqrt(Ls), QHardness, &
            LambdaEx*1.d9,Kd*total_volume/(sqrt(Ls)**3)
          write(607, '(A)') "External field direction (x,y,z) &
            &and strength B (T):"
          write(607, '(4A14)') "hx","hy", "hz", "B (T)"
          write(607, '(4E14.6)')  hz(1),hz(2),hz(3),extapp
          write(607, '(A13,I5)') "Path Length: ",PathN

          write(607, *) "Path Energies in Kd V: "
          write(608, '(A)') "Micromagnetic path energy gradient norms: &
            &MERRILL 2015"
          write(609, '(A)') "Micromagnetic path cumulative distances: &
            &MERRILL 2015"
        endif

      ! Stop logging the energy
      case('endlog','closelog','closelogfile')
        if(EnergyLogging) then
          Close(607)
          EnergyLogging=.FALSE.
        endif
        if(PathLoggingQ) Then
          Close(607)
          Close(608)
          Close(609)
        endif
        PathLoggingQ=.FALSE.

      ! Saves current magnetization to path index
      case('magtopath','magnetizationtopath')
        call value(args(2),cnt,ios)
        if(ios.EQ.0) then
          if (cnt<0) cnt=PathN+1-cnt
          if (cnt>0 .AND. cnt<(PathN+1))  PMag(cnt,:,:)= m(:,:)
        endif

      ! Saves current magnetization to path index
      case('pathtomag','pathtomagnetization')
        call value(args(2),cnt,ios)
        if(ios.EQ.0) then
          if (cnt<0) cnt=PathN+1-cnt
          if (cnt>0 .AND. cnt<(PathN+1))  m(:,:)=PMag(cnt,:,:)
        endif

      ! defines the Path distances assuming all magnetizations are filled
      case( 'pathrenew','renewpath','makepath')
        call PathRenewDist( )

      ! defines the Path variables assuming all magnetizations are filled
      case( 'makeinitialpath')
        call MakeInitialPath()

      ! Refines current path to new number of states
      case( 'refinepathto')
        call value(args(2),cnt,ios)
        if(ios.EQ.0) then
          if (cnt>1 ) call RefinePathTo(cnt)
        endif

      ! Exports current PATH to TecPlot file
      case( 'writetecplotpath')
        if (nargs>1) then
          PathOutFile=args(2)
          call WriteTecplotPath( )
        endif

      ! Imports path from a TecPlot file
      case( 'readtecplotpath')
        if (nargs>1) then
          PathInFile=args(2)
          call ReadTecplotPath( )
        endif

      ! Calculates energy for every structure along a path.
      case( 'pathstructureenergies')
        do i = 1, pathn
          dbltmp = structureEnergy(i)
          ! Normalize the energy by the length scale cubed this should result in
          ! and energy value that is in Joules
          dbltmp = dbltmp / sqrt(Ls)**3
          write(*,*) i, dbltmp
        end do

      ! Calculate energy for the current structure
      case( 'currentenergy')
        dbltmp = currentMagEnergy()
        write(*,*) 'current magnetization energy = ', dbltmp

      ! Writes each structure as a separate tecplot file.
      case( 'writepathstructures')
        do i = 1, pathn
           write(*,*) "it's the stem: ", stem
        end do

      ! Parser commands
      case( 'define','undefine','addto')
        continue

      case default
        write(*,*) '       `-> Ignored '
    end select

  END DO

  CLOSE(174)
  if(EnergyLogging) Close(607)
  STOP 'Scripting finished'


CONTAINS

  !ccccccccccccccccccccccccccccccccccccccc

  !---------------------------------------------------------------
  !  subroutine setval(var,val)
  !---------------------------------------------------------------
  subroutine setval(var,val)

    USE strings
    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE magnetostriction

    CHARACTER (LEN=100) ::  var, val, lvar
    integer iv, ios
    double precision dv

    iv=-1
    dv=-1.
    ios=0
    lvar=lowercase(var)
    select case(lvar)
      case('pathn','numberofpathstates')
        call value(val,iv,ios)
        if (ios/=0) goto 77
        PathN = iv
        call PathAllocate()

      case('exchangecalculator')
        call value(val,iv,ios)
        if (ios/=0) goto 77
        if ((iv<5) .and. (iv>0)) WhichExchange = iv
        write (*,*) '   `-> Exchange calculator :',iv

      case('zone','zonevalue' )
        call value(val,dv,ios)
        if (ios/=0) goto 77
        zone = dv
        zoneflag=-1.0 ! Flag for using zone in WriteTecPlot()

      case('zoneinc','zoneincrement')
        call value(val,dv,ios)
        if (ios/=0) goto 77
        zoneinc = dv

      case('mu','permeability')
        call value(val,dv,ios)
        if (ios/=0) goto 77
        mu = dv

      case('lengthscale','ls')  ! in 1/m (legacy)
        call value(val,dv,ios)
        if (ios/=0) goto 77
        ls = dv*dv  !!  Square !
        EnergyUnit= Kd*total_volume/(Ls**(3.0/2.0))

      case('meshscale' )  !  Length scale used in the mesh file (m)
        call value(val,dv,ios)
        if (ios/=0) goto 77
        ls =1./( dv*dv)  !!  Square !
        EnergyUnit= Kd*total_volume/(Ls**(3.0/2.0))


      ! Usual magnetic parameters
      case('ms','satmagnetization')
        call value(val,dv,ios)
        if (ios/=0) goto 77
        Ms = dv
        Kd= 4* (3.1415926535897932)*mu*Ms*Ms*0.5
        EnergyUnit= Kd*total_volume/(Ls**(3.0/2.0))
        if(NONZERO(Aex)) LambdaEx= Sqrt(Aex/Kd)

      case('exchange','aex')
        call value(val,dv,ios)
        if (ios/=0) goto 77
        Aex = dv
        if(NONZERO(Kd)) LambdaEx= Sqrt(Aex/Kd)

      case('anisotropy','k1')
        call value(val,dv,ios)
        if (ios/=0) goto 77
        K1 = dv
        if(NONZERO(Kd)) QHardness= K1/Kd


      ! Magnetoelastic parameters
      case('c11')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         c11 = dv

      case('c12')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         c12 = dv

      case('c44')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         c44 = dv

      case('b1')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         b1 = dv

      case('b2')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         b2 = dv


      ! HubertMinimizer parameters
      case('ftolerance')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         FTolerance=dv

      case('gtolerance')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         GTolerance=dv

      case('alphascale')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         AlphaScale = dv

      case('minalpha')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         MinAlpha = dv

      case('dalpha')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         DAlpha = dv

      case('typicalenergy')
         call value(val,dv,ios)
         if(ios/=0) goto 77
         TypicalEnergy = dv

      case('nebspring','springconstant')
        call value(val,dv,ios)
        if (ios/=0) goto 77
        PHooke = dv

      case('curvatureweight' )
        call value(val,dv,ios)
        if (ios/=0) goto 77
        CurveWeight = dv

      case('maxrestarts' )
        call value(val,iv,ios)
        if (ios/=0) goto 77
        MaxRestarts = iv

      case('maxenergyevaluations','maxenergyeval' )
        call value(val,iv,ios)
        if (ios/=0) goto 77
        MaxEnergyEval = iv

      case('maxpathevaluations','maxpatheval' )
        call value(val,iv,ios)
        if (ios/=0) goto 77
        MaxPathEval = iv

      case('meshnumber','maxmeshnumber')
        call value(val,iv,ios)
        if (ios/=0) goto 77
        MaxMeshNumber = iv

      case('stem','filetemplate','filestem')
        stem=val
        datafile=stem(:LEN_TRIM(stem))//'.dat'

      case('allexchange')
        call value(val,iv,ios)
        if (ios/=0) goto 77
        if(iv>0) then
          CalcAllExchQ=.true.
        else
          CalcAllExchQ=.false.
        endif

      case default
        write(*,*) '       `-> Ignored '
    end select
    return
    77  write (*,*)  'ERROR setting variable: ',var,' to value : ', val
  end subroutine setval


  !---------------------------------------------------------------
  !     subroutine setanis(anis)
  !---------------------------------------------------------------

  subroutine setanis(anis)

    USE strings

    CHARACTER (LEN=100) ::  anis, lvar

    lvar=lowercase(anis)
    select case(anis)
      case('cubic','cubicanisotropy')
        anisform='cubic'

      case('uniaxial','uni' )
        anisform='uni'

      case default
        write(*,*) '       `-> Ignored '
    end select
    write(*,*) "anisform: ", anisform
    return
  end subroutine setanis


  !---------------------------------------------------------------
  !     subroutine seteasyaxis(args(3),args(4),args(5))
  !---------------------------------------------------------------

  subroutine seteasyaxis(ax,ay,az)

    USE strings

    CHARACTER (LEN=100) ::  ax,ay,az
    integer   ios
    double precision anix,aniy,aniz
    REAL(KIND=DP) eanorm

    call value(ax,anix,ios)
    if(ios/=0) write (*,*)  'ERROR setting anisotropy x: ',ax
    call value(ay,aniy,ios)
    if(ios/=0) write (*,*)  'ERROR setting anisotropy y: ',ay
    call value(az,aniz,ios)
    if(ios/=0) write (*,*)  'ERROR setting anisotropy z: ',az

    ea(1)=anix
    ea(2)=aniy
    ea(3)=aniz

    eanorm=sqrt(ea(1)**2 + ea(2)**2 + ea(3)**2)
    IF(NONZERO(eanorm)) THEN
      ea(1)=ea(1)/eanorm
      ea(2)=ea(2)/eanorm
      ea(3)=ea(3)/eanorm
    ENDIF

    return
  end subroutine seteasyaxis

  !---------------------------------------------------------------
  !     subroutine setfield(args(3),args(4),args(5),args(6))
  !---------------------------------------------------------------

  subroutine setfield(str,shx,shy,shz)

    USE strings

    CHARACTER (LEN=100) ::  str,shx,shy,shz
    integer   ios
    double precision dh(3)
    REAL(KIND=DP) hznorm

    select case(lowercase(str) )
      case('direction')
        call value(shx,dh(1),ios)
        if(ios/=0) write (*,*)  'ERROR setting external field x: ',shx
        call value(shy,dh(2),ios)
        if(ios/=0) write (*,*)  'ERROR setting external field y: ',shy
        call value(shz,dh(3),ios)
        if(ios/=0) write (*,*)  'ERROR setting external field z: ',shz
        hz(:)=dh(:)
        hznorm=sqrt(hz(1)**2 + hz(2)**2 + hz(3)**2)
        IF(NONZERO(hznorm)) THEN
          hz(1)=hz(1)/hznorm
          hz(2)=hz(2)/hznorm
          hz(3)=hz(3)/hznorm
        ENDIF

      case('strength')
        call value(shx,dh(1),ios)
        if (lowercase(shy).eq.'mt') dh(1)=dh(1)/1000.
        if (lowercase(shy).eq.'mut') dh(1)=dh(1)/1.e6
        extapp=dh(1)

      case default
        write(*,*) '       `-> Ignored '
    end select


    return
  end subroutine setfield


  !---------------------------------------------------------------
  !     subroutine setuniform(args(3),args(4),args(5),args(6))
  !---------------------------------------------------------------

  subroutine setuniform( smx,smy,smz,bstr)

    USE strings

    CHARACTER (LEN=100) ::  smx,smy,smz,bstr
    integer   ios,i,block
    double precision dm(3),dmnorm

    call value(smx,dm(1),ios)
    if(ios/=0) write (*,*)  'ERROR setting magnetization x: ',smx
    call value(smy,dm(2),ios)
    if(ios/=0) write (*,*)  'ERROR setting magnetization y: ',smy
    call value(smz,dm(3),ios)
    if(ios/=0) write (*,*)  'ERROR setting magnetization z: ',smz
    call value(bstr,block,ios)
    if(ios/=0) write (*,*)  'ERROR setting block number: ',block
    dmnorm=sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
    IF(NONZERO(dmnorm)) THEN
      dm(1)=dm(1)/dmnorm
      dm(2)=dm(2)/dmnorm
      dm(3)=dm(3)/dmnorm
    ENDIF
    do i=1,NNODE
      ! set magnetization if in the right block
      ! block=0 means all magnetizations
      if(block==0 .or. block==BlockNumber(i))  m(i,:)=dm(:)
    enddo
    return
  end subroutine setuniform


  !*****************************************************************************
  !
  !! TIMESTAMP prints the current YMDHMS date as a time stamp.
  !
  !  Example:
  !
  !    31 May 2001   9:45:54.872 AM
  !
  !  Licensing:
  !
  !    This code is distributed under the GNU LGPL license.
  !
  !  Modified:
  !
  !    18 May 2013
  !
  !  Author:
  !
  !    John Burkardt
  !
  !  Parameters:
  !
  !    None
  !

  subroutine tstamp (FileID )

    implicit none

    character ( len = 8 ) ampm
    integer ( kind = 4 ) d
    integer ( kind = 4 ) h
    integer ( kind = 4 ) m
    integer ( kind = 4 ) mm
    character ( len = 9 ), parameter, dimension(12) :: month = (/ &
      'January  ', 'February ', 'March    ', 'April    ', &
      'May      ', 'June     ', 'July     ', 'August   ', &
      'September', 'October  ', 'November ', 'December ' /)
    integer ( kind = 4 ) n
    integer ( kind = 4 ) s
    integer ( kind = 4 ) values(8)
    integer ( kind = 4 ) y
    integer ( kind = 4 ) FileID

    call date_and_time ( values = values )

    y = values(1)
    m = values(2)
    d = values(3)
    h = values(5)
    n = values(6)
    s = values(7)
    mm = values(8)

    if ( h < 12 ) then
      ampm = 'AM'
    else if ( h == 12 ) then
      if ( n == 0 .and. s == 0 ) then
        ampm = 'Noon'
      else
        ampm = 'PM'
      end if
    else
      h = h - 12
      if ( h < 12 ) then
        ampm = 'PM'
      else if ( h == 12 ) then
        if ( n == 0 .and. s == 0 ) then
          ampm = 'Midnight'
        else
          ampm = 'AM'
        end if
      end if
    end if

    write ( FileID, '(i2,1x,a,1x,i4,2x,i2,a1,i2.2,a1,i2.2,a1,i3.3,1x,a)' ) &
      d, trim ( month(m) ), y, h, ':', n, ':', s, '.', mm, trim ( ampm )

    return
  end subroutine tstamp

END  PROGRAM BEM_Script

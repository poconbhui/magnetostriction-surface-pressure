MODULE Hubert_Minimizer
    USE Utils
    IMPLICIT NONE

    ! Use Conjugent Gradient steps?
    LOGICAL :: ConGradQ


    ! Conversion tolerances

    ! Finished if consecutive Energies with distance
    ! FTrailLength are < FTolerance.
    DOUBLE PRECISION :: FTolerance

    ! Finished if Average gradient2 < GTolerance
    DOUBLE PRECISION :: GTolerance


    ! Scales the gradient value to a step size
    DOUBLE PRECISION :: AlphaScale

    ! Sets the minimum alpha step size
    DOUBLE PRECISION :: MinAlpha

    ! Controls the alpha acceleration/deceleration
    DOUBLE PRECISION :: DAlpha


    ! Order of magnitude value for typical micromagnetic energy
    DOUBLE PRECISION :: TypicalEnergy

CONTAINS

    SUBROUTINE InitializeHubertMinimizer()
        FTolerance=1.e-8
        GTolerance=1.e-14

        ConGradQ = .true.

        AlphaScale = 0.01
        MinAlpha = 1e-3
        DAlpha = 2.6

        TypicalEnergy = 0
    END SUBROUTINE InitializeHubertMinimizer


    SUBROUTINE HubertMinimize(N, XGUESS, X, G, FVALUE, neval, dgscale)

        USE Material_Parameters
        USE Tetrahedral_Mesh_Data
        USE Finite_Element

        IMPLICIT NONE

        ! SPECIFICATIONS FOR ARGUMENTS
        INTEGER :: N ,neval
        double precision :: dgscale


        ! SPECIFICATIONS FOR LOCAL VARIABLES

        INTEGER :: CREEPCNT, MAXCREEP, FINISHED, &
            FTrailLength, nstart, i, ResetCnt

        DOUBLE PRECISION :: XNEW(N), GRNEW(N), FVALUE, XGUESS(N), X(N), G(N)

        !  For conjugate gradient steps
        DOUBLE PRECISION :: S(N), GOLD(N), beta, GO2, SPG

        DOUBLE PRECISION :: ALPHA, invTE, &
            FNEW, DeltaF, GSQUARE, FTOLMin, &
            FEMTolMax
        DOUBLE PRECISION, ALLOCATABLE :: FTrail(:)


        ! SPECIFICATIONS FOR INTRINSICS
        INTRINSIC  ABS,MAX,MIN

        X=XGUESS
        !  FOR X
        !  DETERMINE INITIAL ENERGY: FVALUE
        !             AND GRADIENT : G


        MAXCREEP=4         ! Controls the number of creep steps

        FTrailLength=13    ! Length of trailing energy value for finishing
        Allocate(FTrail(FTrailLength))
        nstart=1
        FTrail(:)=0.0
        GOLd(:)=0.
        S(:)=0.
        beta=0.

        FEMTolMax=1.d-10         ! Controlling tolerance of linear CG solver
        FEMTolerance=FEMTolMax  ! via global FEMTolerance
        FTOLMin=FEMTolMax       ! should speed up minimization
        ResetCnt=0
        GSQUARE=0.
        ! ConGradQ =.true.  ! Now global variable set in BEMScript.f90


        ! Calculate typical energy scale
        ! (order of magnitude estimate of micromagnetic energy)
        If(.NOT.(NONZERO(TypicalEnergy))) then
            TypicalEnergy=(sqrt(Ls)**3)*sqrt(Kd*sqrt(Aex*abs(K1))) &
                * (total_volume/(sqrt(Ls)**3))**(5./6.)
        endif
        If(.NOT.(NONZERO(TypicalEnergy))) then
            TypicalEnergy=Kd* total_volume
        endif
        If(.NOT.(NONZERO(TypicalEnergy))) then
            TypicalEnergy= Aex*Ls*total_volume**(1./3.)
        endif
        If(.NOT.(NONZERO(TypicalEnergy))) then
            TypicalEnergy=abs(K1)* total_volume
        endif

        write(*,*)  " Typical Energy: ", TypicalEnergy
        invTE=100./TypicalEnergy

        10     GOLD(:)=G(:)
        call  ET_GRAD(fvalue, G, X, neval, dgscale)
        FValue=FValue*invTE
        G(:)=G(:) *invTE


        GO2=GSQUARE
        GSQUARE=0
        SPG=0.
        DO i=1,N
            GSQUARE = GSQUARE + G(i)*G(i)
            SPG =SPG+GOLD(i)*G(i)
        ENDDO
        GSQUARE=GSQUARE
        if(NONZERO(GO2)) then
          ! for Polak–Ribière CG with reset
          beta=MAX(REAL(0.0, KIND=KIND(GO2)), (GSQUARE-SPG)/GO2)
        else
          beta=0.
        endif
        S(:)=G(:) +beta*S(:)  ! updated conjugate gradient direction

        FTrail(nstart)=FValue
        nstart=nstart+1
        IF(nstart>FTrailLength)  nstart=1
        ALPHA=1.

        if(MODULO(NEval,100).eq.0) then
            write(*,'(I5, 4F15.6)') NEval ,ALPHA, DeltaF/FTolerance, &
                sqrt(MeanMag(1)**2+MeanMag(2)**2+MeanMag(3)**2), FNEW
        endif
        if(MODULO(NEval,1000).eq.1) then
            write(*,'(A5, 4A15)') 'NEval' ,'ALPHA','dEn/TOL', 'Mag', 'Energy'
        endif

        !20 FINISHED=0
        FINISHED=0
        30 CREEPCNT=0
        DO WHILE (CREEPCNT < MAXCREEP)

            if(ConGradQ) then
              XNEW(:)= X(:) - ALPHA*AlphaScale*S(:)
            else
              XNEW(:)= X(:) - ALPHA*AlphaScale*G(:)
            endif
            call  ET_GRAD(FNEW,GRNEW,XNEW,neval,dgscale)
            FNEW=FNEW*invTE
            GRNEW(:)=GRNEW(:) *invTE

            FTrail(nstart)=FNEW
            nstart=nstart+1
            IF(nstart>FTrailLength) nstart=1

            if(MODULO(NEval,100).eq.0) then
                write(*,'(I5, 4F15.6)') NEval ,ALPHA, DeltaF/FTolerance, &
                    sqrt(MeanMag(1)**2+MeanMag(2)**2+MeanMag(3)**2), FNEW
            endif
            if(MODULO(NEval,1000).eq.1) then
                write(*,'(A5, 4A15)') 'NEval' ,'ALPHA','dEn/TOL', &
                    'Mag', 'Energy'
            endif

            ! Average step difference between trailing F and new F
            DeltaF=Abs(FTrail(nstart)-FNEW )/FTrailLength

            IF ( DeltaF < FTolerance)  THEN
              Write(*,*) 'Delta F negligible:',DeltaF
              GOTO 100 ! FINISHED Delta F negligible
            ENDIF
            IF ( NEval .GE. MaxEnergyEval)  THEN
              Write(*,*) 'MAX Energy Evaluations reached !  Delta F:',DeltaF
              GOTO 100 ! FINISHED TOO MANY ENERGY CALLS
            ENDIF
            IF(FVALUE<FNEW) THEN
              CREEPCNT=0
              ALPHA= ALPHA/Dalpha/DAlpha
              !		  Write(*,*) 'QQ HM-> ALPHA = ',ALPHA
              IF(ALPHA<MinAlpha) THEN
                ResetCnt=ResetCnt+1
                write(*,'(I5, 4F15.6,A11,I3)') &
                  NEval ,ALPHA, DeltaF/FTolerance, &
                  sqrt(MeanMag(1)**2+MeanMag(2)**2+MeanMag(3)**2), &
                  FNEW,' ->/```** magic: ', ResetCnt
                if(ResetCnt>0) then
                  ! Try magic !
                  ! 	  Write(*,*)   '**  --> MAGIC '

                  ! Reset angles
                  ! (Implicit random perturbation !?
                  ! Remove gimbal lock effects?)
                  do i=1,NNODE
                      m(i,1)=sin(X(2*i-1))*cos(X(2*i))
                      m(i,2)=sin(X(2*i-1))*sin(X(2*i))
                      m(i,3)=cos(X(2*i-1))
                      X(2*i-1)=acos(m(i,3))
                      X(2*i)= atan2(m(i,2),m(i,1))
                  enddo
                endif
                if(ResetCnt>20) then
                  write(*,*) '+++++   FAILED TO CONVERGE +++++'
                  goto 100
                endif

                GOTO 10 !    !!  ALPHA TOO SMALL: RESTART
              ENDIF
            ELSE
              CREEPCNT=CREEPCNT+1
              FVALUE=FNEW
              X(:)= XNEW(:)
              GOLD(:)=G(:)
              G(:)=GRNEW(:)

              GO2=GSQUARE
              GSQUARE=0
              SPG=0.
              DO i=1,N
                  GSQUARE = GSQUARE + GRNEW(i)*GRNEW(i)
                  SPG =SPG+GOLD(i)*GRNEW(i)
              ENDDO
              ! for Polak–Ribière CG with reset
              beta=MAX(REAL(0.0, KIND=KIND(GO2)), (GSQUARE-SPG)/GO2 )
              S(:)=G(:)+beta*S(:)  ! updated conjugate gradient direction

              IF ( GSQUARE/N < GTolerance)   THEN
                Write(*,*) 'GRADIENT Negligible:',GSQUARE
                GOTO 100 ! FINISHED Grad=0
              ENDIF
            ENDIF
        ENDDO
        ALPHA=DAlpha*ALPHA
        ResetCnt=0
        !	  Write(*,*) 'QQ HM-> ALPHA = ',ALPHA
        GOTO 30

        100           Write(*,*)
        Write(*,*) 'MINIMIZATION FINISHED '
        Write(*,*)
        Write(*,*) '               ||      Energy Calls:',neval
        Write(*,*) '               ||      Final Energy:',FVALUE
        Write(*,*) '               ||        DeltaF/TOL:',DeltaF/FTolerance
        Write(*,*) '               || sqrt(grad^2/GTOL):',GSQUARE/GTolerance

        RETURN

    END SUBROUTINE HubertMinimize

END MODULE Hubert_Minimizer

MODULE Magnetostriction

    USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE, C_INT

    USE Utils

    IMPLICIT NONE


    INTERFACE

        ! magnetostriction_perform is defined in magnetostriction.cpp
        !
        ! void magnetostriction_perform();
        SUBROUTINE magnetostriction_perform() BIND(C)
        END SUBROUTINE magnetostriction_perform

        SUBROUTINE magnetostriction_copy_merrill_to_dolfin() BIND(C)
        END SUBROUTINE magnetostriction_copy_merrill_to_dolfin

        SUBROUTINE magnetostriction_initialize(verbose) BIND(C)
            USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_INT
            INTEGER(KIND=C_INT), INTENT(IN) :: verbose
        END SUBROUTINE magnetostriction_initialize

        SUBROUTINE magnetostriction_solve_mechanical() BIND(C)
        END SUBROUTINE magnetostriction_solve_mechanical

        SUBROUTINE magnetostriction_solve_magnetic() BIND(C)
        END SUBROUTINE magnetostriction_solve_magnetic

        SUBROUTINE magnetostriction_solve_energy() BIND(C)
        END SUBROUTINE magnetostriction_solve_energy

        SUBROUTINE magnetostriction_copy_dolfin_to_merrill() BIND(C)
        END SUBROUTINE magnetostriction_copy_dolfin_to_merrill


        ! The returned value "value" is effectively the force per unit area
        ! on the surface. Returned values shouldn't worry about Ls scaling,
        ! although the position input will be in the scaled mesh units.
        !
        ! void traction(double[3] value, double[3] position, double[3] normal)
        SUBROUTINE traction_interface(value, position, normal)
            USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE

            REAL(C_DOUBLE), INTENT(OUT) :: value(3)
            REAL(C_DOUBLE), INTENT(IN)  :: position(3), normal(3)
        END SUBROUTINE traction_interface

    END INTERFACE


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Parameters                                                            !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! Elastic and magnetoelastic constants
    REAL(KIND=DP) c11, c12, c44, b1, b2

    ! Test if magnetostriction is being calculated
    LOGICAL :: calculating_magnetostriction

    ! Dirty bit. If set, magnetostriction matrices are rebuilt
    LOGICAL :: magnetostriction_dirty


    PROCEDURE(traction_interface), pointer :: traction_fnptr => null()


CONTAINS

    SUBROUTINE InitializeMagnetostriction()

        IMPLICIT NONE

        c11 = 0.0
        c12 = 0.0
        c44 = 0.0
        b1  = 0.0
        b2  = 0.0

        calculating_magnetostriction = .FALSE.

        magnetostriction_dirty = .TRUE.

        traction_fnptr => zero_traction

    END SUBROUTINE InitializeMagnetostriction


    SUBROUTINE zero_traction(value, position, normal)
        REAL(C_DOUBLE), INTENT(OUT) :: value(3)
        REAL(C_DOUBLE), INTENT(IN)  :: position(3), normal(3)

        value = 0.0
    END SUBROUTINE zero_traction


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Fortran to C interfaces                                               !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    ! Get parameters

    ! void get_mstr_parameters(
    !   double* Ms_d, double* c11_d, double* c12_d, double* c44_d,
    !   double* b1_d, double* b2_d, double* scale_d
    ! );
    SUBROUTINE get_mstr_parameters( &
        Ms_d, c11_d, c12_d, c44_d, b1_d, b2_d, scale_d &
    ) &
    BIND(C, name="get_mstr_parameters")
        USE Material_Parameters
        IMPLICIT NONE

        REAL(C_DOUBLE), INTENT(OUT) :: &
            Ms_d, c11_d, c12_d, c44_d, b1_d, b2_d, scale_d

        Ms_d  = Ms
        c11_d = c11
        c12_d = c12
        c44_d = c44
        b1_d  = b1
        b2_d  = b2
        scale_d = 1.0/sqrt(Ls) ! Ls is length scale squared
    END SUBROUTINE get_mstr_parameters


    ! Get/set m, h, energy

    ! void get_m_i(int* i, double m_i[3]);
    SUBROUTINE get_m_i(i, m_i) BIND(C, name="get_m_i")
        USE Tetrahedral_Mesh_Data
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(IN)  :: i
        REAL(C_DOUBLE), INTENT(OUT) :: m_i(3)

        m_i = m(i,:)
    END SUBROUTINE get_m_i

    ! void get_umstr_i(int* i, double umstr_i[3]);
    SUBROUTINE get_umstr_i(i, umstr_i) BIND(C, name="get_umstr_i")
        USE Material_Parameters
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(IN)  :: i
        REAL(C_DOUBLE), INTENT(OUT) ::umstr_i(3)

        umstr_i = umstr(i,:)
    END SUBROUTINE get_umstr_i

    ! void set_umstr_i(int* i, double umstr_i[3]);
    SUBROUTINE set_umstr_i(i, umstr_i) BIND(C, name="set_umstr_i")
        USE Material_Parameters
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(IN) :: i
        REAL(C_DOUBLE), INTENT(IN) ::umstr_i(3)

        umstr(i,:) = umstr_i
    END SUBROUTINE set_umstr_i

    ! void set_hmstr_i(int* i, double hmstr_i[3]);
    SUBROUTINE set_hmstr_i(i, hmstr_i) BIND(C, name="set_hmstr_i")
        USE Material_Parameters
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(IN) :: i
        REAL(C_DOUBLE), INTENT(IN) :: hmstr_i(3)

        hmstr(i,:) = hmstr_i
    END SUBROUTINE set_hmstr_i

    ! void set_mstr_energ(double* mstr_energ);
    SUBROUTINE set_mstr_energ(mstr_energ) BIND(C, name="set_mstr_energ")
        USE Material_Parameters
        IMPLICIT NONE

        REAL(C_DOUBLE), INTENT(IN) :: mstr_energ

        MstrEnerg = mstr_energ
    END SUBROUTINE set_mstr_energ

    ! Get/set dirty bit

    ! void get_mstr_dirty(int* dirty);
    SUBROUTINE get_mstr_dirty(dirty) BIND(C, name="get_mstr_dirty")
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(OUT) :: dirty

        IF(magnetostriction_dirty .EQV. .TRUE.) THEN
            dirty = 1
        ELSE
            dirty = 0
        END IF
    END SUBROUTINE get_mstr_dirty

    ! void set_mstr_dirty(int* dirty);
    SUBROUTINE set_mstr_dirty(dirty) BIND(C, name="set_mstr_dirty")
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(IN) :: dirty

        IF(dirty .EQ. 1) THEN
            magnetostriction_dirty = .TRUE.
        ELSE
            magnetostriction_dirty = .FALSE.
        END IF
    END SUBROUTINE set_mstr_dirty


    ! Get mesh info

    ! void get_vertex_i(int* i, double point[3]);
    SUBROUTINE get_vertex_i(i, point) BIND(C, name="get_vertex_i")
        USE Tetrahedral_Mesh_Data
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(IN) :: i
        REAL(C_DOUBLE), INTENT(OUT) :: point(3)

        point = VCL(i,:)
    END SUBROUTINE get_vertex_i

    ! void get_nnode(int* n);
    SUBROUTINE get_nnode(n) BIND(C, name="get_nnode")
        USE Tetrahedral_Mesh_Data
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(OUT) :: n

        n = NNODE
    END SUBROUTINE get_nnode

    ! void get_tet_i(int* i, int vertex_ids[4]);
    SUBROUTINE get_tet_i(i, vertex_ids) BIND(C, name="get_tet_i")
        USE Tetrahedral_Mesh_Data
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(IN) :: i
        INTEGER(C_INT), INTENT(OUT) :: vertex_ids(4)

        vertex_ids = TIL(i,:)
    END SUBROUTINE

    ! void get_ntri(int* ntri);
    SUBROUTINE get_ntri(n) BIND(C, name="get_ntri")
        USE Tetrahedral_Mesh_Data
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(OUT) :: n

        n = NTRI
    END SUBROUTINE get_ntri

    ! void get_vbox_i(int* i, double* vbox_i);
    SUBROUTINE get_vbox_i(i, vbox_i) BIND(C, name="get_vbox_i")
        USE Tetrahedral_Mesh_Data
        IMPLICIT NONE

        INTEGER(C_INT), INTENT(IN) :: i
        REAL(C_DOUBLE), INTENT(OUT) :: vbox_i

        vbox_i = vbox(i)
    END SUBROUTINE get_vbox_i


    ! Wrapper for calling traction_fnptr in C

    ! void traction(double[3] value, double[3] position, double[3] normal)
    SUBROUTINE traction(value, position, normal) BIND(C, name="traction")
        REAL(C_DOUBLE), INTENT(OUT) :: value(3)
        REAL(C_DOUBLE), INTENT(IN)  :: position(3), normal(3)

        call traction_fnptr(value, position, normal)
    END SUBROUTINE traction

END MODULE Magnetostriction

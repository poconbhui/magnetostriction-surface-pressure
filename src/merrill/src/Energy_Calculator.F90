MODULE Energy_Calculator
  USE Utils
  IMPLICIT NONE

  CONTAINS


  !---------------------------------------------------------------
  !  TOP LEVEL ENERGY MINIMIZATION ROUTINE
  !---------------------------------------------------------------

  SUBROUTINE EnergyMin( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element
    USE Hubert_Minimizer


    IMPLICIT NONE

    integer i,neval
    double precision etot,dgscale,Eold,Ediff

    double precision   XGUESS(2*NNODE) ,XX(2*NNODE) ,GRAD(2*NNODE)

    integer  maxvar, restartcount

    maxvar=2*NNODE
    neval=0
    dgscale=0.0
    EnergyUnit= Kd*total_volume/(Ls**(3.0/2.0))

    Eold=999.
    Ediff=999.
    restartcount=0
    do while (abs(ediff)>1.d-8 .AND. restartcount.lt.MaxRestarts)
      print*,'RESTART = ', restartcount
      restartcount=restartcount+1
      if (restartcount.eq.MaxRestarts) print*,'FINAL ENERGY ITERATION'

      do i = 1, NNODE
        XGUESS(2*i-1)=acos(m(i,3))
        XGUESS(2*i)=atan2(m(i,2),m(i,1))
      enddo

      XX(:)=0.
      GRAD(:)=0.

      call HubertMinimize(MAXVAR, XGUESS, XX, GRAD, ETOT, neval, dgscale)

      ediff=Etot-Eold

      Eold=Etot
      ! results of minimisation returned in X
      do i=1,NNODE
        m(i,1)=sin(XX(2*i-1))*cos(XX(2*i))
        m(i,2)=sin(XX(2*i-1))*sin(XX(2*i))
        m(i,3)=cos(XX(2*i-1))
      enddo

    enddo !  do while restart loop


    print*,'*** FINISHED MINIMIZATION RESTARTS'

    RETURN
  END SUBROUTINE EnergyMin


  !---------------------------------------------------------------
  ! BEM  : ET_GRAD(ETOT,G,X,neval,dgscale)
  !        Only remaining energy and gradient function
  !---------------------------------------------------------------

  SUBROUTINE  ET_GRAD(ETOT,grad,X,neval,dgscale)

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element
    USE Magnetization_Path

    IMPLICIT NONE

    integer  i,N,neval
    integer(kind=8), save :: global_neval = 0
    double precision   grad(NNODE*2), X(NNODE*2),etot,dgscale
    double precision   phi,theta,edist,dist,UsedExchangeEn
    double precision   EnScale


    integer  WhichExchangeHere

    global_neval = global_neval+1

    EnScale= Kd*total_volume  ! Energy scale to transform into units of Kd V
    ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
    totfx=fx+fx2

    ! increment the counter for number of energy evalulations
    neval=neval+1

    MeanMag(:)=0.

    WhichExchangeHere = WhichExchange

    ! when calling with negative neval the cartesian magnetization
    !   is not changed!!
    if( neval >0) then
      ! this is used for Magnetization paths --> PathEnergyAt(pos) !!
      do n=1,NNODE
        m(n,1)=sin(X(2*n-1))*cos(X(2*n))
        m(n,2)=sin(X(2*n-1))*sin(X(2*n))
        m(n,3)=cos(X(2*n-1))
        MeanMag(:)=MeanMag(:)+m(n,:)*vbox(n)
      enddo
      MeanMag(:)=MeanMag(:)/total_volume
    endif


    ! Calculates demag and Laplace exchange (gradient+energy)
    call CalcDemagEx()
    ! Calculates anisotropy and external field (gradient+energy)
    call CalcAnisExt()
    ! Calculates the magnetostriction energy and gradient
    call CalcMstr()


    if(CalcAllExchQ .eqv. .true.) then  ! calculate  all exchange energies
      call ExEnergyGrad3(X)  !  Test alternative exchange energy -> ExEnerg2
      if (WhichExchange==3 .and. NBadTets>0) then
        WhichExchangeHere=2
        write(*,*) 'Ex changed 3 -> 2, NBad =',NBadTets
      endif
      call ExGrad()
      call ExGrad4()
    else ! calculate  only the requested exchange energy (for faster execution)
      select case(WhichExchangeHere)
      case(2)
        call ExGrad()
      case(3)
        call ExEnergyGrad3(X)  !  Use alternative exchange energy -> ExEnerg2
        if (WhichExchange==3 .and. NBadTets>0) then
          WhichExchangeHere=2
          call ExGrad()
          write(*,*) 'Ex changed 3 -> 2, NBad =',NBadTets
        endif
      case(4)
        call ExGrad4()
      end select
    endif

    !
    !      1)   Collect total energy
    !
    select case(WhichExchangeHere)
      case(1)
        UsedExchangeEn=ExchangeE
      case(2)
        UsedExchangeEn= ExEnerg2
      case(3)
        UsedExchangeEn= ExEnerg3
      case(4)
        UsedExchangeEn= ExEnerg4
    end select

    etot=AnisEnerg+ UsedExchangeEn+ DemagEnerg + BEnerg + MstrEnerg

    !write(*,*) "Ls (in Energy_Calculator.f90) = ",sqrt(Ls)

    !write(*,*) "AnisEnerg=",AnisEnerg
    !write(*,*) "UsedExchangeEn=",UsedExchangeEn
    !write(*,*) "DemagEnerg=",DemagEnerg
    !write(*,*) "BEnerg=",BEnerg

    if(AddInitPathEnergyQ) then
      edist= InitDelta-Distance(PMag(InitRefPos,:,:),m(:,:))
      edist=InitAlpha*edist*edist
      etot=etot+edist
    endif

    if(EnergyLogging) then
      if(AddInitPathEnergyQ) then
        write(607, '(2I12,TR2,9E28.20)') &
          global_neval, neval,AnisEnerg/EnScale, UsedExchangeEn/EnScale, &
          BEnerg/EnScale, DemagEnerg/EnScale, edist/EnScale, &
          etot/EnScale, &
          MeanMag(1),MeanMag(2),MeanMag(3)
      else
        write(607, '(2I12,TR2,11E28.20)')  &
          global_neval, neval,AnisEnerg/EnScale, ExchangeE/EnScale, &
          ExEnerg2/EnScale, ExEnerg3/EnScale,ExEnerg4/EnScale, &
          BEnerg/EnScale, DemagEnerg/EnScale, etot/EnScale, &
          MeanMag(1),MeanMag(2),MeanMag(3)
      endif
    endif
    !
    !      2)   Collect total gradient
    !
    DO i =1, NNODE
      gradc(i,:) = hanis(i,:) + hdemag(i,:) + hext(i,:) + hmstr(i,:)
      select case(WhichExchangeHere)
        case(1)
          gradc(i,:)= gradc(i,:)+hexch(i,:)
        case(2)
          gradc(i,:)= gradc(i,:)+hexch2(i,:)
        case(3)
          !                   gradc(i,:)= gradc(i,:)+hexch3(i,:)
          continue
        case(4)
          gradc(i,:)= gradc(i,:)+hexch4(i,:)
      end select
    END DO

    if(AddInitPathEnergyQ) then !! InitPathEnergy gradient should be added!!
      dist=Distance(PMag(InitRefPos,:,:),m(:,:))
      DO i =1, NNODE
        gradc(i,:) = gradc(i,:) &
          + 2*InitAlpha*(InitDelta/dist-1) &
            *PMag(InitRefPos,i,:) &
            *vbox(i)/total_volume  !! InitPathEnergy gradient!!
      END DO
    endif
    !
    !      2)   Transform gradient to polar magnetizations
    !
    DO i =1, NNODE
      phi= X(2*i)
      theta=X(2*i-1)
      grad(2*i-1)= gradc(i,1)*cos(theta )*cos(phi)   &
        &   + gradc(i,2)*cos(theta )*sin(phi)  &
        &   - gradc(i,3) * sin(theta )
      grad(2*i)= -gradc(i,1)*sin(theta )*sin(phi ) &
        &        + gradc(i,2)*sin(theta )*cos(phi)
      if (WhichExchangeHere==3) then
        grad(2*i-1)=grad(2*i-1) +Aex*ls*DExTheta(i)
        grad(2*i)= grad(2*i) +Aex*ls*DExPhi(i)
      endif

      ! projection on m
      ! sp=gradc(i,1)*m(i,1)+gradc(i,2)*m(i,2)+gradc(i,3)*m(i,3)
      ! make gradc perpendicular to m
      ! gradc(i,:)=gradc(i,:)-sp*m(i,:)
    END DO
    !
    !      3)   Set gradient at fixed magnetizations to zero
    !

    if (NFIX >0) THEN   !  In case of fixed nodes
      Do i=1,NNODE
        if (BlockNumber(i)>1) then  ! All free nodes are in block number 1 !!
          grad(2*i)=0      ! set gradient =0 for fixed nodes
          grad(2*i+1)=0
        endif
      enddo
    endif

    DO i=1,NNODE
    END DO

    RETURN
  END SUBROUTINE ET_GRAD


  !---------------------------------------------------------------
  ! BEM  :  GradientTest()
  !---------------------------------------------------------------

  SUBROUTINE  GradientTest()

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    integer  i,neval,ind
    double precision   G(NNODE*2), G2(NNODE*2), X(NNODE*2),etot,dgscale
    double precision    eref,delta

    ! Controlling tolerance of linear CG solver
    FEMTolerance=1.d-10   ! via global FEMTolerance

    neval=1
    totfx=fx+fx2  ! is this necessary ? Occurs also in CalcDemagEx
    MeanMag(:)=0.
    ! Theta (co-lat) indicies= 2n-1, phi (azimuth)  indicies= 2n
    do i=1,NNODE
      X(2*i-1)=acos(m(i,3))
      X(2*i)= atan2(m(i,2),m(i,1))
      MeanMag(:)=MeanMag(:)+m(i,:)*vbox(i)
    enddo
    MeanMag(:)=MeanMag(:)/total_volume

    call ET_GRAD(eref,G,X,neval,dgscale)


    delta=0.001
    do ind=50,Min( NNode,100)
      X(2*ind)=X(2*ind)+delta
      m(ind,1)=sin(X(2*ind-1))*cos(X(2*ind))
      m(ind,2)=sin(X(2*ind-1))*sin(X(2*ind))
      m(ind,3)=cos(X(2*ind-1))


      call ET_GRAD(ETOT,G2,X,neval,dgscale)

      write(*,*) "phi:   dE, DE/d:",ind,G(2*ind),(etot-eref)/delta
      X(2*ind)=X(2*ind)-delta
      X(2*ind-1)=X(2*ind-1)+delta
      m(ind,1)=sin(X(2*ind-1))*cos(X(2*ind))
      m(ind,2)=sin(X(2*ind-1))*sin(X(2*ind))
      m(ind,3)=cos(X(2*ind-1))

      call ET_GRAD(ETOT,G2,X,neval,dgscale)

      write(*,*) "theta: dE, DE/d:",ind,G(2*ind-1),(etot-eref)/delta
      X(2*ind-1)=X(2*ind-1)-delta
      m(ind,1)=sin(X(2*ind-1))*cos(X(2*ind))
      m(ind,2)=sin(X(2*ind-1))*sin(X(2*ind))
      m(ind,3)=cos(X(2*ind-1))

    enddo


    RETURN
  END SUBROUTINE GradientTest


  !---------------------------------------------------------------
  ! BEM  :  ReportEnergy()
  !---------------------------------------------------------------

  SUBROUTINE  ReportEnergy()

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    integer  i,neval
    double precision   G(NNODE*2), X(NNODE*2),etot,dgscale
    double precision   EnScale,TypEn,KdV
    Logical :: TmpQ


    EnScale= Kd*total_volume  ! Energy scale to transform into units of Kd V
    ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
    TmpQ=CalcAllExchQ
    CalcAllExchQ=.true.

    ! Controlling tolerance of linear CG solver
    FEMTolerance=1.d-10   ! via global FEMTolerance

    neval=1
    totfx=fx+fx2  ! is this necessary ? Occurs also in CalcDemagEx
    MeanMag(:)=0.
    ! Theta (co-lat) indicies= 2n-1, phi (azimuth)  indicies= 2n
    do i=1,NNODE
      X(2*i-1)=acos(m(i,3))
      X(2*i)= atan2(m(i,2),m(i,1))
      MeanMag(:)=MeanMag(:)+m(i,:)*vbox(i)
    enddo
    MeanMag(:)=MeanMag(:)/total_volume

    call ET_GRAD(etot,G,X,neval,dgscale)
    KdV= Kd*total_volume/(sqrt(Ls)**3)
    TypEn= sqrt(Kd*sqrt(Aex*abs(K1)))*(total_volume/(sqrt(Ls)**3))**(5./6.)
    If(.NOT.(NONZERO(TypEn))) TypEn=Kd*total_volume/(sqrt(Ls)**3)
    If(.NOT.(NONZERO(TypEn))) TypEn=Aex*total_volume**(1./3.)/sqrt(Ls)
    If(.NOT.(NONZERO(TypEn))) TypEn=abs(K1)*total_volume/(sqrt(Ls)**3)

    write (*,*)
    write(*, '(A)') "MERRILL Version 1.0 (Williams and Fabian 2015)"
    write (*,*)
    write(*, '(A)') "Mesh data:"
    write(*, '(5A14)') "Nodes","Tetrahedra", "Bndry nodes", "Bndry faces", &
      "Volume"
    write(*, '(4I14,E14.6)') NNODE,NTRI,BNODE,BFCE,total_volume
    write (*,*)
    write(*, '(A)') "Material data:"
    write(*, '(7A16)') "Ms","K1", "A_ex", "Vol^(1/3) (m)", "QHardness", &
      "Lam_Ex (nm)", "Kd V (J)"
    write(*, '(7E16.6)') Ms, K1, Aex,  (total_volume**(1./3.))/sqrt(Ls), &
      QHardness, LambdaEx*1.d9, Kd*total_volume/(sqrt(Ls)**3)
    write (*,*)
    write(*, '(A)') "External field direction (x,y,z) and strength B (T):"
    write(*, '(4A14)') "hx","hy", "hz", "B (T)"
    write(*, '(4E14.6)')  hz(1),hz(2),hz(3),extapp
    write (*,*)
    write(*, '(A)') "Energies in units of Kd V:"
    write(*, '( 3A16)')  "E-Anis",  "E-ext", "E-Demag"
    write(*, '( 3E16.7)') AnisEnerg/EnScale,   BEnerg/EnScale, &
      DemagEnerg/EnScale

    write(*, '( 4A16)')  "E-Exch1","E-Exch2","E-Exch3","E-Exch4"
    write(*, '( 4E16.7)') ExchangeE/EnScale, ExEnerg2/EnScale,  &
      ExEnerg3/EnScale,ExEnerg4/EnScale
    write(*, '( 1A16)')  "E-Mstr"
    write(*, '( 1E16.7)') MstrEnerg/EnScale

    write(*, '( 4A15)')  "E-Tot","Mx","My","Mz"
    write(*, '( 4E15.7)')   etot/EnScale, MeanMag(1),MeanMag(2),MeanMag(3)
    write (*,*)
    write(*, '( A)')  "Typical energy scale Kd^(1/2) (A K1)^(1/4) V^(5/6) &
      &:    J          Kd V"
    write(*, '( A31,2E15.7)')  ' ', TypEn, TypEn/KdV
    write (*,*)
    write (*,*)

    CalcAllExchQ = TmpQ
    RETURN
  END SUBROUTINE ReportEnergy


  !---------------------------------------------------------------
  ! BEM  : ENERGY CALCULATIONS
  !---------------------------------------------------------------

  SUBROUTINE CalcDemagEx( )
    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    REAL(KIND=DP), ALLOCATABLE  :: RWORK(:)
    REAL(KIND=DP) f(NNODE), f2(NNODE), TOL, ERR, pi
    INTEGER i,j,l
    INTEGER ITER,ISYM,ITOL,IERR,IUNIT,LENIW,ITMAX &
      & ,LENW,NSAVE
    INTEGER, ALLOCATABLE  :: IWORK(:)

    pi=4.0d0*atan(1.0d0)

    !     calculate phi1, i.e. f
    !     Add div m to force vector
    !     Add m.n Neumann condition to force vector

    DO i=1,NNODE
      f(i)=0.0d0
      DO j=CNR(i),CNR(i+1)-1
        f(i)=f(i)+m(RNR(j),1)*FAX(j)+m(RNR(j),2)*FAY(j)+m(RNR(j),3)*FAZ(j)
      END DO
    END DO


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !     solve linear equations DSICCG values
    !     need IWORK(3700) changed in 3 places
    ISYM=0
    ITOL=1
    TOL=FEMTolerance
    ITMAX=4000
    IUNIT=0
    LENIW=2*(nze+NNODE+11)
    NSAVE=100
    LENW=12*(nze+5*NNODE)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Allocate( RWORK(LENW), IWORK(LENIW))

    CALL DSICCG(NNODE,f,fx,nze,RNR,CNR,YA,ISYM,ITOL  &
      & ,TOL,ITMAX,ITER,ERR,IERR,IUNIT,RWORK,LENW,IWORK,LENIW)

    DEALLOCATE(RWORK,IWORK)

    IF (IERR/=0)  WRITE(*,*) 'GMRES error in phi 1:',IERR


    !!!!!!!!!!!!!!!!!!!
    !     calculate phi2, i.e. f2

    DO i=1,NNODE
      f2(i)=0.0
      IF ((BCNR(i)/=0)) THEN
        l=1
        DO WHILE (BCNR(i+l)==0)
          l=l+1
        END DO ! while
        DO j=BCNR(i),BCNR(i+l)-1
          f2(i)=f2(i)+fx(BRNR(j))*BA(j)
        ENDDO ! j loop
      ENDIF
    ENDDO ! i loop

    !! remove non symmetric entries from f2 (column entries for boundary)

    DO i=1,NNODE
      DO l=CNR(i),CNR(i+1)-1
        IF ((BCNR(i)/=0).AND.(BCNR(RNR(l))==0) ) THEN
          f2(RNR(l))=f2(RNR(l))-YA(l)*f2(i)
        ENDIF
      END DO
    END DO

    Allocate( RWORK(LENW), IWORK(LENIW))

    CALL DSICCG(NNODE,f2,fx2,nze2,RNR2,CNR2,YA2,ISYM,ITOL  &
      & ,TOL,ITMAX,ITER,ERR,IERR,IUNIT,RWORK,LENW,IWORK,LENIW)

    DEALLOCATE(RWORK,IWORK)

    IF (IERR/=0)  WRITE(*,*) 'GMRES error in phi 2:',IERR

    totfx(:)=fx(:)+fx2(:)

    ! directly calculate energy and gradient in correct units
    hdemag(:,:)=0.0d0
    DemagEnerg=0.
    hexch(:,:)=0.0d0
    ExchangeE=0.
    DO i=1,NNODE
      DO j=CNR(i),CNR(i+1)-1
        hdemag(i,:)=hdemag(i,:)+totfx(RNR(j))*YA4(j,:)
        hexch(i,:)=hexch(i,:)+m(RNR(j),:)*YA(j)
      END DO
      hdemag(i,:)=Ms*Ms*mu*hdemag(i,:)/4  ! GRADIENT of the demag energy !!
      DemagEnerg = DemagEnerg &
        & +(hdemag(i,1)*m(i,1)+hdemag(i,2)*m(i,2)+hdemag(i,3)*m(i,3))/2.
      ! GRADIENT of the Laplacian exchange energy !!
      hexch(i,:)=2*hexch(i,:)*Aex*ls
      ExchangeE = ExchangeE &
        & +(hexch(i,1)*m(i,1)+hexch(i,2)*m(i,2)+hexch(i,3)*m(i,3))/2.
    END DO

    RETURN
  END SUBROUTINE CalcDemagEx


  !---------------------------------------------------------------
  ! ExEnergy3( ) : New Exchange energy  calculation using
  !                    Eex = (grad theta)^2 +sin^2 theta (grad phi)^2
  !---------------------------------------------------------------

  SUBROUTINE ExEnergy3( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,j,s
    REAL(KIND=DP)  vv, exen, th0
    REAL(KIND=DP)  dph(3),dth(3),phi,theta,mavg(3),pavg


    !     calculate exchange energy based on gradient phi and theta
    !                    Eex = (grad theta)^2 +sin^2 theta (grad phi)^2

    exen=0.

    DO i=1,NTRI
      vv= vol(i)      ! represented volume per tetrahedron
      mavg(:)=m(Til(i,1),:)+m(Til(i,2),:)+m(Til(i,3),:)+m(Til(i,4),:)
      ! average angle phi for backrotation around z- axis
      pavg=atan2(mavg(2),mavg(1))
      th0=0.
      dph(:)=0
      dth(:)=0
      DO j=1,4
        s=TIL(i,j)
        ! back rotation of m by - pavg
        ! avoids discontinuity for angles phi near -pi
        phi=atan2(m(s,2)*cos(pavg)-m(s,1)*sin(pavg),&
          &   m(s,1)*cos(pavg)+m(s,2)*sin(pavg))
        ! Note that back rotation around z doesn't change any gradient !!
        theta=acos(m(s,3))
        dph(1)=dph(1)+phi*b(i,j)
        dph(2)=dph(2)+phi*c(i,j)
        dph(3)=dph(3)+phi*d(i,j)
        dth(1)=dth(1)+theta*b(i,j)
        dth(2)=dth(2)+theta*c(i,j)
        dth(3)=dth(3)+theta*d(i,j)
        th0=th0+theta
      END DO
      exen=exen+  (dth(1)**2+dth(2)**2+dth(3)**2)/(36*vv)
      exen=exen+ (dph(1)**2+dph(2)**2+dph(3)**2)*(sin(th0*0.25))**2/(36*vv)
    END DO
    ExEnerg3 =   exen !  set global variable in module Material_Parameters
    RETURN
  END SUBROUTINE ExEnergy3


  !---------------------------------------------------------------
  ! ExEnergyGrad3( ) : New Exchange energy  gradient calculation using
  !                    Eex = (grad theta)^2 +sin^2 theta (grad phi)^2
  !---------------------------------------------------------------

  SUBROUTINE ExEnergyGrad3(X )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,s,tt
    REAL(KIND=DP)  vv,   exen, th0,ddp,ddt,sqdenom,rtdenom
    REAL(KIND=DP)  tmp, mtt(3),rm(3,3),cp,sp,ct,st
    REAL(KIND=DP)  dph(3),dth(3),phi,theta,mavg(3),pavg,tavg
    double precision   X(NNODE*2)


    !     calculate exchange energy based on gradient phi and theta
    !                    Eex = (grad theta)^2 +sin^2 theta (grad phi)^2
    !     this works only when the magnetizations at all tetrahedra
    !     vertices lie in one common half space
    !     tetrahedra where this does not apply are counted in NBadTets
    !     if bad tetrahedra occur this hinders nucleation because the
    !     exchange energy is then substantially overestimated

    exen=0.
    DExPhi(:)=0.
    DExTheta(:)=0.
    NBadTets=0

    DO tt=1,NTRI
      vv= vol(tt)     ! represented volume per tetrahedron
      mavg(:)=0.
      DO i=1,4
        mavg(:)=mavg(:)+m(TIL(tt,i),:) ! calculate average magnetization
      END DO
      tmp=sqrt(mavg(1)*mavg(1)+mavg(2)*mavg(2)+mavg(3)*mavg(3))
      mavg(:)=mavg(:)/tmp
      DO i=1,4
        sp = &
          mavg(1)*m(TIL(tt,i),1) &
          + mavg(2)*m(TIL(tt,i),2) &
          + mavg(3)*m(TIL(tt,i),3)
        If (sp <0) NBadTets=NBadTets+1
      END DO
      pavg=atan2(mavg(2),mavg(1))  ! optimal direction to rotate x-axis to
      tavg=acos(mavg(3))
      ct=cos(tavg); st=sin(tavg) ! direction cosines
      cp=cos(pavg); sp=sin(pavg)
      ! rotation matrix for optimal rotation
      rm(1,1)=cp*ct; rm(1,2)=sp*ct;rm(1,3)=st;
      ! first  - pavg around z then -tavg around y
      rm(2,1)=-sp; rm(2,2)=cp;rm(2,3)=0;
      rm(3,1)=-cp*st; rm(3,2)=-sp*st;rm(3,3)=ct;
      th0=0.
      dph(:)=0.
      dth(:)=0.
      DO i=1,4
        s=TIL(tt,i)
        ! rotate m to optimal coord. system
        mtt(:)=rm(:,1)*m(s,1)+rm(:,2)*m(s,2)+rm(:,3)*m(s,3)
        phi=atan2(mtt(2),mtt(1))
        theta=acos(mtt(3))
        dph(1)=dph(1)+phi *b(tt,i)  ! phi gradient in original coord. system
        dph(2)=dph(2)+phi *c(tt,i)  ! is phi grad divided by st
        dph(3)=dph(3)+phi *d(tt,i)
        dth(1)=dth(1)+theta*b(tt,i)
        dth(2)=dth(2)+theta*c(tt,i)
        dth(3)=dth(3)+theta*d(tt,i)
        th0=th0+theta
      END DO
      DO i=1,4
        s=TIL(tt,i)
        phi=atan2(m(s,2),m(s,1))
        theta=acos(m(s,3))
        ddp=2*( &
            b(tt,i)*dph(1)+c(tt,i)*dph(2)+d(tt,i)*dph(3) &
          )*(sin(th0*0.25))**2/(36*vv)
        ddt=2*(b(tt,i)*dth(1)+c(tt,i)*dth(2)+d(tt,i)*dth(3))/(36*vv)
        ddt=ddt+  (dph(1)**2+dph(2)**2+dph(3)**2)*sin(th0*0.5)*0.25/(36*vv)
        rtdenom=cos(pavg-phi)*st*sin(theta)-ct*cos(theta)
        rtdenom=sqrt(1-rtdenom*rtdenom)
        sqdenom=st*st*cos(theta)**2+st*ct*cos(pavg-phi)*sin(2*theta)
        sqdenom=sqdenom+(1-st*st**cos(pavg-phi)**2)*sin(theta)**2
        tmp=cp*st*cos(theta)*cos(phi)+cos(theta)*sin(phi)*sp*st+ct*sin(theta)
        tmp=tmp*sin(theta)/sqdenom
        !if(tmp/=tmp) tmp=0.
        if(ISNAN(tmp)) tmp=0.
        DExPhi(s)=DExPhi(s) +ddp*tmp
        tmp= st*sin(pavg-phi)*sin(theta)/rtdenom
        !if(tmp/=tmp) tmp=0.
        if(ISNAN(tmp)) tmp=0.
        DExPhi(s)=DExPhi(s) +ddt*tmp
        tmp= -st*sin(pavg-phi)/sqdenom
        !if(tmp/=tmp) tmp=0.
        if(ISNAN(tmp)) tmp=0.
        DExTheta(s)=DExTheta(s) +ddp*tmp
        tmp=cos(pavg-phi)*cos(theta)*st+ct*sin(theta)
        tmp=tmp/rtdenom
        !if(tmp/=tmp) tmp=0.
        if(ISNAN(tmp)) tmp=0.
        DExTheta(s)=DExTheta(s) +ddt*tmp
      END DO
      exen=exen+  (dth(1)**2+dth(2)**2+dth(3)**2)/(36*vv)
      exen=exen+ (dph(1)**2+dph(2)**2+dph(3)**2)*(sin(th0*0.25))**2/(36*vv)
    END DO
    !  set global variable in module Material_Parameters
    ExEnerg3 =   Aex*ls*exen
    ! Do i=1,NNODE  ! transform from polar gradients to cartesian gradients
    !   phi=X(2*i)
    !   theta=X(2*i-1)
    !   dmp= -sin(theta) *sin(phi)
    !   dmt= cos(theta) *cos(phi)
    !   if(abs(dmp)> MachEps)  hexch3(i,1)=Aex*ls*DExPhi(i)/dmp
    !   if(abs(dmt)> MachEps)  hexch3(i,1)=hexch3(i,1)+Aex*ls*DExTheta(i)/dmt
    !   dmp= sin(theta) *cos(phi)
    !   dmt= cos(theta) *sin(phi)
    !   if(abs(dmp)> MachEps)  hexch3(i,2)=Aex*ls*DExPhi(i)/dmp
    !   if(abs(dmt)> MachEps)  hexch3(i,2)=hexch3(i,2)+Aex*ls*DExTheta(i)/dmt
    !   dmt= -sin(theta)
    !   if(abs(dmt)> MachEps)  hexch3(i,3)=Aex*ls*DExTheta(i)/dmt
    ! End do
    RETURN
  END SUBROUTINE ExEnergyGrad3


  !---------------------------------------------------------------
  ! ExEnerg( ) : New Exchange energy  calculation
  !---------------------------------------------------------------

  SUBROUTINE ExEnerg( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,j,k,s,tt
    REAL(KIND=DP)  vv,   sp, len2, exen,ac,tmp


    ! calculate exchange energy based on linear angular rotation along edges
    ! each edge in tetrahedron TIL(i,:) has associated volume  vv = vol(i)/6
    ! each edge contributes multiple times for each tetrahedron it belongs to
    ! the exchange between nodes s and t is estimated as
    !   A * vv/len^2 * arccos( m(s,:).m(t,:))^2
    ! len is the distance between the nodes
    !   len^2 = (VCL(s,:)-VCL(t,:)).(VCL(s,:)-VCL(t,:))

    exen=0.

    DO i=1,NTRI
      vv= vol(i)/2. ! represented volume per tetrahedral edge = 3 *vol /6
      ! The factor 3 occurs because for each of the 4 nodes
      ! the gradient has three terms d/dx,d/dy,d/dz represented by
      ! the three edges starting at the node
      tmp=0.
      if ( vv > MachEps) then
        Do j=1,3
          s=TIL(i,j)
          Do k=j+1,4
            tt=TIL(i,k)
            len2= ( VCL(s,1)-VCL(tt,1))*( VCL(s,1)-VCL(tt,1))   +  &
              &( VCL(s,2)-VCL(tt,2))*( VCL(s,2)-VCL(tt,2))   +  &
              &( VCL(s,3)-VCL(tt,3))*( VCL(s,3)-VCL(tt,3))
            !            scalar product of node magnetizations
            sp= m(s,1)*m(tt,1)+m(s,2)*m(tt,2)+m(s,3)*m(tt,3)
            ac=acos(sp)
            ac=ac*ac/len2
            !if(ac/=ac)   ac=0. !  Catch NaN
            if(ISNAN(ac))   ac=0. !  Catch NaN
            tmp=tmp+ ac     !  correct phi^2
            !if(tmp/=tmp) then
            if(ISNAN(tmp)) then
              tmp=1
              vv=0d0
              !            write(*,*) 'ExchangeEnergy2 NaN in Tet:',i,j,k,vv
            endif
          End Do
        End Do
        exen=exen+ vv*tmp
      endif
    END DO
    ! set global variable in module Material_Parameters
    ExEnerg2 =  Aex*ls* exen
    RETURN
  END SUBROUTINE ExEnerg


  !---------------------------------------------------------------
  ! ExGrad( ) : New Exchange gradient  calculation
  !---------------------------------------------------------------

  SUBROUTINE ExGrad( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,j,k,s,tt
    REAL(KIND=DP)  vv,   sp, len2, exen,ac,tmp,rt


    ! calculate exchange energy based on linear angular rotation along edges
    ! each edge in tetrahedron TIL(i,:) has associated volume  vv = vol(i)/6
    ! each edge contributes multiple times for each tetrahedron it belongs to
    ! the exchange between nodes s and t is estimated as
    !   A * vv/len^2 * arccos( m(s,:).m(t,:))^2
    ! len is the distance between the nodes
    !   len^2 = (VCL(s,:)-VCL(t,:)).(VCL(s,:)-VCL(t,:))

    exen=0.
    hexch2(:,:)=0.0  !  contains afterwards the GRADIENT not the exchange field
    DO i=1,NTRI
      vv= vol(i)/6.  ! volume per edge of the tetrahedron
      tmp=0.
      if(vv > MachEps) then
        Do j=1,3
          s=TIL(i,j)
          Do k=j+1,4
            tt=TIL(i,k)
            len2= ( VCL(s,1)-VCL(tt,1))*( VCL(s,1)-VCL(tt,1))   +  &
              &( VCL(s,2)-VCL(tt,2))*( VCL(s,2)-VCL(tt,2))   +  &
              &( VCL(s,3)-VCL(tt,3))*( VCL(s,3)-VCL(tt,3))
            !            scalar product of node magnetizations
            sp= m(s,1)*m(tt,1)+m(s,2)*m(tt,2)+m(s,3)*m(tt,3)
            ac=acos(sp)
            ac=ac*ac/len2
            !if(ac/=ac)   ac=0. !  Catch NaN
            if(ISNAN(ac))   ac=0. !  Catch NaN
            tmp=tmp+ ac     !  correct phi^2
            !if(tmp/=tmp) then
            if(ISNAN(tmp)) then
              tmp=1
              vv=0d0
            endif
            rt=sqrt(1-sp*sp) ! in that case acos(sp)=0 anyway
            ac=-6*vv/len2*acos(sp)/rt
            !if(ac/=ac) ac=0.  ! NaN here implies vv=0
            if(ISNAN(ac)) ac=0.  ! NaN here implies vv=0
            hexch2(s,:) =hexch2(s,:)+ ac*m(tt,:)
            hexch2(tt,:) =hexch2(tt,:)+ ac*m(s,:)
          End Do
        End Do
        exen=exen+ 3*vv*tmp
      endif
    END DO
    ! use only gradient components perpendicular to m
    DO i=1,NNODE
      hexch2(i,:)=Aex*ls*hexch2(i,:)
    END DO
    ! set global variable in module Material_Parameters
    ExEnerg2 =  Aex*ls* exen
    RETURN
  END SUBROUTINE ExGrad


  !---------------------------------------------------------------
  ! ExGrad( ) : New Exchange gradient  calculation
  !---------------------------------------------------------------

  SUBROUTINE ExGrad4( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i,j,k,ind(4)
    REAL(KIND=DP)  vv,   sp,  exen,ac,tmp,rt,sprt
    REAL(KIND=DP)  mq(3), mqs(3), rq(3) ,dst(4), rs(4,3)


    !     calculate exchange energy based on linear angular rotation
    !     from tetrahedron centroid to its 4 vertices
    !
    !     each of the four lines  in tetrahedron TIL(i,:)
    !     has associated volume  vv = vol(i)/4
    !     each line contributes three times (as x,y,z coordinate direction)
    !     the exchange between nodes  t  and centroid s
    !      is estimated as A * vv/len^2 * arccos( mq.m(t,:))^2
    !      len is the distance between s and t

    exen=0.
    hexch4(:,:)=0.0  !  contains afterwards the GRADIENT not the exchange field
    DO i=1,NTRI
      vv= 0.75*vol(i)  ! 3/4 volume per line inside the tetrahedron
      if(vv > MachEps) then
        tmp=0.
        ind(:)=TIL(i,:)
        rs(:,:)=VCL(ind(:),:)
        rq(:)= (rs(1,:) +rs(2,:) +rs(3,:) +rs(4,:))*0.25
        dst(:)= sqrt((rq(1)-rs(:,1))**2+(rq(2)-rs(:,2))**2+(rq(3)-rs(:,3))**2)
        mqs(:)= m(ind(1),:)+m(ind(2),:)+m(ind(3),:)+m(ind(4),:)
        rt=sqrt(mqs(1)*mqs(1)+mqs(2)*mqs(2)+mqs(3)*mqs(3))
        if(rt<MachEps) then
          rt=1.
          mqs(1)=1.
        endif
        mq(:)=mqs(:)/rt
        Do j=1,4
          sp= m(ind(j),1)*mq(1)+m(ind(j),2)*mq(2)+m(ind(j),3)*mq(3)
          ac=acos(sp )
          tmp=ac*ac/(dst(j)*dst(j))*vv
          !if(tmp/=tmp)   tmp=0. !  Catch NaN
          if(ISNAN(tmp))   tmp=0. !  Catch NaN
          exen=exen+tmp
          sprt=sqrt(1-sp*sp )
          tmp=-2*vv *ac/sprt
          Do k=1,4
            if(j==k) then
              hexch4(ind(k), :)=hexch4(ind(k), :)+mq(:)*tmp/(dst(k)*dst(k))
            endif

            hexch4(ind(k), :) = hexch4(ind(k), :) &
              + (m(ind(j),:)-sp*mq(:))/rt*tmp/(dst(j)*dst(j))
          End Do
        End Do
      endif
    END DO
    DO i=1,NNODE
      hexch4(i,:)=Aex*ls*hexch4(i,:)
    END DO
    ! set global variable in module Material_Parameters
    ExEnerg4 =  Aex*ls* exen
    RETURN
  END SUBROUTINE ExGrad4



  !---------------------------------------------------------------
  ! CalcAnis( ) : Anisotropy energy and gradient
  !---------------------------------------------------------------

  SUBROUTINE CalcAnisExt( )

    USE Material_Parameters
    USE Tetrahedral_Mesh_Data
    USE Finite_Element

    IMPLICIT NONE

    INTEGER i
    REAL(KIND=DP)  vv, a(3),m2(3),ga(3)


    !     calculate anisotropy energy and gradient (wrt m) in correct units (J)

    AnisEnerg=0.
    hanis(:,:)=0.0  !  contains afterwards the GRADIENT not the anisotropy field
    if ( anisform=='cubic') then
      DO i=1,NNODE
        vv= vbox(i)
        if(vv > MachEps) then
          ! Rotated magnetizations
          a(:)=m(i,1)*rot(1,:)+m(i,2)*rot(2,:)+m(i,3)*rot(3,:)
          m2(:)=a(:)**2
          ga(1) = 2*K1*a(1)*(m2(2)+m2(3))*vv  ! Rotated gradient
          ga(2) = 2*K1*a(2)*(m2(1)+m2(3))*vv
          ga(3) = 2*K1*a(3)*(m2(2)+m2(1))*vv
          ! Backrotated gradient
          hanis(i,:)=ga(1)*rot(:,1)+ga(2)*rot(:,2)+ga(3)*rot(:,3)
          ! Energy in rotated coordinates
          AnisEnerg=AnisEnerg+ (m2(1)*m2(2)+m2(1)*m2(3)+m2(3)*m2(2))*vv
        endif
      END DO
    endif

    if ( anisform=='uni') then
      DO i=1,NNODE
        vv= vbox(i)
        if(vv > MachEps) then
          ! Uniaxial gradient
          hanis(i,:)=-2*K1*(ea(1)*m(i,1)+ ea(2)*m(i,2)+ ea(3)*m(i,3))*ea(:)*vv
          ! Energy
          AnisEnerg = AnisEnerg &
            + vv*(1-(ea(1)*m(i,1)+ ea(2)*m(i,2)+ ea(3)*m(i,3))**2 )
        endif
      END DO
    endif

    AnisEnerg=AnisEnerg*K1

    BEnerg=0.
    hext(:,:)=0.
    DO i=1,NNODE
      vv= vbox(i)
      hext(i,:)=-Ms*extapp*hz(:)*vv ! Uniform field energy GRADIENT
      ! Energy
      BEnerg=BEnerg-Ms*extapp*(hz(1)*m(i,1)+hz(2)*m(i,2)+hz(3)*m(i,3))*vv
    END DO

    RETURN
  END SUBROUTINE CalcAnisExt


  !---------------------------------------------------------------
  ! Calculate magnetostriction energy
  !---------------------------------------------------------------
  SUBROUTINE CalcMstr()

    USE Magnetostriction
    USE Material_Parameters

    IMPLICIT NONE

    IF ( calculating_magnetostriction .EQV. .TRUE. ) THEN
#ifdef MERRILL_ENABLE_MAGNETOSTRICTION
            call magnetostriction_perform()
#else
            WRITE(*,*) "Magnetostriction disabled!"
            STOP
#endif
    ELSE
        hmstr = 0.0
        MstrEnerg = 0.0
    END IF

    RETURN
  END SUBROUTINE CalcMstr

END MODULE Energy_Calculator


!
! Export global routines because of circular dependencies in
! Magnetization_Path.
!
SUBROUTINE EnergyMin()
  USE Energy_Calculator, only: ec_EnergyMin => EnergyMin

  IMPLICIT NONE

  CALL ec_EnergyMin()
END SUBROUTINE EnergyMin

SUBROUTINE  ET_GRAD(ETOT,grad,X,neval,dgscale)

  USE Tetrahedral_Mesh_Data
  USE Energy_Calculator, only: ec_ET_GRAD => ET_GRAD

  IMPLICIT NONE

  INTEGER  neval
  DOUBLE PRECISION   grad(NNODE*2), X(NNODE*2),ETOT,dgscale

  CALL ec_et_grad(ETOT, grad, X, neval, dgscale)
END SUBROUTINE ET_GRAD

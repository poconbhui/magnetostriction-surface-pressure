#include <iostream>
#include <cstring>
#include <functional>

#include "magnetostriction.h"
#define MERRILL_USE_PETSC
//#define MERRILL_USE_EIGEN
#ifdef MERRILL_USE_PETSC
#include <petsc.h>
#endif
#include <dolfin.h>

extern "C" {

// Definitions in Magnetostriction.f90

// Fortran to C interfaces
void get_mstr_parameters(
    double* Ms,
    double* c11, double* c12, double* c44, double* b1, double* b2,
    double* scale
);
void get_m_i(int* i, double* m_i);
void get_umstr_i(int* i, double* umstr_i);
void set_umstr_i(int* i, double* umstr_i);
void set_hmstr_i(int* i, double* hmstr_i);
void set_mstr_energ(double* mstr_energ);
void get_mstr_dirty(int* dirty);
void set_mstr_dirty(int* dirty);

void get_vertex_i(int* i, double* vertex_i);
void get_nnode(int* nnode);

void get_tet_i(int* i, int* tri_i);
void get_ntri(int* ntri);
void get_vbox_i(int* i, double* vbox_i);

void traction(double values[3], double position[3], double normal[3]);


// C to Fortran interfaces
void magnetostriction_perform();
void magnetostriction_copy_merrill_to_dolfin();
void magnetostriction_initialize(int* verbose);
void magnetostriction_solve_mechanical();
void magnetostriction_solve_magnetic();
void magnetostriction_solve_energy();
void magnetostriction_copy_dolfin_to_merrill();

} // extern "C"



namespace merrill {



//
// Dolfin, ie MPI and PETSc, initialization manager.
//
// Make sure this is called before any static dolfin types are declared
// to make sure dolfin's MPI/PETSc manager finalizes these after the static
// variables are destructed.
//
class DolfinManager {
public:
    typedef std::shared_ptr<void> Guard;

    static Guard guard() {
        initialize();

        static Guard guard(NULL, DolfinManager::guard_finalize);

        return guard;
    }

    static void initialize() {
        static bool first_entry = true;
        if(!first_entry) return;
        first_entry = false;

        #ifdef MERRILL_USE_PETSC
        dolfin::SubSystemsManager::init_petsc();

        // For dolfin 1.6.0 init_petsc adds these options to fix
        // potential issues with some libraries, but we're not using
        // any of them and petsc gives a warning that they've not been used
        // when it finalizes.
        PetscOptionsClearValue("-mat_mumps_icntl_7");
        PetscOptionsClearValue("-mat_superlu_dist_colperm");

        if(!dolfin::SubSystemsManager::responsible_petsc()) {
            std::cout
                << "Warning: Dolfin not initializing/destroying PETSc!"
                << std::endl
                << "This is not the expected behaviour..."
                << std::endl;
        }
        #endif

        #ifdef MERRILL_USE_PETSC
        dolfin::parameters["linear_algebra_backend"] = "PETSc";
        #endif

        #ifdef MERRILL_USE_EIGEN
        dolfin::parameters["linear_algebra_backend"] = "Eigen";
        #endif
    }

    static void finalize() {
        static bool first_entry = true;
        if(!first_entry) return;
        first_entry = false;

        dolfin::SubSystemsManager::finalize();
    }

    // For compatibility with shared_ptr deleter signature
    static void guard_finalize(void*) {
        finalize();
    }


private:
    DolfinManager() {}
    ~DolfinManager() {}
};



//
// Elasticity Null Space functions
//

// Null space of elasticity with neumann conditions
// representing rigid body translation and rigid body rotation.
class FreeElasticityNullSpaceExpression: public dolfin::Expression {
    int expression_id;

public:

    FreeElasticityNullSpaceExpression(int expression_id):
        dolfin::Expression(3),
        expression_id(expression_id)
    {}

    typedef dolfin::Array<double> Vector;

    void eval(Vector& vs, const Vector& xs) const {
        switch (expression_id) {
            case 0:
                expr_0(vs, xs);
                break;
            case 1:
                expr_1(vs, xs);
                break;
            case 2:
                expr_2(vs, xs);
                break;
            case 3:
                expr_3(vs, xs);
                break;
            case 4:
                expr_4(vs, xs);
                break;
            case 5:
                expr_5(vs, xs);
                break;
        }
    }


    // Rigid body displacement

    static void expr_0(Vector& vs, const Vector& xs) {
        vs[0] = 1.0; vs[1] = 0.0; vs[2] = 0.0;
    }

    static void expr_1(Vector& vs, const Vector& xs) {
        vs[0] = 0.0; vs[1] = 1.0; vs[2] = 0.0;
    }

    static void expr_2(Vector& vs, const Vector& xs) {
        vs[0] = 0.0; vs[1] = 0.0; vs[2] = 1.0;
    }


    // Rigid body rotation

    static void expr_3(Vector& vs, const Vector& xs) {
        vs[0] = xs[1]; vs[1] = -xs[0]; vs[2] = 0.0;
    }

    static void expr_4(Vector& vs, const Vector& xs) {
        vs[0] = xs[2]; vs[1] = 0.0; vs[2] = -xs[0];
    }

    static void expr_5(Vector& vs, const Vector& xs) {
        vs[0] = 0.0; vs[1] = xs[2]; vs[2] = -xs[1];
    }

};


//
// Generate VectorSpaceBasis representing the null space for
// an elastic problem with Neumann conditions
//
template<typename FunctionSpace>
std::shared_ptr<dolfin::VectorSpaceBasis> get_free_elasticity_nullspace(
    const FunctionSpace& V
) {

    std::vector<std::shared_ptr<dolfin::GenericVector>> null_vectors(6);

    // Interpolate null space expressions onto vectors
    for(int i=0; i<6; i++) {
        dolfin::Function f(V);
        f.interpolate(FreeElasticityNullSpaceExpression(i));
        null_vectors[i] = f.vector();
    }

    // Orthonormalize basis vectors
    // https://en.wikipedia.org/wiki/Gram%E2%80%93Schmidt_process#Algorithm
    for(int i=0; i<6; i++) {
        dolfin::GenericVector& v_i = *(null_vectors[i]);

        v_i /= v_i.norm("l2");

        for(int j=i+1; j<6; j++) {
            dolfin::GenericVector& v_j = *(null_vectors[j]);

            double j_dot_i = v_j.inner(v_i);
            double i_dot_i = v_i.inner(v_i);

            v_j.axpy(-j_dot_i/i_dot_i, v_i);
        }
    }

    return std::make_shared<dolfin::VectorSpaceBasis>(null_vectors);
}


//
// Generate a dolfin mesh from the merrill mesh
//
std::shared_ptr<const dolfin::Mesh> merrill_mesh_to_dolfin() {

    std::shared_ptr<dolfin::Mesh> mesh = std::make_shared<dolfin::Mesh>();

    //
    // Get mesh points and connectivities from MERRILL and
    // build a dolfin mesh with them.
    //
    dolfin::MeshEditor mesh_editor;
    mesh_editor.open(*mesh, dolfin::CellType::tetrahedron, 3, 3);

    int nnode = 0;
    int ntri = 0;

    get_nnode(&nnode);
    get_ntri(&ntri);

    mesh_editor.init_vertices(nnode);
    mesh_editor.init_cells(ntri);

    // Vertices
    double point[3] = {0.0};
    for(int i=1; i<nnode+1; i++) {
        get_vertex_i(&i, point);
        mesh_editor.add_vertex(i-1, point[0], point[1], point[2]);
    }

    // Connectivities
    int cell[4] = {0};
    for(int i=1; i<ntri+1; i++) {
        get_tet_i(&i, cell);
        mesh_editor.add_cell(
            i-1, cell[0]-1, cell[1]-1, cell[2]-1, cell[3]-1
        );
    }
    mesh_editor.close();


    //
    // Test function value ordering is the same as the vertex ordering
    // between MERRILL and Dolfin functions.
    //
    class XsExpr: public dolfin::Expression {
    public:
        XsExpr(): Expression(3) {}

        void eval(
            dolfin::Array<double>& vs, const dolfin::Array<double>& xs
        ) const {
            vs[0] = xs[0];
            vs[1] = xs[1];
            vs[2] = xs[2];
        }
    };

    magnetostriction::FunctionSpace V(
        std::const_pointer_cast<const dolfin::Mesh>(mesh)
    );
    dolfin::Function m(V);
    m.interpolate(XsExpr());

    std::vector<dolfin::la_index> vtd_map = dolfin::vertex_to_dof_map(V);

    for(int i=0; i<mesh->num_vertices(); i++) {
        double p[3];
        double mn[3];

        // Get Merrill vertex
        int f_i = i+1;
        get_vertex_i(&f_i, p);

        // Get Dolfin fuction values
        //std::vector<std::size_t> dofs(3*mesh->num_vertices());
        //dolfin::la_index ind[3] = {3*i+0, 3*i+1, 3*i+2};
        dolfin::la_index ind[3] = {
            vtd_map[3*i+0], vtd_map[3*i+1], vtd_map[3*i+2]
        };
        m.vector()->get_local(mn, 3, ind);

        double d2 =
            std::pow(p[0] - mn[0], 2)
            + std::pow(p[1] - mn[1], 2)
            + std::pow(p[2] - mn[2], 2);

        if(d2 > std::pow(1e-6,2)) {
            throw std::runtime_error(
                "Difference between Merrill and Dolfin representations"
                " is large!!"
            );
        }
    }

    return mesh;
}



class TractionWrap: public dolfin::Expression {
public:
    typedef dolfin::Array<double> Array;

    std::shared_ptr<const dolfin::Mesh> mesh;

    TractionWrap(std::shared_ptr<const dolfin::Mesh> mesh):
        dolfin::Expression(3),
        mesh(mesh)
    {}

    void eval(Array& values, const Array& x, const ufc::cell& ufc_cell) const {
        dolfin::Cell dolfin_cell(*mesh, ufc_cell.index);
        dolfin::Point normal = dolfin_cell.normal(ufc_cell.local_facet);

        double d_value[3];
        double d_position[3] = {x[0], x[1], x[2]};
        double d_normal[3]   = {normal[0], normal[1], normal[2]};

        // Call Fortran Magnetostriction::traction
        traction(d_value, d_position, d_normal);

        values[0] = d_value[0];
        values[1] = d_value[1];
        values[2] = d_value[2];
    }
};




//
// Perform magnetostriction calculations
//

class Magnetostriction {
public:
    #ifdef MERRILL_USE_PETSC
    #warning "Using PETSc"
    typedef dolfin::PETScVector DolfinVector;
    typedef dolfin::PETScMatrix DolfinMatrix;
    typedef dolfin::PETScKrylovSolver KrylovSolver;
    #endif

    #ifdef MERRILL_USE_EIGEN
    #warning "Using Eigen"
    typedef dolfin::EigenVector DolfinVector;
    typedef dolfin::EigenMatrix DolfinMatrix;
    typedef dolfin::EigenKrylovSolver KrylovSolver;
    #endif

    DolfinManager::Guard dolfin_guard;
    std::shared_ptr<dolfin::VectorSpaceBasis> mechanical_nullspace;

    std::shared_ptr<const dolfin::Mesh> mesh;
    std::shared_ptr<magnetostriction::FunctionSpace> V;

    std::shared_ptr<dolfin::Function> m;
    std::shared_ptr<dolfin::Function> u0;
    std::shared_ptr<dolfin::Function> h;

    std::shared_ptr<magnetostriction::Form_mechanical_L> form_mechanical_L;
    std::shared_ptr<DolfinVector> vector_mechanical_L;
    KrylovSolver mechanical_solver;

    std::shared_ptr<magnetostriction::Form_magnetic_L> form_magnetic_L;
    std::shared_ptr<DolfinVector> vector_magnetic_L;
    KrylovSolver magnetic_solver;

    std::shared_ptr<magnetostriction::Form_energy> form_energy;
    std::shared_ptr<dolfin::Scalar> scalar_energy;
    double energy;

    std::vector<dolfin::la_index> vtd_map;


    Magnetostriction():
        #ifdef MERRILL_USE_PETSC
        dolfin_guard(DolfinManager::guard()),
        #endif
        mechanical_solver("cg", "none"),
        magnetic_solver("cg"),
        energy(0)
    {}


    void initialize(bool verbose = true) {
        namespace mstr = magnetostriction;

        if(verbose) {
            std::cout
                << std::endl
                << " Initializing Magnetostriction module" << std::endl
                << " ------------------------------------" << std::endl;
        }


        //
        // Set up the mesh
        //

        mesh = merrill_mesh_to_dolfin();


        //
        // Define Variables and Constants
        //

        V = std::make_shared<magnetostriction::FunctionSpace>(mesh);

        m  = std::make_shared<dolfin::Function>(V);
        u0 = std::make_shared<dolfin::Function>(V);
        h  = std::make_shared<dolfin::Function>(V);


        double ms_d, c11_d, c12_d, c44_d, b1_d, b2_d, scale_d;
        get_mstr_parameters(
            &ms_d, &c11_d, &c12_d, &c44_d, &b1_d, &b2_d, &scale_d
        );

        if(verbose) {
            std::cout
                << std::endl
                << " - Magnetostriction parameters:"
                << std::endl
                << "     Ms:  " << ms_d << std::endl
                << "     c11: " << c11_d << std::endl
                << "     c12: " << c12_d << std::endl
                << "     c44: " << c44_d << std::endl
                << "     b1:  " << b1_d  << std::endl
                << "     b2:  " << b2_d  << std::endl
                << "     scale: " << scale_d
                << std::endl
                << std::endl;
        }

        auto c11 = std::make_shared<dolfin::Constant>(c11_d);
        auto c12 = std::make_shared<dolfin::Constant>(c12_d);
        auto c44 = std::make_shared<dolfin::Constant>(c44_d);

        auto scale = std::make_shared<dolfin::Constant>(scale_d);

        auto b = std::make_shared<dolfin::Constant>(b1_d, b2_d);

        auto e1 = std::make_shared<dolfin::Constant>(1.0, 0.0, 0.0);
        auto e2 = std::make_shared<dolfin::Constant>(0.0, 1.0, 0.0);
        auto e3 = std::make_shared<dolfin::Constant>(0.0, 0.0, 1.0);

        auto Ms = std::make_shared<dolfin::Constant>(ms_d);

        //auto t = std::make_shared<Pressure>(mesh, pressure_d);
        auto t = std::make_shared<TractionWrap>(mesh);


        //
        // Set up forms
        //

        mstr::Form_mechanical_a form_mechanical_a(V,V);

        form_mechanical_a.set_coefficient("scale", scale);
        form_mechanical_a.set_coefficient("c11", c11);
        form_mechanical_a.set_coefficient("c12", c12);
        form_mechanical_a.set_coefficient("c44", c44);

        mstr::Form_mechanical_a_perturb form_mechanical_a_perturb(V,V);


        form_mechanical_L = std::make_shared<mstr::Form_mechanical_L>(V);

        form_mechanical_L->set_coefficient("scale", scale);
        form_mechanical_L->set_coefficient("m",  m);
        form_mechanical_L->set_coefficient("b",  b);
        form_mechanical_L->set_coefficient("e1", e1);
        form_mechanical_L->set_coefficient("e2", e2);
        form_mechanical_L->set_coefficient("e3", e3);
        form_mechanical_L->set_coefficient("t", t);


        mstr::Form_magnetic_a form_magnetic_a(V,V);

        form_magnetic_a.set_coefficient("scale", scale);


        form_magnetic_L = std::make_shared<mstr::Form_magnetic_L>(V);

        form_magnetic_L->set_coefficient("scale", scale);
        form_magnetic_L->set_coefficient("m",  m);
        form_magnetic_L->set_coefficient("u0", u0);
        form_magnetic_L->set_coefficient("b",  b);
        form_magnetic_L->set_coefficient("Ms", Ms);
        form_magnetic_L->set_coefficient("e1", e1);
        form_magnetic_L->set_coefficient("e2", e2);
        form_magnetic_L->set_coefficient("e3", e3);


        form_energy = std::make_shared<mstr::Form_energy>(mesh);

        form_energy->set_coefficient("scale", scale);
        form_energy->set_coefficient("m",  m);
        form_energy->set_coefficient("u0", u0);
        form_energy->set_coefficient("b",  b);
        form_energy->set_coefficient("c11", c11);
        form_energy->set_coefficient("c12", c12);
        form_energy->set_coefficient("c44", c44);
        form_energy->set_coefficient("e1", e1);
        form_energy->set_coefficient("e2", e2);
        form_energy->set_coefficient("e3", e3);


        //
        // Assemble the matrices
        //
        auto matrix_mechanical_a = std::make_shared<DolfinMatrix>();
        auto matrix_mechanical_a_perturb = std::make_shared<DolfinMatrix>();
        vector_mechanical_L = std::make_shared<DolfinVector>();
        auto matrix_magnetic_a = std::make_shared<DolfinMatrix>();
        vector_magnetic_L = std::make_shared<DolfinVector>();
        scalar_energy = std::make_shared<dolfin::Scalar>();

        dolfin::assemble(*matrix_mechanical_a, form_mechanical_a);
        dolfin::assemble(
            *matrix_mechanical_a_perturb, form_mechanical_a_perturb
        );
        dolfin::assemble(*matrix_magnetic_a,   form_magnetic_a);

        matrix_mechanical_a->init_vector(*vector_mechanical_L, 0);
        matrix_magnetic_a->init_vector(*vector_magnetic_L, 0);

        if(verbose) {
            std::cout
                << " - FEM Matrices:" << std::endl
                << "     Mechanical Equilibrium:     [ "
                    << matrix_mechanical_a->size(0)
                    << " X "
                    << matrix_mechanical_a->size(1)
                << " ]"
                << std::endl
                << "     Effective Field Projection: [ "
                    << matrix_magnetic_a->size(0)
                    << " X "
                    << matrix_magnetic_a->size(1)
                << " ]"
                << std::endl
                << std::endl;
        }

        double a_max = matrix_mechanical_a->norm("frobenius");
        double a_perturb_max = matrix_mechanical_a_perturb->norm("frobenius");
        double perturb_scale = a_max/a_perturb_max * 1e-10;
        #ifdef MERRILL_USE_EIGEN
        matrix_mechanical_a->axpy(
            perturb_scale, *matrix_mechanical_a_perturb, true
        );
        #endif
        #ifdef MERRILL_USE_PETSC
        mechanical_nullspace = get_free_elasticity_nullspace(V);
        matrix_mechanical_a->set_nullspace(*mechanical_nullspace);
        #endif
        mechanical_solver.set_operator(matrix_mechanical_a);
        mechanical_solver.parameters["report"] = false;
        mechanical_solver.parameters["monitor_convergence"] = false;
        mechanical_solver.parameters["maximum_iterations"] = 10000;
        mechanical_solver.parameters["relative_tolerance"] = 1e-17;
        mechanical_solver.parameters["absolute_tolerance"] = 1e-17;

        magnetic_solver.set_operator(matrix_magnetic_a);
        magnetic_solver.parameters["report"] = false;


        // Generate the vertex to dof and dof to vertex maps
        vtd_map = dolfin::vertex_to_dof_map(*V);
    }


    void copy_merrill_to_dolfin() {
        // Copy values from Merrill to Dolfin
        for(int i=0; i<mesh->num_vertices(); i++) {
            double m_i[3];
            double u_i[3];

            int i_merrill = i+1;
            get_m_i(&i_merrill, m_i);
            get_umstr_i(&i_merrill, u_i);

            dolfin::la_index i_dolfin[3] = {
                vtd_map[3*i+0], vtd_map[3*i+1], vtd_map[3*i+2]
            };
            m->vector()->set_local(m_i, 3, i_dolfin);
            u0->vector()->set_local(u_i, 3, i_dolfin);
        }
        m->vector()->apply("insert");
        u0->vector()->apply("insert");
    }

    void solve_mechanical() {
        // Solve the mechanical equilibrium problem
        dolfin::assemble(*vector_mechanical_L, *form_mechanical_L);
        #ifdef MERRILL_USE_PETSC
        mechanical_nullspace->orthogonalize(*vector_mechanical_L);
        #endif
        double mechanical_scale = std::max(
            std::abs(vector_mechanical_L->max()),
            std::abs(vector_mechanical_L->min())
        );
        //std::cout << "mechanical_scale: " << mechanical_scale << std::endl;
        //mechanical_scale = std::max(mechanical_scale, 1.0);
        //std::cout << "mechanical_scale: " << mechanical_scale << std::endl;
        //std::cout << vector_mechanical_L->str(true) << std::endl;
        *vector_mechanical_L /= mechanical_scale;
        //vector_mechanical_L->zero();
        u0->vector()->zero();
        mechanical_solver.solve(*(u0->vector()), *vector_mechanical_L);
        *(u0->vector()) *= mechanical_scale;
    }

    void solve_magnetic() {
        // Solve the magnetic projection
        dolfin::assemble(*vector_magnetic_L, *form_magnetic_L);
        double magnetic_scale = std::max(
            std::abs(vector_magnetic_L->max()),
            std::abs(vector_magnetic_L->min())
        );
        *vector_magnetic_L /= magnetic_scale;
        magnetic_solver.solve(*(h->vector()), *vector_magnetic_L);
        *(h->vector()) *= magnetic_scale;
    }

    void solve_energy() {
        // Solve the energy
        energy = dolfin::assemble(*form_energy);
    }

    void copy_dolfin_to_merrill() {
        // Copy values from Dolfin to Merrill
        double Ms = *std::dynamic_pointer_cast<const dolfin::Constant>(
            form_magnetic_L->coefficient("Ms")
        );
        for(int i=0; i<mesh->num_vertices(); i++) {
            int i_merrill = i+1;

            double u_i[3];
            double h_i[3];

            dolfin::la_index i_dolfin[3] = {
                vtd_map[3*i+0], vtd_map[3*i+1], vtd_map[3*i+2]
            };
            u0->vector()->get_local(u_i, 3, i_dolfin);
            h->vector()->get_local(h_i, 3, i_dolfin);

            // Change from H field to Energy Gradient
            double vbox_i = 0.0;
            get_vbox_i(&i_merrill, &vbox_i);
            h_i[0] *= - vbox_i * Ms;
            h_i[1] *= - vbox_i * Ms;
            h_i[2] *= - vbox_i * Ms;

            set_umstr_i(&i_merrill, u_i);
            set_hmstr_i(&i_merrill, h_i);
        }

        // Change from absolute energy to scaled energy
        double scale = *std::dynamic_pointer_cast<const dolfin::Constant>(
            form_energy->coefficient("scale")
        );
        energy /= scale*scale*scale;
        set_mstr_energ(&energy);
    }

    void magnetostriction_perform() {

        int magnetostriction_dirty;
        get_mstr_dirty(&magnetostriction_dirty);

        if((!mesh) || (magnetostriction_dirty == 1)) {
            initialize();
        }

        copy_merrill_to_dolfin();

        magnetostriction_dirty = 0;
        set_mstr_dirty(&magnetostriction_dirty);

        solve_mechanical();
        solve_magnetic();
        solve_energy();

        copy_dolfin_to_merrill();
    }

};


} // namespace merrill


static merrill::Magnetostriction& merrill_magnetostriction() {
    static merrill::Magnetostriction magnetostriction;

    return magnetostriction;
}

void magnetostriction_perform() {
    merrill_magnetostriction().magnetostriction_perform();
}

void magnetostriction_copy_merrill_to_dolfin() {
    merrill_magnetostriction().copy_merrill_to_dolfin();
}

void magnetostriction_initialize(int* verbose) {
    bool b_verbose = true;
    if(verbose != NULL && *verbose < 0) b_verbose = false;
    merrill_magnetostriction().initialize(b_verbose);
}

void magnetostriction_solve_mechanical() {
    merrill_magnetostriction().solve_mechanical();
}

void magnetostriction_solve_magnetic() {
    merrill_magnetostriction().solve_magnetic();
}

void magnetostriction_solve_energy() {
    merrill_magnetostriction().solve_energy();
}

void magnetostriction_copy_dolfin_to_merrill() {
    merrill_magnetostriction().copy_dolfin_to_merrill();
}

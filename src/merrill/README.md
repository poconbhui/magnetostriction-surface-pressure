# README #

This is the source code for the Micromagnetic modelling programme using the finite element method.

Micromagnetic Earth Related Robust Interpreter Language Laboratory
Finite element solver
 by
 Wyn Williams 2005
 with contributions
 Hubert-Minimization / Paths / Scripting
 by Karl Fabian  2014

 Open source routines  by
 G. Benthien  :   string module
 NAG library  :   sparse matrix solver

### What is this repository for? ###

This is an open source code and we welcome contributors.

### How do I get set up? ###

The code is configured using CMake and can be compiled with
the intel Fortran compiler ifort, or the GNU Fortran compiler gfortran.
It is known to compile on Linux and OSX.


### Contribution guidelines ###

### Who do I talk to? ###

Further information can be obtained from
Wyn Williams at wyn.williams@ed.ac.uk
Karl Fabian at karl.fabian@ngu.no

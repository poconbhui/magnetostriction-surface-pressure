PROGRAM MagnetostrictionTest

    USE Magnetostriction
    USE Energy_Calculator

    IMPLICIT NONE

    DOUBLE PRECISION :: lmbda100, lmbda111
    DOUBLE PRECISION :: e1(3), e2(3), e3(3)
    DOUBLE PRECISION :: pressure

    DOUBLE PRECISION :: easy_axes(3, 6), hard_axes(3, 8)

    CHARACTER(len=100) :: test_meshfile

    ! Our "did the test pass" and "did every test pass" variables
    LOGICAL :: ok, total_ok

    call GET_COMMAND_ARGUMENT(1, test_meshfile)


    !test_meshfile = "/home/s1165102/prog/merrill/test/cube_50nm_5nm.neu"
    !test_meshfile = "/home/paddy/merrill/test/cube_50nm_5nm.neu"

    call initialize_magnetostriction_test()


    ok = .TRUE.
    total_ok = .TRUE.

    call test_uniform_axes(ok)
    total_ok = total_ok .AND. ok

    call test_uniform_field(ok)
    total_ok = total_ok .AND. ok

    call test_pressure(ok)
    total_ok = total_ok .AND. ok


    WRITE(*,*)
    IF ( total_ok .EQV. .TRUE. ) THEN
        WRITE(*,*) "Tests passed!"
    ELSE
        WRITE(*,*) "Test failed"
    END IF
    WRITE(*,*)


CONTAINS

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Tese subroutines                                                         !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    ! Compare uniform solutions along crystal axes with expected values
    SUBROUTINE test_uniform_axes(ok)
        USE Tetrahedral_Mesh_Data
        USE Material_Parameters
        IMPLICIT NONE

        ! Our "did the test pass" variable
        LOGICAL, INTENT(OUT) :: ok

        INTEGER :: theta_ns, theta_nz, theta_si, theta_zi
        DOUBLE PRECISION :: theta_s, theta_z

        DOUBLE PRECISION :: test_m(3), energy, analytic_energy, test_diff
        DOUBLE PRECISION :: test_axis(3)

        INTEGER :: i

        ok = .TRUE.

        WRITE(*,*) "Running: test_uniform_axes..."


        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! Compare solved and analytic solutions for uniform magnetisation.    !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !
        ! Check over a number of points over a sphere
        !
        theta_ns = 7
        theta_nz = 7
        DO theta_si = 0, theta_ns - 1
        DO theta_zi = 0, theta_nz
            theta_s = 2.0*3.14159265*(REAL(theta_si, kind(theta_s))/theta_ns)
            theta_z = REAL(theta_zi, kind(theta_z))/theta_nz

            test_m(1) = COS(theta_s) * SQRT(ABS(1.0-theta_z**2))
            test_m(2) = SIN(theta_s) * SQRT(ABS(1.0-theta_z**2))
            test_m(3) = theta_z

            test_axis = test_m

            DO i = 1, NNODE
                m(i,:) = test_m
            END DO

            ! Perform full magnetostriction calculation
            CALL magnetostriction_perform()

            ! Get full magnetostriction energy
            energy = MstrEnerg

            ! Get analytic magnetostriction energy
            CALL analytic_energy_density(test_m, analytic_energy)
            analytic_energy = analytic_energy * total_volume


            ! Use 1e-15 because typical energy for current parameters
            ! is 1e-2. For "0" energy, energy seems to be ~1e-12
            ! and analytic_energy is ~1e-17
            test_diff = ABS(energy - analytic_energy)
            IF (                                    &
                ( ABS(energy) .GT. 1e-15 )          &
                .AND.                               &
                ( ABS(analytic_energy) .GT. 1e-15 ) &
            ) THEN
                test_diff = ABS(test_diff / energy)
            END IF

            IF ( ABS(test_diff) .GT. 1e-6 ) THEN
                WRITE(*,*) "Computed energy not close to analytic energy."
                WRITE(*,*) " Relative Error:", ABS(test_diff)
                WRITE(*,*) " Computed Energy:", energy
                WRITE(*,*) " Analytic Energy:", analytic_energy
                WRITE(*,*) " Axis:", test_axis

                ok = .FALSE.
            END IF
        END DO ! theta_zi = 0, theta_nz
        END DO ! theta_si = 0, theta_ns - 1


        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! Compare solutions along equivalent axes.                            !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! Internal check.
        ! Check energies for all the easy axes match and all
        ! the hard axes match.

        CALL analytic_energy_density(easy_axes(:,1), analytic_energy)
        DO i = 1, SIZE(easy_axes, 2)
            CALL analytic_energy_density(easy_axes(:,i), energy)

            IF ( ABS(energy - analytic_energy) .GT. 1e-6 ) THEN
                WRITE(*,*) "Easy axis energies not all the same!"
                WRITE(*,*) "Expected", analytic_energy, "Got", energy

                ok = .FALSE.
            END IF
        END DO

        CALL analytic_energy_density(hard_axes(:,1), analytic_energy)
        DO i = 1, SIZE(hard_axes, 2)
            CALL analytic_energy_density(hard_axes(:,i), energy)

            IF ( ABS(energy - analytic_energy) .GT. 1e-6 ) THEN
                WRITE(*,*) "Hard axis energies not all the same!"
                WRITE(*,*) "Expected", analytic_energy, "Got", energy

                ok = .FALSE.
            END IF
        END DO


        ! Test hard axis energy > easy axis energy
        CALL analytic_energy_density(hard_axes(:,1), energy)
        CALL analytic_energy_density(easy_axes(:,1), analytic_energy)

        IF ( energy .LE. analytic_energy) THEN
            WRITE(*,*) "Hard axis energy not greater than easy axis!"
            WRITE(*,*) "Hard axis energy:", energy
            WRITE(*,*) "Easy axis energy:", analytic_energy

            ok = .FALSE.
        END IF



        IF ( ok .EQV. .TRUE. ) THEN
            WRITE(*,*) "- Passed"
        ELSE
            WRITE(*,*) "- Failed"
        END IF
    END SUBROUTINE test_uniform_axes


    ! For a uniform magnetisation, the magnetostrictive energy density
    ! can be expressed in the form
    !     e = K_lambda(alpha_1^2 alpha_2^2 + ...) + C,
    ! so the H field is
    !     H = d/dM e
    !       = K_lambda(2*(alpha_1)(d/dM(alpha_1)) alpha_2^2 + ...)

    ! We can compare computed solutions to this.
    SUBROUTINE test_uniform_field(ok)
        USE Tetrahedral_Mesh_Data
        USE Material_Parameters
        IMPLICIT NONE

        ! Our "did the test pass" variable
        LOGICAL, INTENT(OUT) :: ok

        INTEGER :: theta_ns, theta_nz, theta_si, theta_zi
        DOUBLE PRECISION :: theta_s, theta_z
        DOUBLE PRECISION :: test_m(3), test_h(3), test_h_i(3)
        DOUBLE PRECISION :: test_analytic_diff_i, test_analytic_diff
        DOUBLE PRECISION :: test_anis_diff_i, test_anis_diff
        
        DOUBLE PRECISION :: K1_old

        INTEGER :: i

        ok = .TRUE.


        WRITE(*,*) "Running: test_uniform_field..."


        ! Check over a number of points over a sphere
        theta_ns = 7
        theta_nz = 7
        DO theta_si = 0, theta_ns - 1
        DO theta_zi = 0, theta_nz
            theta_s = 2.0*3.14159265*(REAL(theta_si, kind(theta_s))/theta_ns)
            theta_z = REAL(theta_zi, kind(theta_z))/theta_nz

            test_m(1) = COS(theta_s) * SQRT(ABS(1.0-theta_z**2))
            test_m(2) = SIN(theta_s) * SQRT(ABS(1.0-theta_z**2))
            test_m(3) = theta_z

            DO i = 1, NNODE
                m(i,:) = test_m
            END DO

            ! Generate the field from the magnetostriction module
            CALL magnetostriction_perform()

            ! Generate the equivalent field from the anisotropy calculator
            K1_old = K1
            K1 = (9./4.)*(                                             &
                (c11-c12)*lmbda100*lmbda100 - 2.*c44*lmbda111*lmbda111 &
            )
            CALL CalcAnisExt()
            K1 = K1_old

            CALL analytic_effective_field(test_m, test_h)
            !hanis = hanis / 2.0

            test_analytic_diff = 0.0
            test_anis_diff = 0.0
            DO i = 1, NNODE
                ! magnetostriction_perform makes energy gradients, where
                ! analytic_effective_field makes H fields.
                test_h_i = -test_h*vbox(i)*Ms

                test_analytic_diff_i = SQRT( DOT_PRODUCT( &
                    hmstr(i,:) - test_h_i, &
                    hmstr(i,:) - test_h_i  &
                ) )


                ! Test against build in anisotropy needs to remove components
                ! of fields parallel to m, due to differences in derivation.
                test_anis_diff_i = SQRT( DOT_PRODUCT(            &
                    (                                            &
                        hmstr(i,:)                               &
                        - DOT_PRODUCT(hmstr(i,:),m(i,:))*m(i,:)  &
                    )                                            &
                    - (                                          &
                        hanis(i,:)                               &
                        - DOT_PRODUCT(hanis(i,:), m(i,:))*m(i,:) &
                    ),                                           &
                    (                                            &
                        hmstr(i,:)                               &
                        - DOT_PRODUCT(hmstr(i,:),m(i,:))*m(i,:)  &
                    )                                            &
                    - (                                          &
                        hanis(i,:)                               &
                        - DOT_PRODUCT(hanis(i,:), m(i,:))*m(i,:) &
                    )                                            &
                ) )


                IF (                                              &
                    SQRT( DOT_PRODUCT(hmstr(i,:), hmstr(i,:)) ) .GT. 1e-30 &
                    .AND.                                                  &
                    SQRT( DOT_PRODUCT(test_h_i, test_h_i) ) .GT. 1e-30 &
                ) THEN
                    test_analytic_diff_i =   &
                        test_analytic_diff_i &
                        / SQRT( DOT_PRODUCT(hmstr(i,:), hmstr(i,:)) )

                    test_anis_diff_i =   &
                        test_anis_diff_i &
                        / SQRT( DOT_PRODUCT(hmstr(i,:), hmstr(i,:)) )
                        
                END IF

                IF ( ABS(test_analytic_diff_i) .GT. 1e-3 ) THEN
                    write(*,*) "Here"
                    WRITE(*,*) "Expected |H - h_analytic| close."
                    WRITE(*,*) "Found relative difference of", &
                        ABS(test_analytic_diff_i)
                    WRITE(*,*) " for uniform M =", test_m
                    WRITE(*,*) " hmstr(i,:):", hmstr(i,:), "for i=",i
                    WRITE(*,*) " test_h_i:", test_h_i

                    ok = .FALSE.
                END IF

                IF ( ABS(test_anis_diff_i) .GT. 1e-3 ) THEN
                    write(*,*) "Here"
                    WRITE(*,*) "Expected |H - h_anis| close."
                    WRITE(*,*) "Found relative difference of", &
                        ABS(test_anis_diff_i)
                    WRITE(*,*) " for uniform M =", test_m
                    WRITE(*,*) " hmstr(i,:):", hmstr(i,:), "for i=", i
                    WRITE(*,*) " hanis(i,:):", hanis(i,:), "for i=", i

                    ok = .FALSE.
                END IF

                test_analytic_diff = &
                    test_analytic_diff + ABS(test_analytic_diff_i)
                test_anis_diff = &
                    test_anis_diff + ABS(test_anis_diff_i)

            END DO

            test_analytic_diff = test_analytic_diff / NNODE
            test_anis_diff = test_anis_diff / NNODE

            IF ( ABS(test_analytic_diff) .GT. 1e-3 ) THEN
                WRITE(*,*) "Expected |H - h_analytic| close."
                WRITE(*,*) "Found relative difference of", &
                    ABS(test_analytic_diff)
                WRITE(*,*) " for uniform M =", test_m

                ok = .FALSE.
            END IF

            IF ( ABS(test_anis_diff) .GT. 1e-3 ) THEN
                WRITE(*,*) "Expected |H - h_anis| close."
                WRITE(*,*) "Found relative difference of", &
                    ABS(test_anis_diff)
                WRITE(*,*) " for uniform M =", test_m

                ok = .FALSE.
            END IF

        END DO ! theta_zi = 0, theta_nz
        END DO ! theta_si = 0, theta_ns - 1


        IF ( ok .EQV. .TRUE. ) THEN
            WRITE(*,*) "- Passed"
        ELSE
            WRITE(*,*) "- Failed"
        END IF
    END SUBROUTINE test_uniform_field


    SUBROUTINE test_pressure(ok)
        USE Tetrahedral_Mesh_Data
        USE Material_Parameters
        IMPLICIT NONE

        ! Our "did the test pass" variable
        LOGICAL, INTENT(OUT) :: ok

        INTEGER :: pressure_i
        DOUBLE PRECISION :: x1(3), x2(3), x3(3), x4(3)
        DOUBLE PRECISION :: u1(3), u2(3), u3(3), u4(3)
        DOUBLE PRECISION :: volume_init_i, volume_init_total
        DOUBLE PRECISION :: volume_disp_i, volume_disp_total
        DOUBLE PRECISION :: prev_volume_disp_total

        INTEGER :: i


        ok = .TRUE.

        WRITE(*,*) "Running: test_pressure..."

        ! Set uniform M
        DO i=1, NNODE
            m(i,:) = (/ 1.0/SQRT(3.0), 1.0/SQRT(3.0), 1.0/SQRT(3.0) /)
        END DO

        ! Set initial volume big
        volume_disp_total = 1E10

        ! Set traction function
        traction_fnptr => pressure_traction

        DO pressure_i=0, 10, 1

            ! Save previous volume
            prev_volume_disp_total = volume_disp_total

            pressure = 10**(pressure_i)

            
            ! Generate the displacement
            magnetostriction_dirty = .TRUE.
            CALL magnetostriction_perform()

            volume_init_i = 0
            volume_init_total = 0
            volume_disp_i = 0
            volume_disp_total = 0
            DO i=1, NNODE
                x1 = VCL(TIL(i,1), 1:3)
                x2 = VCL(TIL(i,2), 1:3)
                x3 = VCL(TIL(i,3), 1:3)
                x4 = VCL(TIL(i,4), 1:3)

                u1 = umstr(TIL(i,1), 1:3)*SQRT(Ls)
                u2 = umstr(TIL(i,2), 1:3)*SQRT(Ls)
                u3 = umstr(TIL(i,3), 1:3)*SQRT(Ls)
                u4 = umstr(TIL(i,4), 1:3)*SQRT(Ls)

                volume_init_i = tetrahedron_volume(x1, x2, x3, x4)
                volume_init_total = volume_init_total + volume_init_i

                volume_disp_i = tetrahedron_volume(x1+u1, x2+u2, x3+u3, x4+u4)
                volume_disp_total = volume_disp_total + volume_disp_i
            END DO

            write(*,*) "Volume: ", volume_disp_total
            IF(volume_disp_total >= prev_volume_disp_total) THEN
                WRITE(*,*) "Expected volume(i) < volume(i-1) for&
                    & increasing pressure"
                WRITE(*,*) "Prev Volume: ", prev_volume_disp_total
                WRITE(*,*) "This Volume: ", volume_disp_total
                WRITE(*,*) "Pressure: ", pressure

                ok = .FALSE.
            END IF
        END DO


    END SUBROUTINE test_pressure


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Helper subroutines                                                       !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    SUBROUTINE pressure_traction(value, position, normal)
        USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE

        IMPLICIT NONE

        REAL(C_DOUBLE), INTENT(OUT) :: value(3)
        REAL(C_DOUBLE), INTENT(IN)  :: position(3), normal(3)

        value(1) = - normal(1)*pressure;
        value(2) = - normal(2)*pressure;
        value(3) = - normal(3)*pressure;
    END SUBROUTINE pressure_traction

    FUNCTION tetrahedron_volume(a, b, c, d)
        IMPLICIT NONE
        DOUBLE PRECISION :: tetrahedron_volume
        DOUBLE PRECISION, INTENT(IN) :: a(3), b(3), c(3), d(3)

        tetrahedron_volume = abs(dot_product(a-d, cross_product(b-d, c-d)))/6.0
    END FUNCTION

    FUNCTION cross_product(a, b)
        IMPLICIT NONE
        DOUBLE PRECISION :: cross_product(3)
        DOUBLE PRECISION, INTENT(IN) :: a(3), b(3)

        cross_product(1) = a(2) * b(3) - a(3) * b(2)
        cross_product(2) = a(3) * b(1) - a(1) * b(3)
        cross_product(3) = a(1) * b(2) - a(2) * b(1)
    END FUNCTION cross_product


    ! The minimised strain, epsilon0, for a given m,
    ! ignoring compatibility condition for epsilon0
    SUBROUTINE analytic_minimised_strain(m, epsilon0)
        USE Material_Parameters
        IMPLICIT NONE

        DOUBLE PRECISION, INTENT(IN) :: m(3)
        DOUBLE PRECISION, INTENT(OUT) :: epsilon0(3,3)

        DOUBLE PRECISION :: a(3)

        INTEGER :: i, j

        ! Directional cosine, assume unit m,e
        a(1) = DOT_PRODUCT(m, e1)
        a(2) = DOT_PRODUCT(m, e2)
        a(3) = DOT_PRODUCT(m, e3)

        DO i = 1, 3
            DO j = 1, 3
                IF ( i .EQ. j ) THEN
                    epsilon0(i,j) = (                     &
                        b1*(c12 - a(i)*a(j)*(c11+2*c12)) &
                        / ((c11-c12)*(c11+2*c12))        &
                    )
                ELSE
                    epsilon0(i,j) = -0.5*b2*a(i)*a(j)/c44
                END IF
            END DO
        END DO

    END SUBROUTINE analytic_minimised_strain


    ! Analytic effective field for m, ignoring compatibility conditions
    SUBROUTINE analytic_effective_field(m, h)
        USE Material_Parameters
        IMPLICIT NONE

        DOUBLE PRECISION, INTENT(IN)  :: m(3)
        DOUBLE PRECISION, INTENT(OUT) :: h(3)

        DOUBLE PRECISION :: a(3), d_a(3,3)

        DOUBLE PRECISION :: k_lambda

        ! Assuming |m| = |e| = 1
        !alpha = lambda m, e: dolfin.dot(m, e)
        a(1) = DOT_PRODUCT(m, e1)
        a(2) = DOT_PRODUCT(m, e2)
        a(3) = DOT_PRODUCT(m, e3)
        ! d/dM_i alpha(m, e)
        ! d_alpha = lambda m, e, i: e[i] - m[i]*alpha(m, e)
        d_a(:,1) = e1(:) - m(:)*a(1)
        d_a(:,2) = e2(:) - m(:)*a(2)
        d_a(:,3) = e3(:) - m(:)*a(3)

        k_lambda = &
            (9./4.)*( (c11-c12)*lmbda100*lmbda100 - 2.*c44*lmbda111*lmbda111 )

        h = -1/Ms*k_lambda*(                    &

              2*d_a(:,1)*a(1) * a(2)*a(2)       &
            + a(1)*a(1)       * 2*d_a(:,2)*a(2) &

            + 2*d_a(:,2)*a(2) * a(3)*a(3)       &
            + a(2)*a(2)       * 2*d_a(:,3)*a(3) &

            + 2*d_a(:,3)*a(3) * a(1)*a(1)       &
            + a(3)*a(3)       * 2*d_a(:,1)*a(1) &
        )

    END SUBROUTINE analytic_effective_field


    ! Analytic energy density for m, ignoring compatibility conditions
    SUBROUTINE analytic_energy_density(m, energy_density)
        USE Material_Parameters
        IMPLICIT NONE

        DOUBLE PRECISION, INTENT(IN) :: m(3)
        DOUBLE PRECISION, INTENT(OUT) :: energy_density

        DOUBLE PRECISION :: epsilon0(3,3), alpha(3), offset

        call analytic_minimised_strain(m, epsilon0)

        offset = - (b1**2) * (c11 + c12) / ( 2 * (c11 - c12) * (c11 + 2*c12) )

        alpha(1) = DOT_PRODUCT(m, e1)
        alpha(2) = DOT_PRODUCT(m, e2)
        alpha(3) = DOT_PRODUCT(m, e3)

        energy_density = (                                        &
            b1*(                                                  &
                  (alpha(1)*alpha(1))*epsilon0(1,1)               &
                + (alpha(2)*alpha(2))*epsilon0(2,2)               &
                + (alpha(3)*alpha(3))*epsilon0(3,3)               &
            )                                                     &
            + b2*(                                                &
                  alpha(2)*alpha(3)*(epsilon0(2,3)+epsilon0(3,2)) &
                + alpha(1)*alpha(3)*(epsilon0(1,3)+epsilon0(3,1)) &
                + alpha(1)*alpha(2)*(epsilon0(1,2)+epsilon0(2,1)) &
            )                                                     &
            + 0.5*c11*(                                           &
                  epsilon0(1,1)*epsilon0(1,1)                     &
                + epsilon0(2,2)*epsilon0(2,2)                     &
                + epsilon0(3,3)*epsilon0(3,3)                     &
            )                                                     &
            + 0.5*c44*(                                           &
                  (epsilon0(1,2)+epsilon0(2,1))                   &
                    * (epsilon0(1,2)+epsilon0(2,1))               &
                + (epsilon0(2,3)+epsilon0(3,2))                   &
                    * (epsilon0(2,3)+epsilon0(3,2))               &
                + (epsilon0(1,3)+epsilon0(3,1))                   &
                    * (epsilon0(1,3)+epsilon0(3,1))               &
            )                                                     &
            + c12*(                                               &
                  epsilon0(1,1)*epsilon0(2,2)                     &
                + epsilon0(2,2)*epsilon0(3,3)                     &
                + epsilon0(1,1)*epsilon0(3,3)                     &
            )                                                     &
        ) - offset

    END SUBROUTINE analytic_energy_density


    ! Initialize MERRILL with appropriate values and load a mesh
    SUBROUTINE initialize_magnetostriction_test()

        USE Merrill

        IMPLICIT NONE

        CALL InitializeMerrill()


        ! Load the mesh file
        meshfile = test_meshfile
        CALL readmeshpat() !-> MODULE Tetrahedral_Mesh_Data
        NMAX=NNODE
        CALL demagstiff()      !-> MODULE Finite_Element
        CALL nonzerostiff()    !-> MODULE Finite_Element
        CALL forcemat()        !-> MODULE Finite_Element
        CALL boundmata()       !-> MODULE Finite_Element
        WRITE(*,*) 'Number of boundary nodes',bnode
        !  Allocates field variables -> MODULE Material_Parameters
        CALL FieldAllocate()
        CALL SaveMesh(1)
        CALL SaveFEM(1)
        

        !
        ! Set values for TM60
        !

        e1 = rot(:,1)
        e2 = rot(:,2)
        e3 = rot(:,3)

        ! Provided by Karl Fabian
        ! Magnetic Constants
        K1  = 2.02e3       ! J/m^3
        Aex = 1.82349e-12  ! J/m
        Ms  = 1.15738e5    ! A/m
        anisform='cubic'

        Kd        = 4* (3.1415926535897932)*mu*Ms*Ms*0.5
        LambdaEx  = Sqrt(Aex/Kd)
        QHardness = K1/Kd


        ! Elastic Constants (N/m^2)
        c11 = 1.36468e11
        c12 = 5.30848e10
        c44 = 6.28431e10

        ! Magnetostriction constants
        ! (Sahu & Moskowiz 1995)
        lmbda111 = 95.4e-6  ! non-dim
        lmbda100 = 142.5e-6 ! non-dim

        ! Magnetoelastic coupling
        ! (in m^3/N)
        b1 = lmbda100*(-3./2.)*(c11 - c12)
        b2 = lmbda111*(-3.)*c44

        ! Pressure
        pressure = 0

        ! List the hard axes for klambda > 0
        easy_axes = RESHAPE(    &
            (/                  &
                 1.0, 0.0, 0.0, &
                 0.0, 1.0, 0.0, &
                 0.0, 0.0, 1.0, &
                -1.0, 0.0, 0.0, &
                 0.0,-1.0, 0.0, &
                 0.0, 0.0,-1.0  &
            /),                 &
            SHAPE(easy_axes)    &
        )

        ! List the easy axes for k1 > 0
        hard_axes = RESHAPE(                                  &
            (/                                                &
                 1.0/SQRT(3.0), 1.0/SQRT(3.0), 1.0/SQRT(3.0), &
                -1.0/SQRT(3.0), 1.0/SQRT(3.0), 1.0/SQRT(3.0), &
                -1.0/SQRT(3.0),-1.0/SQRT(3.0), 1.0/SQRT(3.0), &
                 1.0/SQRT(3.0),-1.0/SQRT(3.0), 1.0/SQRT(3.0), &
                 1.0/SQRT(3.0), 1.0/SQRT(3.0),-1.0/SQRT(3.0), &
                -1.0/SQRT(3.0), 1.0/SQRT(3.0),-1.0/SQRT(3.0), &
                -1.0/SQRT(3.0),-1.0/SQRT(3.0),-1.0/SQRT(3.0), &
                 1.0/SQRT(3.0),-1.0/SQRT(3.0),-1.0/SQRT(3.0)  &
            /),                                               &
            SHAPE(hard_axes)                                  &
        )
    END SUBROUTINE initialize_magnetostriction_test

END PROGRAM MagnetostrictionTest

include(ExternalProject)


set(
    PETSC_URL "http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-3.6.4.tar.gz" CACHE STRING
    "The URL to get the petsc tarball."
)
set(
    PETSC_MD5 "e7a5253621253eef8f5a19ddc03dd0d4" CACHE STRING
    "MD5 hash of the petsc tarball"
)


set(PETSC_PREFIX       "${CMAKE_BINARY_DIR}/petsc"   CACHE PATH "")
set(PETSC_DOWNLOAD_DIR "${PETSC_PREFIX}/src"         CACHE PATH "")
set(PETSC_BUILD_DIR    "${PETSC_PREFIX}/build/build" CACHE PATH "")
set(PETSC_SOURCE_DIR   "${PETSC_PREFIX}/build/src"   CACHE PATH "")
set(PETSC_TMP_DIR      "${PETSC_PREFIX}/build/tmp"   CACHE PATH "")
set(PETSC_INSTALL_DIR  "${PETSC_PREFIX}"             CACHE PATH "")

set(
    PETSC_BUILD_EXTRA_OPTIONS "" CACHE STRING
    "Extra options to pass to the PETSc build command"
)


if(CMAKE_VERBOSE_MAKEFILE)
    set(PETSC_VERBOSITY "-d+2")
else()
    set(PETSC_VERBOSITY)
endif()


if(BUILD_SHARED_LIBS)
    set(PETSC_LINKAGE "--enable-shared=0;--with-blas-lib=libblas.so;--with-lapack-lib=liblapack.so")
else()
    set(
        #PETSC_LINKAGE "--enable-shared=0;--with-clib-autodetect=0;--with-fortranlib-autodetect=0;--with-cxxlib-autodetect=0;--with-blas-lapack-lib=[liblapack.a,libblas.a,libgfortran.a,libquadmath.a,-lm]"
        PETSC_LINKAGE "--enable-shared=0;--with-clib-autodetect=0;--with-fortranlib-autodetect=0;--with-cxxlib-autodetect=0;--with-blas-lapack-lib=[/usr/lib64/liblapack.a,/usr/lib64/libblas.a]"
    )
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(PETSC_DEBUG_FLAG "--enable-debug=1")
elseif(CMAKE_BUILD_TYPE STREQUAL "Release")
    set(PETSC_DEBUG_FLAG "--enable-debug=0")
endif()


ExternalProject_Add(
    petsc

    PREFIX ${PETSC_PREFIX}

    SOURCE_DIR ${PETSC_SOURCE_DIR}
    BINARY_DIR ${PETSC_BUILD_DIR}
    INSTALL_DIR ${PETSC_INSTALL_DIR}

    STAMP_DIR ${PETSC_BUILD_DIR}
    TMP_DIR   ${PETSC_TMP_DIR}

    URL ${PETSC_URL}
    URL_MD5 ${PETSC_MD5}
    DOWNLOAD_DIR ${PETSC_DOWNLOAD_DIR}

    CONFIGURE_COMMAND
        ${CMAKE_COMMAND} -E copy_directory ${PETSC_SOURCE_DIR} ${PETSC_BUILD_DIR}

    COMMAND
        ${CMAKE_COMMAND} -E chdir ${PETSC_BUILD_DIR}
            ${PYTHON_EXECUTABLE} configure
                --prefix=${PETSC_INSTALL_DIR}
                --PETSC_DIR=${PETSC_BUILD_DIR}
                --PETSC_ARCH=merrill-build
                --CC=${CMAKE_C_COMPILER}
                --CFLAGS=${CMAKE_C_FLAGS}
                --COPTFLAGS=${CMAKE_C_FLAGS}
                --CXX=${CMAKE_CXX_COMPILER}
                --CXXFLAGS=${CMAKE_CXX_FLAGS}
                --CXXOPTFLAGS=${CMAKE_CXX_FLAGS}
                --FC=${CMAKE_Fortran_COMPILER}
                --FFLAGS=${CMAKE_Fortran_FLAGS}
                --FOPTFLAGS=${CMAKE_Fortran_FLAGS}
                "${PETSC_DEBUG_FLAG}"
                "${PETSC_LINKAGE}"
                --with-mpi=0
                --with-windows-graphics=0
                --with-ssl=0
                --with-x=0
                --with-hwloc=0
                --with-pthread=0
                --with-c2html=0

    BUILD_COMMAND
        ${CMAKE_MAKE_PROGRAM} -C ${PETSC_BUILD_DIR}
            PETSC_DIR=${PETSC_BUILD_DIR}
            PETSC_ARCH=merrill-build
            ${PETSC_BUILD_EXTRA_OPTIONS}

    INSTALL_COMMAND
        ${CMAKE_MAKE_PROGRAM} -C ${PETSC_BUILD_DIR}
            PETSC_DIR=${PETSC_BUILD_DIR}
            PETSC_ARCH=merrill-build
            install
)

include(ExternalProject)

find_package(PythonInterp REQUIRED)


set(
    INSTANT_URL "https://bitbucket.org/fenics-project/instant/downloads/instant-1.6.0.tar.gz" CACHE STRING
    "The URL to get the instant tarball."
)
set(
    INSTANT_MD5 "5f2522eb032a5bebbad6597b6fe0732a" CACHE STRING
    "MD5 hash of the instant tarball"
)


set(INSTANT_PREFIX       "${CMAKE_BINARY_DIR}/instant"   CACHE PATH "")
set(INSTANT_DOWNLOAD_DIR "${INSTANT_PREFIX}/src"         CACHE PATH "")
set(INSTANT_BUILD_DIR    "${INSTANT_PREFIX}/build/build" CACHE PATH "")
set(INSTANT_SOURCE_DIR   "${INSTANT_PREFIX}/build/src"   CACHE PATH "")
set(INSTANT_TMP_DIR      "${INSTANT_PREFIX}/build/tmp"   CACHE PATH "")
set(INSTANT_INSTALL_DIR  "${INSTANT_PREFIX}"             CACHE PATH "")

set(INSTANT_PYTHON_INSTALL_DIR "${INSTANT_INSTALL_DIR}/lib/python" CACHE PATH "")


ExternalProject_Add(
    instant

    PREFIX ${INSTANT_PREFIX}

    SOURCE_DIR ${INSTANT_SOURCE_DIR}
    BINARY_DIR ${INSTANT_BUILD_DIR}
    INSTALL_DIR ${INSTANT_INSTALL_DIR}

    STAMP_DIR ${INSTANT_BUILD_DIR}
    TMP_DIR ${INSTANT_TMP_DIR}

    URL ${INSTANT_URL}
    URL_MD5 ${INSTANT_MD5}
    DOWNLOAD_DIR ${INSTANT_DOWNLOAD_DIR}

    CONFIGURE_COMMAND
        ${CMAKE_COMMAND} -E make_directory ${INSTANT_BUILD_DIR}
    COMMAND
        ${CMAKE_COMMAND} -E copy_directory ${INSTANT_SOURCE_DIR} ${INSTANT_BUILD_DIR}

    BUILD_COMMAND
        ${CMAKE_COMMAND} -E chdir ${INSTANT_BUILD_DIR}
            ${PYTHON_EXECUTABLE} setup.py build
    INSTALL_COMMAND
        ${CMAKE_COMMAND} -E chdir ${INSTANT_BUILD_DIR}
            ${PYTHON_EXECUTABLE} setup.py install
                "--prefix=${INSTANT_INSTALL_DIR}"
                "--install-lib=${INSTANT_PYTHON_INSTALL_DIR}"
)

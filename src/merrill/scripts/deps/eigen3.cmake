include(ExternalProject)


set(
    EIGEN3_URL "http://bitbucket.org/eigen/eigen/get/3.2.8.tar.gz" CACHE STRING
    "The URL to get the eigen3 tarball."
)
set(
    EIGEN3_MD5 "135d8d43aaee5fb54cf5f3e981b1a6db" CACHE STRING
    "MD5 hash of the eigen3 tarball"
)

set(
    EIGEN3_TOOLSET "gcc" CACHE STRING
    "The toolset to use with Eigen3. eg. gcc, clang. This can be useful when the OSX gcc/clang toolset calls 'g++' when g++ isn't a clang wrapper."
)


set(EIGEN3_PREFIX       "${CMAKE_BINARY_DIR}/eigen3"   CACHE PATH "")
set(EIGEN3_DOWNLOAD_DIR "${EIGEN3_PREFIX}/src"         CACHE PATH "")
set(EIGEN3_BUILD_DIR    "${EIGEN3_PREFIX}/build/build" CACHE PATH "")
set(EIGEN3_SOURCE_DIR   "${EIGEN3_PREFIX}/build/src"   CACHE PATH "")
set(EIGEN3_TMP_DIR      "${EIGEN3_PREFIX}/build/tmp"   CACHE PATH "")
set(EIGEN3_INSTALL_DIR  "${EIGEN3_PREFIX}"             CACHE PATH "")

set(
    EIGEN3_BUILD_EXTRA_OPTIONS "" CACHE STRING
    "Extra options to pass to the Eigen3 build command"
)


ExternalProject_Add(
    eigen3

    PREFIX ${EIGEN3_PREFIX}

    SOURCE_DIR ${EIGEN3_SOURCE_DIR}
    BINARY_DIR ${EIGEN3_BUILD_DIR}
    INSTALL_DIR ${EIGEN3_INSTALL_DIR}

    STAMP_DIR ${EIGEN3_BUILD_DIR}
    TMP_DIR   ${EIGEN3_TMP_DIR}

    URL ${EIGEN3_URL}
    URL_MD5 ${EIGEN3_MD5}
    DOWNLOAD_DIR ${EIGEN3_DOWNLOAD_DIR}

    CMAKE_ARGS
        "-DCMAKE_INSTALL_PREFIX:PATH=${EIGEN3_INSTALL_DIR}"
        "-DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}"
        "-DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}"
        "-DCMAKE_VERBOSE_MAKEFILE:BOOL=${CMAKE_VERBOSE_MAKEFILE}"

    BUILD_COMMAND
        ${CMAKE_COMMAND} 
            --build ${EIGEN3_BUILD_DIR}
            ${EIGEN3_BUILD_EXTRA_OPTIONS}

)

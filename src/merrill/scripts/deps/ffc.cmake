include(ExternalProject)

find_package(PythonInterp REQUIRED)


set(
    FFC_URL "https://bitbucket.org/fenics-project/ffc/downloads/ffc-1.6.0.tar.gz" CACHE STRING
    "The URL to get the ffc tarball."
)
set(
    FFC_MD5 "358faa3e9da62a1b1a717070217b793e" CACHE STRING
    "MD5 hash of the ffc tarball"
)


set(FFC_PREFIX       "${CMAKE_BINARY_DIR}/ffc"   CACHE PATH "")
set(FFC_DOWNLOAD_DIR "${FFC_PREFIX}/src"         CACHE PATH "")
set(FFC_BUILD_DIR    "${FFC_PREFIX}/build/build" CACHE PATH "")
set(FFC_SOURCE_DIR   "${FFC_PREFIX}/build/src"   CACHE PATH "")
set(FFC_TMP_DIR      "${FFC_PREFIX}/build/tmp"   CACHE PATH "")
set(FFC_INSTALL_DIR  "${FFC_PREFIX}"             CACHE PATH "")

set(FFC_PYTHON_INSTALL_DIR "${FFC_INSTALL_DIR}/lib/python" CACHE PATH "")


ExternalProject_Add(
    ffc

    DEPENDS swig ufl fiat instant

    PREFIX ${FFC_PREFIX}

    SOURCE_DIR ${FFC_SOURCE_DIR}
    BINARY_DIR ${FFC_BUILD_DIR}
    INSTALL_DIR ${FFC_INSTALL_DIR}

    STAMP_DIR ${FFC_BUILD_DIR}
    TMP_DIR   ${FFC_TMP_DIR}

    URL ${FFC_URL}
    URL_MD5 ${FFC_MD5}
    DOWNLOAD_DIR ${FFC_DOWNLOAD_DIR}

    CONFIGURE_COMMAND
        ${CMAKE_COMMAND} -E make_directory ${FFC_BUILD_DIR}
    COMMAND
        ${CMAKE_COMMAND} -E copy_directory ${FFC_SOURCE_DIR} ${FFC_BUILD_DIR}

    BUILD_COMMAND
        ${CMAKE_COMMAND} -E env PATH=${SWIG_INSTALL_DIR}/bin/:$ENV{PATH}
        ${CMAKE_COMMAND} -E chdir ${FFC_BUILD_DIR}
            ${PYTHON_EXECUTABLE} setup.py build
    INSTALL_COMMAND
        ${CMAKE_COMMAND} -E env PATH=${SWIG_INSTALL_DIR}/bin/:$ENV{PATH}
        ${CMAKE_COMMAND} -E chdir ${FFC_BUILD_DIR}
            ${PYTHON_EXECUTABLE} setup.py install
                "--prefix=${FFC_INSTALL_DIR}"
                "--install-lib=${FFC_PYTHON_INSTALL_DIR}"
)

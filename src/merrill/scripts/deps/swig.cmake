include(ExternalProject)

find_package(PythonInterp REQUIRED)


set(
    SWIG_URL "https://sourceforge.net/projects/swig/files/swig/swig-3.0.10/swig-3.0.10.tar.gz" CACHE STRING
    "The URL to get the swig tarball."
)
set(
    SWIG_MD5 "bb4ab8047159469add7d00910e203124" CACHE STRING
    "MD5 hash of the swig tarball"
)

set(SWIG_PREFIX       "${CMAKE_BINARY_DIR}/swig"   CACHE PATH "")
set(SWIG_DOWNLOAD_DIR "${SWIG_PREFIX}/src"         CACHE PATH "")
set(SWIG_BUILD_DIR    "${SWIG_PREFIX}/build/build" CACHE PATH "")
set(SWIG_SOURCE_DIR   "${SWIG_PREFIX}/build/src"   CACHE PATH "")
set(SWIG_TMP_DIR      "${SWIG_PREFIX}/build/tmp"   CACHE PATH "")
set(SWIG_INSTALL_DIR  "${SWIG_PREFIX}"             CACHE PATH "")

if(CMAKE_VERBOSE_MAKEFILE)
    set(SWIG_VERBOSITY "--disable-silent-rules")
else()
    set(SWIG_VERBOSITY "--enable-silent-rules")
endif()


if(BUILD_SHARED_LIBS)
    set(SWIG_LINKAGE "link=shared;runtime-link=shared")
else()
    set(SWIG_LINKAGE "link=static;runtime-link=static")
endif()

ExternalProject_Add(
    swig

    DEPENDS boost

    PREFIX ${SWIG_PREFIX}

    SOURCE_DIR ${SWIG_SOURCE_DIR}
    BINARY_DIR ${SWIG_BUILD_DIR}
    INSTALL_DIR ${SWIG_INSTALL_DIR}

    STAMP_DIR ${SWIG_BUILD_DIR}
    TMP_DIR   ${SWIG_TMP_DIR}

    URL ${SWIG_URL}
    URL_MD5 ${SWIG_MD5}
    DOWNLOAD_DIR ${SWIG_DOWNLOAD_DIR}

    CONFIGURE_COMMAND
        ${CMAKE_COMMAND} -E chdir ${SWIG_BUILD_DIR}
            ${SWIG_SOURCE_DIR}/configure
                "--prefix=${SWIG_PREFIX}"
                "--without-alllang"
                "--with-python=${PYTHON_EXECUTABLE}"
                "--with-boost=${BOOST_INSTALL_DIR}"
                "${SWIG_VERBOSITY}"

    BUILD_COMMAND
        ${CMAKE_MAKE_PROGRAM} -C ${SWIG_BUILD_DIR}

    INSTALL_COMMAND
        ${CMAKE_MAKE_PROGRAM} -C ${SWIG_BUILD_DIR} install
)

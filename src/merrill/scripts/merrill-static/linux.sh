#!/usr/bin/env bash

source_dir="$(cd "$(dirname "$(which ${BASH_SOURCE[0]})")"; echo $PWD)"

function help_message() {
    echo "Usage $(basename $0) [--prefix INSTALL_DIR] [--build BUILD_DIR] [-- CMake args]"
    exit 0
}

function prefix_error() {
    echo "$(basename $0): Expected argument to --prefix" >&2
    exit 1
}

function prefix_error() {
    echo "$(basename $0): Expected argument to --build" >&2
    exit 1
}

while :; do
    case $1 in
        -h|--help)
            help_message
            ;;

        # Prefix argument
        -p|--prefix)
            if [ -n "$2" ]; then
                install_dir="$2"
                shift
            else
                prefix_error
            fi
            ;;
        --prefix=?*)
            install_dir="${1#*=}"
            ;;
        --prefix=)
            prefix_error
            ;;

        # Build argument
        -b|--build)
            if [ -n "$2" ]; then
                build_dir="$2"
                shift
            else
                build_error
            fi
            ;;
        --build=?*)
            build_dir="${1#*=}"
            ;;
        --build=)
            build_error
            ;;

        # Rest of arguments passed to CMake
        --)
            shift
            break
            ;;

        # Default
        *)
            break
            ;;
    esac

    shift
done



: ${PYTHON_EXECUTABLE:=$(which python)}
: ${FC:=$(which gfortran)}
: ${CC:=$(which gcc)}
: ${CXX:=$(which g++)}

export PYTHON_EXECUTABLE
export CC
export CXX


spec_dir=$build_dir/spec
mkdir -p $spec_dir
{
cat <<EOF
%rename lib liborig
*lib: %{static-libgfortran:-zignore} %{static-libgfortran:-Bstatic -lquadmath -Bdynamic} %{!static-libgfortran:-lquadmath} %{static-libgfortran:-zrecord} -lm %(libgcc) %(liborig)
EOF
} > $spec_dir/libgfortran.spec


# Write gcc/g++ wrappers that strip out any .so files
fc_wrap_dir="$build_dir/fc_wrap"
mkdir -p "$fc_wrap_dir"
{
cat <<EOF
#!${PYTHON_EXECUTABLE}
import os.path
import sys
import re
import os

fc = os.path.abspath("$(which $FC)")
args = sys.argv[1:]

# Pop off *.so.* arguments
so_arg = re.compile('.*\.so\.?.*')
args = [arg for arg in args if not so_arg.match(arg)]
args = [fc] + args
#print args
os.execv(fc, args)
EOF
} > $fc_wrap_dir/fc

chmod 0755 "$fc_wrap_dir/fc"
export FC="$fc_wrap_dir/fc"


cmake \
    -B$build_dir -H$source_dir \
    -DCMAKE_INSTALL_PREFIX:PATH=$install_dir \
    -DCMAKE_Fortran_COMPILER:PATH="$FC" \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -DBUILD_SHARED_LIBS:BOOL=OFF \
    -DPYTHON_EXECUTABLE:PATH="$PYTHON_EXECUTABLE" \
    -DCMAKE_BUILD_TYPE:STRING="Release" \
    -DMERRILL_ENABLE_MAGNETOSTRICTION:BOOL=ON \
    -DMERRILL_EXTRA_LIBS:STRING="-pthread -lrt -lz -l:libboost_chrono.a -l:libboost_timer.a -l:liblapack.a -l:libblas.a -l:libstdc++.a -l:libgfortran.a -l:libquadmath.a -l:libm.a"\
    -DCMAKE_EXE_LINKER_FLAGS:STRING="-B $spec_dir -static-libgcc -static-libgfortran -L$build_dir/boost/lib -Wl,--as-needed" \
    "$@"

cmake --build $build_dir

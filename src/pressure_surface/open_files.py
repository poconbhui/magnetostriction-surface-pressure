#!/usr/bin/env python

from glob import glob
import re

file_template = "cube_100nm_%sPa.mstr%s.stl"
files = glob(file_template%("*", ""))

pressure_re = re.compile(file_template%("(.*)", ""))

pressures = [pressure_re.match(file).group(1) for file in files]
pressures.sort(key=float)

for pressure in pressures:
    OpenDataFile(file_template%(pressure, ""), guiName = pressure + " abs")
    OpenDataFile(file_template%(pressure, ".rel"), guiName = pressure + " rel")

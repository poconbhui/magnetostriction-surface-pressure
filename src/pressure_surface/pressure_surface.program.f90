PROGRAM pressure_surface
    USE MERRILL
    USE, INTRINSIC :: ISO_FORTRAN_ENV, ONLY: ERROR_UNIT
    IMPLICIT NONE

    REAL(KIND=DP), PARAMETER :: PI = 4D0*DATAN(1.0D0)
    REAL(KIND=DP) :: uniform_m(3)

    INTEGER :: nsextant=6, ni, nj
    INTEGER :: sextant
    INTEGER :: i, j, k

    REAL(KIND=DP), ALLOCATABLE :: energies(:, :, :, :)

    REAL(KIND=C_DOUBLE) :: pressure

    CHARACTER(len=1024) :: stl_filename_base

    CHARACTER(len=1024) :: displacement_tec_filename

    INTEGER :: argc
    CHARACTER(len=1024) :: argv

    REAL(KIND=DP) :: TotalEnergy

    LOGICAL :: do_print_cubic_axes
    LOGICAL :: do_generate_surface

    INTEGER :: generate_surface_curr_progress, generate_surface_prev_progress
    INTEGER :: generate_surface_progress_width

    CHARACTER(len=100) :: mechanical_solve_mode


    CALL InitializeMerrill()


    ! Default values
    stl_filename_base = "surface"
    displacement_tec_filename = "displacement.tec"
    meshfile = "octahedron_100nm.neu"

    do_print_cubic_axes = .TRUE.
    do_generate_surface = .TRUE.

    ni = 10
    nj = 10

    traction_fnptr => pressure_traction
    pressure = 0

    mechanical_solve_mode = "dynamic"

    argc = COMMAND_ARGUMENT_COUNT()
    i = 1
    DO WHILE(.TRUE.)
        CALL GET_COMMAND_ARGUMENT(i, argv)

        SELECT CASE (TRIM(argv))
            CASE("--mesh")
                IF(argc < i+1) THEN
                    WRITE(ERROR_UNIT, *) "Expected argument for ", argv
                    STOP(1)
                END IF
                CALL GET_COMMAND_ARGUMENT(i+1, meshfile)
                i = i+1

            CASE("--outbase")
                IF(argc < i+1) THEN
                    WRITE(ERROR_UNIT, *) "Expected argument for ", argv
                    STOP(1)
                END IF
                CALL GET_COMMAND_ARGUMENT(i+1, argv)

                stl_filename_base = TRIM(argv)
                displacement_tec_filename = TRIM(argv) // ".displacement.tec"

                i = i+1

            CASE("--ni")
                IF(argc < i+1) THEN
                    WRITE(ERROR_UNIT, *) "Expected argument for ", argv
                    STOP(1)
                END IF
                CALL GET_COMMAND_ARGUMENT(i+1, argv)
                READ(argv, *) ni
                i = i+1

            CASE("--nj")
                IF(argc < i+1) THEN
                    WRITE(ERROR_UNIT, *) "Expected argument for ", argv
                    STOP(1)
                END IF
                CALL GET_COMMAND_ARGUMENT(i+1, argv)
                READ(argv, *) nj
                i = i+1

            CASE("--pressure")
                IF(argc < i+1) THEN
                    WRITE(ERROR_UNIT, *) "Expected argument for ", argv
                    STOP(1)
                END IF
                CALL GET_COMMAND_ARGUMENT(i+1, argv)
                READ(argv, *) pressure
                i = i+1

            CASE("--pressure-mode")
                IF(argc < i+1) THEN
                    WRITE(ERROR_UNIT, *) "Expected argument for ", argv
                    STOP(1)
                END IF
                CALL GET_COMMAND_ARGUMENT(i+1, argv)

                SELECT CASE(TRIM(argv))

                    CASE("static")
                        WRITE(ERROR_UNIT,*) "Using pressure_traction"
                        traction_fnptr => pressure_traction

                    CASE("[100]")
                        WRITE(ERROR_UNIT,*) "Using uniaxial_100_traction"
                        traction_fnptr => uniaxial_100_traction

                    CASE("[111]")
                        WRITE(ERROR_UNIT,*) "Using uniaxial_111_traction"
                        traction_fnptr => uniaxial_111_traction

                    CASE DEFAULT
                        WRITE(ERROR_UNIT, *) &
                            "Error: unknown pressure mode", argv
                        STOP(1)

                END SELECT

                i=i+1
                
            CASE("--print-cubic-axes")
                do_print_cubic_axes = .TRUE.

            CASE("--no-print-cubic-axes")
                do_print_cubic_axes = .FALSE.

            CASE("--generate-surface")
                do_generate_surface = .TRUE.

            CASE("--no-generate-surface")
                do_generate_surface = .FALSE.

            CASE("--mechanical-solve-mode")
                IF(argc < i+1) THEN
                    WRITE(ERROR_UNIT, *) "Expected argument for ", argv
                    STOP(1)
                END IF
                CALL GET_COMMAND_ARGUMENT(i+1, argv)

                SELECT CASE(TRIM(argv))
                    CASE("dynamic", "static-0", "static-[100]", "static-[111]")
                        mechanical_solve_mode = TRIM(argv)

                    CASE DEFAULT
                        WRITE(ERROR_UNIT, *) &
                            "Error: unknown mechanical solve mode", argv
                        STOP(1)
                END SELECT

                i=i+1
                

            CASE DEFAULT
                WRITE(*,*) "Unknown argument: ", argv
                STOP(1)

        END SELECT

        i = i+1
        IF(i > argc) EXIT
    END DO


    WRITE(*,*)
    WRITE(*,*) "FILES:"
    WRITE(*,*) "    Mesh:  ", meshfile
    WRITE(*,*)
    WRITE(*,*) "    Anis:  ", TRIM(stl_filename_base) // ".anis.stl"
    WRITE(*,*) "    Mstr:  ", TRIM(stl_filename_base) // ".mstr.stl"
    WRITE(*,*) "    Total: ", TRIM(stl_filename_base) // ".total.stl"
    WRITE(*,*) "    Displ: ", TRIM(displacement_tec_filename)
    WRITE(*,*)
    WRITE(*,*) "DIMS:"
    WRITE(*,*) "    ", ni, " x ", nj, " x ", nsextant, " = ", ni*nj*nsextant
    WRITE(*,*)
    WRITE(*,*) "PRESSURE:"
    WRITE(*,*) "    ", pressure
    WRITE(*,*)
    WRITE(*,*) "MECHANICAL SOLVE:"
    WRITE(*,*) "    ", mechanical_solve_mode
    WRITE(*,*)

    ALLOCATE(energies(3, nsextant, ni, nj))


    CALL InitializePressureSurface()


    ! Print out directions of interest
    CALL set_uniform_m((/ 1.0D0, 0.0D0, 0.0D0 /))
    CALL calculate_energies()

    IF(do_print_cubic_axes) THEN
        CALL print_cubic_axes()
    END IF


    CALL set_uniform_m((/ 1.0D0, 0.0D0, 0.0D0 /))
    CALL calculate_energies()

    CALL WriteDisplacementTecplot(displacement_tec_filename)



    IF(.NOT. do_generate_surface) THEN
        STOP(0)
    END IF


    generate_surface_curr_progress = 0
    generate_surface_prev_progress = 0
    generate_surface_progress_width = 40

    WRITE(ERROR_UNIT, *)
    WRITE(ERROR_UNIT, '(A)', advance="no") "|"
    DO i=1,generate_surface_progress_width
        WRITE(ERROR_UNIT, '(A)', advance="no") "-"
    END DO
    WRITE(ERROR_UNIT, '(A)', advance="yes") "|"
    WRITE(ERROR_UNIT, '(A)', advance="no") "|"

    DO i=1,ni
    DO j=1,nj
    DO sextant=1,nsextant
        generate_surface_curr_progress = &
            ( (i-1)*nj*nsextant + (j-1)*nsextant + sextant-1 ) &
            * generate_surface_progress_width &
            / (ni*nj*nsextant - 1) ! -1 to hit 100% on last iteration.

        DO k=generate_surface_prev_progress+1, generate_surface_curr_progress
            WRITE(ERROR_UNIT, '(A)', advance="no") '#'
        END DO
        generate_surface_prev_progress = generate_surface_curr_progress
        !WRITE(ERROR_UNIT, *) &
        !    (i-1)*nj*nsextant + (j-1)*nsextant + sextant-1, &
        !    "/", &
        !    ni*nj*nsextant

        CALL sphere_point(i, ni, j, nj, sextant, uniform_m)

        CALL set_uniform_m(uniform_m)
        CALL calculate_energies()
        !AnisEnerg=1
        !MstrEnerg=1

        energies(1, sextant, i, j) = AnisEnerg
        energies(2, sextant, i, j) = MstrEnerg
        energies(3, sextant, i, j) = TotalEnergy
    END DO
    END DO
    END DO

    WRITE(ERROR_UNIT, '(A)') "|"
    WRITE(ERROR_UNIT, *)


    CALL write_stl_file( &
        TRIM(stl_filename_base) // ".anis.stl",  energies(1,:,:,:) &
    )
    CALL write_stl_file( &
        TRIM(stl_filename_base) // ".mstr.stl",  energies(2,:,:,:) &
    )
    CALL write_stl_file( &
        TRIM(stl_filename_base) // ".total.stl", energies(3,:,:,:) &
    )


    CALL rezero_energies(energies)

    CALL write_stl_file( &
        TRIM(stl_filename_base) // ".anis.rel.stl",  energies(1,:,:,:) &
    )
    CALL write_stl_file( &
        TRIM(stl_filename_base) // ".mstr.rel.stl",  energies(2,:,:,:) &
    )
    CALL write_stl_file( &
        TRIM(stl_filename_base) // ".total.rel.stl", energies(3,:,:,:) &
    )


CONTAINS

    SUBROUTINE InitializePressureSurface()
        REAL(KIND=DP) :: lambda_111, lambda_100

        ! Load the mesh file
        CALL readmeshpat() !-> MODULE Tetrahedral_Mesh_Data
        NMAX=NNODE
        CALL demagstiff()      !-> MODULE Finite_Element
        CALL nonzerostiff()    !-> MODULE Finite_Element
        CALL forcemat()        !-> MODULE Finite_Element
        CALL boundmata()       !-> MODULE Finite_Element
        WRITE(*,*) 'Number of boundary nodes',bnode
        !  Allocates field variables -> MODULE Material_Parameters
        CALL FieldAllocate()
        CALL SaveMesh(1)
        CALL SaveFEM(1)


        ! Set TM60 parameters


        ! Magnetic constants
        K1  = 784.05274973200039 ! K1 without Mstr correction
        Aex = 1.8234899999999999e-12
        Ms  = 115738.0

        anisform='cubic'

        Kd        = 4* (3.1415926535897932)*mu*Ms*Ms*0.5
        LambdaEx  = Sqrt(Aex/Kd)
        QHardness = K1/Kd


        ! Elastic Constants (N/m^2)
        c11 = 136468000000.0
        c12 = 53084800000.0
        c44 = 62843100000.0

        ! Magnetostriction constants
        ! (Sahu & Moskowiz 1995)
        lambda_100 = 142.5e-6 ! non-dim
        lambda_111 = 95.4e-6  ! non-dim
        !lambda_111 = lambda_100 ! set for isotropic

        ! Magnetoelastic coupling
        ! (in m^3/N)
        b1 = lambda_100*(-3./2.)*(c11 - c12)
        b2 = lambda_111*(-3.)*c44

        ! Magnetoelastic coupling
        !b1 = -17823159.0
        !b2 = -17985695.220000003


        calculating_magnetostriction = .TRUE.
        magnetostriction_dirty = .TRUE.

    END SUBROUTINE InitializePressureSurface


    SUBROUTINE set_uniform_m(uniform_m)
        REAL(KIND=DP), INTENT(IN) :: uniform_m(3)
        REAL(KIND=DP) :: um_size
        INTEGER :: i

        um_size = SQRT(uniform_m(1)**2 + uniform_m(2)**2 + uniform_m(3)**2)
        DO i=1,NNODE
            m(i,:) = uniform_m/um_size
        END DO
    END SUBROUTINE set_uniform_m


    SUBROUTINE calculate_energies()
        DOUBLE PRECISION :: G(NNODE*2), X(NNODE*2), dgscale
        INTEGER          :: i, neval
        DOUBLE PRECISION :: TypEn, KdV

        DOUBLE PRECISION :: old_m(NNODE, 3)


        CalcAllExchQ=.FALSE.

        ! Controlling tolerance of linear CG solver
        FEMTolerance=1.d-10   ! via global FEMTolerance

        neval=1
        totfx=fx+fx2  ! is this necessary ? Occurs also in CalcDemagEx
        MeanMag(:)=0.
        ! Theta (co-lat) indicies= 2n-1, phi (azimuth)  indicies= 2n
        do i=1,NNODE
          X(2*i-1)=acos(m(i,3))
          X(2*i)= atan2(m(i,2),m(i,1))
          MeanMag(:)=MeanMag(:)+m(i,:)*vbox(i)
        enddo
        MeanMag(:)=MeanMag(:)/total_volume


        calculating_magnetostriction = .FALSE.
        call ET_GRAD(TotalEnergy, G, X, neval, dgscale)

        ! Do custom magnetostriction solve
        calculating_magnetostriction = .TRUE.

        IF(magnetostriction_dirty .EQV. .TRUE.) THEN
            CALL magnetostriction_initialize(-1)

            DO i=1,NNODE
                old_m(i,:) = m(i,:)
            END DO
            SELECT CASE(mechanical_solve_mode)
                CASE("static-[100]")
                    DO i=1,NNODE
                        m(i,:) = (/ 1.0, 0.0, 0.0 /)
                    END DO

                CASE("static-[111]")
                    DO i=1,NNODE
                        m(i,:) = (/ 1.0, 1.0, 1.0 /) / SQRT(3.0)
                    END DO

                CASE("static-0")
                    m = 0.0

            END SELECT

            CALL magnetostriction_copy_merrill_to_dolfin()
            CALL magnetostriction_solve_mechanical()
            CALL magnetostriction_copy_dolfin_to_merrill()

            DO i=1,NNODE
                m(i,:) = old_m(i,:)
            END DO
        END IF
        magnetostriction_dirty = .FALSE.

        CALL magnetostriction_copy_merrill_to_dolfin()

        IF(mechanical_solve_mode .EQ. "dynamic") THEN
            CALL magnetostriction_solve_mechanical()
        END IF

        CALL magnetostriction_solve_magnetic()
        CALL magnetostriction_solve_energy()

        CALL magnetostriction_copy_dolfin_to_merrill()


        TotalEnergy = TotalEnergy + MstrEnerg
    END SUBROUTINE calculate_energies

    SUBROUTINE cube_to_sphere(cube_point, sphere_point)
        REAL(KIND=DP), INTENT(IN)  :: cube_point(3)
        REAL(KIND=DP), INTENT(OUT) :: sphere_point(3)

        sphere_point(1) = &
            cube_point(1)*SQRT( &
                1 &
                - (cube_point(2)**2)/2 &
                - (cube_point(3)**2)/2 &
                + (cube_point(2)**2)*(cube_point(3)**2)/3 &
            )

        sphere_point(2) = &
            cube_point(2)*SQRT( &
                1 &
                - (cube_point(3)**2)/2 &
                - (cube_point(1)**2)/2 &
                + (cube_point(3)**2)*(cube_point(1)**2)/3 &
            )

        sphere_point(3) = &
            cube_point(3)*SQRT( &
                1 &
                - (cube_point(1)**2)/2 &
                - (cube_point(2)**2)/2 &
                + (cube_point(1)**2)*(cube_point(2)**2)/3 &
            )

    END SUBROUTINE cube_to_sphere

    SUBROUTINE sphere_point(i, ni, j, nj, sextant, point)
        INTEGER, INTENT(IN) :: i, ni, j, nj, sextant
        REAL(KIND=DP), INTENT(OUT) :: point(3)

        REAL(KIND=DP) :: a, b

        a = -1.0D0 + (i-1)*2.0/(ni-1)
        b = -1.0D0 + (j-1)*2.0/(nj-1)

        SELECT CASE(sextant)
            CASE(1) ! x, y, +1
                CALL cube_to_sphere( (/ a, b,  1.0D0 /), point )
            CASE(2) ! x, y, -1
                CALL cube_to_sphere( (/ a, b, -1.0D0 /), point )
            CASE(3) ! x, +1, y
                CALL cube_to_sphere( (/ a,  1.0D0, b /), point )
            CASE(4) ! x, -1, y
                CALL cube_to_sphere( (/ a, -1.0D0, b /), point )
            CASE(5) ! +1, y, z
                CALL cube_to_sphere( (/  1.0D0, a, b /), point )
            CASE(6) ! -1, y, z
                CALL cube_to_sphere( (/ -1.0D0, a, b /), point )
        END SELECT

    END SUBROUTINE sphere_point

    SUBROUTINE pressure_traction(values, position, normal)
        REAL(KIND=C_DOUBLE), INTENT(OUT) :: values(3)
        REAL(KIND=C_DOUBLE), INTENT(IN)  :: position(3), normal(3)
        
        values = -normal*pressure
    END SUBROUTINE pressure_traction

    SUBROUTINE uniaxial_100_traction(values, position, normal)
        REAL(KIND=C_DOUBLE), INTENT(OUT) :: values(3)
        REAL(KIND=C_DOUBLE), INTENT(IN)  :: position(3), normal(3)

        values = 0
        IF(ABS(normal(1)) > 0.1) THEN
            values(1) = - SIGN(pressure, normal(1))
        END IF
    END SUBROUTINE uniaxial_100_traction

    SUBROUTINE uniaxial_111_traction(values, position, normal)
        REAL(KIND=C_DOUBLE), INTENT(OUT) :: values(3)
        REAL(KIND=C_DOUBLE), INTENT(IN)  :: position(3), normal(3)

        REAL(KIND=C_DOUBLE) :: v_111(3) = (/ 1.0, 1.0, 1.0 /)/SQRT(3.0)
        REAL(KIND=C_DOUBLE) :: d

        d = DOT_PRODUCT(normal, v_111)

        values = 0
        values = - SIGN(pressure*v_111, d)
    END SUBROUTINE uniaxial_111_traction

    SUBROUTINE write_stl_file(stl_filename, energy)
        CHARACTER(len=*), INTENT(IN) :: stl_filename
        REAL(KIND=DP), INTENT(IN) :: energy(:,:,:)

        INTEGER, PARAMETER :: stl_file_unit = 1401

        REAL(KIND=DP) vertices(3,4)
        INTEGER :: nsextant, ni, nj
        INTEGER :: sextant, i, j

        nsextant = SIZE(energy, 1)
        ni = SIZE(energy, 2)
        nj = SIZE(energy, 3)

        OPEN(UNIT=stl_file_unit, FILE=stl_filename, ACTION="WRITE")

        WRITE(UNIT=stl_file_unit, FMT="(A)") "solid energy_surface"

        DO sextant=1,nsextant
        DO i=1,(ni-1)
        DO j=1,(nj-1)
            CALL sphere_point((i+0), ni, (j+0), nj, sextant, vertices(:,1))
            vertices(:,1)  = energy(sextant, (i+0), (j+0))*vertices(:,1)

            CALL sphere_point((i+1), ni, (j+0), nj, sextant, vertices(:,2))
            vertices(:,2)  = energy(sextant, (i+1), (j+0))*vertices(:,2)

            CALL sphere_point((i+1), ni, (j+1), nj, sextant, vertices(:,3))
            vertices(:,3)  = energy(sextant, (i+1), (j+1))*vertices(:,3)

            CALL sphere_point((i+0), ni, (j+1), nj, sextant, vertices(:,4))
            vertices(:,4)  = energy(sextant, (i+0), (j+1))*vertices(:,4)


            CALL write_facets(stl_file_unit,  vertices)
        END DO
        END DO
        END DO

        WRITE(UNIT=stl_file_unit, FMT="(A)") "endsolid energy_surface"

        CLOSE(UNIT=stl_file_unit)

    END SUBROUTINE write_stl_file

    SUBROUTINE write_facets(stl_file_unit, vertices)
        INTEGER, INTENT(IN) :: stl_file_unit
        REAL(KIND=DP), INTENT(IN) :: vertices(3,4)

        WRITE(UNIT=stl_file_unit, FMT='(A)') "facet normal 0 0 0"
        WRITE(UNIT=stl_file_unit, FMT='(A)') "    outer loop"
        WRITE(UNIT=stl_file_unit, FMT='(A, 3E20.12)') &
            "        vertex ", vertices(:,1)
        WRITE(UNIT=stl_file_unit, FMT='(A, 3E20.12)') &
            "        vertex ", vertices(:,2)
        WRITE(UNIT=stl_file_unit, FMT='(A, 3E20.12)') &
            "        vertex ", vertices(:,3)
        WRITE(UNIT=stl_file_unit, FMT='(A)') "    endloop"
        WRITE(UNIT=stl_file_unit, FMT='(A)') "endfacet"
        WRITE(UNIT=stl_file_unit, FMT='(A)') "facet normal 0 0 0"
        WRITE(UNIT=stl_file_unit, FMT='(A)') "    outer loop"
        WRITE(UNIT=stl_file_unit, FMT='(A, 3E20.12)') &
            "        vertex ", vertices(:,1)
        WRITE(UNIT=stl_file_unit, FMT='(A, 3E20.12)') &
            "        vertex ", vertices(:,3)
        WRITE(UNIT=stl_file_unit, FMT='(A, 3E20.12)') &
            "        vertex ", vertices(:,4)
        WRITE(UNIT=stl_file_unit, FMT='(A)') "    endloop"
        WRITE(UNIT=stl_file_unit, FMT='(A)') "endfacet"
    END SUBROUTINE write_facets

    SUBROUTINE rezero_energies(energies)
        REAL(KIND=DP), INTENT(INOUT) :: energies(:,:,:,:)
        INTEGER :: ei, s, i, j
        REAL(KIND=DP) :: e, min_e, max_e

        WRITE(*,*) &
            "DIMS: ", &
            SIZE(energies,1), SIZE(energies,2), &
            SIZE(energies,3), SIZE(energies, 4)

        DO ei=1,SIZE(energies,1) 
            min_e = energies(ei, 1, 1, 1)
            max_e = min_e
            WRITE(*,*)
            WRITE(*,*) "init min ", ei, ": ", min_e
            WRITE(*,*) "init max ", ei, ": ", min_e

            DO j=1,SIZE(energies,4)
            DO i=1,SIZE(energies,3)
            DO s=1,SIZE(energies,2)
                e = energies(ei, s, i, j)
                IF( e .LT. min_e) min_e = e
                IF( e .GT. max_e) max_e = e
            END DO
            END DO
            END DO

            WRITE(*,*) "final min ", ei, ": ", min_e
            WRITE(*,*) "final max ", ei, ": ", max_e

            DO j=1,SIZE(energies,4)
            DO i=1,SIZE(energies,3)
            DO s=1,SIZE(energies,2)
                energies(ei, s, i, j) = energies(ei, s, i, j) - min_e
            END DO
            END DO
            END DO
        END DO
    END SUBROUTINE rezero_energies

    SUBROUTINE print_cubic_axes()
        REAL(KIND=DP) :: uniform_m(3)
        INTEGER :: i, j, k

        ! [100]
        WRITE(*,*)
        WRITE(*,*) "[100]"

        DO i=-1,1,2
            DO j=1,3
                uniform_m = 0
                uniform_m(j) = i

                CALL set_uniform_m(uniform_m)
                CALL calculate_energies()

                WRITE(*, '(3F5.1, E20.13E2)') &
                    uniform_m(1), uniform_m(2), uniform_m(3), &
                    MstrEnerg
            END DO
        END DO


        ! [110]
        WRITE(*,*)
        WRITE(*,*) "[110]"

        DO i=-1,1,2
        DO j=-1,1,2
            DO k=1,3
                uniform_m(MOD(1+k,3)+1) = i
                uniform_m(MOD(2+k,3)+1) = j
                uniform_m(MOD(3+k,3)+1) = 0

                CALL set_uniform_m(uniform_m)
                CALL calculate_energies()

                WRITE(*, '(3F5.1, E20.13E2)') &
                    uniform_m(1), uniform_m(2), uniform_m(3), &
                    MstrEnerg
            END DO
        END DO
        END DO


        ! [111]
        WRITE(*,*)
        WRITE(*,*) "[111]"

        DO i=-1,1,2
        DO j=-1,1,2
        DO k=-1,1,2
            uniform_m = (/ i, j, k /)

            CALL set_uniform_m(uniform_m)
            CALL calculate_energies()

            WRITE(*, '(3F5.1, E20.13E2)') &
                uniform_m(1), uniform_m(2), uniform_m(3), &
                MstrEnerg
        END DO
        END DO
        END DO
    END SUBROUTINE print_cubic_axes

END PROGRAM pressure_surface

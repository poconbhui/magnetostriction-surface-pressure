#!/usr/bin/env bash

set -e

script_dir="$(dirname "$(readlink -f "$(which "$0")")")"
source ${script_dir}/../spack/share/spack/setup-env.sh

spack load gcc@4.9.3
spack load fenics@1.6.0
spack load merrill@current

MERRILL_DIR=$(spack location --install-dir merrill@current)

# script in $project/src/pressure_surface
project_dir=${script_dir}/../..

install_dir=${project_dir}/opt/pressure_surface
mkdir -p "${install_dir}/bin"

gfortran \
    ${script_dir}/pressure_surface.program.f90 \
    -I$MERRILL_DIR/lib/merrill/modules/gnu/ \
    -L$MERRILL_DIR/lib -lmerrill -ldolfin \
    -O3 \
    -Wall -Wextra -pedantic \
    -o "${install_dir}/bin/pressure_surface"

export PATH=${install_dir}/bin:$PATH

data_dir=${project_dir}/data
mkdir -p ${data_dir}

#module load efence
#for mechanical_solve_mode in "static-0"
for mechanical_solve_mode in "dynamic" "static-0" "static-[100]" "static-[111]"
do
    #for pressure_mode in "[111]"
    for pressure_mode in static "[100]" "[111]"
    do
        #for pressure in 1e2 1e4 1e6 5e6 1e7 5e7 1e8 1e12
        #for pressure in 0
        for pressure in 0 1e2 1e4 1e6 5e6 1e7 5e7 1e8 1e12
        do

            if [[ $mechanical_solve_mode = "static-0" && $pressure = 0 ]]
            then
                continue
            fi

            mkdir -p "${data_dir}/tm60/${pressure_mode}/${mechanical_solve_mode}"
            cd "${data_dir}/tm60/${pressure_mode}/${mechanical_solve_mode}"

            #--mesh octahedron_100nm.neu \
            #pressure_mode="static"
            pressure_surface \
                --mesh "${script_dir}/cube_100nm.neu" \
                --ni 21 --nj 21 \
                --pressure "${pressure}" \
                --pressure-mode "${pressure_mode}" \
                --mechanical-solve-mode "${mechanical_solve_mode}" \
                --outbase "cube_100nm_${pressure}Pa" \
                --no-print-cubic-axes \
                #--no-generate-surface \

        done
    done
done
